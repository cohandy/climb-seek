<?php
Class UserRecover extends User {

	function sendRecoveryEmail() {
		//first check for existing email in db
		$stmt = $this->pdo->prepare("SELECT email, username FROM users WHERE email=?");
		$stmt->execute([$this->email]);
		if($user = $stmt->fetch()) {
			//generate random # and create new recover db entry, recover_code will be sent with the recovery email and checked against on email link return
			$recover_code = self::generateNum();
			//delete any old recoveries
			self::deleteRecover();
			//insert new recovery
			self::createRecover($recover_code);
			$html = self::emailHTML($recover_code, $user['username'], $user['email']);
			self::sendEmailToUser($html, $user['email'], $user['username'], "Recover Password");
			return true;
		} else return false;
	}

	function sendEmailToUser($html, $user_email, $user_name, $subject) {
	    $mail = new PHPMailer;
	    $mail->isSMTP();
	    $mail->SMTPSecure = 'ssl';
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Port = 465;
	    $mail->SMTPAuth = true;
	    $mail->Username = 'climbandseek@gmail.com';
	    $mail->Password = 'snapAttack0990';
	    $mail->setFrom('climbandseek@gmail.com', 'Info');
	    $mail->addAddress($user_email, $user_name);
	    $mail->Subject = $subject;
	    $mail->MsgHTML($html);
	    $mail->IsHTML(true);
	    if(!$mail->send()) {
	        return false;
	    }
	}

	function emailHTML($num, $username, $email) {
		$new_message = '<div style="text-align:center">' . 
							'<div style="font-size:1.6em">Hi ' . $username . ",</div><br>" .
							'<div style="font-size:1.1em">This email was sent to recover your password to climbandseek.com</div>' .
							'<div style="font-size:1.1em">If this isn\'t you please kindly ignore this email, if it is follow this link...</div><br>' .
							'<div><a href="www.climbandseek.com/recover/' . $email . '/' . $num . '" style="font-size:1.3em">Recover Password</a></div><br><br>' . 
						'</div>';
		return $new_message;

	}

	function generateNum() {
		return mt_rand(10000000, 10000000000000000);
	}

	function deleteRecover() {
		$stmt = $this->pdo->prepare("DELETE FROM password_recoveries WHERE email=?");
		$stmt->execute([$this->email]);
	}

	function createRecover($code) {
		$stmt = $this->pdo->prepare("INSERT INTO password_recoveries(email, recover_code) VALUES(?, ?)");
		$string_code = (string)$code;
		$stmt->execute([$this->email, $string_code]);
	}

	function checkRecover($email, $recover, $pdo) {
		$stmt = $pdo->prepare("SELECT * FROM password_recoveries WHERE email=?");
		$stmt->execute([$email]);
		if($check = $stmt->fetch()) {
			if($recover == $check['recover_code']) {
				$_SESSION['recover'] = array('email' => $check['email']);
				return true;
			} else return false;
		} else return false;
	}

	function updatePassword() {
		$password_check = parent::validatePassword();
		$confirm_check = parent::confirmPassword();
		if($password_check['success'] && $confirm_check['success']) {
			//fetch user profile
			$stmt = $this->pdo->prepare("SELECT email, username, id, avatar, location FROM users WHERE email=?");
			$stmt->execute([$_SESSION['recover']['email']]);
			if($user = $stmt->fetch()) {
				$stmt2 = $this->pdo->prepare("UPDATE users SET password=? WHERE id=?");
				//hash password
				$hashed_pass = password_hash($this->password, PASSWORD_DEFAULT);
				if($stmt2->execute([$hashed_pass, $user['id']])) {
					$_SESSION['user'] = array('username' => $user['username'], 'id' => $user['id'], 'avatar' => $user['avatar'], 'location' => $user['location'], 'email' => $user['email']);
					$this->email = $user['email'];
					self::deleteRecover();
					return array('success' => true, 'username' => $user['username']);
				} else {
					return parent::returnError("Action could not be completed, please try again later");
				}
			} else {
				return parent::returnError("No account was found under email provided, try resending recovery email");
			}
		} else {
			if(!$password_check['success']) {
				return $password_check;
			} else {
				return $confirm_check;
			}
		}
	}
}
?>
<?php
class Map {
	private $pdo;

	function __construct($coords, $pdo = false) {
		foreach($coords as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}
	
	function getSpotsInLong() {
		$stmt = $this->pdo->prepare("SELECT latitude, longitude, name, type_climbing, id, num_routes, ropes_avg_diff, bouldering_avg_diff, confirmed, top_pic, upvotes, description, region, area_id FROM spots WHERE MATCH(search_long) AGAINST (? IN BOOLEAN MODE) AND filter_type=?
			UNION ALL
			SELECT latitude, longitude, name, type_climbing, id, num_routes, ropes_avg_diff, bouldering_avg_diff, confirmed, top_pic, upvotes, description, region, area_id FROM spots WHERE MATCH(search_long) AGAINST (? IN BOOLEAN MODE) AND filter_type=?");
		$stmt->execute([$this->long, $this->type, $this->long, "Both"]);
		$spots = $stmt->fetchAll(PDO::FETCH_ASSOC);
		//find areas 
		$stmt2 = $this->pdo->prepare("SELECT id, latitude, longitude, size FROM areas WHERE MATCH(search_long) AGAINST (? IN BOOLEAN MODE) AND filter_type=?
									  UNION ALL
									  SELECT id, latitude, longitude, size FROM areas WHERE MATCH(search_long) AGAINST (? IN BOOLEAN MODE) AND filter_type=?");
		$stmt2->execute([$this->long, $this->type, $this->long, "Both"]);
		$areas = $stmt2->fetchAll(PDO::FETCH_ASSOC);
		if($spots && $areas) {
			return array_merge($spots, $areas);
		} else if($spots) {
			return $spots;
		} else if($areas) {
			return $areas;
		} else {
			return false;
		}
	}

	function getGymsInLong() {
		$stmt = $this->pdo->prepare("SELECT latitude, longitude, name, description, phonenum, state, city, url, upvotes, top_pic, id, address FROM gyms WHERE MATCH(search_long) AGAINST (? IN BOOLEAN MODE)");
		$stmt->execute([$this->long]);
		$gyms = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $gyms;
	}
}
?>
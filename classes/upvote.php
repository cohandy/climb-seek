<?php
class Upvote {
	private $pdo;

	function __construct($query, $pdo = false) {
		foreach($query as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function upvoteContent() {
		if(isset($_SESSION['user'])) {
			//check if user hasn't already upvoted
			if(!self::checkUpvote()) {
				if(isset($this->user_page)) {
					$spot_id = self::getContentSpotId();
				} else {
					$spot_id = $_SESSION['spot']['id'];
				}
				//get id of content owner
				$content_owner_id = self::getContentOwner();
				//add new 
				$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, spot_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
				$stmt->execute([$_SESSION['user']['id'], $this->content_id, $this->content_type, $spot_id, $content_owner_id]);
				$return = array('success' => true, 'action' => "upvote");
			} else {
				//delete upvote if already upvoted
				$stmt = $this->pdo->prepare("DELETE FROM upvotes WHERE user_id=? AND content_id=? AND content_type=?");
				$stmt->execute([$_SESSION['user']['id'], $this->content_id, $this->content_type]);
				$return = array('success' => true, 'action' => 'rescind');
			}
			//update upvotes for picture
			//get amount of rows(upvotes) found for this pic in picture_upvotes
			$stmt2 = $this->pdo->prepare("SELECT count(content_id) FROM upvotes WHERE content_id=? AND content_type=?");
			$stmt2->execute([$this->content_id, $this->content_type]);
			//now update content with new count
			$upvotes = $stmt2->fetch();
			$table = $this->content_type . "s";
			if($this->content_type == "picture") {
				$qString = "UPDATE $table SET upvotes=? WHERE public_id=?";
			} else {
				$qString = "UPDATE $table SET upvotes=? WHERE id=?";
			}
			$stmt3 = $this->pdo->prepare($qString);
			$stmt3->execute([$upvotes['count(content_id)'], $this->content_id]);
			return $return;
		} else return array('success' => false);
	}

	function upvoteGymContent() {
		if(isset($_SESSION['user'])) {
			//check if user hasn't already upvoted
			if(!self::checkUpvote()) {
				//add new 
				$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, gym_id) VALUES(?, ?, ?, ?)");
				$stmt->execute([$_SESSION['user']['id'], $this->content_id, $this->content_type, $_SESSION['gym']['id']]);
				$return = array('success' => true, 'action' => "upvote");
			} else {
				//delete upvote if already upvoted
				$stmt = $this->pdo->prepare("DELETE FROM upvotes WHERE user_id=? AND content_id=? AND content_type=?");
				$stmt->execute([$_SESSION['user']['id'], $this->content_id, $this->content_type]);
				$return = array('success' => true, 'action' => 'rescind');
			}
			//update upvotes for picture
			//get amount of rows(upvotes) found for this pic in picture_upvotes
			$stmt2 = $this->pdo->prepare("SELECT count(content_id) FROM upvotes WHERE content_id=? AND content_type=?");
			$stmt2->execute([$this->content_id, $this->content_type]);
			//now update content with new count
			$upvotes = $stmt2->fetch();
			$table = $this->content_type . "s";
			if($this->content_type == "picture") {
				$qString = "UPDATE $table SET upvotes=? WHERE public_id=?";
			} else {
				$qString = "UPDATE $table SET upvotes=? WHERE id=?";
			}
			$stmt3 = $this->pdo->prepare($qString);
			$stmt3->execute([$upvotes['count(content_id)'], $this->content_id]);
			return $return;
		} else return array('success' => false);
	}

	function getContentOwner() {
		$table = $this->content_type . "s";
		if($this->content_type == "picture") {
			$q_string = "SELECT user_id FROM $table WHERE public_id=?";
		} else {
			$q_string = "SELECT user_id FROM $table WHERE id=?";
		}
		$stmt = $this->pdo->prepare($q_string);
		$stmt->execute([$this->content_id]);
		$id = $stmt->fetch();
		return $id['user_id'];
	}

	function getContentSpotId() {
		$table = $this->content_type . "s";
		if($this->content_type == "picture") {
			$q_string = "SELECT spot_id FROM $table WHERE public_id=?";
		} else {
			$q_string = "SELECT spot_id FROM $table WHERE id=?";
		}
		$stmt = $this->pdo->prepare($q_string);
		$stmt->execute([$this->content_id]);
		if($content_row = $stmt->fetch()) {
			return $content_row['spot_id'];
		} else {
			return false;
		}
	}

	function checkUpvote() {
		$stmt = $this->pdo->prepare("SELECT user_id FROM upvotes WHERE user_id=? AND content_id=? AND content_type=?");
		$stmt->execute([$_SESSION['user']['id'], $this->content_id, $this->content_type]);
		if($stmt->fetch()) {
			return true;
		} else return false;
	}

	function getUserSpotUpvotes() {
		if(isset($_SESSION['user'])) {
			$stmt = $this->pdo->prepare("SELECT content_id, content_type FROM upvotes WHERE user_id=? AND spot_id=?");
			$stmt->execute([$_SESSION['user']['id'], $_SESSION['spot']['id']]);
			if($upvotes = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $upvotes;
			} else return false;
		} else return false;
	}

	function getUserGymUpvotes() {
		if(isset($_SESSION['user'])) {
			$stmt = $this->pdo->prepare("SELECT content_id, content_type FROM upvotes WHERE user_id=? AND gym_id=?");
			$stmt->execute([$_SESSION['user']['id'], $_SESSION['gym']['id']]);
			if($upvotes = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $upvotes;
			} else return false;
		} else return false;
	}

	function getUserUpvotesProfile($content_owner_id) {
		if(isset($_SESSION['user'])) {
			$stmt = $this->pdo->prepare("SELECT content_id, content_type FROM upvotes WHERE user_id=? AND content_owner_id=?");
			$stmt->execute([$_SESSION['user']['id'], $content_owner_id]);
			if($upvotes = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $upvotes;
			} else return false;
		} else return false;
	}

	function getUserStats($user_id) {
		$q_string = "SELECT (SELECT count(user_id) FROM routes WHERE user_id=?) AS routes_count,
							(SELECT count(content_id) FROM upvotes WHERE content_owner_id=?) AS upvotes_count,
							(SELECT count(user_id) FROM spots WHERE user_id=?) AS spots_count,
							(SELECT count(user_id) FROM pictures WHERE user_id=?) AS pictures_count,
							(SELECT count(user_id) FROM comments WHERE user_id=?) AS comments_count";
		$stmt = $this->pdo->prepare($q_string);
		$stmt->execute([$user_id, $user_id, $user_id, $user_id, $user_id]);
		$stats = $stmt->fetch(PDO::FETCH_ASSOC);
		return $stats;
	}

	function checkIfUpvoted($user_upvotes, $this_content_id, $this_content_type) {
		if(count($user_upvotes) > 0) {
			for($i=0;$i<count($user_upvotes);$i++) {
				if($user_upvotes[$i]['content_type'] == $this_content_type) {
					if($user_upvotes[$i]['content_id'] == $this_content_id) {
						return true;
					}
				}
			}
			return false;
		} else return false;
	}
}
?>
<?php

	//***********************\\
		//DATABASE CLASS\\
	//***********************\\

class SQLiteDB {

	function __construct() {
		$this->host = "localhost";
		$this->user = "connor";
		$this->password = "ipodnano777";
		$this->database = "climbandseek";
		$this->charset = "utf8";
		$this->dsn = "mysql:host=$this->host;dbname=$this->database;charset=$this->charset";
		$this->opt = [
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES => false,
			];
	}
	//get pdo instance, return error on fail
	function getSQLiteInstance() {
		global $local;
		try {
            if(gethostname() == "Connors-MacBook-Air.local" || gethostname() == "Connors-iMac.local") {
            	if($pdo = new PDO('sqlite:/Applications/xampp/htdocs/ascent/web/db/prod.sqlite3')) {
					//if local set errors
					if($local) {
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						return $pdo;
					} else return $pdo;
				} else return false;
            } else {
				if($pdo = new PDO('sqlite:' . DB_PATH)) {
					//if local set errors
					if($local) {
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						return $pdo;
					} else return $pdo;
				} else return false;
			}
		} catch(PDOException $e) {
			if($local) {
				return $e;
			} else {
				sendEmail($e);
				return false;
			}
		}
	}

	function getDatabaseVars() {
		global $local;
		if($local) {
			$host = "localhost";
			$user = "connor";
			$password = "ipodnano777";
			$database = "climbandseekprod";
			$charset = "utf8";
			$opt = [
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
					PDO::ATTR_EMULATE_PREPARES => false,
				];
		} else {
			$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
			$host = $url['host'];
			$user = $url['user'];
			$password = $url['pass'];
			$database = substr($url['path'], 1);
			$charset = 'utf8';
			$opt = [
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
					PDO::ATTR_EMULATE_PREPARES => false,
				];
		}
		$dsn = "mysql:host=$host;dbname=$database;charset=$charset";
		return array('dsn' => $dsn, 'user' => $user, 'password' => $password, 'opt' => $opt);
	}

	function getInstance() {
		global $local;
		//get db vars depening on env
		$db_vars = self::getDatabaseVars();
		try {
			if($pdo = new PDO($db_vars['dsn'], $db_vars['user'], $db_vars['password'], $db_vars['opt'])) {
				return $pdo;
			} else return false;
		} catch(PDOException $e) {
			if($local) {
				return $e;
			} else {
				sendEmail($e);
				return false;
			}
		}
	}
}
?>

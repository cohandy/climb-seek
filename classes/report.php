<?php
use Aws\S3\S3Client;
Class Report {
	private $pdo;

	function __construct($report, $pdo = false) {
		foreach($report as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}
	/* types of offenses
	- vulgar
	- spam
	- inaccurate
	- false-info
	- off-topic
	*/
	function newReport() {
		//check number of reports user has submitted today
		if(isset($_SESSION['user'])) {
			$check_reports = self::checkUserReports();
			if($check_reports['success'] && $this->offense !== "null") {
				$stmt = $this->pdo->prepare("SELECT * FROM reports WHERE content_id=? AND content_type=?");
				$stmt->execute([$this->content_id, $this->content_type]);
				$reports = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if(count($reports) > 0) {
					//check if user has already reported this
					$reported = self::checkDupeReport($reports);
					if(!$reported) {
						return self::returnError("You've already reported this " . $this->content_type);
					}
				}
				//spot reports are more complex and will be passed off to newSpotReport
				if($this->content_type == "spot") {
					$spot_report = self::newSpotReport($reports);
					return $spot_report;					
				} elseif($this->content_type == "gym") {
					$gym_report = self::newGymReport($reports);
					return $gym_report;
				} else {
					//if more than 2 reports delete content, contact content owner
					if(count($reports) >= 2) {
						self::deleteContent();
						self::emailContentOwner();
						return self::returnSuccess();
					} else {
						self::submitReport();
						return self::returnSuccess();
					}
				}
			} else {
				if(!$check_reports['success']) {
					return $check_reports;
				} else {
					return self::returnError("An offense must be chosen");
				}
			}
		} else {
			return self::returnError("You must be logged in to make a report");
		}
	}

	function newSpotReport($reports) {
		if($this->offense == "inaccurate") {
			//count reports with offense 'inaccurate'
			$offenses = 0;
			foreach($reports as $row) {
				if($row['offense'] == "inaccurate") {
					$offenses += 1;
				}
			}
			//if there is at least one other report for inaccuracy, unconfirm spot
			if($offenses >= 1) {
				$stmt = $this->pdo->prepare("UPDATE spots SET confirmed=? WHERE id=?");
				$stmt->execute([false, $this->content_id]);
				//delete reports for this spot with offense 'inaccurate'
				$stmt2 = $this->pdo->prepare("DELETE FROM reports WHERE content_id=? AND content_type=? AND offense=?");
				$stmt2->execute([$this->content_id, $this->content_type, 'inaccurate']);
				return self::returnSuccess();
			} else {
				self::submitReport();
				return self::returnSuccess();
			}
		} else {
			//iterate through reports, check against any offense besides 'inaccurate'
			$offenses = 0;
			foreach($reports as $row) {
				if($row['offense'] != 'inaccurate') {
					$offenses += 1;
				}
			}
			if($offenses > 1) {
				//get old desc/tips if any
				$stmt = $this->pdo->prepare("SELECT * FROM old_spot_info WHERE spot_id=? ORDER BY created_at DESC");
				$stmt->execute([$this->content_id]);
				$old_info = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//if none found, revert desc/tips back to none
				if(count($old_info) > 0) {
					//sort old desc and tips from old_info
					$old_desc = array();
					$old_tips = array();
					foreach($old_info as $row) {
						if($row['type'] == "tips") {
							$old_tips[] = $row;
						} else {
							$old_desc[] = $row;
						}
					}
					//update spot using old info for description and tips
					self::updateFromOldInfo($old_desc, "description");
					self::updateFromOldInfo($old_tips, "tips");
				} else {
					$stmt2 = $this->pdo->prepare("UPDATE spots SET description=?, tips=? WHERE id=?");
					$stmt2->execute(["", "", $this->content_id]);
				}
				//delete any reports that aren't for inaccuracy
				$stmt2 = $this->pdo->prepare("DELETE FROM reports WHERE content_id=? AND content_type=? AND offense != ?");
				$stmt2->execute([$this->content_id, $this->content_type, 'inaccurate']);
				return self::returnSucess();
			} else {
				self::submitReport();
				return self::returnSuccess();
			}
		}
	}

	function newGymReport($reports) {
		if(count($reports) > 4) {
			self::emailMe($this->content_name . "reports", "me", "");
		} else {
			self::submitReport();
		}
	}

	function updateFromOldInfo($info_arr, $type) {
		$count = count($info_arr);
		$q_string = "UPDATE spots SET $type=? WHERE id=?";
		if($count > 0) {
			$stmt = $this->pdo->prepare($q_string);
			$stmt->execute([$info_arr[0], $this->content_id]);
		} else {
			$stmt2 = $this->pdo->prepare($q_string);
			$stmt2->execute(["", $this->content_id]);
		}
	}

	function otherReport() {
		if(isset($_SESSION['user'])) {
			//check number of reports user has submitted today
			$check_reports = self::checkUserReports();
			$stmt = $this->pdo->prepare("SELECT * FROM reports WHERE content_id=? AND content_type=?");
			$stmt->execute([$this->content_id, $this->content_type]);
			$reports = $stmt->fetchAll(PDO::FETCH_ASSOC);
			//check if user has already reported this
			$reported = self::checkDupeReport($reports);
			if($check_reports['success'] && $reported) {
				//send email to climbandseek@gmail.com
				self::emailMe('Report ' . $this->content_type . " " . $this->content_id, $_SESSION['user']['username'], $this->message);
				//submit report
				$stmt2 = $this->pdo->prepare("INSERT INTO reports(content_id, content_type, content_name, offense, user_id) VALUES(?, ?, ?, ?, ?)");
				$stmt2->execute([$this->content_id, $this->content_type, $this->content_name, "other", $_SESSION['user']['id']]);
				return self::returnSuccess();
			} else {
				if(!$check_reports['success']) {
					return $check_reports;
				} else {
					return self::returnError("You've already reported this " . $this->content_type);
				}
			}
		} else {
			return self::returnError("You must be logged in to make a report");
		}
	}

	function emailMe($subject, $name, $message) {
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPSecure = 'ssl';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->Username = 'climbandseek@gmail.com';
		$mail->Password = 'snapAttack0990';
		$mail->setFrom('climbandseek@gmail.com', 'Connor');
		$mail->addAddress('climbandseek@gmail.com', $name);
		$mail->Subject = $subject;
		$mail->Body = $message;
		if(!$mail->send()) {
			$mail->ErrorInfo;
		}
	}

	function submitReport() {
		$stmt2 = $this->pdo->prepare("INSERT INTO reports(content_id, content_type, content_name, offense, user_id) VALUES(?, ?, ?, ?, ?)");
		$stmt2->execute([$this->content_id, $this->content_type, $this->content_name, $this->offense, $_SESSION['user']['id']]);
	}

	function deleteReports() {
		$stmt3 = $this->pdo->prepare("DELETE FROM reports WHERE content_id=? AND content_type=?");
		$stmt3->execute([$this->content_id, $this->content_type]);
	}

	function deleteContent() {
		$table = $this->content_type . "s";
		//delete content
		if($this->content_type == "picture") {
			self::deletePicture();
		} else {
			$q_string = "DELETE FROM $table WHERE id=?";
			$stmt = $this->pdo->prepare($q_string);
			$stmt->execute([$this->content_id]);
		}
		//delete upvotes
		$q_string2 = "DELETE FROM upvotes WHERE content_id=? AND content_type=?";
		$stmt2 = $this->pdo->prepare($q_string2);
		$stmt2->execute([$this->content_id, $this->content_type]);
		//delete reports
		self::deleteReports();
	}

	function deletePicture() {
		//get pic row in db
		$get_pic = $this->pdo->prepare("SELECT * FROM pictures WHERE public_id=?");
		$get_pic->execute([$this->content_id]);
		$picture = $get_pic->fetch();
		//initialize s3 object, provide credentials
		$s3 = S3Client::factory(array(
			'region' => AWS_REGION,
			'version' => '2006-03-01',
			'credentials' => array(
				'key' => AWS_ACCESS_KEY,
				'secret' => AWS_SECRET
			)
		));
		//delete picture from bucket
		$result = $s3->deleteObject(array(
			'Bucket' => AWS_BUCKET,
			'Key' => $this->content_id
		));
		//delete smaller res file
		$small_public_id = $this->content_id . "s";
		$result_s = $s3->deleteObject(array(
			'Bucket' => AWS_BUCKET,
			'Key' => $small_public_id
		));
		//delete pic from local db
		$stmt = $this->pdo->prepare("DELETE FROM pictures WHERE public_id=?");
		$stmt->execute([$this->content_id]);
		//and delete activities
		$stmt3 = $this->pdo->prepare("DELETE FROM activities WHERE content_id=? AND content_type=? AND user_id=?");
		$stmt3->execute([$this->content_id, "picture", $picture['user_id']]);
		//check if pic deleted is routes top pic
		if($picture['route_id']) {
			$check_route = $this->pdo->prepare("SELECT top_pic FROM routes WHERE id=?");
			$check_route->execute([$picture['route_id']]);
			$route = $check_route->fetch(PDO::FETCH_ASSOC);
			//if it is, find replacement pic
			if($route['top_pic'] == $this->content_id) {
				$new_top_pic = self::getRouteTopPic($picture['route_id']);
				//update route with new top pic if any found
				$new_top_pic ? $set_pic = $new_top_pic : $set_pic = null;
				$update = $this->pdo->prepare("UPDATE routes SET top_pic=? WHERE id=?");
				$update->execute([$set_pic, $picture['route_id']]);
			}
		}
		return true;
	}

	function getRouteTopPic($id) {
		$stmt = $this->pdo->prepare("SELECT public_id, upvotes FROM pictures WHERE route_id=? ORDER BY upvotes DESC LIMIT 1");
		$stmt->execute([$id]);
		$top_pic = $stmt->fetch();
		if($top_pic) {
			return $top_pic['public_id'];
		} else return false;
	}

	function emailContentOwner() {
		$owner = self::getContentOwner();
		if($owner) {
			$mail = new PHPMailer;
		    $mail->isSMTP();
		    $mail->SMTPSecure = 'ssl';
		    $mail->Host = 'smtp.gmail.com';
		    $mail->Port = 465;
		    $mail->SMTPAuth = true;
		    $mail->Username = 'climbandseek@gmail.com';
		    $mail->Password = 'snapAttack0990';
		    $mail->setFrom('climbandseek@gmail.com', 'Info');
		    $mail->addAddress($owner['email'], $owner['username']);
		    $mail->Subject = ucwords($this->content_type) . " removed";
		    $mail->MsgHTML(self::emailHTML($owner['username']));
		    $mail->IsHTML(true);
		    if(!$mail->send()) {
		        return false;
		    }
		}
	}

	function emailHTML($user_name) {
		$new_message = '<div style="text-align:center">' . 
							'<div style="font-size:1.6em">Hi ' . $user_name . ",</div><br>" .
							'<div style="font-size:1.1em">This email was sent to notify you that a ' . $this->content_type . ' you posted to climbandseek.com was removed for being ' . $this->offense . '.</div><br>' .
							'<div style="font-size:1.1em">You can reply to this email with any questions</div>' .
						'</div>';
		return $new_message;
	}

	function getContentOwner() {
		$table = $this->content_type . "s";
		//get user_id from using content_id, must fetch contents row
		if($this->content_type == "picture") {
			$q_string = "SELECT user_id FROM $table WHERE public_id=?";
		} else {
			$q_string = "SELECT user_id FROM $table WHERE id=?";
		}
		$stmt = $this->pdo->prepare($q_string);
		$stmt->execute([$this->content_id]);
		$content = $stmt->fetch();
		//if user_id is blank, its from us, return false
		if($content['user_id']) {
			$stmt2 = $this->pdo->prepare("SELECT username, email FROM users WHERE id=?");
			$stmt2->execute([$content['user_id']]);
			$stmt2->fetch();
			return $stmt2;
		} else return false;
	}

	function checkDupeReport($report_arr) {
		//check if user has already reported this content
		$pass = true;
		foreach($report_arr as $row) {
			if($row['user_id'] == $_SESSION['user']['id']) {
				$pass = false;
				break;
			}
		}
		return $pass;
	}

	function checkUserReports() {
		//check if user has exceeded daily reports
		$stmt = $this->pdo->prepare("SELECT created_at FROM reports WHERE user_id=? ORDER BY created_at DESC");
		$stmt->execute([$_SESSION['user']['id']]);
		if($reports = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			//iterate through reports and see if more than 5 have been inserted today
			$pass = self::checkDailyReports(date('Ymd'), $reports);
			if($pass) {
				return self::returnSuccess();
			} else {
				return self::returnError("Maximum daily reports hit, try again tomorrow!");
			}
		} else {
			return self::returnSuccess();
		}
	}

	function checkDailyReports($date, $report_arr) {
		$reports_today = 0;
		foreach($report_arr as $row) {
			//if date is today add to reports_today
			if($date == date('Ymd', strtotime($row['created_at']))) {
				$reports_today += 1;
			}
		}
		if($reports_today >= 5) {
			return false;
		} else return true;
	}

	function returnSuccess() {
		return array('success' => true);
	}

	function returnError($message) {
		return array("success" => false, 'error' => $message);
	}
}
?>
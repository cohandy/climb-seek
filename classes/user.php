<?php
class User {
	protected $pdo;

	function __construct($user, $pdo = false) {
		foreach($user as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function createUser() {
		//validate fields
		$username_check = self::validateUsername();
		$email_check = self::validateEmail();
		$password_check = self::validatePassword();
		$confirm_check = self::confirmPassword();
		if($username_check['success'] && $email_check['success'] && $password_check['success'] && $confirm_check['success']) {
			$stmt = $this->pdo->prepare("INSERT INTO users(username, email, password) VALUES(?, ?, ?)");
			//hash password
			$hashed_pass = password_hash($this->password, PASSWORD_DEFAULT);
			if($stmt->execute([$this->username, $this->email, $hashed_pass])) {
				$new_id = $this->pdo->lastInsertId();
				self::logSession($new_id);
				return array('success' => true, 'user_id' => $new_id, 'username' => sanitize($this->username));
			} else return self::returnError("Sorry, something went wrong. Try again later.");
		} else {
			$errors = array();
			$validations = array($username_check, $email_check, $password_check, $confirm_check);
			foreach($validations as $row) {
				if($row['success'] == false) {
					$errors[] = $row;
				}
			}
			return $errors;
		}
	}

	function loginUser() {
		if(strlen($this->email) <= 0) {
			return self::returnError("Email is required");
		} elseif(strlen($this->password) <= 6) {
			return self::returnError("Password is incorrect");
		}
		$stmt = $this->pdo->prepare("SELECT username, email, password, id, avatar, location FROM users WHERE email=?");
		$stmt->execute([$this->email]);
		if($user = $stmt->fetch()) {
			if(password_verify($this->password, $user['password'])) {
				$this->username = $user['username'];
				self::logSession($user['id'], $user['avatar'], $user['location']);
				//set/destory remember me cookie
				if($this->remember == "true") {
					self::setCookieAuth();
				} else {
					self::destroyCookie();
				}
				return array('success' => true, 'user_id' => $user['id'], 'username' => $user['username'], 'avatar' => $user['avatar'], "email" => $user['email']);
			} else return self::returnError("Password is incorrect");
		} else return self::returnError("No accounts found under $this->email");
	}

	function updateUser() {
		//validate fields
		if($this->email !== $_SESSION['user']['email']) {
			$email_check = self::validateUsername();
		} else $email_check = array("success" => true);
		$password_check = self::validatePassword();
		$confirm_check = self::confirmPassword();
		$location_check = self::validateLocation();
		if($email_check['success'] && $password_check['success'] && $confirm_check['success'] && $location_check['success']) {
			//check is users old password matches user in session
			if(self::updateCheckPassword()) {
				$stmt = $this->pdo->prepare("UPDATE users SET email = ?, password = ? , location = ? WHERE id = ?");
				//hash password
				$hashed_pass = password_hash($this->password, PASSWORD_DEFAULT);
				if($stmt->execute([$this->email, $hashed_pass, $this->location, $_SESSION['user']['id']])) {
					$this->username = $_SESSION['user']['username'];
					self::logSession($_SESSION['user']['id'], $_SESSION['user']['avatar'], $this->location);
					return array('success' => true, 'username' => sanitize($this->username));
				} else return self::returnError("Sorry, something went wrong. Try again later.");
			} else return self::returnError("Old password is incorrect.");
		} else {
			$errors = array();
			$validations = array($email_check, $password_check, $confirm_check, $location_check);
			foreach($validations as $row) {
				if($row['success'] == false) {
					$errors[] = $row;
				}
			}
			return $errors;
		}
	}

	function updateCheckPassword() {
		$stmt = $this->pdo->prepare("SELECT password FROM users WHERE id=?");
		$stmt->execute([$_SESSION['user']['id']]);
		$password = $stmt->fetch();
		if(password_verify($this->old_password, $password['password'])) {
			return true;
		} else return false;
	}

	function updateUserNoPassword() {
		//validate fields
		if($this->email!== $_SESSION['user']['email']) {
			$email_check = self::validateUsername();
		} else $email_check = array("success" => true);
		$location_check = self::validateLocation();
		if($email_check['success'] && $location_check['success']) {
			$stmt = $this->pdo->prepare("UPDATE users SET email = ?, location = ? WHERE id = ?");
			if($stmt->execute([$this->email, $this->location, $_SESSION['user']['id']])) {
				$this->username = $_SESSION['user']['username'];
				self::logSession($_SESSION['user']['id'], $_SESSION['user']['avatar'], $this->location);
				return array('success' => true, "username" => $_SESSION['user']['username']);
			} else return self::returnError("Sorry, something went wrong. Try again later.");

		} else {
			$errors = array();
			$validations = array($email_check, $location_check);
			foreach($validations as $row) {
				if($row['success'] == false) {
					$errors[] = $row;
				}
			}
			return $errors;
		}
	}

	function logSession($id, $avatar = null, $location = null) {
		$_SESSION['user'] = array("username" => sanitize($this->username), "email" => $this->email, "id" => $id, "avatar" => $avatar, "location" => sanitize($location));
	}

	function validateUsername() {
		if(strlen($this->username) < 4) {
			return self::returnError("Username is too short, must exceed 3 characters");
		} elseif(strlen($this->username) > 20) {
			return self::returnError("Username is too long, cannot exceed 20 characters");
		} else {
			$stmt = $this->pdo->prepare("SELECT username FROM users WHERE username=?");
			$stmt->execute([$this->username]);
			if($stmt->fetch()) {
				return self::returnError("Username is already taken");
			} else {
				return self::returnSuccess();
			}
		}
	}

	function validateEmail() {
		if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			return self::returnError("Email is invalid");
		}
		$stmt = $this->pdo->prepare("SELECT email FROM users WHERE email=?");
		$stmt->execute([$this->email]);
		if($stmt->fetch()) {
			return self::returnError("Email is already in use");
		} else {
			return self::returnSuccess();
		}
	}

	function validatePassword() {
		if(strlen($this->password) < 7) {
			return self::returnError("Password is too short, must exceed 6 characters");
		} elseif(strlen($this->password) > 35) {
			return self::returnError("Password is too long, cannot exceed 35 characters");
		} else {
			return self::returnSuccess();
		}
	}

	function confirmPassword() {
		if($this->password != $this->confirm) {
			return self::returnError("Passwords don't match");
		} else {
			return self::returnSuccess();
		}
	}

	function validateLocation() {
		if(strlen($this->location) >= 35) {
			return self::returnError("Location is too long, cannot exceed 35 characters");
		} else {
			return self::returnSuccess();
		}
	}

	function returnError($error) {
		return array("success" => false, "error" => $error);
	}

	function returnSuccess() {
		return array("success" => true);
	}

	function setCoverPic() {
		$stmt = $this->pdo->prepare("UPDATE users SET cover_pic=? WHERE id=?");
		$stmt->execute([$this->public_id, $_SESSION['user']['id']]);
	}

	function setCookieAuth() {
		//make sure randomized token is not already in db
		while(true) {
			$random = self::randString();
			$stmt = $this->pdo->prepare("SELECT token FROM users WHERE token=?");
			$stmt->execute([$random]);
			if($stmt->fetch()) {
				continue;
			} else {
				break;
			}
		}
		$stmt = $this->pdo->prepare("UPDATE users SET token=? WHERE id=?");
		$stmt->execute([$random, $_SESSION['user']['id']]);
		setCookie("remember", $random, time() + (86400 * 30), "/");
	}

	function checkCookie() {
		if(isset($_COOKIE['remember'])) {
			$stmt = $this->pdo->prepare("SELECT id, username, avatar, location, email FROM users WHERE token=?");
			$stmt->execute([$_COOKIE['remember']]);
			if($user = $stmt->fetch()) {
				$_SESSION['user'] = array("username" => sanitize($user['username']), "email" => $user['email'], "id" => $user['id'], "avatar" => $user['avatar'], "location" => sanitize($user['location']));
			}
		}
	}

	function destroyCookie() {
		//set cookie into the past so it goes away, reset user token
		if(isset($_COOKIE['remember'])) {
			setCookie("remember", "", time() - 3600, "/");
			$stmt = $this->pdo->prepare("UPDATE users SET token=? WHERE id=?");
			$stmt->execute(["", $_SESSION['user']['id']]);
		}
	}

	function randString() {
		$letters = array("a", "b", "c", "z", "g", "h", "q", "f", "l", "t", "y", "x", "p", "r", "e", "u", "n", "v");
		$token_string = "";
		for($i=0;$i<35;$i++) {
			if(mt_rand(0, 4) == 1) {
				$rand_letter = $letters[mt_rand(0, count($letters) - 1)];
				$token_string .= $rand_letter;
			} else {
				$token_string .= mt_rand(0, 9);
			}
		}
		return $token_string;
	}
}
?>
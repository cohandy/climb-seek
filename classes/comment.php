<?php
Class Comment {
	private $pdo;

	function __construct($fields, $pdo = false) {
		foreach($fields as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function createComment() {
		$body_check = self::validateBody($this->body);
		if($body_check['success']) {
			if(isset($_SESSION['user'])) {
				$stmt = $this->pdo->prepare("INSERT INTO comments(body, user_id, user_name, spot_id, user_avatar, upvotes, spot_name) VALUES(?, ?, ?, ?, ?, ?, ?)");
				if($stmt->execute([$this->body, $_SESSION['user']['id'], $_SESSION['user']['username'], $_SESSION['spot']['id'], $_SESSION['user']['avatar'], 1, $_SESSION['spot']['name']])) {
					//insert new upvote for new comment
					$comment_id = $this->pdo->lastInsertId();
					self::createCommentUpvote($comment_id);
					//create new activity
					Activity::newActivity($comment_id, "comment", $this->pdo, $this->body);
					return array('success' => true, 'body' => sanitize($this->body), 'id' => $comment_id);
				} else {
					return array("success" => false, "error" => "No connection to database, please try again later");
				}
			} else return array("success" => false, "error" => "You must be logged in to post a comment");
		}
	}

	function createGymComment() {
		$body_check = self::validateBody($this->body);
		if($body_check['success']) {
			if(isset($_SESSION['user'])) {
				$stmt = $this->pdo->prepare("INSERT INTO comments(body, user_id, user_name, gym_id, user_avatar, upvotes, spot_name) VALUES(?, ?, ?, ?, ?, ?, ?)");
				if($stmt->execute([$this->body, $_SESSION['user']['id'], $_SESSION['user']['username'], $_SESSION['gym']['id'], $_SESSION['user']['avatar'], 1, $_SESSION['gym']['name']])) {
					//insert new upvote for new comment
					$comment_id = $this->pdo->lastInsertId();
					self::createGymCommentUpvote($comment_id);
					//create new activity
					Activity::newGymActivity($comment_id, "comment", $this->pdo, $this->body);
					return array('success' => true, 'body' => sanitize($this->body), 'id' => $comment_id);
				} else {
					return array("success" => false, "error" => "No connection to database, please try again later");
				}
			} else return array("success" => false, "error" => "You must be logged in to post a comment");
		}
	}

	function createCommentUpvote($content_id) {
		$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, spot_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
		$stmt->execute([$_SESSION['user']['id'], $content_id, "comment", $_SESSION['spot']['id'], $_SESSION['user']['id']]);
	}

	function createGymCommentUpvote($content_id) {
		$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, gym_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
		$stmt->execute([$_SESSION['user']['id'], $content_id, "comment", $_SESSION['gym']['id'], $_SESSION['user']['id']]);
	}

	function deleteComment() {
		$stmt = $this->pdo->prepare("SELECT user_id FROM comments WHERE id=?");
		$stmt->execute([$this->comment_id]);
		$comment = $stmt->fetch();
		if($comment['user_id'] == $_SESSION['user']['id']) {
			$stmt2 = $this->pdo->prepare("DELETE FROM comments WHERE id=?");
			$stmt2->execute([$this->comment_id]);
			//delete all upvotes to this comment
			$stmt3 = $this->pdo->prepare("DELETE FROM upvotes WHERE content_id=? AND content_type=?");
			$stmt3->execute([$this->comment_id, "comment"]);
			return true;
		} else return false;
	}

	function fetchSpotComments($offset) {
		$stmt = $this->pdo->prepare("SELECT * FROM comments WHERE spot_id=? ORDER BY created_at DESC LIMIT 10 OFFSET ?");
		$stmt->execute([$_SESSION['spot']['id'], $offset]);
		if($comments = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $comments;
		} else return false;
	}

	function fetchGymComments($offset) {
		$stmt = $this->pdo->prepare("SELECT * FROM comments WHERE gym_id=? ORDER BY created_at DESC LIMIT 10 OFFSET ?");
		$stmt->execute([$_SESSION['gym']['id'], $offset]);
		if($comments = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $comments;
		} else return false;
	}

	function fetchUserComments($user_id, $offset) {
		$stmt = $this->pdo->prepare("SELECT * FROM comments WHERE user_id=? ORDER BY created_at DESC LIMIT 6 OFFSET ?");
		$stmt->execute([$user_id, $offset]);
		$comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $comments;
	}

	function validateBody() {
		if(strlen($this->body) > 2500) {
			return array("success" => false, "error" => "Comment is too long, max is 2500 characters");
		} else if(strlen($this->body) <= 0) {
			return array("success" => false, "error" => "Comment is empty!");
		} else {
			return array("success" => true);
		}
	}
}
?>
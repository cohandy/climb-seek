<?php
Class SpotEdit extends Spot {
	
	function updateSpot() {
		if(isset($_SESSION['user']) && $_SESSION['spot']['user_id'] == $_SESSION['user']['id']) {
			//if name is blank, user isn't updating name
			if(strlen($this->name) == 0) {
				$this->name = $_SESSION['spot']['name'];
				$name = parent::returnSuccess();
			} else {
				$name = parent::validateName();
			}
			if(strlen($this->location) == 0) {
				$this->location = $_SESSION['spot']['location'];
				$location = parent::returnSuccess();
			} else {
				$location = parent::validateLocation();
			}
			//convert passed booleans (appears as strings from js)
			if($this->confirmed === "true") {
				$this->confirmed = true;
			} else {
				$this->confirmed = false;
			}
			if($this->edit === "true") {
				$this->edit = true;
			} else {
				$this->edit = false;
			}
			if($name['success'] && $location['success']) {
				$stmt = $this->pdo->prepare("UPDATE spots SET name=?, location=?, confirmed=?, edit=? WHERE id=?");
				if($stmt->execute([$this->name, $this->location, $this->confirmed, $this->edit, $_SESSION['spot']['id']])) {
					return array("success" => true);
				} else return parent::returnError("Database could not be accessed, please try again in a few minutes");
			} else {
				$errors = array();
				$validations = array($name, $location);
				foreach($validations as $row) {
					if($row['success'] == false) {
						$errors[] = $row;
					}
				}
				return $errors;
			}
		} else return parent::returnError("You must be logged in to create a spot");
	}

	function editDesc($current_spot) {
		$validate_desc = parent::validateDesc();
		//check if spot is open to editing, or if owner is editing
		if(!$_SESSION['spot']['edit'] && $_SESSION['spot']['user_id'] !== $_SESSION['user']['id']) {
			return array('success' => false, 'error' => "You can't edit this spot!");
		}
		if($validate_desc['success']) {
			//don't do anything if new desc has 1 char or less
			if(strlen($this->desc) >= 1 && isset($_SESSION['user'])) {
				//first get amount of old descriptions in old_spot_info
				$old_desc_stmt = $this->pdo->prepare("SELECT * FROM old_spot_info WHERE spot_id=?");
				$old_desc_stmt->execute([$_SESSION['spot']['id']]);
				//if old info found, count the amount of desc stored, if less than five
				//add the one thats currently saved in the db, if five, delete second oldest
				//one (first is og desc, for extreme fallback)
				if($old_desc = $old_desc_stmt->fetchAll()) {
					$desc_arr = array();
					foreach($old_desc as $row) {
						if($row['type'] == "desc") {
							$desc_arr[] = $row;
						}
					}
					if(count($desc_arr) >= 5) {
						//sort array into chronological order
						usort($desc_arr, function($item1, $item2){
							$desc1 = strtotime($item1['created_at']);
							$desc2 = strtotime($item2['created_at']);
							return $desc1 - $desc2;
						});
						//delete second oldest desc
						$delete_desc = $this->pdo->prepare("DELETE FROM old_spot_info WHERE id=?");
						$delete_desc->execute([$desc_arr[1]['id']]);
					} 
				} 
				//add current desc into old_spot_info, update current spot 
				$insert_desc = $this->pdo->prepare("INSERT INTO old_spot_info(type, info, spot_id) VALUES(?, ?, ?)");
				$insert_desc->execute(["desc", $current_spot['description'], $_SESSION['spot']['id']]);
				$insert_new = $this->pdo->prepare("UPDATE spots SET description=? WHERE id=?");
				$insert_new->execute([$this->desc, $_SESSION['spot']['id']]);
				return parent::returnSuccess();
			} else return parent::returnError("An error occurred while saving description, please try again later");
		} else {
			return $validate_desc;
		}
	}

	function editTips($current_spot) {
		if(!$_SESSION['spot']['edit'] && $_SESSION['spot']['user_id'] !== $_SESSION['user']['id']) {
			return array('success' => false, 'error' => "You can't edit this spot!");
		}
		//don't do anything if new desc has 1 char or less
		$validate_tips = parent::validateTips();
		if($validate_tips['success']) {
			if(strlen($this->tips) >= 1 && isset($_SESSION['user'])) {
				//first get amount of old descriptions in old_spot_info
				$old_desc_stmt = $this->pdo->prepare("SELECT * FROM old_spot_info WHERE spot_id=?");
				$old_desc_stmt->execute([$_SESSION['spot']['id']]);
				//if old info found, count the amount of desc stored, if less than five
				//add the one thats currently saved in the db, if five, delete second oldest
				//one (first is og desc, for extreme fallback)
				if($old_desc = $old_desc_stmt->fetchAll()) {
					$desc_arr = array();
					foreach($old_desc as $row) {
						if($row['type'] == "tips") {
							$desc_arr[] = $row;
						}
					}
					if(count($desc_arr) >= 5) {
						//sort array into chronological order
						usort($desc_arr, function($item1, $item2){
							$desc1 = strtotime($item1['created_at']);
							$desc2 = strtotime($item2['created_at']);
							return $desc1 - $desc2;
						});
						//delete second oldest desc
						$delete_desc = $this->pdo->prepare("DELETE FROM old_spot_info WHERE id=?");
						$delete_desc->execute([$desc_arr[1]['id']]);
					} 
				} 
				//add current desc into old_spot_info, update current spot 
				$insert_desc = $this->pdo->prepare("INSERT INTO old_spot_info(type, info, spot_id) VALUES(?, ?, ?)");
				$insert_desc->execute(["tips", $current_spot['tips'], $_SESSION['spot']['id']]);
				$insert_new = $this->pdo->prepare("UPDATE spots SET tips=? WHERE id=?");
				$insert_new->execute([$this->tips, $_SESSION['spot']['id']]);
				return parent::returnSuccess();
			} else return parent::returnError("An error occurred while saving tips, please try again later");
		} else {
			return $validate_tips;
		}
	}

	function getCurrentSpot() {
		$stmt = $this->pdo->prepare("SELECT description, tips FROM spots WHERE id=?");
		$stmt->execute([$_SESSION['spot']['id']]);
		if($spot = $stmt->fetch()) {
			return $spot;
		} else return false;
	}
	
	function updateLocation() {
		if(isset($_SESSION['user'])) {
			//check to make sure spot is unconfirmed
			$stmt = $this->pdo->prepare("SELECT confirmed, name, area_id FROM spots WHERE id=?");
			$stmt->execute([$this->id]);
			$spot = $stmt->fetch();
			$spot_name = $spot['name'];
			$area_id = $spot['area_id'];
			if(!$spot['confirmed']) {
				//update spot
				$stmt2 = $this->pdo->prepare("UPDATE spots SET latitude=?, longitude=?, user_id=?, user_name=?, confirmed=?, area_id=?, search_long=? WHERE id=?");
				//get search_long
				$search_long = parent::getSearchLong($this->long);
				$stmt2->execute([$this->lat, $this->long, $_SESSION['user']['id'], $_SESSION['user']['username'], true, null, $search_long, $this->id]);
				//make new activity
				Activity::createSpotActivity($this->id, "confirm", $this->pdo, $spot_name);
				//update area
				if($area_id) {
					self::updateArea($area_id);
					return true;
				} else {
					return true;
				}
			} else return false;
		} else return false;
	}

	function updateArea($area_id, $size = null) {
		if(!$size) {
			$stmt = $this->pdo->prepare("SELECT count(id) FROM spots WHERE area_id=?");
			$stmt->execute([$area_id]);
			$count = $stmt->fetch();
			$size = $count['count(id)'];
		}
		if($size == 1) {
			$stmt2 = $this->pdo->prepare("SELECT id, area_id FROM spots WHERE area_id=?");
			$stmt2->execute([$area_id]);
			$spot = $stmt2->fetch();
			$stmt3 = $this->pdo->prepare("UPDATE spots SET area_id=? WHERE id=?");
			$stmt3->execute([null, $spot['id']]);
			$stmt4 = $this->pdo->prepare("DELETE FROM areas WHERE id=?");
			$stmt4->execute([$area_id]);
		} elseif($size > 1) {
			//get new area filter_type, check spots in area, update accordingly
			$area_changes = self::checkAreaFilterType($area_id);
			if(!$area_changes['delete']) {
				$stmt2 = $this->pdo->prepare("UPDATE areas SET size=?, filter_type=? WHERE id=?");
				$stmt2->execute([$area_changes['size'], $area_changes['filter_type'], $area_id]);
			}
		} else {
			$stmt5 = $this->pdo->prepare("DELETE FROM areas WHERE id=?");
			$stmt5->execute([$area_id]);
		}
	}

	function checkAreaFilterType($area_id) {
		$stmt = $this->pdo->prepare("SELECT id, filter_type FROM spots WHERE area_id=?");
		$stmt->execute([$area_id]);
		$spots = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$size = count($spots);
		if($size > 0) {
			$ropes = 0;
			$single_rope_spot = null;
			$boulder = 0;
			$single_boulder_spot = null;
			$both = 0;
			foreach($spots as $row) {
				if($row['filter_type'] == "Ropes") {
					$ropes += 1;
					if(!$single_rope_spot) {
						$single_rope_spot = $row['id'];
					}
				} elseif($row['filter_type'] == "Bouldering") {
					$boulder += 1;
					if(!$single_boulder_spot) {
						$single_boulder_spot = $row['id'];
					}
				} else {
					$both += 1;
				}
			}
			//check if single rope or boulder spot in area
			$rope_check = self::checkAreaSpots($ropes, $both, $single_rope_spot);
			$boulder_check = self::checkAreaSpots($boulder, $both, $single_boulder_spot);
			if($rope_check) {
				$size -= 1;
				$ropes -= 1;
			}
			if($boulder_check) {
				$size -= 1;
				$boulder -= 1;
			}
			//if size is <= 1 then run update area again with new size
			if($size <= 1) {
				self::updateArea($area_id, $size);
				return array('delete' => true);
			} else {
				//return new filter_type for spots;
				if($both >= 1) {
					return array('filter_type' => "Both", 'size' => $size, 'delete' => false);
				} elseif($ropes >= 1 && $boulder == 0) {
					return array('filter_type' => "Ropes", 'size' => $size, 'delete' => false);
				} else {
					return array('filter_type' => "Bouldering", 'size' => $size, 'delete' => false);
				}
			}
		}
	}

	function checkAreaSpots($filter_count, $both_count, $filter_spot_id) {
		//checks if there are single filter_types in an area, detached from area if so
		if($filter_count == 1 && $both_count == 0) {
			$stmt = $this->pdo->prepare("UPDATE spots SET area_id=? WHERE id=?");
			$stmt->execute([null, $filter_spot_id]);
			return true;
		} else return false;
	}

	function updateTopPic() {
		if($this->type == "spot") {
			$stmt2 = $this->pdo->prepare("UPDATE spots SET top_pic=? WHERE id=?");
			$stmt2->execute([$this->public_id, $_SESSION['spot']['id']]);
		} else {
			$stmt2 = $this->pdo->prepare("UPDATE gyms SET top_pic=? WHERE id=?");
			$stmt2->execute([$this->public_id, $_SESSION['gym']['id']]);
		}
	}
}
?>
<?php
use Aws\S3\S3Client;
Class Picture {

	function __construct($pic, $pdo = false) {
		foreach($pic as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function checkUserAvatar() {
		if(!$_SESSION['user']['avatar']) {
			self::submitAvatarId();
		} else {
			$stmt = $this->pdo->prepare("DELETE FROM activities WHERE content_type=? AND user_id=?");
			$stmt->execute(["avatar", $_SESSION['user']['id']]);
		}
		$_SESSION['user']['avatar'] = $_SESSION['user']['username'] . "avatar";
		//log new activity
		Activity::newAvatarActivity($_SESSION['user']['avatar'], $this->pdo);
		return array('success' => true, 'public_id' => $_SESSION['user']['avatar']);
	}

	function createPublicId() {
		//check if logged in
		if(isset($_SESSION['user'])) {
			$stmt = $this->pdo->prepare("SELECT public_id FROM pictures WHERE public_id=? AND user_id=?");
			$new_id = false;
			//create a new unique id (looks like this 'username43256') loop through users pics to make sure no
			//overwrites happen
			while(true) {
				$rand = self::generateNewId();
				$new_id = $_SESSION['user']['username'] . $rand;
				$stmt->execute([$new_id, $_SESSION['user']['id']]);
				if($found = $stmt->fetch()) {
					continue;
				} else break;
			}
			$this->public_id = $new_id;
			//submit pic into db (will be sent to s3 in js)
			self::submitPublicId();
			return array('success'=> true, 'public_id' => $new_id, "user_avatar" => $_SESSION['user']['avatar'], "user_name" => $_SESSION['user']['username'], "user_id" => $_SESSION['user']['id']);
		} else return array('success' => false);
	}

	function generateNewId() {
		return mt_rand(1, 1000000);
	}

	function submitPublicId() {
		if($this->column == "spot") {
			$stmt = $this->pdo->prepare("INSERT INTO pictures(public_id, spot_id, feature_name, js_date, user_id, user_name, user_avatar, upvotes) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			//execute
			$stmt->execute([$this->public_id, $_SESSION['spot']['id'], $_SESSION['spot']['name'], $this->js_date, $_SESSION['user']['id'], $_SESSION['user']['username'], $_SESSION['user']['avatar'], 1]);
		} elseif($this->column == "route") {
			$stmt = $this->pdo->prepare("INSERT INTO pictures(public_id, spot_id, route_id, feature_name, js_date, user_id, user_name, user_avatar, upvotes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			//execute
			$stmt->execute([$this->public_id, $_SESSION['spot']['id'], $this->column_id, $this->feature_name, $this->js_date, $_SESSION['user']['id'], $_SESSION['user']['username'], $_SESSION['user']['avatar'], 1]);
			//check if route has a top pic assigned
			$route_pic_check = self::getRouteTopPic($this->column_id);
			//update route with new top pic if none found
			if(!$route_pic_check) {
				$new_top_pic = $this->public_id;
			} else $new_top_pic = $route_pic_check;
			$stmt2 = $this->pdo->prepare("UPDATE routes SET top_pic=? WHERE id=?");
			$stmt2->execute([$new_top_pic, $this->column_id]);
		} elseif($this->column == "gym") {
			$stmt = $this->pdo->prepare("INSERT INTO pictures(public_id, gym_id, feature_name, js_date, user_id, user_name, user_avatar, upvotes) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			//execute
			$stmt->execute([$this->public_id, $_SESSION['gym']['id'], $_SESSION['gym']['name'], $this->js_date, $_SESSION['user']['id'], $_SESSION['user']['username'], $_SESSION['user']['avatar'], 1]);
		}
		if($this->column == "gym") {
			//log new activity
			Activity::newGymActivity($this->public_id, "picture", $this->pdo);
			//submit upvote for new pic
			$stmt2 = $this->pdo->prepare("INSERT INTO upvotes(content_id, content_type, user_id, gym_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
			$stmt2->execute([$this->public_id, "picture", $_SESSION['user']['id'], $_SESSION['gym']['id'], $_SESSION['user']['id']]);
		} else {
			//log new activity
			Activity::newActivity($this->public_id, "picture", $this->pdo);
			//submit upvote for new pic
			$stmt2 = $this->pdo->prepare("INSERT INTO upvotes(content_id, content_type, user_id, spot_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
			$stmt2->execute([$this->public_id, "picture", $_SESSION['user']['id'], $_SESSION['spot']['id'], $_SESSION['user']['id']]);
		}
	}

	function submitAvatarId() {
		$avatar_id = $_SESSION['user']['username'] . "avatar";
		$stmt = $this->pdo->prepare("UPDATE users SET avatar=? WHERE id=?");
		$stmt->execute([$avatar_id, $_SESSION['user']['id']]);
	}

	function getRouteTopPic($id) {
		$stmt = $this->pdo->prepare("SELECT public_id, upvotes FROM pictures WHERE route_id=? ORDER BY upvotes DESC LIMIT 1");
		$stmt->execute([$id]);
		$top_pic = $stmt->fetch();
		if($top_pic) {
			return $top_pic['public_id'];
		} else return false;
	}

	function addCaption() {
		$check_user = self::checkUserPic();
		$caption_check = self::validateCaption();
		if($check_user && $caption_check['success']) {
			$stmt = $this->pdo->prepare("UPDATE pictures SET caption=? WHERE public_id=?");
			$stmt->execute([$this->caption, $this->public_id]);
			return array("success" => true);
		} else {
			if(!$check_user) {
				return array("success" => false, "error" => "You can only edit your own pictures!");
			} else {
				return $caption_check;
			}
		}
	}

	function validateCaption() {
		if(strlen($this->caption) <= 250) {
			return array("success" => true);
		} else return array("success" => false, "error" => "Caption is too long, cannot exceed 250 characters");
	}

	function checkUserPic() {
		$check_user = $this->pdo->prepare("SELECT public_id, route_id FROM pictures WHERE user_id=? AND public_id=?");
		$check_user->execute([$_SESSION['user']['id'], $this->public_id]);
		return $check_user->fetch();
	}

	function deletePicture() {
		//initialize s3 object, provide credentials
		$s3 = S3Client::factory(array(
			'region' => AWS_REGION,
			'version' => '2006-03-01',
			'credentials' => array(
				'key' => AWS_ACCESS_KEY,
				'secret' => AWS_SECRET
			)
		));
		//check if picture being deleted belongs to user in session
		$check_user = self::checkUserPic();
		if($check_user) {
			if($this->options == "both") {
				//delete picture from bucket
				$result = $s3->deleteObject(array(
					'Bucket' => AWS_BUCKET,
					'Key' => $this->public_id
				));
				//delete smaller res file
				$small_public_id = $this->public_id . "s";
				$result_s = $s3->deleteObject(array(
					'Bucket' => AWS_BUCKET,
					'Key' => $small_public_id
				));
			}
			//delete pic from local db
			$stmt = $this->pdo->prepare("DELETE FROM pictures WHERE public_id=?");
			$stmt->execute([$this->public_id]);
			//and delete any pic upvotes
			$stmt2 = $this->pdo->prepare("DELETE FROM upvotes WHERE content_id=? AND content_type=?");
			$stmt2->execute([$this->public_id, "picture"]);
			//and delete activities
			$stmt3 = $this->pdo->prepare("DELETE FROM activities WHERE content_id=? AND user_id=?");
			$stmt3->execute([$this->public_id, $_SESSION['user']['id']]);
			//check if pic deleted is routes top pic
			if($check_user['route_id']) {
				$check_route = $this->pdo->prepare("SELECT top_pic FROM routes WHERE id=?");
				$check_route->execute([$check_user['route_id']]);
				$route = $check_route->fetch(PDO::FETCH_ASSOC);
				//if it is, find replacement pic
				if($route['top_pic'] == $this->public_id) {
					$new_top_pic = self::getRouteTopPic($check_user['route_id']);
					//update route with new top pic if any found
					$new_top_pic ? $set_pic = $new_top_pic : $set_pic = null;
					$update = $this->pdo->prepare("UPDATE routes SET top_pic=? WHERE id=?");
					$update->execute([$set_pic, $check_user['route_id']]);
				}
			}
			//check if pic deleted is the spots top pic
			if($this->type == "spot") {
				if($this->public_id == $_SESSION['spot']['top_pic']) {
					self::updateSpotTopPic();
				}
			} else {
				if($this->public_id == $_SESSION['spot']['top_pic']) {
					self::updateSpotTopPic();
				}
			}
			return true;
		} else return false;
	}

	function updateSpotTopPic() {
		if($this->type == "spot") {
			$stmt2 = $this->pdo->prepare("UPDATE spots SET top_pic=? WHERE id=?");
			$stmt2->execute([null, $_SESSION['spot']['id']]);
		} else {
			$stmt2 = $this->pdo->prepare("UPDATE gyms SET top_pic=? WHERE id=?");
			$stmt2->execute([null, $_SESSION['gym']['id']]);
		}
	}

	function getSpotPictures() {
		$stmt = $this->pdo->prepare("SELECT public_id, spot_id, user_name, feature_name, js_date, caption, route_id, upvotes, user_avatar FROM pictures WHERE spot_id=? ORDER BY upvotes DESC");
		$stmt->execute([$_SESSION['spot']['id']]);
		if($pics = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $pics;
		} else return false;
	}

	function getGymPictures() {
		$stmt = $this->pdo->prepare("SELECT public_id, gym_id, user_name, feature_name, js_date, caption, route_id, upvotes, user_avatar FROM pictures WHERE gym_id=? ORDER BY upvotes DESC");
		$stmt->execute([$_SESSION['gym']['id']]);
		if($pics = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $pics;
		} else return false;
	}

	function getUserPictures() {
		$stmt = $this->pdo->prepare("SELECT public_id, spot_id, user_name, feature_name, js_date, caption, route_id, upvotes, user_avatar, gym_id FROM pictures WHERE user_id=? ORDER BY upvotes DESC");
		$stmt->execute([$this->user_id]);
		$pictures = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $pictures;
	}

	function getS3Details($s3Bucket, $region, $acl = 'private') {
		// Options and Settings
	    $awsKey = AWS_ACCESS_KEY;
	    $awsSecret = AWS_SECRET;

	    $algorithm = "AWS4-HMAC-SHA256";
	    $service = "s3";
	    $date = gmdate("Ymd\THis\Z");
	    $shortDate = gmdate("Ymd");
	    $requestType = "aws4_request";
	    $expires = "86400"; // 24 Hours
	    $successStatus = "201";
	    $url = "//{$s3Bucket}.{$service}-{$region}.amazonaws.com";

	    // Step 1: Generate the Scope
	    $scope = [
	        $awsKey,
	        $shortDate,
	        $region,
	        $service,
	        $requestType
	    ];
	    $credentials = implode('/', $scope);

	    // Step 2: Making a Base64 Policy
	    $policy = [
	        'expiration' => gmdate('Y-m-d\TG:i:s\Z', strtotime('+6 hours')),
	        'conditions' => [
	            ['bucket' => $s3Bucket],
	            ['acl' => $acl],
	            ['starts-with', '$key', ''],
	            ['starts-with', '$Content-Type', ''],
	            ['success_action_status' => $successStatus],
	            ['x-amz-credential' => $credentials],
	            ['x-amz-algorithm' => $algorithm],
	            ['x-amz-date' => $date],
	            ['x-amz-expires' => $expires],
	        ]
	    ];
	    $base64Policy = base64_encode(json_encode($policy));

	    // Step 3: Signing your Request (Making a Signature)
	    $dateKey = hash_hmac('sha256', $shortDate, 'AWS4' . $awsSecret, true);
	    $dateRegionKey = hash_hmac('sha256', $region, $dateKey, true);
	    $dateRegionServiceKey = hash_hmac('sha256', $service, $dateRegionKey, true);
	    $signingKey = hash_hmac('sha256', $requestType, $dateRegionServiceKey, true);

	    $signature = hash_hmac('sha256', $base64Policy, $signingKey);

	    // Step 4: Build form inputs
	    // This is the data that will get sent with the form to S3
	    $inputs = [
	        'Content-Type' => '',
	        'acl' => $acl,
	        'success_action_status' => $successStatus,
	        'policy' => $base64Policy,
	        'X-amz-credential' => $credentials,
	        'X-amz-algorithm' => $algorithm,
	        'X-amz-date' => $date,
	        'X-amz-expires' => $expires,
	        'X-amz-signature' => $signature
	    ];

	    return compact('url', 'inputs');
	}
}
?>
<?php
Class Gym {

	function __construct($gym, $pdo = false) {
		foreach($gym as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function getGym($id) {
		$stmt = $this->pdo->prepare("SELECT * FROM gyms WHERE id=?");
		$stmt->execute([$id]);
		if($gym = $stmt->fetch(PDO::FETCH_ASSOC)) {
			return $gym;
		} else return false;
	}
}
?>
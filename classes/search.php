<?php
Class Search {
	private $pdo;

	function __construct($query, $pdo = false) {
		foreach($query as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function searchAll() {
		if($this->pdo) {
			$stmt = $this->pdo->prepare("SELECT name, latitude, longitude, search_type FROM cities WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)
										UNION ALL
										SELECT name, latitude, id, filter_type FROM spots WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)
										UNION ALL
										SELECT name, difficulty, type_climbing, spot_id FROM routes WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)
										UNION ALL
										SELECT name, city, id, filter_type FROM gyms WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)");
			$stmt->execute(["$this->q*", "$this->q*", "$this->q*", "$this->q*"]);
			if($spots = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $spots;
			} else return false;
		} else return false;
	}

	function searchSpots() {
		if($this->pdo) {
			$stmt = $this->pdo->prepare("SELECT name, id, filter_type FROM spots WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)");
			$stmt->execute(["$this->q*"]);
			if($spots = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $spots;
			} else return false;
		} else return false;
	}

	function searchLocations() {
		if($this->pdo) {
			$stmt = $this->pdo->prepare("SELECT name, latitude, longitude FROM cities WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)");
			$stmt->execute(["$this->q*"]);
			if($spots = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $spots;
			} else return false;
		} else return false;
	}

	function searchRoutes() {
		if($this->pdo) {
			$stmt = $this->pdo->prepare("SELECT name, difficulty, type_climbing, spot_id FROM routes WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)");
			$stmt->execute(["$this->q*"]);
			if($spots = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $spots;
			} else return false;
		} else return false;
	}

	function searchIndoor() {
		if($this->pdo) {
			$stmt = $this->pdo->prepare("SELECT name, id, city FROM gyms WHERE MATCH(name) AGAINST (? IN BOOLEAN MODE)");
			$stmt->execute(["$this->q*"]);
			if($spots = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $spots;
			} else return false;
		} else return false;
	}
}
?>
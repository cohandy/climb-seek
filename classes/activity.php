<?php
Class Activity {

	function newActivity($id, $type, $pdo, $message = null) {
		$stmt = $pdo->prepare("INSERT INTO activities(content_id, content_type, spot_id, spot_name, user_id, message) VALUES(?, ?, ?, ?, ?, ?)");
		$stmt->execute([$id, $type, $_SESSION['spot']['id'], $_SESSION['spot']['name'], $_SESSION['user']['id'], $message]);
	}

	function newGymActivity($id, $type, $pdo, $message = null) {
		$stmt = $pdo->prepare("INSERT INTO activities(content_id, content_type, gym_id, spot_name, user_id, message) VALUES(?, ?, ?, ?, ?, ?)");
		$stmt->execute([$id, $type, $_SESSION['gym']['id'], $_SESSION['gym']['name'], $_SESSION['user']['id'], $message]);
	}

	function newAvatarActivity($id, $pdo) {
		$stmt = $pdo->prepare("INSERT INTO activities(content_id, content_type, user_id) VALUES(?, ?, ?)");
		$stmt->execute([$id, "avatar", $_SESSION['user']['id']]);
	}

	function createSpotActivity($id, $type, $pdo, $name) {
		$stmt = $pdo->prepare("INSERT INTO activities(content_id, content_type, spot_id, spot_name, user_id) VALUES(?, ?, ?, ?, ?)");
		$stmt->execute([$id, $type, $id, $name, $_SESSION['user']['id']]);
	}

	function getActivities($user_id, $offset, $pdo) {
		$stmt = $pdo->prepare("SELECT * FROM activities WHERE user_id=? ORDER BY created_at DESC LIMIT 5 OFFSET ?");
		$stmt->execute([$user_id, $offset]);
		$activities = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $activities;
	}

	function activityHTML($activity, $user) {
		if($activity['content_type'] == "comment") {
			$comment = self::commentActivityHTML($activity, $user);
			echo $comment;
		} else if($activity['content_type'] == "picture" || $activity['content_type'] == "avatar") {
			if($activity['content_type'] == "picture") {
				$new_post = self::pictureActivityHTML($activity, $user);
			} else {
				$new_post = self::pictureActivityHTML($activity, $user, true);
			}
			echo $new_post;
		} else if($activity['content_type'] == "spot" || $activity['content_type'] == "confirm") {
			if($activity['content_type'] == "confirm") {
				$new_post = self::spotActivityHTML($activity, $user, true);
			} else {
				$new_post = self::spotActivityHTML($activity, $user);
			}
			echo $new_post;
		} else {
			$new_post = self::routeActivityHTML($activity, $user);
			echo $new_post;
		}
	}

	function commentActivityHTML($comment, $user) {
		$delete = self::activityUserCheck($user);
		if($comment['gym_id']) {
			$url = 'indoor/' . $comment['gym_id'] . "/" . $comment['spot_name'];
		} else {
			$url = 'spot/' . $comment['spot_id'] . "/" . $comment['spot_name'];
		}
		$new_post =  "<div class='activity-feed-div'>" .
						"<div style='display:none'>" .
							"<div class='activity-content-id'>" . $comment['content_id'] . "</div>" .
							"<div class='activity-content-type'>comment</div>" . 
						"</div>" .
						"<div class='comment-time' style='float:none'>About " . time_elapsed_string($comment['created_at']) . "</div>" .
						"<div class='activity-header'>" .
							"<span class='bolder-text theme-link link-activity-text'>" . $user['username'] . "</span>" .
							"<span class='basic-text regular-activity-text'> posted a comment on </span>" .
							"<a href=" . $url . " class='theme-link bolder-text link-activity-text'>" . $comment['spot_name'] . "'s</a>" . "<span class='basic-text regular-activity-text'> page:</span>" .
						"</div>" .
						"<div class='comment-activity basic-text'>" . $comment['message'] . "</div>" .
						$delete .
					 "</div>";
		return $new_post;
	}

	function spotActivityHTML($spot, $user, $confirm = null) {
		if($confirm) {
			$message = " confirmed a spot ";
			$content_type = "<div class='activity-content-type'>confirm</div>";
		} else {
			$message = " discovered a new spot ";
			$content_type = "<div class='activity-content-type'>spot</div>";
		}
		$delete = self::activityUserCheck($user);
		$new_post =  "<div class='activity-feed-div' style='min-height:120px;position:relative'>" .
						"<div style='display:none'>" .
							"<div class='activity-content-id'>" . $spot['content_id'] . "</div>" .
							$content_type .
						"</div>" .
						"<div class='comment-time' style='float:none'>About " . time_elapsed_string($spot['created_at']) . "</div>" .
						"<span class='new-spot-activity-icon glyphicon glyphicon-map-marker'></span>" .
						"<div class='activity-header' style='text-align:center;font-size:1.3em;margin-top:5px;position:relative;z-index:1'>" .
							"<span class='bolder-text theme-link link-activity-text'>" . $user['username'] . "</span>" .
							"<span class='basic-text regular-activity-text'>" . $message . "</span>" .
							"<a href='spot/" . $spot['spot_id'] . "/" . $spot['spot_name'] . "' class='theme-link bolder-text link-activity-text'>" . $spot['spot_name'] . "!</a>" .
						"</div>" .
						$delete . 
					 "</div>";
		return $new_post;
	}

	function routeActivityHTML($route, $user) {
		$delete = self::activityUserCheck($user);
		$new_post =  "<div class='activity-feed-div' style='min-height:120px;position:relative'>" .
						"<div style='display:none'>" .
							"<div class='activity-content-id'>" . $route['content_id'] . "</div>" .
							"<div class='activity-content-type'>route</div>" . 
						"</div>" .
						"<div class='comment-time' style='float:none'>About " . time_elapsed_string($route['created_at']) . "</div>" .
						"<span class='new-spot-activity-icon glyphicon glyphicon-road'></span>" .
						"<div class='activity-header' style='text-align:center;font-size:1.3em;margin-top:5px;position:relative;z-index:1'>" .
							"<span class='bolder-text theme-link link-activity-text'>" . $user['username'] . "</span>" .
							"<span class='basic-text regular-activity-text'> discovered a new route </span>" .
							"<a href='spot/" . $route['spot_id'] . "' class='theme-link bolder-text link-activity-text'>" . $route['message'] . "</a>" .
							"<span class='basic-text regular-activity-text'> at </span>" .
							"<a href='spot/" . $route['spot_id'] . "' class='theme-link bolder-text link-activity-text'>" . $route['spot_name'] . "!</a>" .
						"</div>" .
						$delete .
					 "</div>";
		return $new_post;
	}

	function pictureActivityHTML($picture, $user, $avatar = null) {
		$delete = self::activityUserCheck($user);
		if($picture['gym_id']) {
			$url = 'indoor/' . $picture['gym_id'] . "/" . $picture['spot_name'];
		} else {
			$url = 'spot/' . $picture['spot_id'] . "/" . $picture['spot_name'];
		}
		if($avatar) {
			$pic_message = "<span class='basic-text regular-activity-text'> uploaded a new avatar:</span>";
			$pic_url = "<img src='" . AWS_URL . "/" . $picture['content_id'] . "' class='pic-activity'/>";
			$content_type = "<div class='activity-content-type'>avatar</div>";
		} else {
			$pic_message = "<span class='basic-text regular-activity-text'> posted a picture on </span>" .
							"<a href=" . $url . " class='theme-link bolder-text link-activity-text'>" . $picture['spot_name'] . "'s</a>" . "<span class='basic-text regular-activity-text'> page:</span>";
			$pic_url = "<img src='" . AWS_URL . "/" . $picture['content_id'] . "s" . "' class='pic-activity'/>";
			$content_type = "<div class='activity-content-type'>picture</div>";
		}
		$new_post =  "<div class='activity-feed-div'>" .
						"<div style='display:none'>" .
							"<div class='activity-content-id'>" . $picture['content_id'] . "</div>" .
							$content_type . 
						"</div>" .
						"<div class='comment-time' style='float:none'>About " . time_elapsed_string($picture['created_at']) . "</div>" .
						"<div class='activity-header'>" .
							"<span class='bolder-text theme-link link-activity-text'>" . $user['username'] . "</span>" .
							$pic_message .
						"</div>" .
						"<div style='text-align:center;margin-top:15px;'>" .
							$pic_url . 
						"</div>" .
						$delete . 
					 "</div>";
		return $new_post;
	}

	function deleteActivity($post, $pdo) {
		//check if user owns this activity
		$stmt = $pdo->prepare("SELECT user_id FROM activities WHERE user_id=? AND content_type=? AND content_id=?");
		$stmt->execute([$_SESSION['user']['id'], $post['content_type'], $post['content_id']]);
		$check = $stmt->fetch();
		if($check) {
			$stmt2 = $pdo->prepare("DELETE FROM activities WHERE user_id=? AND content_type=? AND content_id=?");
			$stmt2->execute([$_SESSION['user']['id'], $post['content_type'], $post['content_id']]);
			return true;
		} else return false;
	}

	function activityUserCheck($user) {
		if(isset($_SESSION['user'])) {
			if($_SESSION['user']['id'] == $user['id']) {
				return '<div class="comment-options">' .
							'<span class="delete-activity comment-option">delete</span>' .
						'</div>';
			} else return "";
		} else return "";
	}
}
?>
<?php
Class Route {
	private $pdo;

	function __construct($route, $pdo = false) {
		foreach($route as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function createRoute() {
		if(isset($_SESSION['user'])) {
			$name_check = self::validateName();
			$type_check = self::validateType();
			$desc_check = self::validateDesc();
			$diff_check = self::validateDiff();
			//if all validations check out
			if($name_check['success'] && $type_check['success'] && $desc_check['success'] && $diff_check['success']) {
				$stmt = $this->pdo->prepare("INSERT INTO routes(name, type_climbing, description, difficulty, user_id, spot_id, user_name, upvotes, spot_name) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
				//make caps consistent
				if(strtolower($this->type) == "bouldering") {
					$this->diff = ucwords($this->diff);
				} else {
					$this->diff = strtolower($this->diff);
				}
				if($stmt->execute([$this->name, $this->type, $this->desc, $this->diff, $_SESSION['user']['id'], $_SESSION['spot']['id'], $_SESSION['user']['username'], 1, $_SESSION['spot']['name']])) {
					$route_id = $this->pdo->lastInsertId();
					//check if new route climb type is in this spots type_climbing
					$route_type = preg_match("/$this->type/i", $_SESSION['spot']['type_climbing']);
					//if no match new type will be added to spot 'type_climbing' col
					if(!$route_type) {
						$new_type = $_SESSION['spot']['type_climbing'] . ucwords($this->type) . ", ";
					} else $new_type = $_SESSION['spot']['type_climbing'];
					//get new num_routes and new avg_diff for this routes spot
					$spot_updates = self::determineSpotAvgDiff();
					//check if filter type needs to be changed
					$filter_type = self::determineFilterType();
					//now update spot with new info
					//determine which fields to update
					if($spot_updates['type'] == "bouldering_avg_diff") {
						$stmt_string = "UPDATE spots SET type_climbing=?, bouldering_avg_diff=?, num_routes=?, filter_type=? WHERE id=?";
					} else {
						$stmt_string = "UPDATE spots SET type_climbing=?, ropes_avg_diff=?, num_routes=?, filter_type=? WHERE id=?";
					}
					$stmt3 = $this->pdo->prepare($stmt_string);
					$stmt3->execute([$new_type, $spot_updates['average'], $spot_updates['num_routes'], $filter_type, $_SESSION['spot']['id']]);
					//create upvote for route from user
					self::createRouteUpvote($route_id);
					//log new activity
					Activity::newActivity($route_id, "route", $this->pdo, $this->name);
					//return new route info
					return array("success" => true, "id" => $route_id, "name" => sanitize($this->name), "type_climbing" => sanitize($this->type), "difficulty" => sanitize($this->diff), "description" => sanitize($this->desc));
				} else return self::returnError("Database could not be accessed, please try again in a few minutes");
			} else {
				$errors = array();
				$validations = array($name_check, $type_check, $desc_check, $diff_check);
				foreach($validations as $row) {
					if($row['success'] == false) {
						$errors[] = $row;
					}
				}
				return $errors;
			}
		} else return self::returnError("You must be logged in to create a route");
	}

	function determineSpotAvgDiff() {
		//get all routes from spots to get average diff
		$stmt2 = $this->pdo->prepare("SELECT difficulty, type_climbing FROM routes WHERE spot_id=?");
		$stmt2->execute([$_SESSION['spot']['id']]);
		if($routes = $stmt2->fetchAll(PDO::FETCH_ASSOC)) {
			$num_routes = count($routes);
			//update bouldering average diff if new route type is bouldering
			if(strtolower($this->type) == "bouldering") {
				$boulder_arr = array();
				for($i=0;$i<count($routes);$i++) {
					if(strtolower($routes[$i]['type_climbing']) == "bouldering") {
						$boulder_arr[] = $routes[$i];
					}
				}
				if(count($boulder_arr) == 1) {
					$avg_diff = $this->diff;
				} else {
					$avg_diff = self::avgDiff("bouldering", $boulder_arr);
				}
				return array("type" => "bouldering_avg_diff", "average" => ucwords($avg_diff), "num_routes" => $num_routes);
			} else {
				//otherwise update ropes avg diff
				$ropes_arr = array();
				for($i=0;$i<count($routes);$i++) {
					if(strtolower($routes[$i]['type_climbing']) != "bouldering") {
						$ropes_arr[] = $routes[$i];
					}
				}
				//if only one item in array, that diff is new avg
				if(count($ropes_arr) == 1) {
					$avg_diff = $this->diff;
				} else {
					$avg_diff = self::avgDiff("ropes", $ropes_arr);
				}
				return array("type" => "ropes_avg_diff", "average" => strtolower($avg_diff), "num_routes" => $num_routes);
			}
		} else {
			if(strtolower($this->type) == "bouldering") {
				return array("type" => "bouldering_avg_diff", "average" => $this->diff, "num_routes" => 1);
			} else {
				return array("type" => "ropes_avg_diff", "average" => $this->diff, "num_routes" => 1);
			}
		}
	}

	function determineFilterType() {
		$spot_filter = $_SESSION['spot']['filter_type'];
		if($spot_filter == "Ropes" && strtolower($this->type) == "bouldering" || $spot_filter == "Bouldering" && strtolower($this->type) != "bouldering") {
			return "Both";
		} else {
			return $spot_filter;
		}
	}

	function validateName() {
		if(strlen($this->name) < 3) {
			return self::returnError("Name is too short, must be at least 3 characters");
		} elseif(strlen($this->name) > 30) {
			return self::returnError("Name is too long, cannot exceed 30 characters");
		} else {
			return self::returnSuccess();
		}
	}

	function validateDesc() {
		if(strlen($this->desc) > 500 ) {
			return self::returnError("Description is too long, cannot exceed 500 characters");
		} elseif(strlen($this->desc) < 1) {
			return self::returnError("Description is blank");
		} else {
			return self::returnSuccess();
		}
	}

	function validateDiff() {
		if(strtolower($this->type) == "bouldering") {
			$scale_check = substr($this->diff, 0, 1);
			if(strtolower($scale_check) != "v") {
				return self::returnError("Wrong scale for difficulty, use V scale for bouldering <a href='http://en.wikipedia.org/wiki/Grade_(climbing)#Bouldering_2' target='_blank'>see here for information</a>");
			} else {
				$diff_check = preg_match("/^v(b|[0-9])$|^v1[0-7]$/i", $this->diff);
				if(!$diff_check) {
					return self::returnError("Incorrect difficulty rank, use V scale for bouldering <a href='http://en.wikipedia.org/wiki/Grade_(climbing)#Bouldering_2' target='_blank'>see here for information</a>");
				} else return self::returnSuccess();
			}
		} else {
			$scale_check = substr($this->diff, 0, 1);
			if($scale_check != "5") {
				return self::returnError("Wrong scale for difficulty, we use YDS class 5 rankings <a href='http://en.wikipedia.org/wiki/Grade_(climbing)#Free_climbing_2' target='_blank'>see here for information</a>");
			} else {
				$diff_check = preg_match("/^5\.[0-1][0-5]($|[a-d])$|^5\.[0-9]$/i", $this->diff);
				if(!$diff_check) {
					return self::returnError("Incorrect difficulty rank, must use YDS class 5 rankings <a href='http://en.wikipedia.org/wiki/Grade_(climbing)#Free_climbing_2' target='_blank'>see here for information</a>");
				} return self::returnSuccess();
			}
		}
	}

	function validateType() {
		if(strlen($this->type) < 3 || $this->type == "Types") {
			return self::returnError("Type is not valid");
		} else return self::returnSuccess();
	}

	function createRouteUpvote($content_id) {
		$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, spot_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
		$stmt->execute([$_SESSION['user']['id'], $content_id, "route", $_SESSION['spot']['id'], $_SESSION['user']['id']]);
	}

	function getRoutesFromSpot() {
		if(isset($_SESSION['spot'])) {
			$stmt = $this->pdo->prepare("SELECT * FROM routes WHERE spot_id=? ORDER BY created_at DESC LIMIT 8 OFFSET ?");
			$stmt->execute([$_SESSION['spot']['id'], $this->offset]);
			if($routes = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				return $routes;
			} else return false;
		}
	}

	function getRoutesFromUser() {
		$stmt = $this->pdo->prepare("SELECT * FROM routes WHERE user_id=? ORDER BY created_at DESC LIMIT 8 OFFSET ?");
		$stmt->execute([$this->user_id, $this->offset]);
		if($routes = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $routes;
		} else return false;
	}

	function editDesc() {
		if(isset($_SESSION['user'])) {
			$stmt = $this->pdo->prepare("SELECT user_id FROM routes WHERE id=?");
			$stmt->execute([$this->route_id]);
			$route = $stmt->fetch();
			if(!$route['user_id'] || $_SESSION['user']['id'] == $route['user_id']) {
				$desc_check = self::validateDesc();
				if($desc_check['success']) {
					$stmt2 = $this->pdo->prepare("UPDATE routes SET description=?, user_id=?, user_name=? WHERE id=?");
					$stmt2->execute([$this->desc, $_SESSION['user']['id'], $_SESSION['user']['username'], $this->route_id]);
					return self::returnSuccess();
				} else return $desc_check;
			} else return self::returnError("You haven't discovered this route!");
		} else return self::returnError("Must be logged in to edit!");
	}

	function avgDiff($type, $diff_arr) {
		//average boulder routes V scale
		if(strtolower($type) == "bouldering") {
			$total = 0;
			if(count($diff_arr) > 1) {
				for($i=0;$i<count($diff_arr);$i++) {
					if(strlen($diff_arr[$i]['difficulty']) >= 0) {
						preg_match("/\d+/", $diff_arr[$i]['difficulty'], $full_diff);
						$total += (int) $full_diff[0];
					}
				}
				return "V" . floor($total/count($diff_arr));
			} else {
				if(count($diff_arr) > 0) {
					return $diff_arr[0];
				} else {
					return null;
				}
			}
		//average ropes 5. scale
		} else {
			$total = 0;
			for($i=0;$i<count($diff_arr);$i++) {
				//split on . then grab just numbers after 5.
				$diff = preg_split("/\./", $diff_arr[$i]['difficulty']);
				preg_match("/\d+/", $diff[1], $diff_number);
				$number = (int) $diff_number[0];
				//if diff is higher than 10 a letter is attached
				if($number >= 10) {
					//find letter and attribute it with a weighted float
					//$diff_number[1] will contain a letter if its set
					if(isset($diff_number[1])) {
						preg_match("/\D/", $diff[1], $letter);
						switch(strtolower($letter[0])) {
							case "a":
								$letter[0] = 0.2;
								break;
							case "b":
								$letter[0] = 0.4;
								break;
							case "c":
								$letter[0] = 0.6;
								break;
							case "d":
								$letter[0] = 0.8;
								break;
							default:
								$letter[0] = 0;
								break;
						}
						$number += $letter[0];
					}
				}
				$total += $number;
			}
			//round total ex 11.43
			$avg = round($total/count($diff_arr), 2);
			//if less than 10 no letter is attached return
			if($avg < 10) {
				return "5." . floor($avg);
			} else {
				//check if num is float, if not add 'a'
				preg_match("/\./", $avg, $float);
				if($float) {
					//take off decimal and determine new letter scale
					$letter_grade = preg_split("/\./", $avg);
					$scale = round($letter_grade[1], 1);
					if($scale <= 2) {
						$scale = "a";
					} elseif($scale > 2 && $scale <= 4) {
						$scale = "b";
					} elseif($scale > 4 && $scale <= 7) {
						$scale = "c";
					} else {
						$scale = "d";
					}
					return "5." . $letter_grade[0] . $scale;
				} else {
					return "5." . $avg . "a";
				}
			}
		}
	}

	function returnError($error) {
		return array("success" => false, "error" => $error);
	}

	function returnSuccess() {
		return array("success" => true);
	}
}
?>
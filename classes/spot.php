<?php
	//***********************\\
		  //Spot Model\\
	//***********************\\
Class Spot {
	protected $pdo;

	function __construct($spot, $pdo = false) {
		foreach($spot as $key => $value) {
			$this->$key = $value;
		}
		$this->pdo = $pdo;
	}

	function createSpot() {
		if(isset($_SESSION['user'])) {
			$name = self::validateName();
			$coords = self::validateCoords();
			$desc = self::validateDesc();
			$tips = self::validateTips();
			$filter_type = self::validateType();
			$location = self::validateLocation();
			if($this->edit === "true") {
				$this->edit = true;
			} else {
				$this->edit = false;
			}
			$location_arr = self::determineSpotLocation();
			if($name['success'] && $coords['success'] && $desc['success'] && $tips['success'] && $filter_type['success'] && $location['success']) {
				$stmt = $this->pdo->prepare("INSERT INTO spots(name, latitude, longitude, description, tips, filter_type, user_id, user_name, confirmed, upvotes, edit, state, region, location, search_long) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				//create search_long column, returns digits before '.' for fts searching
				$search_long = self::getSearchLong($this->long);
				if($stmt->execute([$this->name, $this->lat, $this->long, $this->desc, $this->tips, $this->filter_type, $_SESSION['user']['id'], $_SESSION['user']['username'], true, 1, $this->edit, $location_arr['state'], $location_arr['city'], ucwords($this->location), $search_long])) {
					$new_id = $this->pdo->lastInsertId();
					//add new spot into session
					$_SESSION['spot'] = array("id" => $this->pdo->lastInsertId(), "name" => sanitize($this->name), "top_pic" => null);
					//create upvote for new spot
					self::createSpotUpvote($new_id);
					//log new activity
					Activity::newActivity($new_id, "spot", $this->pdo, $this->name);
					return array("success" => true, "spot_id" => $new_id);
				} else return self::returnError("Database could not be accessed, please try again in a few minutes");
			} else {
				$errors = array();
				$validations = array($name, $coords, $desc, $tips, $filter_type, $location);
				foreach($validations as $row) {
					if($row['success'] == false) {
						$errors[] = $row;
					}
				}
				return $errors;
			}
		} else return self::returnError("You must be logged in to create a spot");
	}

	function getSearchLong($long) {
		$shorten_long = explode(".", $long);
		if(array_key_exists(0, $shorten_long)) {
			return $shorten_long[0] . "00";
		} else return false;
	}

	function getSpot($id) {
		$stmt = $this->pdo->prepare("SELECT id, name, latitude, longitude, type_climbing, description, tips, confirmed, user_id, user_name, top_pic, num_routes, upvotes, filter_type, state, region, location, edit FROM spots WHERE id=?");
		$stmt->execute([$id]);
		if($spot = $stmt->fetch(PDO::FETCH_ASSOC)) {
			return $spot;
		} else return false;
	}

	function validateName() {
		if(strlen($this->name) < 5) {
			return self::returnError("Name is too short, must exceed 5 characters");
		} else if(strlen($this->name) > 35) {
			return self::returnError("Name is too long, cannot exceed 35 characters");
		} else {
			return self::returnSuccess();
		}
	}

	function validateCoords() {
		$lat_check = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $this->lat);
		$long_check = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $this->long);
		if($lat_check && $long_check) {
			return self::returnSuccess();
		} else {
			return self::returnError("Coordinates are incorrect, use the map above to get accurate coordinates (Lat/Long must be in EPSG:4326 btw)");
		}
	}

	function validateType() {
		if($this->filter_type == "Bouldering" || $this->filter_type == "Ropes" || $this->filter_type == "Both") {
			return self::returnSuccess();
		} else {
			return self::returnError("Spot type is not checked");
		}
	}

	function validateLocation() {
		if(strlen($this->location < 40)) {
			return self::returnSuccess();
		} else {
			return self::returnError("Location is too long, max is 40 characters");
		}
	}

	function validateDesc() {
		if(strlen($this->desc) < 5) {
			return self::returnError("Needs some sort of description!");
		} else if(strlen($this->desc) > 5000) {
			return self::returnError("Description is too long, we appreciate the enthusiasm though! Max is 5000 characters");
		} else {
			return self::returnSuccess();
		}
	}

	function validateTips() {
		if(strlen($this->tips) > 3000) {
			return self::returnError("Tips is too long! You're full of information, maybe make a comment on the spot page?");
		} else {
			return self::returnSuccess();
		}
	}

	function createSpotUpvote($content_id) {
		$stmt = $this->pdo->prepare("INSERT INTO upvotes(user_id, content_id, content_type, spot_id, content_owner_id) VALUES(?, ?, ?, ?, ?)");
		$stmt->execute([$_SESSION['user']['id'], $content_id, "spot", $_SESSION['spot']['id'], $_SESSION['user']['id']]);
	}

	function getUserSpots($user_id, $offset) {
		$stmt = $this->pdo->prepare("SELECT * FROM spots WHERE user_id=? ORDER BY created_at DESC LIMIT 5 OFFSET ?");
		$stmt->execute([$user_id, $offset]);
		$spots = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $spots;
	}

	function returnError($error) {
		return array("success" => false, "error" => $error);
	}

	function returnSuccess() {
		return array("success" => true);
	}

	function determineSpotLocation() {
		//this gets the closest city to the spots coords
		$stmt = $this->pdo->prepare("SELECT name, latitude, longitude FROM cities WHERE latitude LIKE ? AND longitude LIKE ?");
		$search_lat = self::createSearchCoord($this->lat);
		$search_long = self::createSearchCoord($this->long);
		$stmt->execute([$search_lat, $search_long]);
		if($cities = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			//closest_city will contain the city arr and the difference between its coords and the spots
			$closest_city = "";
			$int_lat = (float)$this->lat;
			$int_long = (float)$this->long;
			foreach($cities as $row) {
				$lat_diff = self::getCoordDiff($int_lat, (float)$row['latitude']);
				$long_diff = self::getCoordDiff($int_long, (float)$row['longitude']);
				$coord_diff = $lat_diff + $long_diff;
				if($closest_city == "") {
					$closest_city = array('city' => $row, 'diff' => $coord_diff);
				} else {
					if($closest_city['diff'] > $coord_diff) {
						$closest_city = array('city' => $row, 'diff' => $coord_diff);
					}
				}
			}
			$city_info = explode(", ", $closest_city['city']['name']);
			return array('state' => $city_info[1], 'city' => $city_info[0]);
		} else {
			return array('state' => null, 'city' => null);
		}
	}

	function createSearchCoord($coord) {
		//this returns the first part of a coordinate for searching is a sql like query
		$coord_arr = explode(".", $coord);
		$search_coord = substr($coord_arr[0], 0, strlen($coord_arr[0]) - 1);
		return $search_coord . "%";
	}

	function getCoordDiff($spot_coord, $test_coord) {
		if($spot_coord > $test_coord) {
			$first_coord = $spot_coord;
			$second_coord = $test_coord;
		} else {
			$first_coord = $test_coord;
			$second_coord = $spot_coord;
		}
		return $first_coord - $second_coord;
	}
}
?>
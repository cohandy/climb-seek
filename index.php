<!DOCTYPE html>
<?php //start session, require all necessary files 
session_start();
require __DIR__ . '/vendor/autoload.php';
require_once "config.php";
require_once "classes/all_classes.php";
//$detect will be used in head and below before switch
$detect = new Mobile_Detect;
$on_mobile = $detect->isMobile();
//check if remember me cookie
if(!isset($_SESSION['user'])) {
	if(!isset($_SESSION['remember'])) {
		$pdo = SQLiteDB::getInstance();
		$remember = new User(array(), $pdo);
		$remember->checkCookie();
		$_SESSION['remember'] = array('checked' => true);
	}
}
?>
<html>
<head>
	<?php
	$host = substr($_SERVER['HTTP_HOST'], 0, 5);
	if(in_array($host, array('local', '127.0', '192.1'))) {
		echo '<base href="/ascent/web/"/>';
	} else {
		echo '<base href="/"/>';
	}
	include "head.php"; 
	?>
</head>
<body>
	<?php
	//set navbar
	echo "<div class='container-fluid'>";
	include "views/partials/_navbar.php";
	echo "</div>";
	//determine page being accessed
	isset($_GET['p']) ? $p = $_GET['p'] : $p = 'home';
	//yield page to $p
	if($on_mobile) {
		include "views/partials/_mobile_flash_info.php";
		//mobile equipped pages
		switch($p) {
			case "explore":
				include "views/explore/mobile_explore.php";
				break;
			case "spot":
				include "views/spots/mobile_show.php";
				break;
			case "new":
				include "views/spots/new.php";
				break;
			case "profile":
				include "views/users/mobile_show.php";
				break;
			case "indoor":
				include "views/gyms/mobile_show.php";
				break;
			case "edit":
				include "views/users/edit.php";
				break;
			case "forgot":
				include "views/users/forgot_password.php";
				break;
			case "recover":
				include "views/users/recover_password.php";
				break;
			case "terms":
				include "views/static_pages/terms.php";
				break;
			case "privacy":
				include "views/static_pages/privacy.php";
				break;
			case "contact":
				include "views/static_pages/contact.php";
				break;
			case "login":
				include "views/users/login.php";
				break;
			case "signup":
				include "views/users/signup.php";
				break;
			case "about":
				include "views/static_pages/about.php";
				break;
			case "error":
				include "views/static_pages/error.php";
				break;
			default:
				include "views/home.php";
				break;
		}
	} else {
		include "views/partials/_flash_info.php";
		//modals
		include "views/partials/_login_signup_modal.php";
		//desktop pages
		switch($p) {
			case "explore":
				include "views/explore/new_ols_map.php";
				break;
			case "spot":
				include "views/spots/show.php";
				break;
			case "new":
				include "views/spots/new.php";
				break;
			case "profile":
				include "views/users/show.php";
				break;
			case "indoor":
				include "views/gyms/show.php";
				break;
			case "edit":
				include "views/users/edit.php";
				break;
			case "forgot":
				include "views/users/forgot_password.php";
				break;
			case "recover":
				include "views/users/recover_password.php";
				break;
			case "terms":
				include "views/static_pages/terms.php";
				break;
			case "privacy":
				include "views/static_pages/privacy.php";
				break;
			case "contact":
				include "views/static_pages/contact.php";
				break;
			case "about":
				include "views/static_pages/about.php";
				break;
			case "error":
				include "views/static_pages/error.php";
				break;
			default:
				include "views/home.php";
				break;
		}
	}
	//footer
	if($p == "explore" || $p == "edit" || $p == "forgot" || $p == "recover" || $p == "test" || $p == "mobile" || $p == "login" || $p == "signup" || $p == "contact" || $p == "error") {
		//no footer
	} else {
		include "views/partials/_footer.php";
	}
	?>
</body>
</html>
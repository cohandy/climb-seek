<?php //set unique page title if present 
	isset($_GET['p']) ? $p = $_GET['p'] : $p = "home";
	switch($p) {
		case "explore":
			$page_title = "Explore | climb&seek";
			break;
		case "spot":
			$page_title = "Spot | climb&seek";
			break;
		case "new":
			$page_title = "New Spot | climb&seek";
			break;
		case "profile":
			$page_title = $_GET['username'] . " | climb&seek";
			break;
		case "indoor":
			$page_title = "Gym | climb&seek";
			break;
		case "edit":
			$page_title = "Edit Profile | climb&seek";
			break;
		case "forgot":
			$page_title = "Recover Password | climb&seek";
			break;
		case "recover":
			$page_title = "Recover Password | climb&seek";
			break;
		case "login":
			$page_title = "Log In | climb&seek";
			break;
		case "signup":
			$page_title = "Sign Up | climb&seek";
			break;
		case "terms":
			$page_title = "Terms | climb&seek";
			break;
		case "privacy":
			$page_title = "Privacy Policy | climb&seek";
			break;
		case "contact":
			$page_title = "Contact | climb&seek";
			break;
		case "about":
			$page_title = "About | climb&seek";
			break;
		case "":
			$page_title = "Home | climb&seek";
			break;
		case "home":
			$page_title = "Home | climb&seek";
			break;
		default:
			$page_title = "Error | climb&seek";
			break;
	}

	echo "<title>" . $page_title . "</title>";
?>
<script src="assets/js/jquery-bootstrap.min.js"></script>
<?php if($on_mobile) {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">';
	echo '<link href="assets/css/mobile_all.css" rel="stylesheet">';
} else {
	echo '<link href="assets/css/custom_all.css" rel="stylesheet">';
}
?>
<link rel="icon" type="image/png" href="assets/images/favicon.png">



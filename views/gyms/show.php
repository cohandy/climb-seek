<?php
$pdo = SQLiteDB::getInstance();
if($pdo && isset($_GET['id'])) {
	//grab spot
	$new_gym = new Gym($_GET, $pdo);
	$gym = $new_gym->getGym($_GET['id']);
	if($gym) {
		//log spot into session for updating purposes
		$_SESSION['gym'] = array('id' => $gym['id'], 'name' => $gym['name'], 'top_pic' => $gym['top_pic']);
		if(!$gym['upvotes']) {
			$gym['upvotes'] = 0;
		}
		if(isset($_SESSION['user'])) {
			//get user upvotes affiliated with this spot
			$user_upvotes = new Upvote($_GET, $pdo);
			$new_upvotes = $user_upvotes->getUserGymUpvotes();
			unset($user_upvotes);
			//check if spot liked
			$gym_liked = Upvote::checkIfUpvoted($new_upvotes, $gym['id'], "gym");
		} else {
			$new_upvotes = false;
			$gym_liked = false;
		}
		//get comments with spot_id
		$new_comments = new Comment($_GET, $pdo);
		$comments = $new_comments->fetchGymComments(0);
		unset($new_comments);
	} else $gym = null;
} else $gym = null;
if($gym) {
?>
<!-- start of page -->
<div id="pic-carousel">
	<span class="glyphicon glyphicon-remove close-fixed-modal" id="close-pic-carousel"></span>
	<div id="pic-left" class="carousel-pic-selector pic-selector-div" style="left:0">
		<img src='assets/images/pic-left.png' class="spot-image-selector"/>
	</div>
	<div id="pic-right" class="carousel-pic-selector pic-selector-div" style="right:0">
		<img src='assets/images/pic-right.png' class="spot-image-selector"/>
	</div>
	<!-- pic info div (user card) -->
	<div id="picture-info-div">
		<span class="report-id" style="display:none"></span>
		<span class="report-type" style="display:none">picture</span>
		<div id="picture-info-card">
			<div class="picture-vote-div" title="Upvote picture!">
				<span class="glyphicon glyphicon-menu-up upvote-icon"></span>
				<div id="pic-upvotes" class="upvote-amount basic-text">0</div>
			</div>
			<img src="assets/images/default-avatar.png" id='user-pic-avatar' class="img-circle"/>
			<div id="pic-user-info">
				<a href="#" id="pic-user-name"></a>
				<div id="pic-upload-date"></div>
			</div>
			<div id="pic-feature-name">
				<div class="pic-feature-col" id="pic-feature-route">
					<div class="pic-feature-type bolder-text">Route:</div>
					<div class="basic-text pic-feature-text"></div>
				</div>
			</div>
			<div id="pic-info-choices">
				<div class="glyphicon glyphicon-trash pic-choice-icon" id="delete-spot-picture" style="color:#ff4d4d" data-toggle="tooltip" data-placement="right" title="Delete Picture"></div>
				<div class="glyphicon glyphicon-edit pic-choice-icon add-caption" id="edit-caption" style="color:#6ba8e1" data-toggle="tooltip" data-placement="right" title="Edit Caption"></div>
			</div>
		</div>
	</div>
	<div id="pic-holder">
		<img src="#" id="current-carousel-pic" class="carousel-pic"/>
		<img src="#" id="next-carousel-pic" class="carousel-pic"/>
		<img src="#" id="last-carousel-pic" class="carousel-pic"/>
	</div>
	<!-- pic caption -->
	<div id="show-banner-caption">
		<div id="caption-text" class="basic-text"></div>
	</div>
	<span id="report-picture" class="activate-report">
		<span class="basic-text">report</span>
	</span>
	<div class="basic-text" id="carousel-pic-count"></div>
</div>
<div class="container-fluid">
	<div class="row" id="show-page-header"><!--picture row -->
			<div id="pic-left" class="pic-selector pic-selector-div" style="left:0">
				<img src='assets/images/pic-left.png' class="spot-image-selector"/>
			</div>
			<div id="pic-right" class="pic-selector pic-selector-div" style="right:0">
				<img src='assets/images/pic-right.png' class="spot-image-selector"/>
			</div>
			<div id="show-spot-main-image">
				<?php 
				if($gym['top_pic']) {
					echo '<img src="' . AWS_URL . "/" . $gym['top_pic'] . '" id="show-spot-image"/>';
				} else {
					echo '<img src="assets/images/gym-default-pic2.jpg" id="show-spot-image"/>';
				}
				?>
			</div>
			<div class="show-spot-banner-main" id="show-spot-banner-name">
				<?php echo sanitize($gym['name']) ?>
				<div id='show-spot-id' style="display:none"><?php echo sanitize($gym['id']) ?></div>
			</div>
			<div class="show-banner-element show-banner-location basic-text" id="spot-location">
			<?php
				echo $gym['city'] . ", " . $gym['state'];
			?>
			</div>
			<div class="show-banner-username show-banner-element basic-text" id="spot-picture-username" style="display:none"></div>
	</div>
	<!-- info bar -->
	<div class="row" id="placeholder-bar" style="display:none;height:60px"></div>
	<div class="row" id="show-spot-info-bar"> <!-- info bar row -->
		<div class="info-bar-column-left" id="info-bar-slider">
			<div class="info-bar-div" id="info-bar-name" style="display:none">
				<?php echo sanitize($gym['name']) ?>
			</div><div class="info-bar-div" id="info-bar-claims">
				<?php $gym['upvotes'] > 1 || $gym['upvotes'] == 0 ? $claims = " LIKES" : $claims = " LIKE";
				echo sanitize($gym['upvotes']) . $claims;
				?>
			</div>
		</div>
		<div class="info-bar-column-right">
			<?php if(!$gym_liked) { ?>
				<div class="sliding-nav-button smallish-button" id="like-spot" style="border-left:3px solid #347db2;">
					<span class="glyphicon glyphicon-thumbs-up"></span>
					<span class="claim-text">Like!</span>
				</div>
			<?php } else { ?>
				<div class="sliding-nav-button smallish-button" id="like-spot" style="border-left:3px solid #347db2;background:#398bc6;color:white">
					<span class="glyphicon glyphicon-thumbs-up"></span>
					<span class="claim-text">Liked</span>
				</div>
			<?php } // end spot like if ?>
			<div class="sliding-nav-button medium-button activate-upload-modal">
				<span class="glyphicon glyphicon-camera" style="margin-right:5px"></span>
				<span class="upload-id-info" style="display:none"><?php echo $gym['id']; ?></span>
				<span class="upload-type-info" style="display:none">gym</span>
				<span class="upload-name-info" style="display:none"><?php echo $gym['name'] ?></span>
				Add Photo
			</div>
			<a href=<?php echo "https://www.google.com/maps/dir/Current+Location/" . $gym['latitude'] . "," . $gym['longitude'];  ?> class="sliding-nav-button large-button" target="_blank"><span class="glyphicon glyphicon-map-marker" style="margin-right:2px"></span>Get Directions</a>
		</div>
	</div>
</div>
<!-- description spot-info row -->
<div class="container-fluid" id="body-background" style="padding:5px 0 25px 0">
	<div class="container" style="padding-top:5px;padding-bottom:5px">
		<div class="row">
			<div class="col-md-12">
				<div class="body-header">
					<div class="body-header-text">DESCRIPTION:</div>
					<div class="desc-icons activate-report" id="desc-report">
						<span class="desc-icon-text">Report</span><span class="glyphicon glyphicon-warning-sign desc-icons-icons"></span>
					</div>
					<div class="report-id" style="display:none"><?php echo $gym['id'] ?></div>
					<div class="report-type" style="display:none">gym</div>
					<div class="report-name" style="display:none"><?php echo $gym['name'] ?></div>
				</div>
				<div class="row" style="margin-top:10px">
					<div class="col-md-1 col-md-offset-1">
						<div class="inside-body-header">OVERVIEW:</div>
					</div>
					<div class="col-md-8 col-md-offset-1" id="desc-column" style="position:relative">
						<span class="inside-body-text" id="spot-info-description"><?php
							if(strlen($gym['description']) <= 0) {
								echo "<em>Been here before? Tell us what you saw.</em>";
							} else {
								echo sanitize(trim($gym['description']));
							}
							?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- start of comments -->
<div class="container-fluid" style="background:#f5f5f5;padding:15px 0 30px 0;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="body-header">
					<div class="body-header-text">COMMENTS:</div>
					<div class="sliding-button large-button comment-button" id="leave-comment">
						<span class="claim-text" style="margin-right:-18px">Leave a Comment</span>
						<span class="glyphicon glyphicon-pencil" style="opacity:0"></span>
					</div>
				</div>
				<!-- new comment form -->
				<div id="new-comment-div" style="display:none;margin-top:10px;padding-bottom:10px">
					<div class="form-error-area" id="comment-form-errors" style="text-align:center"></div>
					<form method="post" action="#" id="new-comment-form">
						<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
						<span class="comment-name"></span>
						<span class="alert alert-danger form-error" style="float:right;position:relative;top:24px" data-toggle="tooltip">
							<span class="glyphicon glyphicon-exclamation-sign"></span>
							<span class="error-text"></span>
						</span>
						<div style="padding-left:55px">
							<textarea rows="3" type="text" name="body" id="new-comment-body" class="new-spot-input" placeholder="Enter comment..." style="padding:10px 10px 0 10px"></textarea>
						</div>
						<div style="height:50px">
							<input type="submit" class="sliding-button smallish-button" style="float:right;margin-top:5px;padding:5px 5px;width:100px;font-size:1.2em;background:#f5f5f5" value="POST" id="comment-submit"/>
						</div>
					</form>
				</div>
				<!-- comments -->
				<div id="spot-comments">
					<?php if($comments) { 
						foreach($comments as $row) { ?>
						<?php //convert/check data
						!$row['upvotes'] ? $upvotes = "0" : $upvotes = $row['upvotes'];
						//change date to readable format
						$date = date_create($row['created_at']);
						$use_date = date_format($date, "F d Y");
						//check if user has upvoted any comments
						if($upvotes) {
							$check_upvote = Upvote::checkIfUpvoted($new_upvotes, $row['id'], "comment");
							if($check_upvote) {
								$upvoted = true;
							} else $upvoted = false;
						}
						?>
						<div class="spot-comment-div">
							<div class="comment-vote-div">
								<?php if($upvoted) {
									echo '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
								} else echo '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
								?>
								<div class="upvote-amount basic-text" style="font-size:1.3em;bottom:10px"><?php echo $upvotes; ?></div>
								<div class="comment-id" style="display:none"><?php echo $row['id']; ?></div>
							</div>
							<div class="comment-header">
								<?php if($row['user_avatar']) { ?>
									<span class="comment-pic"><img src=<?php echo AWS_URL . "/" . $row['user_avatar'] . "s" ?> class="comment-image"/></span>
								<?php } else { ?>
									<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
								<?php } //end avatar if ?>
								<a href=<?php echo "profile/" . $row['user_name']; ?> class="comment-name default-link"><?php echo $row['user_name']; ?></a>
								<span class="comment-time"><?php echo $use_date; ?></span>
								<div class="comment-body"><?php echo $row['body']; ?></div>
								<div class="comment-options">
									<?php 
									if(isset($_SESSION['user']) && $_SESSION['user']['id'] == $row['user_id']) {
										echo '<span class="delete-comment comment-option">delete</span>';
									} else {
										echo '<span class="report-comment comment-option activate-report">report</span>';
									}
									?>
									<div class="report-id" style="display:none"><?php echo $row['id']; ?></div>
									<div class="report-type" style="display:none">comment</div>
								</div>
							</div>
						</div>
					<?php } //end of comment loop
					} else { ?>
						<div id="no-comments" class="spot-page-placeholder-div">
							<div class="basic-text no-comments-text">No Comments....yet</div>
							<div class="glyphicon glyphicon-comment" style="font-size:5em"></div>
							<div class="basic-text no-comments-text">Been here? Tell us about your experience</div>
						</div>
					<?php } //end of comment if ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- load more comments div -->
<div class="load-more-div" id="load-more-comments" style="background:#f5f5f5;margin-top:0;border:none">
	<div class="load-more-background"></div>
	<div class="basic-text">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center;background:#f5f5f5;margin-top:0">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-comments-gif" alt="Loading..."/>
</div>
<?php include "views/partials/_upload_modal.php"; ?>
<?php include "views/partials/_report_modal.php"; ?>
<script type="text/javascript" id="main-script">
//page wide vars that hold spot info
var thisSpot = <?php echo json_encode($gym); ?>;
var userUpvotes = <?php echo json_encode($new_upvotes); ?>;
//set coords of this spot in sessionStorage, so when user goes back to map they'll be in the same spot
sessionStorage.setItem('lat', thisSpot.latitude);
sessionStorage.setItem('long', thisSpot.longitude);
//set page title with spot name
document.title = thisSpot.name + " | climb&seek";
//initialize tooltips
$("[data-toggle='tooltip']").tooltip();
/* ********************************
   --------------------------------
   |     Page load ajax calls     |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Get pic urls from db                   |
//----------------------------------------|
var spotPics = Array();
$.post("ajax/picture/get_gym_pics.php", {"content_type": "picture"}, function(data) {
	if(data) {
		$(".pic-selector").fadeIn();
		spotPics = data;
		//add 'upvoted' col to pics if currently logged in user has upvoted it
		if(userUpvotes.length > 0) {
			for(var i=0;i<spotPics.length;i++) {
				var upvoted = checkIfUpvoted(spotPics[i].public_id, "picture");
				if(upvoted) {
					spotPics[i].upvoted = true;
				}
			}
		}
		//change top pic if first pic in spotPics does not equal current top pic
		if(spotPics[0].public_id !== thisSpot.top_pic) {
			$.post("ajax/spot/update_top_pic.php", {"public_id": spotPics[0].public_id, "type": "gym"});
		}
	} 
}, 'json');
//check if user has upvoted content
function checkIfUpvoted(contentId, contentType) {
	if(userUpvotes.length > 0) {
		for(var i=0;i<userUpvotes.length;i++) {
			if(userUpvotes[i].content_type == contentType) {
				if(userUpvotes[i].content_id == contentId) {
					userUpvotes.splice(i, 1);
					return true;
				}
			}
		}
		return false;
	} else return false;
}
/* ********************************
   --------------------------------
   |        Main Show Pic     	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Attach main pic as background          |
//----------------------------------------|
//setMainPicture();
//setMainImageHeight();
function setMainPicture() {
	//determine main pic display
	if(thisSpot.top_pic) {
		var topPic = "url('" + generateS3URL(thisSpot.top_pic) + "')";
	} else var topPic = "url('assets/images/gym-default-pic2.jpg')";
	$("#show-spot-main-image").css({
		background: topPic,
		backgroundSize: "cover",
		backgroundPosition: "50%, 50%"
	})
}
function setMainImageHeight() {
	var winHeight = $(window).height();
	var newHeight = Math.floor(winHeight * .65);
	$("#show-spot-image-holder").css("height", newHeight + "px");
	$("#show-spot-main-image").css("height", newHeight + "px");
}
/* ********************************
   --------------------------------
   |       Button Animations  	  |
   ________________________________
   ********************************
*/
$("#make-claim").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-23px");
	}
);
$("#leave-comment").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-18px");
	}
);
$("#add-route").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-23px");
	}
);
//slides text in button to left, fades in hidden icon
function buttonAnimationOn(element) {
	element.find(".claim-text").stop().animate({
		marginRight: "0px"
	}, 500);
	element.find(".glyphicon").stop().animate({
		opacity: "1"
	}, 600);
}
function buttonAnimationOff(element, margin) {
	element.find(".claim-text").stop().animate({
		marginRight: margin
	}, 500);
	element.find(".glyphicon").stop().animate({
		opacity: "0"
	}, 300);
}
/* ********************************
   --------------------------------
   |   Route/Comment Animations   |
   ________________________________
   ********************************
*/
//pic selector icon animations
$(".pic-selector-div").hover(
	function() {
		$(this).find(".spot-image-selector").stop().animate({
			opacity: "0.8"
		}, 300);
		$(this).stop().animate({
			width: "90px"
		}, 300);
	},
	function() {
		$(this).find(".spot-image-selector").stop().animate({
			opacity: "0.5"
		}, 300);
		//this moves the icon a little
		$(this).stop().animate({
			width: "100px"
		}, 300);
	}
)
function typeColor(type) {
	switch(type) {
		case "Bouldering":
			return "#4ad153";
			break;
		case "Top Rope":
			return "#ff5555";
			break;
		case "Sport":
			return "#398bc6";
			break;
		case "Trad":
			return "#398bc6";
			break;
		case "Alpine":
			return "#cf7dfd";
			break;
		case "Ice":
			return "#cf7dfd";
			break;
		case "Sport/Trad":
			return "#398bc6";
			break;
		default:
			return "rgba(0, 0, 0, 0.2)";
			break;
	}
}
/* ********************************
   --------------------------------
   |     Info Bar Div Clicks 	  |
   ________________________________
   ********************************
*/
$(".info-bar-div").click(function() {
	var thisId = $(this).attr('id');
	var divOffset = {"top": 0};
	switch(thisId) {
		case "info-bar-routes":
			divOffset = $("#routes-container").offset();
			break;
		default:
			divOffset = {"top": 0};
			break;
	}
	if(!divOffset) {
		divOffset = {"top": 0};
	}
	$("html, body").animate({
		scrollTop: (divOffset.top - 110)
	})
});
/* ********************************
   --------------------------------
   |       Main Pic Carousel  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
//          Carousel Functions            |
//----------------------------------------|
//get next point in array of spotPics
function nextPic(picArr, currentImage, direction) {
	//loop through spotPics and find position of currentImage
	var place = findPublicIdIndex(picArr, currentImage);
	if(direction == "pic-right" || direction == "route-carousel-right") {
		//if place + 1 will be non-existant point, return to beginning
		if((place + 1) > (picArr.length - 1)) {
			return 0;
		} else return place + 1;
	} else {
		//if place - 1 is negative, go to end of array
		if((place - 1) < 0) {
			return picArr.length - 1;
		} else return place - 1;
	}
}
//change pic info on new pic
function updatePicInfo(point) {
	//show pic options if its the users pic
	if(spotPics[point].user_name == sessionStorage.getItem("username")) {
		$("#pic-info-choices").show();
	} else $("#pic-info-choices").hide();
	//replace caption if any
	if(spotPics[point].caption) {
		$("#show-banner-caption").show();
		$("#caption-text").text(spotPics[point].caption);
	} else if(spotPics[point].user_name == sessionStorage.getItem('username')) {
		$("#show-banner-caption").show();
		$("#caption-text").html("<div class='regular-button add-caption'>ADD CAPTION</div>");
	} else $("#show-banner-caption").hide();
	//replace name
	$("#pic-user-name").text(spotPics[point].user_name);
	//set height of show banner caption so text is right under pic
	var offset = $("#current-carousel-pic").offset();
	if(offset.top == 0 || offset.top >= 500) {
		offset = {"top": 50}
	}
	$("#show-banner-caption").css("height", offset.top + "px");
	//replace avatar
	if(spotPics[point].user_avatar != null) {
		$("#user-pic-avatar").attr('src', generateS3URL(spotPics[point].user_avatar));
	} else {
		$("#user-pic-avatar").attr('src', "assets/images/default-avatar.png");
	}
	//upload date
	$("#pic-upload-date").text("Uploaded " + timeSince(new Date(spotPics[point].js_date)) + " ago");
	if(spotPics[point].upvotes) {
		$("#pic-upvotes").text(spotPics[point].upvotes);
	} else {
		$("#pic-upvotes").text("0");
	}
	//if upvoted
	if(spotPics[point].upvoted) {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#347db2");
	} else {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#e9e9e9");
	}
	//if pic belongs to a route
	if(spotPics[point].route_id) {
		$("#pic-feature-name").css("display", "inline-block");
		$("#pic-feature-route").find(".pic-feature-text").html(spotPics[point].feature_name);
	} else {
		$("#pic-feature-name").hide();
	}
	$("#pic-profile").attr("href", "profile/" + spotPics[point].user_name);
	//add public id to report
	$("#picture-info-div").find(".report-id").text(spotPics[point].public_id);
	$("#pic-user-name").attr("href", "profile/" + spotPics[point].user_name);
	//update pic count
	$("#carousel-pic-count").text((point + 1) + " / " + spotPics.length);
};
//loop through spotPics and find position of public id
function findPublicIdIndex(picArr, publicId) {
	for(var i=0;i<picArr.length;i++) {
		if(picArr[i].public_id == publicId) {
			var place = i;
			break;
		}
	}
	return place;
}
function generateS3URL(publicId) {
	return S3URL + "/" + publicId;
}
//----------------------------------------|
// Pic Carousel                           |
//----------------------------------------|
$(".pic-selector").click(function() {
	if(spotPics.length > 0) {
		beginCarousel();
	}
});
$(".carousel-pic-selector").click(function() {
	if($(this).attr('id') == "pic-right") {
		slideNextPic("right");
	} else {
		slideNextPic("left");
	}
});
function beginCarousel() {
	//set first pic in array as current
	$("#current-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id)).show();
	//set next pic as second in array
	if(spotPics[1] !== undefined) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id))
	}
	//set last pic as last entry in array
	var lastPoint = spotPics.length - 1;
	if(spotPics[lastPoint] !== undefined) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[lastPoint].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	$("#pic-carousel").fadeIn();
	updatePicInfo(0);
}
//will be true when next pics are still being decided during animation
var loadingPics = false;
function slideNextPic(direction) {
	if(!loadingPics) {
		loadingPics = true;
		var winWidth = $(window).width() + 10;
		if(direction == "right") {
			var newCurrentPic = "next";
			$("#next-carousel-pic").css("left", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				right: winWidth + "px"
			}, 250);
			$("#next-carousel-pic").animate({
				left: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
		} else {
			var newCurrentPic = "last";
			$("#last-carousel-pic").css("right", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				left: winWidth + "px"
			}, 250);
			$("#last-carousel-pic").animate({
				right: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
		}
	}
}
function setUpNextPic(newCurrentPic) {
	//these vars hold the current status of the ids before swap
	var currentPic = $("#current-carousel-pic");
	var nextPic = $("#next-carousel-pic");
	var lastPic = $("#last-carousel-pic");
	//swap pic ids according to new position
	if(newCurrentPic == "next") {
		currentPic.attr('id', "next-carousel-pic");
		nextPic.attr('id', "current-carousel-pic");
	} else {
		currentPic.attr('id', 'last-carousel-pic');
		lastPic.attr('id', 'current-carousel-pic');
	}
	//remove attr gained from previous slide, using newly swapped ids
	$("#current-carousel-pic").css({
		right: "",
		left: ""
	});
	$("#next-carousel-pic").removeAttr('style');
	$("#last-carousel-pic").removeAttr('style');
	//get public id from new current pic and find its index in spotPics
	var currentPublicId = $("#current-carousel-pic").attr('src').split("/").pop();
	var currentIndex = findPublicIdIndex(spotPics, currentPublicId);
	//update pic card info
	updatePicInfo(currentIndex);
	//going from currentIndex, find out the pic in front and behind it
	if((currentIndex + 1) <= spotPics.length - 1) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex + 1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	if((currentIndex - 1) >= 0) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex - 1].public_id));
	} else {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[spotPics.length - 1].public_id));
	}
	loadingPics = false;
}
//close pic carousel
$("#close-pic-carousel").click(function() {
	$("#pic-carousel").fadeOut();
});
//set width for pic holder for margin auto to work
$("#pic-holder").css("width", $(window).width() + "px");
$(window).resize(function() {
	$("#pic-holder").css("width", $(window).width() + "px");
});
//----------------------------------------|
// Edit/Delete Pic                        |
//----------------------------------------|
//add caption
$(document).on("click", ".add-caption", function() {
	//check if editing or adding caption
	if($("#caption-text").text() != "ADD CAPTION") {
		var oldCaption = $("#caption-text").text();
	} else var oldCaption = "";
	//new textarea + save button
	var captionInput = "<textarea type='text' rows='1' class='add-caption-textarea basic-text' name='caption' placeholder='Enter caption...'>" + oldCaption + "</textarea>" + "<span class='regular-button' id='save-caption' style='border-radius:0 5px 5px 0;vertical-align:top;padding:12.5px 5px;'>Save</span>";
	//replace old button or caption with textarea, focus
	$("#caption-text").html(captionInput);
	$(".add-caption-textarea").focus();
});
//on caption save
$(document).on("click", "#save-caption", function() {
	var capObj = {"caption": $(".add-caption-textarea").val(), "public_id": $("#current-carousel-pic").attr('src').split("/").pop()};
	$.post("ajax/picture/add_caption.php", capObj, function(resp) {
		if(resp.success) {
			$("#caption-text").html(capObj.caption);
		} else {
			showFlash(resp.error, "danger", "alert");
		}
	}, 'json');
});
//add caption textarea css
$(document).on("focus", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0.5)");
});
$(document).on("blur", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0)");
});
//delete existing picture
$("#delete-spot-picture").click(function() {
	if(sessionStorage.getItem("username") == $("#pic-user-name").text()) {
		var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
		$.post("ajax/picture/delete_picture.php", {"public_id": publicId, "options": "both", "type": "gym"}, function(resp) {
			if(resp) {
				var picIndex = findPublicIdIndex(publicId);
				spotPics.splice(picIndex, 1);
				$("#spot-banner-close").click();
			} else {
				showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
				window.location.replace("index.php");
			}
		});
	} else {
		showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
		window.location.replace("index.php");
	}
});
/* ********************************
   --------------------------------
   |       Sticky Info Bar  	  |
   ________________________________
   ********************************
*/
//get position of info bar
var infoBar = $("#show-spot-info-bar");
var barTop = infoBar.offset().top;
//make info bar fixed when user scrolls down
$(window).scroll(function() {
	var winScroll = $(window).scrollTop();
	var infoStick = barTop - $("#navbar").height();
	if(winScroll >= infoStick && infoBar.hasClass("fixed-info-bar") != true) {
		infoBar.addClass("fixed-info-bar");
		//display hidden row the rest of the page doens't jump 50px on transition
		$("#placeholder-bar").show();
		//slide out spot name
		slideName("show");
	} else if(winScroll <= infoStick && infoBar.hasClass("fixed-info-bar")) {
		infoBar.removeClass("fixed-info-bar");
		$("#placeholder-bar").hide();
		slideName("hide");
	}
});
//----------------------------------------|
// Slide out spot name                    |
//----------------------------------------|
var nameWidth = Math.floor($("#info-bar-name").width());
function slideName(visible) {
	var nameDiv = $("#info-bar-slider");
	var spotName = $("#info-bar-name");
	if(visible == "show") {
		nameDiv.css("margin-left", "-" + nameWidth + "px");
		nameDiv.animate({
			"margin-left": "0px"
		}, 300);
		spotName.show();
	} else {
		spotName.hide();
	}
}
/* ********************************
   --------------------------------
   |          Upvotes    	      |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Picture/Comment upvotes                |
//----------------------------------------|
//upvote picture
$(".picture-vote-div").click(function() {
	//grab currently displaying image, split to get public id
	var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
	upvoteCommentPicture($(this), publicId, "picture");
});
//upvote comment
$(document).on("click", ".comment-vote-div", function() {
	var commentId = $(this).find(".comment-id").text();
	upvoteCommentPicture($(this), commentId, "comment");
});
//handles comments/picture upvotes
function upvoteCommentPicture($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		var upObj =  {"content_id": contentId, "content_type": contentType}
		$.post("ajax/upvote/upvote_gym_content.php", upObj, function(resp) {
			if(resp.success) {
				//if resp.action == upvote add one to existing num, if not minus 1
				var upvoteDisplay = $this.find(".upvote-amount");
				var currentUpvotes = parseInt(upvoteDisplay.text());
				var upIcon = $this.find(".glyphicon-menu-up");
				if(resp.action == "upvote") {
					upvoteDisplay.text(currentUpvotes + 1);
					upIcon.css("color", "#347db2");
				} else {
					upvoteDisplay.text(currentUpvotes - 1);
					upIcon.css("color", "#e9e9e9");
				}
				//update spot pics
				if(contentType == "picture") {
					var picIndex = findPublicIdIndex(contentId);
					spotPics[picIndex].upvoted = true;
					spotPics[picIndex].upvotes = $("#pic-upvotes").text();
				}
			} else {
				slideUserModal(false, "login");
			}
		}, 'json');
	} else slideUserModal(false, "login");
}
//----------------------------------------|
// Spot/Route upvotes                     |
//----------------------------------------|
//like spot
$("#like-spot").click(function() {
	var spotId = thisSpot.id;
	upvoteRouteSpot($(this), spotId, "gym");
});
//handles route/spot upvote
function upvoteRouteSpot($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		$.post("ajax/upvote/upvote_gym_content.php", {"content_id": contentId, "content_type": contentType}, function(resp) {
			if(resp.success) {
				if(contentType == "gym") {
					likeSpot(resp.action);
				}
			} else {
				slideUserModal(false, "login");
			}
		}, 'json');
	} else slideUserModal(false, "login");
}
function likeSpot(action) {
	if(action == "upvote") {
		var currentLikes = parseInt($("#info-bar-claims").text()) + 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "#398bc6",
			color: "white"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKED");
	} else {
		var currentLikes = parseInt($("#info-bar-claims").text()) - 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "white",
			color: "#398bc6"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKE!");
	}
}
/* ********************************
   --------------------------------
   |         New Comment   	      |
   ________________________________
   ********************************
*/
//slide down new comment form
$("#leave-comment").click(function() {
	var commentDiv = $("#new-comment-div");
	if(commentDiv.css('display') == "none") {
		if(sessionStorage.getItem('user_id')) {
			commentDiv.slideDown(400);
			commentDiv.find(".comment-name").text(sessionStorage.getItem('username'));
			if(sessionStorage.getItem('avatar')) {
				commentDiv.find(".comment-image").attr('src', generateS3URL(sessionStorage.getItem('avatar') + "s"));
			}
			$("#new-comment-body").focus()
		} else {
			slideUserModal(false, "login");
		}
	} else {
		commentDiv.slideUp();
	}
});
//comment body validations
$("#new-comment-body").blur(function() {
	validateCommentDesc($(this).val());
});
$("#new-comment-body").keyup(function() {
	validateCommentDesc($(this).val());
});
function validateCommentDesc(text) {
	if(text.length > 2500) {
		routeError("Too long!", $("#new-comment-div"), "Max length is 2500 characters");
	} else {
		$("#new-comment-div").find(".form-error").fadeOut();
	}
}
//submit comment form
$("#new-comment-form").submit(function(e) {
	e.preventDefault();
	var commentObj = {"body": $("#new-comment-body").val(), "page": "gym"};
	$.post("ajax/comment/create_comment.php", commentObj, function(resp) {
		if(resp.success) {
			$("#comment-form-errors").html("").hide();
			$("#new-comment-div").slideUp();
			$("#new-comment-body").val("");
			newCommentHTML(resp.id, sessionStorage.getItem('username'), 1, sessionStorage.getItem('avatar'), "just now", resp.body, true, true);
			$("#no-comments").slideUp();
		} else {
			$("#comment-form-errors").append(submitError(resp.error)).show();
		}
	}, 'json');
});
//new comment html
function newCommentHTML(id, name, upvotes, avatar, date, body, upvoted, newComment) {
	//check avatar
	if(avatar) {
		avatar = generateS3URL(avatar) + "s";
	} else avatar = "assets/images/default-avatar.png";
	//check if comment is upvoted by user
	if(upvoted) {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
	} else  {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
	}
	//change comment options depending on whether comment belongs to logged in user
	if(name == sessionStorage.getItem('username')) {
		var commentOptions = '<span class="delete-comment comment-option">delete</span>';
	} else {
		var commentOptions = '<span class="report-comment comment-option activate-report">report</span>'
	}
	var comment = '<div class="spot-comment-div">' +
						'<div class="comment-vote-div">' +
							upvoteIcon + 
							'<div class="upvote-amount basic-text" style="font-size:1.3em;bottom:10px">' + upvotes + '</div>' +
							'<div class="comment-id" style="display:none">' + id + '</div>' +
						'</div>' +
						'<div class="comment-header">' +
							'<span class="comment-pic"><img src="' + avatar + '" class="comment-image"/></span>' +
							'<a href="profile' + name + '" class="comment-name default-link"> ' + name + '</a>' +
							'<span class="comment-time">' + date + '</span>' +
							'<div class="comment-body">' + body + '</div>' +
							'<div class="comment-options">' +
								commentOptions +
								'<div class="report-id" style="display:none">' + id + "</div>" +
								'<div class="report-type" style="display:none">comment</div>' +
								'<div class="report-name" style="display:none">null</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	if(newComment) {
		$("#spot-comments").prepend(comment);
	} else {
		$("#spot-comments").append(comment);
	}
}
//----------------------------------------|
// Comment Events                         |
//----------------------------------------|
//fade in/out comment options
$(document).on("mouseenter", ".spot-comment-div", function() {
	$(this).find(".comment-options").stop().fadeIn("fast");
});
$(document).on("mouseleave", ".spot-comment-div", function() {
	$(this).find(".comment-options").stop().fadeOut("fast");
});
//delete comment
$(document).on("click", ".delete-comment", function() {
	var commentDiv = $(this).parents(".spot-comment-div");
	var commentId = commentDiv.find(".comment-id").text();
	$.post("ajax/comment/delete_comment.php", {"comment_id": commentId}, function(resp) {
		if(resp) {
			commentDiv.fadeOut();
		} else {
			showFlash("You cannot delete a comment that isn't yours!", "danger", "glyphicon-alert");
		}
	})
});
/* ********************************
   --------------------------------
   |       Infinite Loading  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Comments                               |
//----------------------------------------|
//amount of comments db will fetch for each call
var loadCommentLength = 10;
//hide load more button if less than initial db fetch amount
if($(".spot-comment-div").length < loadCommentLength) {
	$("#load-more-comments").hide();
}
$("#load-more-comments").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".spot-comment-div").length;
	//hide load-more button, show loading gif
	$("#load-more-comments").hide();
	$("#load-comments-gif").show()
	$.post("ajax/comment/load_more_gym_comments.php", {"offset":offset}, function(data) {
		$("#load-comments-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				var upvoted = checkIfUpvoted(data[i].id, "comment");
				var displayDate = normalizeDate(new Date(data[i].created_at));
				newCommentHTML(data[i].id, data[i].user_name, data[i].upvotes, data[i].user_avatar, displayDate, data[i].body, upvoted, false);
			}
			//check if all comments have been loaded
			if(data.length == loadCommentLength) {
				$("#load-more-comments").show();
			}
		}
	}, 'json')
});
//turn js date obj into 'Month day year'
function normalizeDate(date) {
	var monthIndex = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October","November", "December"];
	var day = date.getDate();
	var month = monthIndex[date.getMonth()];
	var year = date.getFullYear();
	return month + " " + day + " " + year;
}
//load more div animation 
$(".load-more-div").hover(
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "13px"
		}, 350);
		$(this).find(".big-arrow").stop().animate({
			top:"6px"
		}, 300)
	},
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "3px"
		}, 300);
		$(this).find(".big-arrow").stop().animate({
			top:"0px"
		}, 250)
	}
)
//----------------------------------------|
// Page wide functions                    |
//----------------------------------------|
//get x seconds ago... from date string
function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
</script>
<?php
} else {
	include("views/static_pages/error.php");
}
?>
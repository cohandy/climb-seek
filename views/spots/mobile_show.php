<?php
$pdo = SQLiteDB::getInstance();
if($pdo && isset($_GET['id'])) {
	//grab spot
	$new_spot = new Spot($_GET, $pdo);
	$spot = $new_spot->getSpot($_GET['id']);
	if($spot) {
		//log spot into session for updating purposes
		$_SESSION['spot'] = array('id' => $spot['id'], 'name' => $spot['name'], 'type_climbing' => $spot['type_climbing'], 'top_pic' => $spot['top_pic'], "filter_type" => $spot['filter_type'], 'location' => $spot['location'], 'edit' => $spot['edit'], 'user_id' => $spot['user_id']);
		if(!$spot['upvotes']) {
			$spot['upvotes'] = 0;
		}
		if(isset($_SESSION['user'])) {
			//get user upvotes affiliated with this spot
			$user_upvotes = new Upvote($_GET, $pdo);
			$new_upvotes = $user_upvotes->getUserSpotUpvotes();
			unset($user_upvotes);
			//check if spot liked
			$spot_liked = Upvote::checkIfUpvoted($new_upvotes, $spot['id'], "spot");
		} else {
			$new_upvotes = false;
			$spot_liked = false;
		}
		//get comments with spot_id
		$new_comments = new Comment($_GET, $pdo);
		$comments = $new_comments->fetchSpotComments(0);
		unset($new_comments);
	} else $spot = null;
} else $spot = null;
//if a spot is fetched from db, yield page
if($spot) { 
?>
<!-- start of page -->
<div id="mobile-pic-carousel">
	<span class="glyphicon glyphicon-remove close-fixed-modal" id="close-pic-carousel"></span>
	<!-- pic info div (user card) -->
	<div id="picture-info-div">
		<span class="report-id" style="display:none"></span>
		<span class="report-type" style="display:none">picture</span>
		<div id="picture-info-card">
			<div class="picture-vote-div" title="Upvote picture!">
				<span class="glyphicon glyphicon-menu-up upvote-icon"></span>
				<div id="pic-upvotes" class="upvote-amount basic-text">0</div>
			</div>
			<img src="assets/images/default-avatar.png" id='user-pic-avatar' class="img-circle"/>
			<div id="pic-user-info">
				<a href="#" id="pic-user-name"></a>
				<div id="pic-upload-date"></div>
			</div>
			<div id="picture-feature-name">
				<div class="pic-feature-col" id="pic-feature-route">
					<div class="pic-feature-type bolder-text">Route:</div>
					<div class="basic-text pic-feature-text"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="mobile-pic-holder">
		<img src="#" id="current-carousel-pic" class="mobile-carousel-pic"/>
		<img src="#" id="next-carousel-pic" class="mobile-carousel-pic"/>
		<img src="#" id="last-carousel-pic" class="mobile-carousel-pic"/>
	</div>
	<!-- pic caption -->
	<div id="show-banner-caption">
		<div id="caption-text" class="basic-text"></div>
	</div>
	<span id="report-picture" class="activate-report">
		<span class="basic-text">report</span>
	</span>
	<div id="pic-info-choices">
		<div class="glyphicon glyphicon-trash pic-choice-icon" id="delete-spot-picture" style="color:#ff4d4d" data-toggle="tooltip" data-placement="right" title="Delete Picture"></div>
		<div class="glyphicon glyphicon-edit pic-choice-icon add-caption" id="edit-caption" style="color:#6ba8e1" data-toggle="tooltip" data-placement="right" title="Edit Caption"></div>
	</div>
</div>
<div class="container-fluid" id="main-pic-container">
	<div class="row" id="show-page-header"><!--picture row -->
		<div id="show-spot-main-image">
			<?php 
			if($spot['top_pic']) {
				echo '<img src="' . AWS_URL . "/" . $spot['top_pic'] . '" id="show-spot-image"/>';
			} else {
				echo '<img src="assets/images/extra-image5.jpg" id="show-spot-image"/>';
			}
			?>
		</div>
		<div class="show-spot-banner-main" id="show-spot-banner-name">
			<?php echo sanitize($spot['name']) ?>
			<div id='show-spot-id' style="display:none"><?php echo sanitize($spot['id']) ?></div>
		</div>
		<div class="show-banner-username show-banner-element basic-text" id="spot-founder">
			<?php 
				if($spot['user_name'] != null) {
					echo "Discovered by " . $spot['user_name'];
				} else {
					echo "Discovered by climbandseek";
				}
			?>
		</div>
		<div class="show-banner-username show-banner-element basic-text" id="spot-picture-username" style="display:none"></div>
	</div>
</div>
<div class="container-fluid" style="padding:0">
	<!-- info bar -->
	<div class="row no-gutters" id="mobile-info-bar">
		<div class="col-xs-4">
			<?php if(!$spot_liked) { ?>
				<div class="sliding-nav-button smallish-button" id="like-spot">
					<span class="glyphicon glyphicon-thumbs-up"></span>
					<span class="claim-text">Like!</span>
				</div>
			<?php } else { ?>
				<div class="sliding-nav-button smallish-button" id="like-spot" style="border-left:3px solid #347db2;background:#398bc6;color:white">
					<span class="glyphicon glyphicon-thumbs-up"></span>
					<span class="claim-text">Liked</span>
				</div>
			<?php } // end spot like if ?>
		</div>
		<div class="col-xs-4">
			<div class="sliding-nav-button medium-button activate-upload-modal">
				<span class="glyphicon glyphicon-camera" style="margin-right:5px"></span>
				<span class="upload-id-info" style="display:none"><?php echo $spot['id']; ?></span>
				<span class="upload-type-info" style="display:none">spot</span>
				<span class="upload-name-info" style="display:none"><?php echo $spot['name'] ?></span>
				Add Photo
			</div>
		</div>
		<div class="col-xs-4">
			<a href=<?php echo "https://www.google.com/maps/dir/Current+Location/" . $spot['latitude'] . "," . $spot['longitude'];  ?> class="sliding-nav-button large-button" target="_blank" style="border:none;text-decoration:none"><span class="glyphicon glyphicon-map-marker" style="margin-right:2px;"></span>Directions</a>
		</div>
	</div>
	<div class="row no-gutters">
		<div class="col-xs-6">
			<div class="info-bar-div basic-text" id="info-bar-likes">
				<?php $spot['upvotes'] > 1 || $spot['upvotes'] == 0 ? $claims = " LIKES" : $claims = " LIKE";
				echo sanitize($spot['upvotes']) . $claims;
				?>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="info-bar-div basic-text" id="info-bar-routes">
				<?php $spot['num_routes'] > 1 || $spot['num_routes'] == 0 ? $route_amount = " ROUTES" : $route_amount = " ROUTE";
				echo $spot['num_routes'] . $route_amount;
				?>
			</div>
		</div>
	</div>
</div>
<!-- description spot-info row -->
<div class="container-fluid" id="body-background">
	<div id="edit-spot-errors" class="form-error-area" style="text-align:center"></div>
	<div class="container" style="padding-top:5px;padding-bottom:5px">
		<div class="row">
			<div class="col-md-12">
				<div class="body-header">
					<div class="body-header-text">DESCRIPTION:</div>
					<div class="desc-icons activate-report" id="desc-report">
						<span class="desc-icon-text">Report</span><span class="glyphicon glyphicon-warning-sign desc-icons-icons"></span>
					</div>
					<div class="report-id" style="display:none"><?php echo $spot['id'] ?></div>
					<div class="report-type" style="display:none">spot</div>
					<div class="report-name" style="display:none"><?php echo $spot['name'] ?></div>
					<?php if($spot['edit'] || $spot['user_id'] == $_SESSION['user']['id']) { ?>
						<div class="desc-icons" id="edit-desc">
							<span class="desc-icon-text">Edit</span><span class="glyphicon glyphicon-edit desc-icons-icons"></span>
						</div>
					<?php } // end of edit if ?>
					<div class="desc-icons" id="edit-save" style="display:none">
						<span class="desc-icon-text">Save</span><span class="glyphicon glyphicon-save desc-icons-icons" id="spot-save-icon"></span>
					</div>
					<div class="desc-icons" id="edit-cancel" style="display:none">
						<span class="desc-icon-text">Cancel</span><span class="glyphicon glyphicon-remove desc-icons-icons" id="spot-save-icon"></span>
					</div>
				</div>
				<div class="row" style="margin-top:10px">
					<div class="col-md-1 col-md-offset-1">
						<div class="inside-body-header">OVERVIEW:</div>
					</div>
					<div class="col-md-8 col-md-offset-1" id="desc-column" style="position:relative">
						<span class="inside-body-text" id="spot-info-description"><?php
							if(strlen($spot['description']) <= 0) {
								echo "<em>Been here before? Tell us what you saw.</em>";
							} else {
								echo sanitize(trim($spot['description']));
							}
							?>
						</span>
					</div>
				</div>
				<div class="row" style="margin-top:10px">
					<div class="col-md-1 col-md-offset-1">
						<div class="inside-body-header">TIPS/WARNINGS:</div>
					</div>
					<div class="col-md-8 col-md-offset-1">
						<span class="inside-body-text" id="spot-info-tips"><?php
							if(strlen($spot['tips']) <= 0) {
								echo "<em>Be the first to add a tip!</em>";
							} else {
								echo sanitize(trim($spot['tips']));
							}
							?>
						</span>
					</div>
				</div>
				<div class="row" style="margin-top:10px">
					<div class="col-md-1 col-md-offset-1">
						<div class="inside-body-header">TYPE:</div>
					</div>
					<div class="col-md-8 col-md-offset-1">
						<span class="inside-body-text" id="spot-info-type"><?php
							if($spot['type_climbing']) {
								$types = substr($spot['type_climbing'], 0, strlen($spot['type_climbing']) - 2);
								echo sanitize($types);
							} else {
								echo "<em>Unknown</em>";
							}
							?>	
						</span>
					</div>
				</div>
				<!-- spot edit options -->
				<?php 
				if(isset($_SESSION['user'])) {
					if($_SESSION['user']['id'] == $spot['user_id']) { ?>
				<div id="extra-spot-edits" style="display:none">
					<div class="row" style="margin-top:10px">
						<div class="col-md-6 col-md-offset-1">
							<span class="basic-text" style="font-size:1.2em;letter-spacing:1px"><em>ONLY YOU HAVE ACCESS TO THESE OPTIONS, AS FOUNDER</em></span>
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-1 col-md-offset-1">
							<div class="inside-body-header">NAME:</div>
						</div>
						<div class="col-md-8 col-md-offset-1">
							<pre class="inside-body-text">
								<input type='text' name="name" id="edit-spot-name" placeholder="<?php echo sanitize($spot['name']); ?>" style="background:none;border:none;" />
							</pre>
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-1 col-md-offset-1">
							<div class="inside-body-header">LOCATION:</div>
						</div>
						<div class="col-md-8 col-md-offset-1">
							<pre class="inside-body-text">
								<input type='text' name="location" id="edit-spot-location" placeholder="<?php echo sanitize($spot['location']); ?>" style="background:none;border:none" />
							</pre>
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-1 col-md-offset-1">
							<div class="inside-body-header">EDIT:</div>
						</div>
						<div class="col-md-8 col-md-offset-1" data-type="edit">
							<div class="check-box-div radio-checkbox-div">
							<?php if($spot['edit']) { ?>
								<div class="check-box transition-all" data-checked="true"><span class='glyphicon glyphicon-ok' id='checkmark'></span></div><span class="basic-text radio-text">Allow Public Desc/Tips Edits</span>
							<?php } else { ?>
								<div class="check-box transition-all" data-checked="false"></div><span class="basic-text radio-text">Allow others to edit description/tips</span>
							<?php } //end of edit check if ?>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-1 col-md-offset-1">
							<div class="inside-body-header" id="confirm-check">CONFIRM:</div>
						</div>
						<div class="col-md-8 col-md-offset-1" data-type="confirm">
							<div class="check-box-div radio-checkbox-div">
							<?php if($spot['confirmed']) { ?>
								<div class="check-box transition-all" data-checked="true"><span class='glyphicon glyphicon-ok' id='checkmark'></span></div><span class="basic-text radio-text">Lat/Long Coordinates Confirmed</span>
							<?php } else { ?>
								<div class="check-box transition-all" data-checked="false"></div><span class="basic-text radio-text">Lat/Long Coordinates Confirmed</span>
							<?php } //end of confirm if ?>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				 } //end extra edit options if ?>
			</div>
		</div>
	</div>
</div>
<!-- routes container -->
<div class="container-fluid" style="margin-top:15px" id="routes-container">
	<div class="row">
		<div class="col-lg-12" id="show-spot-routes">
			<div class="body-header">
				<div class="body-header-text">ROUTES:</div>
				<div class="sliding-button large-button comment-button" id="add-route">
					<span class="claim-text" style="margin-right:-23px">Add a Route</span>
					<span class="glyphicon glyphicon-road" style="opacity:0;margin-left:3px"></span>
				</div>
			</div>
			<!-- route form -->
			<div class="container">
				<div id="route-form-div" class="spot-page-form-div">
					<div class="form-error-area" id="route-form-errors" style="text-align:center"></div>
					<form action="#" method="post" id="new-route-form">
						<div class="row some-gutters">
							<div class="col-md-8">
								<div class="new-spot-form-div">
									<span class="new-spot-text">Name</span>
									<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
										<span class="glyphicon glyphicon-exclamation-sign"></span>
										<span class="error-text"></span>
									</span>
									<input type="text" name="name" class="new-spot-input" placeholder="Enter name..." autocomplete="off">
								</div>
							</div>
							<div class="col-md-2">
								<div class="new-spot-form-div">
									<span class="new-spot-text">Difficulty</span>
									<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
										<span class="glyphicon glyphicon-exclamation-sign"></span>
										<span class="error-text"></span>
									</span>
									<input type="text" name="diff" class="new-spot-input" placeholder="Enter difficulty..." autocomplete="off">
								</div>
							</div>
							<div class="col-md-2">
								<div class="new-spot-form-div">
									<span class="new-spot-text">Type</span>
									<div class="custom-select-div new-spot-input"><span id="custom-type-choice">Types</span> <span class="glyphicon glyphicon-menu-down custom-select-icon"></span></div>
									<div class="custom-select-menu">
										<div class="custom-menu-option">Aid</div>
										<div class="custom-menu-option">Alpine</div>
										<div class="custom-menu-option">Bouldering</div>
										<div class="custom-menu-option">Ice</div>
										<div class="custom-menu-option">Sport</div>
										<div class="custom-menu-option">Top Rope</div>
										<div class="custom-menu-option">Trad</div>
										<input type="text" placeholder="Other" id="other-choice" class="basic-text" autocomplete="off">
									</div>
								</div>
							</div>
						</div>
						<div class="row some-gutters">
							<div class="col-md-12">
								<div class="new-spot-form-div">
									<span class="new-spot-text">Description</span>
									<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
										<span class="glyphicon glyphicon-exclamation-sign"></span>
										<span class="error-text"></span>
									</span>
									<textarea rows="3" name="desc" class="new-spot-input" placeholder="Enter description..."></textarea>
								</div>
							</div>
						</div>
						<div class="row some-gutters">
							<div class="new-spot-form-div">
								<input type="submit" class="sliding-button large-button" style="float:right;margin-right:10px;margin-top:-5px;background:white;padding:5px 5px;width:170px;font-size:1.2em" value="SUBMIT" id="route-submit"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- routes go here -->
	<div id="route-area"></div>
	<div id="no-routes" class="spot-page-placeholder-div" style="display:none">
		<div class="basic-text no-comments-text">Missing routes!</div>
		<div class="glyphicon glyphicon-road" style="font-size:5em"></div>
		<div class="basic-text no-comments-text">Add a route and help the community</div>
	</div>
</div>
<!-- load more routes div -->
<div class="load-more-div" id="load-more-routes">
	<div class="basic-text" style="margin-top:10px">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-routes-gif" alt="Loading..."/>
</div>
<!-- start of comments -->
<div class="container-fluid" style="background:#f5f5f5;padding:15px 0;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="body-header">
					<div class="body-header-text">COMMENTS:</div>
					<div class="sliding-button large-button comment-button" id="leave-comment">
						<span class="claim-text" style="margin-right:-18px">Leave a Comment</span>
						<span class="glyphicon glyphicon-pencil" style="opacity:0"></span>
					</div>
				</div>
				<!-- new comment form -->
				<div id="new-comment-div" style="display:none;margin-top:10px;padding-bottom:10px">
					<div class="form-error-area" id="comment-form-errors" style="text-align:center"></div>
					<form method="post" action="#" id="new-comment-form">
						<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
						<span class="comment-name"></span>
						<span class="alert alert-danger form-error" style="float:right;position:relative;top:24px" data-toggle="tooltip">
							<span class="glyphicon glyphicon-exclamation-sign"></span>
							<span class="error-text"></span>
						</span>
						<div style="padding-left:40px">
							<textarea rows="3" type="text" name="body" id="new-comment-body" class="new-spot-input" placeholder="Enter comment..." style="padding:10px 10px 0 10px"></textarea>
						</div>
						<div style="height:50px">
							<input type="submit" class="sliding-button smallish-button" style="float:right;margin-top:5px;padding:5px 5px;width:100px;font-size:1.2em;background:#f5f5f5" value="POST" id="comment-submit"/>
						</div>
					</form>
				</div>
				<!-- comments -->
				<div id="spot-comments">
					<?php if($comments) { 
						foreach($comments as $row) { ?>
						<?php //convert/check data
						!$row['upvotes'] ? $upvotes = "0" : $upvotes = $row['upvotes'];
						//change date to readable format
						$date = date_create($row['created_at']);
						$use_date = date_format($date, "F d Y");
						//check if user has upvoted any comments
						if($upvotes) {
							$check_upvote = Upvote::checkIfUpvoted($new_upvotes, $row['id'], "comment");
							if($check_upvote) {
								$upvoted = true;
							} else $upvoted = false;
						}
						?>
						<div class="spot-comment-div">
							<div class="comment-vote-div">
								<?php if($upvoted) {
									echo '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
								} else echo '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
								?>
								<div class="upvote-amount basic-text" style="font-size:1em;bottom:10px"><?php echo $upvotes; ?></div>
								<div class="comment-id" style="display:none"><?php echo $row['id']; ?></div>
							</div>
							<div class="comment-header">
								<?php if($row['user_avatar']) { ?>
									<span class="comment-pic"><img src=<?php echo AWS_URL . "/" . $row['user_avatar'] . "s" ?> class="comment-image"/></span>
								<?php } else { ?>
									<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
								<?php } //end avatar if ?>
								<a href=<?php echo "profile/" . $row['user_name']; ?> class="comment-name default-link"><?php echo $row['user_name']; ?></a>
								<span class="comment-time"><?php echo $use_date; ?></span>
								<div class="comment-body"><?php echo $row['body']; ?></div>
								<div class="comment-options">
									<?php 
									if(isset($_SESSION['user']) && $_SESSION['user']['id'] == $row['user_id']) {
										echo '<span class="delete-comment comment-option">delete</span>';
									} else {
										echo '<span class="report-comment comment-option activate-report">report</span>';
									}
									?>
									<div class="report-id" style="display:none"><?php echo $row['id']; ?></div>
									<div class="report-type" style="display:none">comment</div>
								</div>
							</div>
						</div>
					<?php } //end of comment loop
					} else { ?>
						<div id="no-comments" class="spot-page-placeholder-div">
							<div class="basic-text no-comments-text">No Comments....yet</div>
							<div class="glyphicon glyphicon-comment" style="font-size:4em"></div>
							<div class="basic-text no-comments-text">Been here? Tell us about your experience</div>
						</div>
					<?php } //end of comment if ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- load more comments div -->
<div class="load-more-div" id="load-more-comments" style="background:#f5f5f5;margin-top:0;border:none">
	<div class="load-more-background"></div>
	<div class="basic-text">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center;background:#f5f5f5;margin-top:0">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-comments-gif" alt="Loading..."/>
</div>
<?php include "views/partials/_mobile_upload_modal.php"; ?>
<?php include "views/partials/_mobile_report_modal.php"; ?>
<script type="text/javascript" id="main-script">
//page wide vars that hold spot info
var thisSpot = <?php echo json_encode($spot); ?>;
var userUpvotes = <?php echo json_encode($new_upvotes); ?>;
//set coords of this spot in sessionStorage, so when user goes back to map they'll be in the same spot
sessionStorage.setItem('lat', thisSpot.latitude);
sessionStorage.setItem('long', thisSpot.longitude);
//set page title with spot name
document.title = thisSpot.name + " | climb&seek";
//initialize tooltips
$("[data-toggle='tooltip']").tooltip();
/* ********************************
   --------------------------------
   |     Page load ajax calls     |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Get pic urls from db                   |
//----------------------------------------|
var spotPics = Array();
$.post("ajax/picture/get_spot_pics.php", {"content_type": "picture"}, function(data) {
	if(data.length > 0) {
		$(".pic-selector").fadeIn();
		spotPics = data;
		//add 'upvoted' col to pics if currently logged in user has upvoted it
		if(userUpvotes.length > 0) {
			for(var i=0;i<spotPics.length;i++) {
				var upvoted = checkIfUpvoted(spotPics[i].public_id, "picture");
				if(upvoted) {
					spotPics[i].upvoted = true;
				}
			}
		}
		//change top pic if first pic in spotPics does not equal current top pic
		if(spotPics[0].public_id !== thisSpot.top_pic) {
			$.post("ajax/spot/update_top_pic.php", {"public_id": spotPics[0].public_id, "type": "spot"});
		}
	} 
}, 'json');
//----------------------------------------|
// Get routes from db                     |
//----------------------------------------|
$.post("ajax/route/get_routes_from_spot.php", {"offset": 0}, function(data) {
	if(data) {
		//hide/show load more routes button
		if(data.length < 8 || parseInt($("#info-bar-routes").text()) == 8) {
			$("#load-more-routes").hide();
		} else {
			$("#load-more-routes").show();
		}
		for(var i=0;i<data.length;i++) {
			newRouteHTML(data[i].id, data[i].name, data[i].top_pic, data[i].user_name, data[i].description, data[i].difficulty, data[i].type_climbing, data[i].upvotes, false);
		}
	} else {
		$("#no-routes").show();
	}
}, 'json');
function newRouteHTML(id, name, topPic, userName, desc, diff, type, upvotes, newRoute) {
	//check vars and replace if null/too long
	if(!desc) {
		desc = "<em>Climbed this route? Enter a description and win 10 climb dollars!<em>";
	}
	if(!userName) {
		userName = "climbandseek";
	}
	if(!upvotes) {
		upvotes = 0;
	}
	//shorten name if too long
	if(name.length > 24) {
		var displayName = name.substring(0, 22) + "...";
	} else var displayName = name;
	//shorten desc if too long, put full version hidden below
	if(desc.length > 120) {
		var displayDesc = desc.substring(0, 120) + "...<span class='default-link more-desc'>more</span>";
		desc = desc + " ...<span class='default-link less-desc'>less</span>";
	} else var displayDesc = desc;
	//check if route has been upvoted alter css if so
	var checkUpvote = checkIfUpvoted(id, "route");
	if(checkUpvote || newRoute) {
		var recommend = '<div class="route-button basic-text upvote-route" style="background:rgba(0, 0, 0, 0.8)">Recommended</div>'
	} else var recommend = '<div class="route-button basic-text upvote-route">Recommend</div>';
	//set picture
	if(topPic) {
		var picture = generateS3URL(topPic) + "s";
		var carousel = '<div class="route-carousel-div transition-all">' +
							'<span class="route-carousel-selector glyphicon glyphicon-chevron-left transition-all" id= "route-carousel-left" style="left:3px"></span>' +
							'<span class="route-carousel-selector glyphicon glyphicon-chevron-right transition-all" id= "route-carousel-right" style="right:3px"></span>' +
						'</div>';
	} else {
		var picture = "assets/images/route-default.jpg";
		var carousel = "";
	}
	//get unique color depending on type
	var iconColor = typeColor(type);
	var route = '<div class="route-spot-div">' +
					'<div class="route-id" style="display:none">' + id + '</div>' +
					'<div class="route-name bolder-text">' + displayName + '</div>' +
					'<div class="route-user basic-text">Discovered by ' + userName + '</div>' +
					'<div class="route-pic-holder">' +
						carousel +
						'<img src="' + picture + '" class="route-pic" id="' + topPic + '"/>' +
						'<div class="row no-gutters route-interactions">' +
							'<div class="col-xs-4">' +
								recommend +
							'</div>' +
							'<div class="col-xs-4">' +
								'<div class="route-button basic-text activate-upload-modal">' + 
									'<span class="upload-id-info" style="display:none">' + id + '</span>' +
									'<span class="upload-type-info" style="display:none">route</span>' +
									'<span class="upload-name-info" style="display:none">' + name + '</span>' +
									'Add Photo' +
								'</div>' +
							'</div>' +
							'<div class="col-xs-4">' +
								'<div class="route-button basic-text activate-report" style="border-right:none;">Report</div>' +
								'<div class="report-id" style="display:none">' + id + '</div>' +
								'<div class="report-type" style="display:none">route</div>' +
								'<div class="report-name" style="display:none">' + name + '</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="row no-gutters route-info-squares">' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-stats info-square-icon"></span>' +
							'<div class="route-square-text basic-text">' + diff + '</div>' +
						'</div>' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-link info-square-icon" style="opacity:0.3;color:' + iconColor + '"></span>' +
							'<div class="route-square-text basic-text">' + type + '</div>' +
						'</div>' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-thumbs-up info-square-icon"></span>' +
							'<div class="route-square-text basic-text route-upvotes">' + upvotes + '</div>' +
						'</div>' +
					'</div>' +
					'<div class="route-description basic-text">' + displayDesc + '</div>' +
					'<div class="route-description basic-text long-desc" style="display:none">' + desc + '</div>' +
				 '</div>';
	if(newRoute) {
		$("#route-area").prepend(route);
	} else {
		$("#route-area").append(route);
	}
}
//check if user has upvoted content
function checkIfUpvoted(contentId, contentType) {
	if(userUpvotes.length > 0) {
		for(var i=0;i<userUpvotes.length;i++) {
			if(userUpvotes[i].content_type == contentType) {
				if(userUpvotes[i].content_id == contentId) {
					userUpvotes.splice(i, 1);
					return true;
				}
			}
		}
		return false;
	} else return false;
}
/* ********************************
   --------------------------------
   |   Route/Comment Animations   |
   ________________________________
   ********************************
*/
//shows rest of description
$(document).on("click", ".more-desc", function() {
	$(this).parent().hide();
	$(this).parent().next().show();
});
$(document).on("click", ".less-desc", function() {
	$(this).parent().hide();
	$(this).parent().prev().show();
});
function typeColor(type) {
	switch(type) {
		case "Bouldering":
			return "#4ad153";
			break;
		case "Top Rope":
			return "#ff5555";
			break;
		case "Sport":
			return "#398bc6";
			break;
		case "Trad":
			return "#398bc6";
			break;
		case "Alpine":
			return "#cf7dfd";
			break;
		case "Ice":
			return "#cf7dfd";
			break;
		case "Sport/Trad":
			return "#398bc6";
			break;
		default:
			return "rgba(0, 0, 0, 0.2)";
			break;
	}
}
/* ********************************
   --------------------------------
   |     Info Bar Div Clicks 	  |
   ________________________________
   ********************************
*/
$(".info-bar-div").click(function() {
	var thisId = $(this).attr('id');
	var divOffset = {"top": 0};
	switch(thisId) {
		case "info-bar-routes":
			divOffset = $("#routes-container").offset();
			break;
		default:
			divOffset = {"top": 0};
			break;
	}
	if(!divOffset) {
		divOffset = {"top": 0};
	}
	$("html, body").animate({
		scrollTop: (divOffset.top - 110)
	})
});
/* ********************************
   --------------------------------
   |       Main Pic Carousel  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
//          Carousel Functions            |
//----------------------------------------|
//get next point in array of spotPics
function nextPic(picArr, currentImage, direction) {
	//loop through spotPics and find position of currentImage
	var place = findPublicIdIndex(picArr, currentImage);
	if(direction == "pic-right" || direction == "route-carousel-right") {
		//if place + 1 will be non-existant point, return to beginning
		if((place + 1) > (picArr.length - 1)) {
			return 0;
		} else return place + 1;
	} else {
		//if place - 1 is negative, go to end of array
		if((place - 1) < 0) {
			return picArr.length - 1;
		} else return place - 1;
	}
}
//change pic info on new pic
function updatePicInfo(point) {
	//show pic options if its the users pic
	if(spotPics[point].user_name == sessionStorage.getItem("username")) {
		$("#pic-info-choices").show();
	} else $("#pic-info-choices").hide();
	//replace caption if any
	if(spotPics[point].caption) {
		$("#show-banner-caption").show();
		$("#caption-text").text(spotPics[point].caption);
	} else if(spotPics[point].user_name == sessionStorage.getItem('username')) {
		$("#show-banner-caption").show();
		$("#caption-text").html("<div class='regular-button add-caption'>ADD CAPTION</div>");
	} else $("#show-banner-caption").hide();
	//set height of show banner caption so text is right under pic
	var offset = $("#current-carousel-pic").offset();
	$("#show-banner-caption").css("height", offset.top + "px");
	//replace name
	$("#pic-user-name").text(spotPics[point].user_name);
	//replace avatar
	if(spotPics[point].user_avatar != null) {
		$("#user-pic-avatar").attr('src', generateS3URL(spotPics[point].user_avatar));
	} else {
		$("#user-pic-avatar").attr('src', "assets/images/default-avatar.png");
	}
	//upload date
	$("#pic-upload-date").text("Uploaded " + timeSince(new Date(spotPics[point].js_date)) + " ago");
	if(spotPics[point].upvotes) {
		$("#pic-upvotes").text(spotPics[point].upvotes);
	} else {
		$("#pic-upvotes").text("0");
	}
	//if upvoted
	if(spotPics[point].upvoted) {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#347db2");
	} else {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#e9e9e9");
	}
	//if pic belongs to a route
	if(spotPics[point].route_id) {
		$("#pic-feature-route").css('opacity', "1");
		$("#pic-feature-route").find(".pic-feature-text").html(spotPics[point].feature_name);
	} else {
		$("#pic-feature-route").css('opacity', "0");
	}
	$("#pic-user-name").attr("href", "profile/" + spotPics[point].user_name);
	//add public id to report
	$("#picture-info-div").find(".report-id").text(spotPics[point].public_id);
};
//loop through spotPics and find position of public id
function findPublicIdIndex(picArr, publicId) {
	for(var i=0;i<picArr.length;i++) {
		if(picArr[i].public_id == publicId) {
			var place = i;
			break;
		}
	}
	return place;
}
function generateS3URL(publicId) {
	return S3URL + "/" + publicId;
}
//----------------------------------------|
// Mobile Carousel                        |
//----------------------------------------|
$("#main-pic-container").click(function() {
	if(spotPics.length > 0) {
		beginCarousel();
	}
});
function beginCarousel() {
	//set first pic in array as current
	$("#current-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id)).show();
	//set next pic as second in array
	if(spotPics[1] !== undefined) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id))
	}
	//set last pic as last entry in array
	var lastPoint = spotPics.length - 1;
	if(spotPics[lastPoint] !== undefined) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[lastPoint].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	$("#mobile-pic-carousel").show();
	updatePicInfo(0);
}
//stores last beginning touch
var lastTouch = null;
//will be true when next pics are still being decided during animation
var loadingPics = false;
//records start of touch event
$(".mobile-carousel-pic").on('touchstart', function(e) {
	e.preventDefault();
	var touch = e.touches[0];
	lastTouch = touch.pageX;
});
//records end of touch event
$(".mobile-carousel-pic").on('touchend', function(e) {
	e.preventDefault();
	var touch = e.changedTouches[0];
	var touchDiff = lastTouch - touch.pageX;
	//if the diff between first and last touch is negative user wants to go left and vice versa
	if(touchDiff < 0) {
		slideNextPic("left");
	} else {
		slideNextPic("right");
	}
});
function slideNextPic(direction) {
	if(!loadingPics) {
		loadingPics = true;
		var winWidth = $(window).width() + 10;
		if(direction == "right") {
			$("#next-carousel-pic").css("left", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				right: winWidth + "px"
			}, 250);
			$("#next-carousel-pic").animate({
				left: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
			var newCurrentPic = "next";
		} else {
			$("#last-carousel-pic").css("right", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				left: winWidth + "px"
			}, 250);
			$("#last-carousel-pic").animate({
				right: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
			var newCurrentPic = "last";
		}
	}
}
function setUpNextPic(newCurrentPic) {
	//these vars hold the current status of the ids before swap
	var currentPic = $("#current-carousel-pic");
	var nextPic = $("#next-carousel-pic");
	var lastPic = $("#last-carousel-pic");
	//swap pic ids according to new position
	if(newCurrentPic == "next") {
		currentPic.attr('id', "next-carousel-pic");
		nextPic.attr('id', "current-carousel-pic");
	} else {
		currentPic.attr('id', 'last-carousel-pic');
		lastPic.attr('id', 'current-carousel-pic');
	}
	//remove attr gained from previous slide, using newly swapped ids
	$("#current-carousel-pic").css({
		right: "",
		left: ""
	});
	$("#next-carousel-pic").removeAttr('style');
	$("#last-carousel-pic").removeAttr('style');
	//get public id from new current pic and find its index in spotPics
	var currentPublicId = $("#current-carousel-pic").attr('src').split("/").pop();
	var currentIndex = findPublicIdIndex(spotPics, currentPublicId);
	//update pic card info
	updatePicInfo(currentIndex);
	//going from currentIndex, find out the pic in front and behind it
	if((currentIndex + 1) <= spotPics.length - 1) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex + 1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	if((currentIndex - 1) >= 0) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex - 1].public_id));
	} else {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[spotPics.length - 1].public_id));
	}
	loadingPics = false;
}
//close pic carousel
$("#close-pic-carousel").click(function() {
	$("#mobile-pic-carousel").fadeOut();
});

//set width for pic holder for margin auto to work
$("#mobile-pic-holder").css("width", $(window).width() + "px");
$(window).resize(function() {
	$("#mobile-pic-holder").css("width", $(window).width() + "px");
});
//----------------------------------------|
// Edit/Delete Pic                        |
//----------------------------------------|
//add caption
$(document).on("click", ".add-caption", function() {
	//check if editing or adding caption
	if($("#caption-text").text() != "ADD CAPTION") {
		var oldCaption = $("#caption-text").text();
	} else var oldCaption = "";
	//new textarea + save button
	var captionInput = "<textarea type='text' rows='1' class='add-caption-textarea basic-text' name='caption' placeholder='Enter caption...'>" + oldCaption + "</textarea>" + "<span class='regular-button' id='save-caption'>Save</span>";
	//replace old button or caption with textarea, focus
	$("#caption-text").html(captionInput);
	$(".add-caption-textarea").focus();
});
//on caption save
$(document).on("click", "#save-caption", function() {
	var capObj = {"caption": $(".add-caption-textarea").val(), "public_id": $("#current-carousel-pic").attr('src').split("/").pop()};
	$.post("ajax/picture/add_caption.php", capObj, function(resp) {
		if(resp.success) {
			$("#caption-text").html(capObj.caption);
		} else {
			showFlash(resp.error, "danger", "alert");
		}
	}, 'json');
});
//add caption textarea css
$(document).on("focus", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0.5)");
});
$(document).on("blur", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0)");
});
//delete existing picture
$("#delete-spot-picture").click(function() {
	if(sessionStorage.getItem("username") == $("#pic-user-name").text()) {
		var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
		$.post("ajax/picture/delete_picture.php", {"public_id": publicId, "options": "both", "type": "spot"}, function(resp) {
			if(resp) {
				var picIndex = findPublicIdIndex(publicId);
				spotPics.splice(picIndex, 1);
				$("#spot-banner-close").click();
			} else {
				showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
				window.location.replace("index.php");
			}
		});
	} else {
		showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
		window.location.replace("index.php");
	}
});
/* ********************************
   --------------------------------
   |      Route Pic Carousel  	  |
   ________________________________
   ********************************
*/
var routePics = Array();
function swapRoutePic($this) {
	if(spotPics.length > 0) {
		var routeDiv = $this.parents(".route-spot-div");
		var currentRoutePic = routeDiv.find(".route-pic").attr('id');
		findRoutePics(routeDiv, $this);
		var picIndex = nextPic(routePics, currentRoutePic, $this.attr('id'));
		routeDiv.find('.route-pic').attr('src', generateS3URL(routePics[picIndex].public_id) + 's').attr('id', routePics[picIndex].public_id);
	}
}
function findRoutePics(routeDiv, $this) {
	var routeDiv = $this.parents(".route-spot-div");
	var routeId = routeDiv.find(".upload-id-info").text();
	if(routePics.length == 0 || routePics[0].route_id != routeId) {
		routePics = Array();
		for(var i=0;i<spotPics.length;i++) {
			if(spotPics[i].route_id == routeId) {
				routePics.push(spotPics[i]);
			}
		}
	}
}
//in route carousel, show current image in main carousel
$(document).on("click", ".see-full-image", function() {
	var routeDiv = $(this).parents(".route-spot-div");
	var smallImage = routeDiv.find('.route-pic').attr('id');
	var place = findPublicIdIndex(spotPics, smallImage);
	$("#current-carousel-pic").attr("src", generateS3URL(spotPics[place].public_id)).attr('id', spotPics[place].public_id);
	if($("#show-spot-main-image").css('display') !== "none") {
		transformPicHolder();
	}
	updatePicInfo(place);
	$(".spot-carousel-image").show();
	//scroll to top
	$("html, body").animate({
		scrollTop: "0"
	}, 300);
});
$(document).on('touchend', '.route-carousel-div', function(e) {
	e.preventDefault();
	swapRoutePic($(this));
});
/* ********************************
   --------------------------------
   |          Upvotes    	      |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Picture/Comment upvotes                |
//----------------------------------------|
//upvote picture
$(".picture-vote-div").click(function() {
	//grab currently displaying image, split to get public id
	var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
	upvoteCommentPicture($(this), publicId, "picture");
});
//upvote comment
$(document).on("click", ".comment-vote-div", function() {
	var commentId = $(this).find(".comment-id").text();
	upvoteCommentPicture($(this), commentId, "comment");
});
//handles comments/picture upvotes
function upvoteCommentPicture($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		var upObj =  {"content_id": contentId, "content_type": contentType}
		$.post("ajax/upvote/upvote_content.php", upObj, function(resp) {
			if(resp.success) {
				//if resp.action == upvote add one to existing num, if not minus 1
				var upvoteDisplay = $this.find(".upvote-amount");
				var currentUpvotes = parseInt(upvoteDisplay.text());
				var upIcon = $this.find(".glyphicon-menu-up");
				if(resp.action == "upvote") {
					upvoteDisplay.text(currentUpvotes + 1);
					upIcon.css("color", "#347db2");
				} else {
					upvoteDisplay.text(currentUpvotes - 1);
					upIcon.css("color", "#e9e9e9");
				}
				//update spot pics
				if(contentType == "picture") {
					var picIndex = findPublicIdIndex(contentId);
					spotPics[picIndex].upvoted = true;
					spotPics[picIndex].upvotes = $("#pic-upvotes").text();
				}
			} 
		}, 'json');
	} else showFlash("You must be logged in to upvote!", "danger", "icon");
}
//----------------------------------------|
// Spot/Route upvotes                     |
//----------------------------------------|
//like spot
$("#like-spot").click(function() {
	var spotId = thisSpot.id;
	upvoteRouteSpot($(this), spotId, "spot");
});
$(document).on("click", ".upvote-route", function() {
	var routeId = $(this).parents(".route-interactions").find(".upload-id-info").text();
	upvoteRouteSpot($(this), routeId, "route");
});
//handles route/spot upvote
function upvoteRouteSpot($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		$.post("ajax/upvote/upvote_content.php", {"content_id": contentId, "content_type": contentType}, function(resp) {
			if(resp.success) {
				if(contentType == "spot") {
					likeSpot(resp.action);
				} else {
					upvoteRoute($this, resp.action);
				}
			}
		}, 'json');
	} else showFlash("You must be logged in to upvote!", "danger", "icon");
}
function likeSpot(action) {
	if(action == "upvote") {
		var currentLikes = parseInt($("#info-bar-claims").text()) + 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "#398bc6",
			color: "white"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKED");
	} else {
		var currentLikes = parseInt($("#info-bar-claims").text()) - 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "white",
			color: "#398bc6"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKE!");
	}
}
function upvoteRoute($this, action) {
	if(action == "upvote") {
		$this.text("Recommended")
		$this.css({
			background: "rgba(0, 0, 0, 0.8)"
		});
		upvoteDiv = $this.parents(".route-spot-div").find(".route-upvotes")
		var currentUpvotes = parseInt(upvoteDiv.text()) + 1;
		upvoteDiv.text(currentUpvotes);
	} else {
		$this.text("Recommend")
		$this.css({
			background: "rgba(0, 0, 0, 0.5)"
		});
		upvoteDiv = $this.parents(".route-spot-div").find(".route-upvotes")
		var currentUpvotes = parseInt(upvoteDiv.text()) - 1;
		upvoteDiv.text(currentUpvotes);
	}
}
/* ********************************
   --------------------------------
   |         New Route   	      |
   ________________________________
   ********************************
*/
//slide out route form
$("#add-route").click(function() {
	if($("#route-form-div").css('display') == "none") {
		if(sessionStorage.getItem('user_id')) {
			$("#route-form-div").slideDown(400, function() {
				$("#no-routes").remove();
			});
		} else {
			showFlash("You must be logged in to add a route!", "danger", "icon");
		}
	} else {
		$("#route-form-div").slideUp();
	}
})
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
//select div click
$(".custom-select-div").click(function() {
	if($(this).next().css("display") == "none") {
		$(this).css("border", "2px solid #347db2");
		$(this).next().slideDown();
		$(this).parent().find(".custom-select-icon").removeClass().addClass("custom-select-icon glyphicon glyphicon-menu-up").css("color", "#347db2");
	} else {
		resetSelect($(this));
	}
});
//select option click
$(".custom-menu-option").click(function() {
	var parentDiv = $(this).parents(".new-spot-form-div")
	var newOption = $(this).html();
	if(newOption.length > 0) {
		$("#custom-type-choice").html(newOption);
	} else $("#custom-type-choice").html("Types");
	resetSelect(parentDiv.find(".custom-select-div"));
	//validate diff from new choice
	var diffInput = $("#new-route-form input[name='diff']")
	if(diffInput.val().length > 0) {
		validateDiff(diffInput);
	}
});
//for input inside of custom select menu, handles resetting select div on blur/enter press
$("#other-choice").blur(function() {
	var text = $(this).val();
	if(text.length > 0) {
		$("#custom-type-choice").html(text);
	} else $("#custom-type-choice").html("Types");
	resetSelect($(this).parents(".new-spot-form-div").find(".custom-select-div"));
});
$("#other-choice").keydown(function(e) {
	var newOption = $("#custom-type-choice").html();
	if(e.keyCode == 13) {
		e.preventDefault();
		var text = $(this).val();
		if(text.length > 0) {
			$("#custom-type-choice").html(text);
		} else $("#custom-type-choice").html("Types");
		resetSelect($(this).parents(".new-spot-form-div").find(".custom-select-div"));
	}
});
//reset select div
function resetSelect($this) {
	$this.next().slideUp();
	$this.css("border", "2px solid rgba(0, 0, 0, 0.2)");
	$this.parent().find(".custom-select-icon").removeClass().addClass("custom-select-icon glyphicon glyphicon-menu-down").css("color", "rgba(0, 0, 0, 0.6)");
}
//submit new route
$("#new-route-form").submit(function(e) {
	e.preventDefault();
	var formData = $(this).serialize();
	formData += "&type=" + $("#custom-type-choice").html();
	$("#route-submit").hide();
	$("#route-submit").parent().append('<img src="assets/loaders/ripple.gif" style="height:60px;width:60px;float:right" id="route-loader"/>');
	$.post("ajax/spot/create_route.php", formData, function(resp) {
		$("#route-loader").remove();
		$("#route-submit").show()
		if(resp.length == undefined) {
			if(!resp.success) {
				$("#route-form-errors").html("").show();
					$("#route-form-errors").append(submitError(resp.error));
			} else {
				$("#route-form-div").slideUp();
				$("#route-form-errors").hide();
				//add new route to route list
				newRouteHTML(resp.id, resp.name, false, sessionStorage.getItem('username'), resp.description, resp.difficulty, resp.type_climbing, 1, true);
			}
		} else {
			$("#route-form-errors").html("").show();
				for(var i=0;i<resp.length;i++) {
					$("#route-form-errors").append(submitError(resp[i].error));
				}
		}
	}, 'json')
});
function submitError(error) {
	return "<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + error + "</div>";
}
//----------------------------------------|
// Validations                            |
//----------------------------------------|
//name
$("#new-route-form input[name='name']").keyup(function() {
	if($(this).val().length >= 3) {
		validateName($(this));
	}
});
$("#new-route-form input[name='name']").blur(function() {
	validateName($(this));
});
//description
$("#new-route-form textarea[name='desc']").blur(function() {
	if($(this).val().length > 1000) {
		routeError("Too long!", $(this).parent(), "Max length is 1000 characters");
	} else {
		$(this).parent().find(".form-error").fadeOut();
	}
});
//difficulty
$("#new-route-form input[name='diff']").blur(function() {
	validateDiff($(this));
});
$("#new-route-form input[name='diff']").keyup(function() {
	var text = $(this).val();
	if($("#custom-type-choice").html() == "Bouldering") {
		if(text.length > 1) {
			validateDiff($(this));
		}
	} else {
		if(text.length > 2) {
			validateDiff($(this));
		}
	}
});
function validateName($this) {
	var parent = $this.parent();
	var text = $this.val();
	if(text.length < 3) {
		routeError("Too short!", parent, "Min length is 3 characters")
	} else if(text.length > 30) {
		routeError("Too long!", parent, "Max length is 30 characters")
	} else {
		parent.find(".form-error").fadeOut();
	}
}
function validateDiff(element) {
	var parent = element.parent();
	var typeChoice = $("#custom-type-choice").html();
	var diff = element.val();
	//don't do anything if no types chosen yet
	if(typeChoice !== "Types") {
		//check bouldering scale
		if(typeChoice == "Bouldering") {
			var scaleCheck = diff.substring(0, 1);
			if(scaleCheck.toLowerCase() !== "v") {
				routeError("", parent, "Use V scale");
			} else {
				var diffSplit = diff.toLowerCase().split("v");
				if(parseInt(diffSplit[1]) < 17 || diffSplit[1].toLowerCase() == "b") {
					parent.find(".form-error").fadeOut();
				} else routeError("", parent, "V scale max is 17");
			}
		//check ropes scale
		} else {
			var scaleCheck = diff.substring(0, 1);
			if(scaleCheck !== "5") {
				routeError("", parent, "Use YDS scale ex. '5.11a'");
			} else {
				var pass = /^5\.[0-1][0-5]($|[a-d])$|^5\.[0-9]$/i.test(diff);
				if(!pass) {
					routeError("", parent, "Incorrect rank, must conform to YDS class 5 scale");
				} else parent.find(".form-error").fadeOut();
			}
		}
	}
}
//inline validation error
function routeError(message, element, tip) {
	var formError = element.find(".form-error")
	element.find(".error-text").text(message);
	formError.show();
	formError.attr("data-original-title", tip);
}
/* ********************************
   --------------------------------
   |         New Comment   	      |
   ________________________________
   ********************************
*/
//slide down new comment form
$("#leave-comment").click(function() {
	var commentDiv = $("#new-comment-div");
	if(commentDiv.css('display') == "none") {
		if(sessionStorage.getItem('user_id')) {
			commentDiv.slideDown(400);
			commentDiv.find(".comment-name").text(sessionStorage.getItem('username'));
			if(sessionStorage.getItem('avatar')) {
				commentDiv.find(".comment-image").attr('src', generateS3URL(sessionStorage.getItem('avatar') + "s"));
			}
			$("#new-comment-body").focus()
		} else {
			showFlash("You must be logged in to leave a comment!", "danger", "icon");
		}
	} else {
		commentDiv.slideUp();
	}
});
//comment body validations
$("#new-comment-body").blur(function() {
	validateCommentDesc($(this).val());
});
$("#new-comment-body").keyup(function() {
	validateCommentDesc($(this).val());
});
function validateCommentDesc(text) {
	if(text.length > 2500) {
		routeError("Too long!", $("#new-comment-div"), "Max length is 2500 characters");
	} else {
		$("#new-comment-div").find(".form-error").fadeOut();
	}
}
//submit comment form
$("#new-comment-form").submit(function(e) {
	e.preventDefault();
	var commentObj = {"body": $("#new-comment-body").val(), "page": "spot"};
	$.post("ajax/comment/create_comment.php", commentObj, function(resp) {
		if(resp.success) {
			$("#comment-form-errors").html("").hide();
			$("#new-comment-div").slideUp();
			$("#new-comment-body").val("");
			newCommentHTML(resp.id, sessionStorage.getItem('username'), 1, sessionStorage.getItem('avatar'), "just now", resp.body, true, true);
			$("#no-comments").slideUp();
		} else {
			$("#comment-form-errors").append(submitError(resp.error)).show();
		}
	}, 'json');
});
//new comment html
function newCommentHTML(id, name, upvotes, avatar, date, body, upvoted, newComment) {
	//check avatar
	if(avatar) {
		avatar = generateS3URL(avatar) + "s";
	} else avatar = "assets/images/default-avatar.png";
	//check if comment is upvoted by user
	if(upvoted) {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
	} else  {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
	}
	//change comment options depending on whether comment belongs to logged in user
	if(name == sessionStorage.getItem('username')) {
		var commentOptions = '<span class="delete-comment comment-option">delete</span>';
	} else {
		var commentOptions = '<span class="report-comment comment-option activate report">report</span>'
	}
	var comment = '<div class="spot-comment-div">' +
						'<div class="comment-vote-div">' +
							upvoteIcon + 
							'<div class="upvote-amount basic-text" style="font-size:1.3em;bottom:10px">' + upvotes + '</div>' +
							'<div class="comment-id" style="display:none">' + id + '</div>' +
						'</div>' +
						'<div class="comment-header">' +
							'<span class="comment-pic"><img src="' + avatar + '" class="comment-image"/></span>' +
							'<a href="profile/' + name + '" class="comment-name default-link"> ' + name + '</a>' +
							'<span class="comment-time">' + date + '</span>' +
							'<div class="comment-body">' + body + '</div>' +
							'<div class="comment-options">' +
								commentOptions +
								'<div class="report-id" style="display:none">' + id + "</div>" +
								'<div class="report-type" style="display:none">comment</div>' +
								'<div class="report-name" style="display:none">null</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	if(newComment) {
		$("#spot-comments").prepend(comment);
	} else {
		$("#spot-comments").append(comment);
	}
}
//----------------------------------------|
// Comment Events                         |
//----------------------------------------|
//delete comment
$(document).on("click", ".delete-comment", function() {
	var commentDiv = $(this).parents(".spot-comment-div");
	var commentId = commentDiv.find(".comment-id").text();
	$.post("ajax/comment/delete_comment.php", {"comment_id": commentId}, function(resp) {
		if(resp) {
			commentDiv.fadeOut();
		} else {
			showFlash("You cannot delete a comment that isn't yours!", "danger", "glyphicon-alert");
		}
	})
});
/* ********************************
   --------------------------------
   |       Infinite Loading  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Comments                               |
//----------------------------------------|
//amount of comments db will fetch for each call
var loadCommentLength = 10;
//hide load more button if less than initial db fetch amount
if($(".spot-comment-div").length < loadCommentLength) {
	$("#load-more-comments").hide();
}
$("#load-more-comments").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".spot-comment-div").length;
	//hide load-more button, show loading gif
	$("#load-more-comments").hide();
	$("#load-comments-gif").show()
	$.post("ajax/comment/load_more_comments.php", {"offset":offset}, function(data) {
		$("#load-comments-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				var upvoted = checkIfUpvoted(data[i].id, "comment");
				var displayDate = normalizeDate(new Date(data[i].created_at));
				newCommentHTML(data[i].id, data[i].user_name, data[i].upvotes, data[i].user_avatar, displayDate, data[i].body, upvoted, false);
			}
			//check if all comments have been loaded
			if(data.length == loadCommentLength) {
				$("#load-more-comments").show();
			}
		}
	}, 'json')
});
//turn js date obj into 'Month day year'
function normalizeDate(date) {
	var monthIndex = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October","November", "December"];
	var day = date.getDate();
	var month = monthIndex[date.getMonth()];
	var year = date.getFullYear();
	return month + " " + day + " " + year;
}
//----------------------------------------|
// Routes                                 |
//----------------------------------------|
var loadRouteLength = 8;
if($(".route-spot-div").length < loadRouteLength) {
	$("#load-more-routes").hide();
}
$("#load-more-routes").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".route-spot-div").length;
	//hide load-more button, show loading gif
	$("#load-more-routes").hide();
	$("#load-routes-gif").show()
	$.post("ajax/route/get_routes_from_spot.php", {"offset":offset}, function(data) {
		$("#load-routes-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				newRouteHTML(data[i].id, data[i].name, data[i].top_pic, data[i].user_name, data[i].description, data[i].difficulty, data[i].type_climbing, data[i].upvotes, false);
			}
			//check if all comments have been loaded
			if(data.length == loadRouteLength) {
				$("#load-more-routes").show();
			}
		}
	}, 'json')
});
//load more div animation 
$(".load-more-div").hover(
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "13px"
		}, 350);
		$(this).find(".big-arrow").stop().animate({
			top:"6px"
		}, 300)
	},
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "3px"
		}, 300);
		$(this).find(".big-arrow").stop().animate({
			top:"0px"
		}, 250)
	}
)
/* ********************************
   --------------------------------
   |          Desc Edit  	      |
   ________________________________
   ********************************
*/
//----------------------------------------|
// On Edit Click                          |
//----------------------------------------|
//Make desc editable
$("#edit-desc").click(function() {
	if(sessionStorage.getItem('username')) {
		editInfo($("#spot-info-description"), "new-desc-form");
		editInfo($("#spot-info-tips"), "new-tip-form");
		$("#new-desc-form").focus();
		//hide edit/show save
		$(this).hide();
		$("#desc-report").hide();
		$("#edit-save").show();
		$("#edit-cancel").show();
		//show extra spots edits (only in DOM if user is same as spot user_id)
		$("#extra-spot-edits").show();
	} else {
		showFlash("You must be logged in to edit this spot!", "danger", "icon");
	}
});
//----------------------------------------|
// On Save Click                          |
//----------------------------------------|
$("#edit-save").click(function() {
	//collect forms, create new object and pass to ajax
	var overview = $("#new-desc-form").val();
	var tips = $("#new-tip-form").val();
	var spotName = $("#edit-spot-name").val();
	var spotLocation = $("#edit-spot-location").val();
	var editObj = {"desc": overview, "tips": tips, "name": spotName, "location": spotLocation, "confirmed": confirmChoice, "edit": editChoice};
	var saveIcon = $(this).find(".desc-icon-text");
	saveIcon.html("Saving...");
	$.post("ajax/spot/edit_info.php", editObj, function(resp) {
		//if resp.length is undefined that means an array of errors was returned
		if(resp.length == undefined) {
			if(!resp.success) {
				$("#edit-spot-errors").html("").show();
				$("#edit-spot-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
			} else {
				window.location.reload();
			}
		} else {
			$("#edit-spot-errors").html("").show();
			for(var i=0;i<resp.length;i++) {
				$("#edit-spot-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
			}
		}
	}, 'json');
});
//on cancel edit, just refresh page
$("#edit-cancel").click(function() {
	window.location.reload();
})
//----------------------------------------|
// Edit functions                         |
//----------------------------------------|
//make specific column editable, add in textarea 
function editInfo(element, id) {
	//get height of desc before edit
	var editHeight = element.height();
	//replace desc with textarea, put og text in form
	var newForm = "<textarea name='body' id='" + id + "' class='spot-edit-form' wrap='hard'>" + element.text().trim() + "</textarea>";
	element.html(newForm);
	//give new textarea height of desc, and full width of column
	$("#" + id).css({
		width: $("#desc-column").width() + "px",
		height: (editHeight + 20) + "px"
	});
}
//----------------------------------------|
// Textarea style functions               |
//----------------------------------------|
//dynamically increase the height of the textarea depending on text
$(document).on("keyup", ".spot-edit-form", function() {
	$(this).css("height", Math.max(50, this.scrollHeight) + "px");
});
//----------------------------------------|
// Page wide functions                    |
//----------------------------------------|
//get x seconds ago... from date string
function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}
//----------------------------------------|
//           Custom checkbox      	      |
//----------------------------------------|
//convert php bools to js strings (will be converted back to reg bools in php)
if(thisSpot.edit) {
	var editChoice = "true";
} else {
	var editChoice = "false";
}
if(thisSpot.confirmed) {
	var confirmChoice = "true";
} else {
	var confirmChoice = "false";
}
$(".check-box-div").hover(
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.9)");
		$(this).find(".check-box").css("border", "2px solid rgba(0, 0, 0, 0.9)");
	},
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.6)");
		$(this).find(".check-box").css("border", "2px solid rgba(0, 0, 0, 0.3)");
	}
);
$(".check-box-div").click(function() {
	var checked;
	var checkBox = $(this).find(".check-box");
	if(checkBox.attr('data-checked') == "true") {
		$(this).find(".check-box").html("");
		checked = "false";
		//change data-checked
		checkBox.attr('data-checked', "false");
	} else {
		checkBox.html("<span class='glyphicon glyphicon-ok' id='checkmark'></span>");
		checked = "true";
		//change data-checked
		checkBox.attr('data-checked', "true");
	}
	if($(this).parent().attr('data-type') == "edit") {
		editChoice = checked;
	} else {
		confirmChoice = checked
	}
});
</script>
<?php } else { 
	include("views/static_pages/error.php");
}?>
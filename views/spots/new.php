<div class="container-fluid" style="margin-top:50px;text-align:center;">
	<span class="add-spot-header">Add New Spot</span>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="add-spot-map">
			<div id="map" style="box-shadow: 2px 4px 25px -12px rgba(0,0,0,0.75);">
				<div id="map-event-info" class="alert alert-warning hide-on-mobile" role="alert" style="top:5px">
					<span class="glyphicon glyphicon-alert alert-icon" style="margin-right:10px"></span>
					<span id="map-event-text">Drag the marker to your spots location or fill in the coordinates below</span>
				</div>
				<div id="map-event-info" class="hide-on-desktop show-on-mobile">
					<span class="glyphicon glyphicon-alert alert-icon" style="margin-right:10px"></span>
					<span class="map-event-text">Drag the marker to your spots location</span>
				</div>
			</div>
		</div>
		<div id="spot-form-errors" class="form-error-area" style="text-align:center"></div>
	</div>
</div>
<div class="container">
	<form method="post" action="#" id="new-spot-form">
		<div class="row some-gutters">
			<div class="col-md-6">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Name*</span>
					<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
						<span class="glyphicon glyphicon-exclamation-sign"></span>
						<span class="error-text" style="margin-left:2px"></span>
					</span>
					<input type="text" name="name" class="new-spot-input" placeholder="Enter name...">
				</div>
			</div>
			<div class="col-md-3">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Latitude*</span>
					<input type="text" name="lat" class="new-spot-input add-spot-coords-input" id="add-spot-lat" placeholder="Enter latitude...">
				</div>
			</div>
			<div class="col-md-3">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Longitude*</span>
					<input type="text" name="long" id="add-spot-long" class="new-spot-input add-spot-coords-input" placeholder="Enter longitude...">
				</div>
			</div>
		</div>
		<div class="row some-gutters">
			<div class="col-md-12">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Description*</span>
					<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
						<span class="glyphicon glyphicon-exclamation-sign"></span>
						<span class="error-text" style="margin-left:2px"></span>
					</span>
					<textarea rows="5" name="desc" class="new-spot-input" placeholder="Describe this spots features"></textarea>
				</div>
			</div>
		</div>
		<div class="row some-gutters">
			<div class="col-md-12">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Tips/Warnings</span>
					<span class="alert alert-danger form-error" style="float:right">
						<span class="glyphicon glyphicon-exclamation-sign" style="margin-right:2px"></span>
						<span class="error-text"></span>
					</span>
					<textarea rows="2" name="tips" class="new-spot-input" placeholder="Anything other climbers should know about?"></textarea>
				</div>
			</div>
		</div>
		<div class="row some-gutters">
			<div class="col-md-5">
				<div class="new-spot-form-div">
					<span class="new-spot-text">Location</span>
					<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
						<span class="glyphicon glyphicon-exclamation-sign"></span>
						<span class="error-text" style="margin-left:2px"></span>
					</span>
					<input type="text" name="location" class="new-spot-input" placeholder="What's the nearest town/park to this spot?">
				</div>
			</div>
			<div class="col-md-3">
				<div class="new-spot-form-div" style="padding-bottom:20px">
					<div class="new-spot-text">Type*</div>
					<div class="radio-choice radio-checkbox-div">
						<div class="empty-circle"></div>
						<span class="basic-text radio-text">Ropes</span>
					</div>
					<div class="radio-choice radio-checkbox-div">
						<div class="empty-circle"></div>
						<span class="basic-text radio-text">Bouldering</span>
					</div>
					<div class="radio-choice radio-checkbox-div">
						<div class="empty-circle"></div>
						<span class="basic-text radio-text">Both</span>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="new-spot-form-div" style="padding-bottom:20px">
					<div class="new-spot-text">Edit</div>
					<div class="check-box-div radio-checkbox-div">
						<div class="check-box transition-all"></div><span class="basic-text radio-text">Allow others to edit description/tips</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row some-gutters">
			<div class="col-md-4 col-md-offset-8">
				<input type="submit" class="sliding-button large-button" style="float:right;margin:15px 0;background:white;padding:10px 20px;width:170px;font-size:1.5em" value="SUBMIT"/>
			</div>
		</div>
	</form>
</div>
<div id="mouse-position" style="display:none"></div>
<script src="assets/js/ol.js"></script>
<script>
//initialize tooltips
$("[data-toggle='tooltip']").tooltip();
/******************************************
-------------------------------------------
 	          Form events
-------------------------------------------
*******************************************/
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
$("input[name='name']").keyup(function() {
	if($(this).val().length > 4) {
		validateName($(this));
	}
})
//validations
$("input[name='name']").blur(function() {
	validateName($(this));
});
$("textarea[name='description']").blur(function() {
	var text = $(this).val();
	var parent = $(this).parent();
	if(text.length > 5000) {
		spotError("Too long!", parent, "Max is 5000 characters");
	} else {
		parent.find(".form-error").fadeOut();
	}
});
$("textarea[name='tips']").blur(function() {
	var text = $(this).val();
	var parent = $(this).parent();
	if(text.length > 3000) {
		spotError("Too long!", parent, "Max is 3000 characters");
	} else {
		parent.find(".form-error").fadeOut();
	}
});
function validateName($this) {
	var parent = $this.parent();
	var text = $this.val();
	if(text.length < 5) {
		spotError("Too short!", parent, "Min is 5 characters");
	} else if(text.length > 40) {
		spotError("Too long!", parent, "Max is 40 characters");
	} else {
		parent.find(".form-error").fadeOut();
	}
}
//inline validation error
function spotError(message, element, tip) {
	var formError = element.find(".form-error")
	element.find(".error-text").html(message);
	formError.show();
	formError.attr("data-original-title", tip);
}
//submit to db
$("#new-spot-form").submit(function(e) {
	e.preventDefault();
	var formData = $(this).serialize();
	formData += "&filter_type=" + filterTypeChoice;
	formData += "&edit=" + editChoice;
	$.ajax({
		url: "ajax/spot/create_spot.php",
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function(resp) {
			//if resp.length is undefined that means an array of errors was returned
			if(resp.length == undefined) {
				if(!resp.success) {
					$("#spot-form-errors").html("").show();
					$("#spot-form-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
				} else {
					//go to newly created spot page
					window.location.replace("index.php?p=spot&id=" + resp.spot_id);
				}
			} else {
				$("#spot-form-errors").html("").show();
				for(var i=0;i<resp.length;i++) {
					$("#spot-form-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
				}
			}
		}
	})
});
//----------------------------------------|
//          Custom radio buttons  	      |
//----------------------------------------|
var filterTypeChoice = "";
$(".radio-choice").hover(
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.9)");
		$(this).find(".empty-circle").css("border", "2px solid rgba(0, 0, 0, 0.9)");
	},
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.6)");
		$(this).find(".empty-circle").css("border", "2px solid rgba(0, 0, 0, 0.3)");
	}
);
//add inside-dot, change active filterTypeChoice
$(".radio-choice").click(function() {
	var circle = $(this).find(".empty-circle");
	$(".inside-dot").remove();
	circle.append('<div class="inside-dot"></div>');
	$(".empty-circle").css("background", "white");
	circle.css("background", "#398bc6");
	filterTypeChoice = $(this).find(".radio-text").html();
});
//----------------------------------------|
//           Custom checkbox      	      |
//----------------------------------------|
var editChoice = false;
$(".check-box-div").hover(
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.9)");
		$(this).find(".check-box").css("border", "2px solid rgba(0, 0, 0, 0.9)");
	},
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.6)");
		$(this).find(".check-box").css("border", "2px solid rgba(0, 0, 0, 0.3)");
	}
)
$(".check-box-div").click(function() {
	if(editChoice) {
		$(this).find(".check-box").html("");
		editChoice = false;
	} else {
		$(this).find(".check-box").html("<span class='glyphicon glyphicon-ok' id='checkmark'></span>");
		editChoice = true;
	}
});
/******************************************
-------------------------------------------
 	     Map for drag/drop marker
-------------------------------------------
*******************************************/
//----------------------------------------|
//         Set up layers/styles 	      |
//----------------------------------------|
var modifyCollection = new ol.Collection();
var modifySource = new ol.source.Vector({
	features: modifyCollection
});
//styles for different layers
function setFeatureStyle(image, visible) {
	var style = new ol.style.Style({
		image: new ol.style.Icon({
			src: image,
			anchor: [.46, .1],
			anchorOrigin: "bottom-left",
			scale: 0.4,
			opacity: visible
		})
	});
	return [style];
}
//this layer is purely for modifying features, feats will be copied over when modifying or adding
var modifyLayer = new ol.layer.Vector({
	source: modifySource,
	style: setFeatureStyle("assets/images/black-pin.png", 1)
});
var rasterLayer = new ol.layer.Tile({
	source: new ol.source.OSM(),
	preload: Infinity
});
//----------------------------------------|
// set up map controls                    |
//----------------------------------------|
var mousePosControl = new ol.control.MousePosition({
	coordinateFormat: ol.coordinate.createStringXY(4),
	projection: 'EPSG:4326',
	className: "mouse-coords",
	target: document.getElementById("mouse-position")
});
//----------------------------------------|
//            initialize map 			  |
//----------------------------------------|
var map = new ol.Map({
	target: 'map',
	view: new ol.View({
		center: ol.proj.transform([-95.7129, 37.0902], 'EPSG:4326', 'EPSG:3857'),
		zoom: 4,
		maxZoom: 20
	}),
	layers: [rasterLayer, modifyLayer],
	controls: ol.control.defaults({
		attributeOptions: /* */ ({
			collapsible:false
		})
	}).extend([mousePosControl])
});
//if geolocation set, center map on users location
var newFeature = new ol.Feature();
newFeature.setId(1);
navigator.geolocation.getCurrentPosition(function(position) {
	centerMap(position.coords.latitude, position.coords.longitude, 10);
	//add new feature with geolocate coords
	var featCoords = ol.proj.transform([position.coords.longitude, position.coords.latitude], 'EPSG:4326', 'EPSG:3857');
	var geom = new ol.geom.Point(featCoords);
	newFeature.setGeometry(geom);
	modifySource.addFeature(newFeature);
}, function(error) {
	//if no geolocate, place marker in the middle of the US
	var featCoords = ol.proj.transform([-95.7129, 37.0902], 'EPSG:4326', 'EPSG:3857');
	var geom = new ol.geom.Point(featCoords);
	newFeature.setGeometry(geom);
	modifySource.addFeature(newFeature)
});
//center map on specified lat, long
function centerMap(lat, long, zoom) {
	map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
	map.getView().setZoom(zoom);
}
//----------------------------------------|
//            Modify Feature  			  |
//----------------------------------------|
modifyInteraction = new ol.interaction.Modify({
	features: modifyCollection,
	pixelTolerance: 30,
	style: setFeatureStyle("assets/images/black-pin.png", 1)
});
map.addInteraction(modifyInteraction);
//on drag end of feature, record the location the mouse pointer is at
modifyInteraction.on('modifyend', function(e) {
	var modifiedCoords = ol.proj.transform(modifySource.getFeatureById(1).getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
	$("#add-spot-lat").val(modifiedCoords[1]);
	$("#add-spot-long").val(modifiedCoords[0]);
});
//for manual entries in lat/long inputs, adjust view/feature accordingly
$(".add-spot-coords-input").keyup(function() {
	var lat = $("#add-spot-lat").val();
	var long = $("#add-spot-long").val();
	if(lat.length > 1 && long.length > 1) {
		centerMap(parseFloat(lat), parseFloat(long), map.getView().getZoom(), 10);
		var featCoords = ol.proj.transform([parseFloat(long), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857');
		var geom = new ol.geom.Point(featCoords);
		newFeature.setGeometry(geom);
	}
});
</script>
<div class="container-fluid" style="margin-top:50px">
	<div class="row">
		<div id="explore-sidebar">
			<div id="list-filters">
				<div id="spot-num"></div>
				<div id="list-filter-options">
					<div class="list-filter-choice" style="background:linear-gradient(#d8edf3, #3e99f4);color:white"><span class="list-filter-text" id="claims">Popularity</span><span class='glyphicon glyphicon-triangle-top'></span>
						<div class="list-filter-sort" style="display:none">
							<div class="filter-menu-text" style="background:rgba(0, 0, 0, 0.5);color:white">Ascending</div>
							<div class="filter-menu-text">Descending</div>
						</div>
					</div>
					<div class="list-filter-choice"><span class="list-filter-text" id="type_climbing">Type</span><span class='glyphicon glyphicon-triangle-bottom'></span>
						<div class="list-filter-sort" style="display:none">
							<div class="filter-menu-text">Ascending</div>
							<div class="filter-menu-text">Descending</div>
						</div>
					</div>
				</div>
			</div>
			<div id="spot-list"></div>
		</div>
		<div style="padding:0" id="map-column">
			<div id="map">
				<div id="map-filters">
					<div class="row no-gutters">
						<div class="col-sm-6">
							<form method="post" action="#" id="map-search-form">
								<div class="search-form-div">
									<span class="glyphicon glyphicon-search" id="map-search-submit"></span>
									<input type="text" name="q" class="form-control" placeholder="Search..." autofocus="true" id="map-search-bar">
									<input type="hidden" name="radius" value="20" id="radius-input">
									<input type="hidden" name="popularity" value="none" id="pop-input">
									<input type="hidden" name="difficulty" value="any" id="difficulty-input">
									<input type="hidden" name="type" value="any" id="type-input">
								</div>
							</form>
						</div>
						<div class="col-sm-6">
							<div id="map-filter-options">
								<div class="row no-gutters">
									<div class="col-xs-4">
										<div class="explore-search-div">
											<div class="explore-search-text">Radius</div>
											<div class="explore-search-menu" id="Radius">
												<div class="explore-search-options">Explore</div>
												<div class="explore-search-options">20 Miles</div>
												<div class="explore-search-options">50 Miles</div>
												<div class="explore-search-options">75 Miles</div>
												<div class="explore-search-options">100+</div>
											</div>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="explore-search-div">
											<div class="explore-search-text">Difficulty</div>
											<div class="explore-search-menu" id="Difficulty">
												<div class="explore-search-options">All</div>
												<div class="explore-search-options">V1-V5</div>
												<div class="explore-search-options">V6-V10</div>
												<div class="explore-search-options">5.1-5.5</div>
												<div class="explore-search-options">5.6-5.11</div>
											</div>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="explore-search-div">
											<div class="explore-search-text">Type</div>
											<div class="explore-search-menu" id="Type">
												<div class="explore-search-options">All</div>
												<div class="explore-search-options">Bouldering</div>
												<div class="explore-search-options">Sport</div>
												<div class="explore-search-options">Trad</div>
												<div class="explore-search-options">Top Rope</div>
												<div class="explore-search-options">Alpine</div>
												<div class="explore-search-options">Ice</div>
												<div class="explore-search-options">Aid</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="popup-overlay" class="popup">
				<div id="popup-content">
					<div class="row no-gutters">
						<div class="col-sm-7" style="margin-left:-3px">
							<div id="popup-name"></div>
							<div id="popup-city"></div>
							<div id="popup-type"></div><br>
							<span class="glyphicon glyphicon-flag"></span>
							<span id="popup-claims" class="popup-text"></span>
						</div>
						<div class="col-sm-5">
							<div id="popup-image-holder">
								<img src="assets/images/footer-image2.jpg" class="popup-image"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
/* ********************************
   --------------------------------
   |        Column Sizing  	      |
   ________________________________
   ********************************
*/
adjustHeight($("#map-column"));
adjustHeight($("#explore-sidebar"));
adjustMapWidth();
adjustExploreSidebar();
$(window).resize(function() {
	adjustHeight($("#map-column"));
	adjustHeight($("#explore-sidebar"));
	adjustMapWidth();
	adjustExploreSidebar();
})
function adjustHeight(column) {
	var winHeight = $(window).height() - $("#navbar").height();
	column.css("height", winHeight + "px");
}
function adjustMapWidth() {
	var screenWidth = $(window).width() - $("#explore-sidebar").width();
	$("#map-column").css("width", screenWidth)
	$("#map-column").css("margin-left", $("#explore-sidebar").width() + "px")
}
function adjustExploreSidebar() {
	$("#explore-sidebar").css("top", $("#navbar").height());
	$("#spot-list").css("height", (($(window).height() - $("#list-filters").height()) - $("#navbar").height()) + "px");
}

/* ********************************
   --------------------------------
   |       Map Filters Div  	  |
   ________________________________
   ********************************
*/
//on hover over filters, expand to show menu
$(".explore-search-div").hover(
	//mouseenter
	function() {
		$(this).stop().animate({
			height: "100px",
			bottom: "30px"
		}, 250)
		$(this).find(".explore-search-text").hide();
		$(this).find(".explore-search-menu").show();
	},
	//mouseout
	function() {
		$(this).stop().animate({
			height: "38px",
			bottom: "0px"
		}, 250)
		$(this).find(".explore-search-text").show();
		$(this).find(".explore-search-menu").hide();
	}
)
//vars that hold filters
var filteredTypes = [];
var filteredDifficulty = [];
//on menu select, filter or unfilter selection
$(".explore-search-options").click(function() {
	var parent = $(this).parent().attr("id");
	switch(parent) {
		case "Type":
			filterSelectors(filteredTypes, $(this));
			break;
		case "Radius":
			break;
		case "Difficulty":
			filterSelectors(filteredDifficulty, $(this));
			break;
	}
})
//handle css/array push for filters
function filterSelectors(filterArray, element) {
	var filterName = element.html().toLowerCase();
	if(filterName != "all") {
		//check if element is already in filterArray
		var thisIndex = filterArray.indexOf(filterName) 
		if(thisIndex === -1) {
			filterArray.push(filterName);
			clickFilterCss(element, "true");
		} else {
			filterArray.splice(thisIndex, 1)
			clickFilterCss(element, "false");
		}
		clickFilterCss($(element.parent().children()[0]), "false");
	} else {
		//if all is chosen, dump contents of array, unhighlight all options except all
		filterArray.splice(0, filterArray.length);
		element.parent().children().each(function() {
			clickFilterCss($(this), "false");
		})
		clickFilterCss(element, "true");
	}
	//display new filter in non-hover state
	var text = element.parents(".explore-search-div").children(".explore-search-text");
	if(filterArray.length > 1) {
		text.html(filterArray.length + " " + element.parent().attr("id") + "s");
	} else if(filterArray.length == 1) {
		text.html(element.html());
	} else {
		text.html(element.parent().attr("id"))
	}
	//refresh map 
	mapFeatures();
}
//fixes hovering over options after selection
$(".explore-search-options").hover(
	function() {
		if($(this).css("background-color") == "transparent") {
			$(this).css("background", "rgba(0, 0, 0, 0.4)");
		}
	},
	function() {
		if($(this).css("background-color") == "rgba(0, 0, 0, 0.4)") {
			$(this).css("background", "none");
		}
	}
)
//select/deselect css for map filters
function clickFilterCss(element, select) {
	if(select === "true") {
		element.css({
			background: "white",
			color: "black"
		})
	} else {
		element.css({
			background: "none",
			color: "white"
		})
	}
}

/* ********************************
   --------------------------------
   |       List Filters Div  	  |
   ________________________________
   ********************************
*/
//array holds list sort options, 0 is order, 1 is type
var sortOptions = ["Ascending", "claims"];
//slidedown options div
$(".list-filter-choice").hover(
	function() {
		$(this).children(".list-filter-sort").stop().slideDown("fast");
	},
	function() {
		$(this).children(".list-filter-sort").stop().slideUp("fast");
	}
)
//on option click 
$(".filter-menu-text").click(function() {
	var order = $(this).html();
	sortOptions[0] = order;
	sortOptions[1] = $(this).parents(".list-filter-choice").find(".list-filter-text").attr("id");
	//bring all other options to default state
	$(".filter-menu-text").each(function() {
		$(this).css({
			background: "none",
			color: "black"
		})
	})
	$(this).css({
		background: "rgba(0, 0, 0, 0.5)",
		color: "white"
	});
	//bring type divs to default state
	$(".list-filter-choice").each(function() {
		$(this).css({
			background: "white",
			color: "black"
		})
		$(this).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-bottom");
	})
	//give chosen options parent div selected css
	var parent = $(this).parents(".list-filter-choice");
	parent.css({
		background: "linear-gradient(#d8edf3, #3e99f4)",
		color: "white"
	});
	//change icon depending on choice
	if(order == "Ascending") {
		parent.find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-top");
	} else {
		parent.find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-bottom");
	}
	//run mapFeatures again to reshuffle list
	mapFeatures();
});

/* ********************************
   --------------------------------
   |             MAP   			  |
   ________________________________
   ********************************
*/
//global color vars
var green = "#40cc42";
var purple = "#c260fc";
var red = "#ff4a52";
var blue = "#00d5ff";
var orange = "#ff751a";
var gray = "#a9a9a9";
var blackGray = "#313132";
//----------------------------------------|
// set up styles and layers for map       |
//----------------------------------------|
//set up sources for different layers
var spotSource = new ol.source.Vector();
//styles for different layers
function setFeatureStyle(image, visible) {
	var style = new ol.style.Style({
		image: new ol.style.Icon({
			src: image,
			anchor: [.46, .1],
			anchorOrigin: "bottom-left",
			scale: 0.8,
			opacity: visible
		})
	});
	return [style];
}
//set up vector layers for each different source
var spotLayer = new ol.layer.Vector({
	source: spotSource,
	style: setFeatureStyle("assets/images/markers/blue-pin.png", 0)
});
//raster layer for map
//preload is better, but not sure of long term costs
var rasterLayer = new ol.layer.Tile({
	source: new ol.source.OSM(),
	preload: Infinity
});
//----------------------------------------|
//            initialize map 			  |
//----------------------------------------|
var map = new ol.Map({
	target: 'map',
	view: new ol.View({
		center: ol.proj.transform([-95.7129, 37.0902], 'EPSG:4326', 'EPSG:3857'),
		zoom: 4,
		maxZoom: 20
	}),
	layers: [rasterLayer, spotLayer]
});
//if geolocation set, center map on users location
if(navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
			centerMap(position.coords.latitude, position.coords.longitude, 9);
		})
}
//center map on specified lat, long
function centerMap(lat, long, zoom) {
	map.getView().setCenter(ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
	map.getView().setZoom(zoom);
}

//----------------------------------------|
//            Get spots ajax 			  |
//----------------------------------------|
$.post("ajax/explore/get_location.php", function(data) {
	if(data) {
		//set projection
		var transform = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
		for(var i=0;i<data.length;i++) {
			var feature = new ol.Feature(data[i]);
			//add info to feature
			feature.set('name', data[i].name);
			feature.set('type_climbing', data[i].type_climbing);
			feature.set('claims', data[i].claims);
			feature.setId(data[i].id);
			//pass long/lat to feature
			var coords = transform([parseFloat(data[i].long), parseFloat(data[i].lat)]);
			var geom = new ol.geom.Point(coords);
			feature.setGeometry(geom);
			//place feature in source
			spotSource.addFeature(feature);
		}
	}
}, 'json')

//----------------------------------------|
//          Feature interaction 	      |
//----------------------------------------|
//add new select interaction to spot layer
var select = new ol.interaction.Select({
	layers: [spotLayer],
	style: setFeatureStyle("assets/images/markers/black-pin.png", 1),
	condition: ol.events.condition.pointerMove
});
map.addInteraction(select);
//create popup overlay for hover
var popup = document.getElementById("popup-overlay");
var overlay = new ol.Overlay({
	element: popup,
	positioning: 'center-center',
	offset: [0, -98]
});
map.addOverlay(overlay);
//events for selection
var selected = select.getFeatures();
selected.on('add', function(e) {
	var feature = e.target.item(0);
	//reset style if feature has changed styles
	feature.setStyle(setFeatureStyle("assets/images/markers/black-pin.png", 1))
	var type = feature.get('type_climbing');
	var name = feature.get('name');
	var long = feature.get('long');
	var lat = feature.get('lat');
	//activate overlay
	var transform = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
	var coords = transform([parseFloat(long), parseFloat(lat)]);
	overlay.setPosition(coords);
	//correct offset if name is long
	if(name.length > 15) {
		overlay.setOffset([0, -103]);
	} else overlay.setOffset([0, -98]);
	//customize popup
	popupHTML(name, type, 0);

	//close overlay
	selected.on("remove", function(e) {
		feature.setStyle(setFeatureStyle("assets/images/markers/blue-pin.png", 1))
		overlay.setPosition(undefined);
	});
});

//handle click on feature, find correct feature then send to show page
map.on("click", function(e) {
	var feats = [];
	//push any features found into array
	map.forEachFeatureAtPixel(e.pixel, function(feature, layer) {
		feats.push(feature)
	});
	if(feats.length > 0) {
		//iterate through found features, if feature matches name of last/current name in #popup-name
		//activate link to show page
		for(var i=0;i<feats.length;i++) {
			if(feats[i].get('name') == $("#popup-name").html()) {
				window.location.replace("index.php?p=spot&id=" + feats[i].get('id'));
			}
		}
	}
})

//change popup info on feature hover
function popupHTML(name, type, claims) {
	var newPopup = $("#popup");
	$("#popup-name").html(name);
	$("#popup-type").html(type);
	//set bckg different on type
	var color = typeColor(type);
	$("#popup-type").css("background", color)
	$("#popup-claims").html(claims);
}
//change type background color for popup/divs based on type
function typeColor(type) {
	if(type == "Bouldering") {
		return "#40cc42";
	} else if(type == "Alpine" || type == "Ice") {
		return "#c260fc";
	} else if(type == "Top Rope") {
		return "#fb3b44";
	} else {
		return "#2e6fd1";
	}
}

/* ********************************
   --------------------------------
   |   Search Sidebar/Filtering   |
   ________________________________
   ********************************
*/

//----------------------------------------|
//           Main List Events	          |
//----------------------------------------|
//get features from map bounding box if zoomed in enough, hide spotLayer if not, add in heatmap
map.on("moveend", function() {
	if(map.getView().getZoom() < 7) {
		spotLayer.setVisible(false);
		//add heatmap
	} else {
		spotLayer.setVisible(true);
		mapFeatures();
	}
});
//so mapFeatures runs to refresh any new info from ajax
$(document).ajaxComplete(mapFeatures);

//----------------------------------------|
//         Get visible Features 	      |
//----------------------------------------|
var joinedArr = [];
//puts unfiltered features in spot-list, hides filtered features on map
function mapFeatures(e) {
	$("#spot-list").html("");
	//refresh or initialize typeArrs object
	var typeArrs = {"sportTrad": [], "trad": [], "bouldering": [], "sport": [], "topRope": [], "alpine": [], "ice": [], "aid": [], "other": []};
	//get bounding box of map
	var mapExtent = map.getView().calculateExtent(map.getSize());
	//get features in map view, organize into types
	featuresFromExtent(spotSource, mapExtent, typeArrs);
	//filter/join arrays from featuresFromExtent
	joinedArr = concatArrays(typeArrs);
	//clear object of arrays
	typeArrs = {};
	//iterate through newly joined array
	for(var i=0, len = joinedArr.length;i<len;i++) {
		joinedArr[i].setStyle(setFeatureStyle("assets/images/markers/blue-pin.png", 1));
		//only append first 20, rest will be by pagination
		if(i < 20) {
			spotDivHTML(joinedArr[i]);
		}
	}
	//get amount of spots in spot-list
	$("#spot-num").html("Showing " + joinedArr.length + " locations")
	//load more button
	if(joinedArr.length > $(".spot-div").length) {
		$("#spot-list").append("<div id='explore-load-more'><span class='load-more-text'>Load More</span></div>")
	}
	//scroll spot list back to top
	$("#spot-list").animate({
		scrollTop:0
	}, 200)
}

//----------------------------------------|
//     Functions used in mapFeatures 	  |
//----------------------------------------|
//html for spot in spot-list
function spotDivHTML(element) {
	var color = typeColor(element.get("type_climbing"));
	var spotDiv = "<div class='spot-div'>" +
					"<div class='row no-gutters'>" +
						"<div class='col-xs-5'>" +
							"<div class='spot-image-holder'>" +
								"<img src='assets/images/footer-image1.jpg' class='spot-image'/>" +
							"</div>" +
						"</div>" +
						"<div class='col-xs-7'>" +
							"<div class='spot-name'><a href='index.php?p=spot&id=" + element.get('id') +
							"' class='spot-link' style='color:black'>" + element.get('name') + "</a></div>" +
							"<div class='spot-id' style='display:none'>" + element.getId() + "</div>" +
							"<div class='spot-type' style='background:" + color + "'>" + element.get('type_climbing') + "</div>" +
							"<div class='spot-comment'></div>" +
							"<span class='glyphicon glyphicon-flag'></span>" +
							"<span class='popup-text' style='font-size:1.1em'> " + element.get('claims') + "</span>" +
						"</div>" +
					"</div>" +
				"</div>";
	$("#spot-list").append(spotDiv);
}
//get features inside viewable map area
function featuresFromExtent(source, extent, array) {
	source.forEachFeatureInExtent(extent, function(feature) {
		var type = feature.get('type_climbing');
		feature.setStyle(setFeatureStyle("assets/images/markers/blue-pin.png", 0));
		//put each feature into correct typeArrs element for filtering
		switch(type) {
			case "Sport":
				array.sport.push(feature);
				break;
			case "Trad":
				array.trad.push(feature);
				break;
			case "Bouldering":
				array.bouldering.push(feature);
				break;
			case "Alpine":
				array.alpine.push(feature);
				break;
			case "Ice":
				array.ice.push(feature);
				break;
			case "Aid":
				array.aid.push(feature);
				break;
			case "Top Rope":
				array.topRope.push(feature);
				break;
			case "Sport/Trad":
				array.sportTrad.push(feature);
				break;
			default:
				array.other.push(feature);
				break;
		}
	})
}
//iterate over array of objects, find whats filtered, concat into one array
function concatArrays(typeObj) {
	var keyArr = [];
	//if filteredTypes is empty, nothing is being filtered, do all
	if(filteredTypes.length !== 0) {
		for(var key in typeObj) {
			//find better solution other than copying entire array into another array to then be copied into another array
			if(filteredTypes.indexOf(key) !== -1) {
				keyArr.push(typeObj[key]);
			}
		}
		return mergeSort(Array.prototype.concat.apply([], keyArr));
	} else {
		for(var key in typeObj) {
			var allTypes = [typeObj['sport'], typeObj['trad'], typeObj['sportTrad'],  typeObj['bouldering'], typeObj['alpine'], typeObj['ice'], typeObj['aid'], typeObj['topRope'], typeObj['other']];
			return mergeSort(Array.prototype.concat.apply([], allTypes));
		}
	}
}
//----------------------------------------|
//       	Sorting Functions	          |
//----------------------------------------|
//sorts array depending on vars stored in sortOptions
function mergeSort(arr) {
	if(arr.length < 2) return arr;
	var half = Math.floor(arr.length/2);
	var subLeft = mergeSort(arr.slice(0, half));
	var subRight = mergeSort(arr.slice(half));
	if(sortOptions[0] == "Ascending") {
		return mergeAsc(subLeft, subRight);
	} else {
		return mergeDesc(subLeft, subRight);
	}
}
function mergeAsc(a, b) {
	var result = [];
	if(sortOptions[1] == 'claims') {
		while(a.length > 0 && b.length > 0) {
			result.push(parseInt(a[0].get(sortOptions[1])) < parseInt(b[0].get(sortOptions[1])) ? a.shift() : b.shift());
		}
	} else {
		while(a.length > 0 && b.length > 0) {
			result.push(a[0].get(sortOptions[1]) < b[0].get(sortOptions[1]) ? a.shift() : b.shift());
		}
	}
	return result.concat(a.length ? a : b);
}
function mergeDesc(a, b) {
	var result = [];
	if(sortOptions[1] == 'claims') {
		while(a.length > 0 && b.length > 0) {
			result.push(parseInt(a[0].get(sortOptions[1])) > parseInt(b[0].get(sortOptions[1])) ? a.shift() : b.shift());
		}
	} else {
		while(a.length > 0 && b.length > 0) {
			result.push(a[0].get(sortOptions[1]) > b[0].get(sortOptions[1]) ? a.shift() : b.shift());
		}
	}
	return result.concat(a.length ? a : b);
}

//----------------------------------------|
//          Inside List Events	          |
//----------------------------------------|
//load more spot divs
$(document).on("click", "#explore-load-more", function() {
	var i = 0;
	var length = $(".spot-div").length;
	//loading gif
	$(this).attr('id', "").html("<img src='assets/loaders/ripple.gif' class='loader-ripple'/>")
	while(i < 20) {
		if(joinedArr[length + i] !== undefined) {
			spotDivHTML(joinedArr[length + i]);
			i++;
		} else {
			break;
		}
	}
	//remove gif, add another load more button if more spots left
	$(this).remove();
	if(joinedArr.length > $(".spot-div").length) {
		$("#spot-list").append("<div id='explore-load-more'><span class='load-more-text'>Load More</span></div>");
	}
});

//highlight spot on map when hovering over sidebar div
$(document).on({
	mouseenter: function(e) {
		var id = $(this).find(".spot-id").html();
		var thisFeat = spotSource.getFeatureById(id);
		selected.push(thisFeat);
	},
	mouseleave: function(e) {
		selected.clear();
	}
}, '.spot-div');
</script>
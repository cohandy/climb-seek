<div id="explore-map-cover">
	<img src='assets/loaders/ripple.gif' class='loader-ripple map-cover-loader'/>
</div>
<!-- search bar -->
<div id="mobile-search-bar">
	<!-- search form -->
	<form method="post" action="#" id="map-search-form">
		<div class="search-form-div">
			<span class="glyphicon glyphicon-search map-search-icon" id="map-search-submit"></span>
			<input type="text" name="q" class="form-control bolder-text" placeholder="Search" autocomplete="off" id="map-search-bar">
			<span class="glyphicon glyphicon-remove map-search-icon" id="map-search-close"></span>
		</div>
	</form>
	<!-- results -->
	<div id="search-results">
		<div id="search-top-bar">
			<div class="search-filter-bar">
				<div class="inside-search-filter" style="font-size:1.5em;color:black;">All</div>
				<div class="inside-search-filter">Places</div>
				<div class="inside-search-filter">Spots</div>
				<div class="inside-search-filter">Routes</div>
				<div class="inside-search-filter">Gyms</div>
			</div>
		</div>
		<div id="search-results-scroll">
			<div id="search-results-info"></div>
			<div id="search-loading-div">
				<div id="search-loading-text">Loading...</div>
			</div>
			<div class="search-results-div" id="city-results">
				<div class="search-results-header">
					<div class="search-results-type">Places</div>
				</div>
				<div class="search-results"></div>
				<a href="#" class="view-all-results" id="more-cities">View All Places...</a>
				<img src="assets/loaders/ripple.gif" class="infinite-load-gif" alt="loading"/>
			</div>
			<div class="search-results-div" id="spot-results">
				<div class="search-results-header">
					<div class="search-results-type">Spots</div>
				</div>
				<div class="search-results"></div>
				<a href="#" class="view-all-results" id="more-spots">View All Spots...</a>
				<img src="assets/loaders/ripple.gif" class="infinite-load-gif" alt="loading"/>
			</div>
			<div class="search-results-div" id="route-results">
				<div class="search-results-header">
					<div class="search-results-type">Routes</div>
				</div>
				<div class="search-results"></div>
				<a href="#" class="view-all-results" id="more-routes">View All Routes...</a>
				<img src="assets/loaders/ripple.gif" class="infinite-load-gif" alt="loading"/>
			</div>
			<div class="search-results-div" id="indoor-results">
				<div class="search-results-header">
					<div class="search-results-type">Indoor/Gyms</div>
				</div>
				<div class="search-results"></div>
				<a href="#" class="view-all-results" id="more-gyms">View All Gyms...</a>
				<img src="assets/loaders/ripple.gif" class="infinite-load-gif" alt="loading"/>
			</div>
		</div>
	</div>
</div>
<!-- map container -->
<div class="container-fluid mobile-explore-container" id="mobile-explore-map">
	<!-- main filters -->
	<div class="row no-gutters" id="main-map-filters">
		<div class="col-xs-4" style="overflow:hidden">
			<div class="climbing-choice-div" id="Ropes">
				<div class="inside-climbing-choice">Rock Climbing</div>
			</div>
		</div>
		<div class="col-xs-4" style="overflow:hidden">
			<div class="climbing-choice-div" id="Bouldering">
				<div class="inside-climbing-choice">Bouldering</div>
			</div>
		</div>
		<div class="col-xs-4" style="overflow:hidden">
			<div class="climbing-choice-div" id="Indoor">
				<div class="inside-climbing-choice">Indoor/Gyms</div>
			</div>
		</div>
	</div>
	<!-- map -->
	<div id="map">
		<div id="specific-map-filters">
			<!-- specific type filters -->
			<div class="specific-type-filters" id="rope-filters" style="display:none">
				<div class="row no-gutters specific-filters-row">
					<div class="col-xs-6 specific-filter-column">
						<div class="specific-map-filter" style="border-right: 2px solid rgba(0, 0, 0, 0.2)">
							<div class="specific-map-filter-text specific-filter-name">Type</div>
							<span class="glyphicon glyphicon-triangle-bottom filter-select-icon"></span>
						</div>
						<div class="specific-map-filter-menu" id="specific-type-filter">
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter all-filter basic-text">All</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Aid</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Alpine</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Ice</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Sport</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Top Rope</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Trad</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text type-filter basic-text">Other</span>
								<span class="specific-menu-check specific-type-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
						</div>
					</div>
					<div class="col-xs-6 specific-filter-column">
						<div class="specific-map-filter">
							<div class="specific-map-filter-text specific-filter-name">Difficulty</div>
							<span class="glyphicon glyphicon-triangle-bottom filter-select-icon"></span>
						</div>
						<div class="specific-map-filter-menu" id="specific-diff-filter">
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter all-filter basic-text">All</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.0 - 5.5</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.6 - 5.9</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.10a - 5.10d</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.11a - 5.11b</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.12a - 5.12b</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.13a - 5.13d</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">5.14+</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="specific-type-filters" id="boulder-filters" style="display:none">
				<div class="row no-gutters">
					<div class="col-xs-12 specific-filter-column">
						<div class="specific-map-filter">
							<div class="specific-map-filter-text specific-filter-name">Difficulty</div>
							<span class="glyphicon glyphicon-triangle-bottom filter-select-icon"></span>
						</div>
						<div class="specific-map-filter-menu" id="specific-diff-filter">
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter all-filter basic-text">All</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">V0 - V2</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">V3 - V5</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">V6 - V9</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">V10-V13</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
							<div class="specific-menu-choice">
								<span class="specific-menu-text difficulty-filter basic-text">V15+</span>
								<span class="specific-menu-check specific-diff-icon glyphicon glyphicon-ok-circle" style="display:none"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- info bar, for modifying/adding -->
		<div id="map-event-info">
			<span id="map-event-text" class="map-event-text">Drag the marker to its proper location.</span>
			<div class="small-regular-button basic-text" id="confirm-new-location">CONFIRM</div>
			<span id="confirm-loader" style="display:none"><img src="assets/loaders/ripple.gif" class="small-loader-ripple"/></span>
			<span class="glyphicon glyphicon-remove info-remove-icon" id="info-remove-icon"></span>
		</div>
		<!-- custom map controls -->
		<div id="cluster-info-text" class="basic-text transition-all map-info-div">Zoom in to discover spots or use search</div>
		<!-- loading gif for ajax calls -->
		<div id="map-ajax-gif"><img src="assets/loaders/spinner.gif" class="spinner-gif"/></div>
		</div>
		<!-- map overlay/popup for spot info -->
		<div id="spot-map-popup" class="mobile-popup">
			<div class="map-popup">
				<div class="area-div-background spot-div-white"></div>
				<div class="area-div-background area-div-pic"><img src="assets/images/default-spot-small.jpg" class="mobile-background-pic" id="spot-popup-pic"/></div>
				<div class="spot-id" style="display:none" id="spot-popup-id"></div>
				<div class="main-area-div">
					<a href="#" class="spot-div-name bolder-text default-link" id="popup-name"></a>
					<div class="spot-div-user basic-text" id="popup-user"></div>
					<div class="row no-gutters spot-div-columns-row">
						<div class="col-xs-4">
							<div class="spot-div-column transition-all">
								<span class="glyphicon glyphicon-road" style="font-size:.9em"></span>
								<span class="basic-text" id="popup-routes"></span>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="spot-div-column transition-all">
								<span class="glyphicon glyphicon-stats"></span>
								<span class="basic-text" id="popup-diff"></span>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="spot-div-column transition-all" style="border-right:none">
								<span class="glyphicon glyphicon-thumbs-up"></span>
								<span class="basic-text" id="popup-likes"></span>
							</div>
						</div>
					</div>
					<div class="spot-div-types basic-text" id="popup-type"></div>
				</div>
			</div>
			<div id="popup-confirm"></div>
		</div>
		<!-- popup for areas -->
		<div id="area-popup" class="mobile-popup">
			<div id="area-popup-div">
				<div class="basic-text area-popup-header">UNCONFIRMED SPOTS IN AREA</div>
				<div id="area-spot-list"></div>
			</div>
		</div>
		<!-- popup for gyms -->
		<div id="indoor-popup" class="mobile-popup">
			<div class="map-popup">
				<div class="area-div-background spot-div-white"></div>
				<div class="area-div-background area-div-pic"><img src="assets/images/gym-default-small.jpg" class="mobile-background-pic" id="indoor-popup-pic"/></div>
				<div class="spot-id" style="display:none" id="indoor-popup-id"></div>
				<div class="main-area-div">
					<a href="#" class="spot-div-name bolder-text default-link" id="indoor-popup-name"></a>
					<div class="spot-div-user basic-text" id="indoor-popup-location"></div>
					<div class="spot-div-types basic-text" id="indoor-popup-type"></div>
					<div class="indoor-div-likes">
						<span class="glyphicon glyphicon-thumbs-up" style="margin-right:3px;font-size:1.1em"></span>
						<span class="basic-text" id="indoor-popup-likes"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- list container -->
<div class="container-fluid mobile-explore-container" id="mobile-explore-list">
	<!-- list filters -->
	<div id="list-filters">
		<div class="row no-gutters">
			<div class="col-xs-4 list-filter" id="routes-filter">
				<div class="inactive-filter"></div>
				<div class="list-filter-div transition-all" style="border-left:1px solid rgba(0, 0, 0, 0.2);background:rgba(0, 0, 0, 0.4);color:white">
					<span class="glyphicon glyphicon-road list-filter-icon"></span>
					<span class="list-filter-type basic-text">Routes</span>
					<span class="glyphicon glyphicon-triangle-top list-filter-triangle"></span>
				</div>
				<div class="list-filter-menu">
					<div class="list-filter-option descending basic-text" style="color:white;background:rgba(0, 0, 0, 0.8)">Most Routes</div>
					<div class="list-filter-option ascending basic-text">Least Routes</div>
				</div>
			</div>
			<div class="col-xs-4 list-filter" id="diff-filter">
				<div class="inactive-filter"></div>
				<div class="list-filter-div transition-all">
					<span class="glyphicon glyphicon-stats list-filter-icon"></span>
					<span class="list-filter-type basic-text">Difficulty</span>
					<span class="glyphicon glyphicon-triangle-bottom list-filter-triangle"></span>
				</div>
				<div class="list-filter-menu">
					<div class="list-filter-option descending basic-text">Most Difficult</div>
					<div class="list-filter-option ascending basic-text">Least Difficult</div>
				</div>
			</div>
			<div class="col-xs-4 list-filter" id="popularity-filter">
				<div class="list-filter-div transition-all">
					<span class="glyphicon glyphicon-thumbs-up list-filter-icon"></span>
					<span class="list-filter-type basic-text">Popularity</span>
					<span class="glyphicon glyphicon-triangle-bottom list-filter-triangle"></span>
				</div>
				<div class="list-filter-menu">
					<div class="list-filter-option descending basic-text" id="most-popular-option">Most Popular</div>
					<div class="list-filter-option ascending basic-text">Least Popular</div>
				</div>
			</div>
		</div>
		<div id="spot-num"></div>
	</div>
	<!-- spot list -->
	<div id="spot-list">
		<div id='blank-spot-list' class='basic-text' style='color:rgba(0, 0, 0, 0.4);font-size:2.5em'>Zoom in to discover spots!</div>
	</div>
</div>
<!-- list switch -->
<div id="mobile-container-switch">
	<div class="row no-gutters">
		<div class="col-xs-6">
			<div class="container-switch-div basic-text" id="mobile-list" style="border-right:1px solid rgba(0, 0, 0, 0.2)">View List</div>
		</div>
		<div class="col-xs-6">
			<div class="container-switch-div basic-text" id="mobile-map">View Map</div>
		</div>
	</div>
</div>
<div id="mouse-position" style="display:none"></div>
<script src="assets/js/ol.js"></script>
<script>
//get rid of page load overlay, reveal hidden popups
setTimeout(function() { doneLoadingPage() }, 100);
setTimeout(function() { $("#explore-map-cover").fadeOut(); }, 1000);
function doneLoadingPage() {
	$(".hide-on-page-load").show();
}
adjustColumnHeight($("#map"));
adjustSpotListHeight($("#spot-list"));
function adjustColumnHeight(column) {
	var winHeight = $(window).height() - ($("#navbar").height() + $("#map-search-bar").height() + $("#mobile-container-switch").height() + $("#main-map-filters").height());
	column.css("height", winHeight + "px");
}
function adjustSpotListHeight(column) {
	var winHeight = $(window).height() - ($("#navbar").height() + $("#map-search-bar").height() + $("#mobile-container-switch").height() + $("#main-map-filters").height() + $("#spot-num").height());
	column.css("height", winHeight + "px");
}
$(window).resize(function() {
	adjustColumnHeight($("#map"));
	adjustSpotListHeight($("#spot-list"));
});
//----------------------------------------|
// global vars                            |
//----------------------------------------|
//array holds list sort options, 0 is order, 1 is type
var sortOptions = ["Descending", "routes"];
//holds already loaded coordinates
var loadedPoints = [];
//global var holding last viewed area's features
var areaSpots = Array();
//array that holds all features in filterSource, for use in spot list sidebar
var spotListFeatures = Array();
//holds main filter types
var spotFilterType;
//holds all selected filters
var selectedFilters = {"type": Array(), "difficulty": Array()};
//holds current ajax spot/gym request
var currentAjaxRequest = null;
//get/set active filters
getFilterSession();

/* ********************************
   --------------------------------
   |    Mobile Container Swaps    |
   ________________________________
   ********************************
*/
 $(".container-switch-div").click(function() {
 	var thisId = $(this).attr('id');
 	if(thisId == "mobile-list") {
 		var thisContainer = $("#mobile-explore-map");
 		var swapContainer = $("#mobile-explore-list");
 	} else {
 		var thisContainer = $("#mobile-explore-list");
 		var swapContainer = $("#mobile-explore-map");
 	}
 	thisContainer.fadeOut(400, function() {
 		swapContainer.css("z-index", "2");
 		thisContainer.css("z-index", "1").show();
 	});
 	if(spotListFeatures.length < 1) {
 		$("#spot-list").html("<div id='blank-spot-list' class='basic-text' style='color:rgba(0, 0, 0, 0.4);font-size:2em'>Finds spots on the map first to see a list view</div>")
 	}
 });
/* ********************************
   --------------------------------
   |       Map Filters Div  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Main climbing choice filters           |
//----------------------------------------|
//handle click on main filter-types, change spotFilterType to current spot type choice
$(".climbing-choice-div").click(function() {
	if(spotFilterType != $(this).attr('id')) {
		$(".inside-climbing-choice").css({
			background: "rgba(0, 0, 0, 0.5)",
			color: "rgba(255, 255, 255, 0.3)"
		});
		$(this).find(".inside-climbing-choice").css({
			color: "white",
			background: "rgba(0,0,0, 0.1)"
		});
		spotFilterType = $(this).attr('id');
		setFilterSession(spotFilterType);
		//grays out list filters if indoor is selected
		inactiveListFilters();
		if(map.getView().getZoom() >= 8) {
			clearFeatures();
			loadMultipleCoords();
			//set specific filters
			setSpecificFilters(spotFilterType, true);
		} 
		//reset chosen filters
		selectedFilters.difficulty = [];
		selectedFilters.type = [];
		//hide all popups
		hideAllPopups();
		//reset cluster data source
		setClusterSource();
		//deactivate/activate indoor select, it screws with spot select
		if(spotFilterType == "Indoor") {
			indoorSelect.setActive(true);
		} else {
			indoorSelect.setActive(false);
		}
	}
});
//determine active filters
function getFilterSession() {
	var activeFilter = sessionStorage.getItem('main_map_filter');
	if(!activeFilter) {
		sessionStorage.setItem('main_map_filter', 'Ropes');
		activeFilter = "Ropes";
	}
	$("#" + activeFilter).find(".inside-climbing-choice").css({
		color: "white",
		background: "rgba(0,0,0, 0.1)"
	});
	spotFilterType = activeFilter;
	inactiveListFilters(true);
	//set specific filters
	setSpecificFilters(spotFilterType, false);
}
//set new session on any filter changed
function setFilterSession(type) {
	sessionStorage.setItem('main_map_filter', type);
}
//on main filter click, this will change the specific filters depending on type
function setSpecificFilters(type, showFilters) {
	var allTypeHTML = '<div class="specific-filter-choice">' +
							'<div class="specific-filter-choice-text all-type">all types</div>' +
						'</div>';
	var allDiffHTML = '<div class="specific-filter-choice">' +
						'<div class="specific-filter-choice-text all-difficulty">all difficulties</div>' +
					'</div>';
	if(type == "Ropes") {
		var showFilter = $("#rope-filters");
		var hideFilter = $("#boulder-filters");
		//append 'all' div
		$(".specific-filter-choices-type").html(allTypeHTML);
		$(".specific-filter-choices-difficulty").html(allDiffHTML);
	} else if(type == "Bouldering") {
		var hideFilter = $("#rope-filters");
		var showFilter = $("#boulder-filters");
		//append all div
		$(".specific-filter-choices-difficulty").html(allDiffHTML);
	} else {
		showFilters = false;
		$("#boulder-filters").hide();
		$("#rope-filters").hide();
	}
	if(showFilters) {
		showFilter.show();
		hideFilter.hide();
	}
}
//----------------------------------------|
// Specific filters (under filter div)    |
//----------------------------------------|
//show select menu on click
$(".specific-map-filter").click(function(e) {
	e.stopPropagation();
	var menu = $(this).parent().find(".specific-map-filter-menu");
	if(menu.css("display") != "none") {
		menu.hide();
		$(this).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-bottom filter-select-icon");
	} else {
		menu.show();
		$(this).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-top filter-select-icon");
	}
});
//adds new filter
$(".specific-menu-choice").click(function(e) {
	e.stopPropagation();
	//get text of new filter, and get type for accessing selectedFilters obj
	var newFilter = $(this).find(".specific-menu-text").text();
	//find id from this menu, get filterType from id
	var menuId = $(this).parents(".specific-map-filter-menu").attr('id');
	if(menuId == "specific-diff-filter") {
		var filterType = "difficulty";
		var filterIconClass = ".specific-diff-icon";
	} else {
		var filterType = "type";
		var filterIconClass = ".specific-type-icon";
	}
	//if all is chosen
	if(newFilter == "All") {
		selectedFilters[filterType] = [];
		//remove all checkmarks in this menu
		$(filterIconClass).each(function() {
			$(this).hide();
		});
		//show checkmark next to all icon
		$(this).find(".specific-menu-check").show();
	//if new filter is not in type array, add to chosen filters and array
	} else if(selectedFilters[filterType].indexOf(newFilter) === -1) {
		$(this).find(".specific-menu-check").show();
		selectedFilters[filterType].push(newFilter);
	//remove from list and array if filter is already in array
	} else {
		var filterIndex = selectedFilters[filterType].indexOf(newFilter);
		selectedFilters[filterType].splice(filterIndex, 1);
		$(this).find(".specific-menu-check").hide();
	}
	var filterLength = selectedFilters[filterType].length;
	//make filterType's first letter capitalized
	var filterText = filterType.substring(0, 1).toUpperCase() + filterType.substring(1);
	//make display number of active filters in specific-map-filter
	if(filterLength > 0) {
		//remove 'all' checkmark
		$(this).parent().find('.all-filter').next().hide();
		$(this).parents(".specific-filter-column").find(".specific-filter-name").text(filterText + " " + "(" + filterLength + ")");
	} else {
		//add 'all' checkmark
		$(this).parent().find('.all-filter').next().show();
		$(this).parents(".specific-filter-column").find(".specific-filter-name").text(filterText);
	}
	//filter features
	filterFeatures();
});
//close menu if one is open and map is clicked
$(".specific-filters-row").click(function() {
	$(".specific-map-filter-menu").hide();
	$(".specific-map-filter").each(function() {
		$(this).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-triangle-bottom filter-select-icon");
	});
})
/* ********************************
   --------------------------------
   |       List Filters Div  	  |
   ________________________________
   ********************************
*/
//drop down list on click
$(".list-filter").click(function() {
	var menu = $(this).find(".list-filter-menu");
	if(menu.css("display") == "none") {
		if(spotFilterType != "Indoor") {
			$(this).find(".list-filter-menu").stop().slideDown("fast");
		} else {
			if($(this).attr('id') == "popularity-filter") {
				$(this).find(".list-filter-menu").stop().slideDown("fast");
			}
		}
	} else {
		$(this).find(".list-filter-menu").stop().slideUp("fast");
	}
});
//add new params to sortOptions, adjust css
$(".list-filter-option").click(function() {
	//set new sort option type
	var parentDiv = $(this).parents(".list-filter");
	var divType = parentDiv.find(".list-filter-type").text();
	switch(divType) {
		case "Popularity":
			sortOptions[1] = "likes";
			break;
		case "Difficulty":
			sortOptions[1] = "avg_diff";
			break;
		case "Routes":
			sortOptions[1] = "routes";
			break;
	}
	//set new sort option order
	if($(this).hasClass("ascending")) {
		sortOptions[0] = "Ascending";
	} else {
		sortOptions[0] = "Descending";
	}
	//changes to css
	$(".list-filter-option").css({
		color: "black",
		background: "transparent"
	});
	$(this).css({
		background: "rgba(0, 0, 0, 0.8)",
		color: "white"
	});
	$(".list-filter-div").css({
		background: "transparent",
		color: "rgba(0, 0, 0, 0.8)"
	})
	parentDiv.find(".list-filter-div").css({
		background: "rgba(0, 0, 0, 0.4)",
		color: "white"
	});
	//change glyphicon to reflect direction of sort
	$(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-bottom list-filter-triangle");
	if(sortOptions[0] == "Descending") {
		parentDiv.find(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-top list-filter-triangle");
	} else {
		parentDiv.find(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-bottom list-filter-triangle");
	}
	$(".list-filter-menu").hide();
	//resort spot list
	sortListFeatures();
});
//if spotFilterType is Indoor, make routes and diff filters inactive
function inactiveListFilters() {
	if(spotFilterType == "Indoor") {
		$(".inactive-filter").show();
		$(".list-filter-div").css({
			background: "transparent",
			color: "rgba(0, 0, 0, 0.8)"
		});
		$(".list-filter-option").css({
			color: "black",
			background: "transparent"
		});
		$("#most-popular-option").css({
			background: "rgba(0, 0, 0, 0.8)",
			color: "white"
		});
		$('#popularity-filter').find(".list-filter-div").css({
			background: "rgba(0, 0, 0, 0.4)",
			color: "white"
		});
		sortOptions[0] = "Descending";
		sortOptions[1] = "likes";
		//change glyphicon to reflect direction of sort
		$(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-bottom list-filter-triangle");
		if(sortOptions[0] == "Descending") {
			$('#popularity-filter').find(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-top list-filter-triangle");
		} else {
			$('#popularity-filter').find(".list-filter-triangle").removeClass().addClass("glyphicon glyphicon-triangle-bottom list-filter-triangle");
		}
	} else {
		if(sortOptions[1] == "likes") {
			$(".inactive-filter").hide();
			$(".list-filter-div").css({
				background: "transparent",
				color: "rgba(0, 0, 0, 0.8)"
			});
			$('#popularity-filter').find(".list-filter-div").css({
				background: "rgba(0, 0, 0, 0.4)",
				color: "white"
			});
		}
	}
}
/******************************************
*******************************************
*******************************************
-------------------------------------------
 			      MAP
-------------------------------------------
*******************************************
*******************************************
*******************************************/
//----------------------------------------|
// set up sources/styles/layers           |
//----------------------------------------|
//set up sources for different layers
var spotSource = new ol.source.Vector();
var filterSource = new ol.source.Vector();
var modifyCollection = new ol.Collection();
var areaSource = new ol.source.Vector();
var indoorSource = new ol.source.Vector();
var modifySource = new ol.source.Vector({
	features: modifyCollection
});
//styles for different layers
function setFeatureStyle(image, visible) {
	var style = new ol.style.Style({
		image: new ol.style.Icon({
			src: image,
			anchor: [.45, .1],
			anchorOrigin: "bottom-left",
			scale: .4,
			opacity: visible
		})
	});
	return [style];
}
function setAreaStyle(feature, resolution) {
	var features = feature.get('features');
	var spotsInCluster = getSpotsInCluster(features);
	var radius = determineAreaClusterSize(spotsInCluster);
	var style = new ol.style.Style({
		image: new ol.style.Circle({
			radius: radius,
			fill: new ol.style.Fill({
				color: "rgba(0, 0, 0, 0.3)"
			})
		}),
		text: new ol.style.Text({
			text: "?",
			fill: new ol.style.Fill({
				color: "#ffffff",
			}),
			font: "25px 'dosis', sans-serif"
		})
	});
	return style;
}
function setAreaHoverStyle(feature, resolution) {
	var features = feature.get('features');
	var spotsInCluster = getSpotsInCluster(features);
	var radius = determineAreaClusterSize(spotsInCluster) + 10;
	var style = new ol.style.Style({
		image: new ol.style.Circle({
			radius: radius,
			fill: new ol.style.Fill({
				color: "rgba(0, 0, 0, 0.2)"
			}),
			stroke: new ol.style.Stroke({
				color: "rgba(0, 0, 0, 0.5)",
				width: 2
			})
		}),
		text: new ol.style.Text({
			text: "?",
			fill: new ol.style.Fill({
				color: "#ffffff",
			}),
			font: "25px 'dosis', sans-serif"
		})
	});
	return style;
}
function determineAreaClusterSize(spots) {
	var zoomLvl = map.getView().getZoom();
	var radius = 20;
	if(spots <= 10) {
		radius = 35;
	} else if(spots > 10 && spots <= 20) {
		radius = 45;
	} else if(spots > 20 && spots <= 50) {
		radius = 55;
	} else {
		radius = 65;
	}
	var newRadius = radiusOnZoom(radius, zoomLvl);
	return newRadius;
}
function radiusOnZoom(radius, zoomLvl) {
	if(zoomLvl <= 9) {
		return radius;
	} else {
		if(zoomLvl > 12) {
			var addMultiplier = 1;
		} else {
			var addMultiplier = .5;
		}
		var multiplier = 1;
		var difference = zoomLvl - 9;
		for(var i=0;i<difference;i++) {
			multiplier += addMultiplier;
		}
	}
	return radius * multiplier;
}
function getSpotsInCluster(features) {
	var spotsInCluster = 0;
	for(var i=0;i<features.length;i++) {
		spotsInCluster += parseInt(features[i].get('size'));
	}
	return spotsInCluster;
}
//set up vector layers for each different source
var spotLayer = new ol.layer.Vector({
	source: spotSource,
	style: setFeatureStyle("assets/images/blue-pin.png", 1)
});
//this layer holds all the features that match any specific filters
var filterLayer = new ol.layer.Vector({
	source: filterSource,
	style: setFeatureStyle("assets/images/blue-pin.png", 1)
});
//this layer is purely for modifying features, feats will be copied over when modifying or adding
var modifyLayer = new ol.layer.Vector({
	source: modifySource,
	style: setFeatureStyle("assets/images/black-pin.png", 1)
});
//indoor layer
var indoorLayer = new ol.layer.Vector({
	source: indoorSource,
	style: setFeatureStyle("assets/images/blue-pin.png", 1)
});
//raster layer for map
//preload is better, but not sure of long term costs
var rasterLayer = new ol.layer.Tile({
	source: new ol.source.OSM(),
	preload: Infinity
});
var areaClusterSource = new ol.source.Cluster({
	source: areaSource,
	distance: 100
});
var areaLayer = new ol.layer.Vector({
	source: areaClusterSource,
	style: setAreaStyle
});
//filter area sources/layers
var filterAreaSource = new ol.source.Vector();
var filterAreaClusterSource = new ol.source.Cluster({
	source: filterAreaSource,
	distance: 100
});
var filterAreaLayer = new ol.layer.Vector({
	source: filterAreaClusterSource,
	style: setAreaStyle
})
//----------------------------------------|
// set up map controls                    |
//----------------------------------------|
var mousePosControl = new ol.control.MousePosition({
	coordinateFormat: ol.coordinate.createStringXY(4),
	projection: 'EPSG:4326',
	className: "mouse-coords",
	target: document.getElementById("mouse-position")
});
/* ********************************
   --------------------------------
   |        Cluster Layer   	  |
   ________________________________
   ********************************
*/
//set up source and layer
var ropesClusterSource = new ol.source.Cluster({
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: "assets/ropesclusterdata.geojson",
		projection: 'EPSG:4326'
	}),
	distance: 100
});
var boulderingClusterSource = new ol.source.Cluster({
	attributions: [{"name": "clusterSourceTwo"}],
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: "assets/boulderingclusterdata.geojson",
		projection: 'EPSG:4326'
	}),
	distance: 100
});
var gymsClusterSource = new ol.source.Cluster({
	attributions: [{"name": "clusterSourceTwo"}],
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: "assets/gymclusterdata.geojson",
		projection: 'EPSG:4326'
	}),
	distance: 100
});
var clusterLayer = new ol.layer.Vector({
	source: ropesClusterSource,
	style: clusterStyle
});
//on page load change cluster source according to active main filter
setClusterSource();
function clusterStyle(feature, resolution) {
	var featSize = feature.get('features').length;
	if(spotFilterType == "Indoor") { var clusterMultiplier = 1 } else { var clusterMultiplier = 3 }
	var displayText = String((featSize * clusterMultiplier) + "+");
	if(featSize < 2) {
		displayText = "1";
	}
	var style = new ol.style.Style({
		image: new ol.style.Circle({
			radius: determineClusterRadius(featSize, resolution),
			fill: new ol.style.Fill({
				color: "rgba(0, 0, 0, 0.3)"
			})
		}),
		text: new ol.style.Text({
			text: displayText,
			fill: new ol.style.Fill({
				color: "#ffffff",
			}),
			font: "20px 'dosis', sans-serif"
		})
	});
	return style;
}
function setClusterSource() {
	var mainFilter = sessionStorage.getItem("main_map_filter")
	if(mainFilter == "Ropes") {
		clusterLayer.setSource(ropesClusterSource);
	} else if(mainFilter == "Bouldering") {
		clusterLayer.setSource(boulderingClusterSource);
	} else {
		clusterLayer.setSource(gymsClusterSource);
	}
}
function determineClusterRadius(size, resolution) {
	var radius = 15;
	if(size <= 10) {
		radius = 10;
	} else if(size > 10 && size <= 25) {
		radius = 15;
	} else if(size > 25 && size <= 50) {
		radius = 20;
	} else if(size > 50 && size <= 100) {
		radius = 25;
	} else if(size > 100 && size <= 150) {
		radius = 30;
	} else if(size > 150 && size <= 200) {
		radius = 35;
	} else if(size > 200) {
		radius = 40;
	}
	var newRadius = mainClusterRadiusZoom(radius);
	return newRadius;
}
function mainClusterRadiusZoom(radius) {
	var zoomLvl = map.getView().getZoom();
	if(zoomLvl <= 5) {
		return radius;
	} else {
		var addMultiplier = .2;
		var multiplier = 1;
		var difference = zoomLvl - 5;
		for(var i=0;i<difference;i++) {
			multiplier += addMultiplier;
		}
	}
	return radius * multiplier;
}
//----------------------------------------|
//            initialize map 			  |
//----------------------------------------|
var interactions = ol.interaction.defaults({altShiftDragRotate:false, pinchRotate:false});
var map = new ol.Map({
	target: 'map',
	view: new ol.View({
		center: ol.proj.transform([-95.7129, 37.0902], 'EPSG:4326', 'EPSG:3857'),
		zoom: 3,
		maxZoom: 17,
		minZoom: 3,
		interactions: interactions
	}),
	layers: [rasterLayer, clusterLayer, spotLayer, filterLayer, indoorLayer, areaLayer, filterAreaLayer, modifyLayer],
	controls: ol.control.defaults({
		attributeOptions: /* */ ({
			collapsible:false
		})
	}).extend([mousePosControl])
});
if(!sessionStorage.getItem('lat')) {
	//if geolocation set, center map on users location
	navigator.geolocation.getCurrentPosition(function(position) {
		centerMap(position.coords.latitude, position.coords.longitude, 10);
	});
} else {
	var activeLat = sessionStorage.getItem('lat');
	var activeLong = sessionStorage.getItem('long');
	centerMap(activeLat, activeLong, 10);
}
//center map on specified lat, long
function centerMap(lat, long, zoom) {
	map.getView().setCenter(ol.proj.transform([parseFloat(long), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857'));
	map.getView().setZoom(zoom);
}

/* ********************************
   --------------------------------
   |       Spot AJAX Load   	  |
   ________________________________
   ********************************
*/
//after each map move determine features to load in
map.on("moveend", function(e) {
	mapMoveEnd();
});
function mapMoveEnd() {
	var zoomLvl = map.getView().getZoom();
	if(zoomLvl >= 8) {
		//hide overview cluster layer, show specific filters
		hideOverviewLayers(true);
		if(spotLayer.getVisible() || filterLayer.getVisible()) {
			//fetch spots pertaining to current extent
			loadMultipleCoords();
		}
	} else if(zoomLvl >= 6) {
		//show zoom prompt
		resetSpotList();
		//show overview layers
		hideOverviewLayers(false);
		//clear spots from map
		clearFeatures();
	}
}
function clearFeatures() {
	spotSource.clear();
	filterSource.clear();
	areaSource.clear();
	filterAreaSource.clear();
	indoorSource.clear();
	loadedPoints = Array();
}
function hideOverviewLayers(hide) {
	if(hide) {
		clusterLayer.setVisible(false);
		$("#cluster-info-text").hide();
		if(spotLayer.getVisible() || filterLayer.getVisible()) {
			if(spotFilterType == "Ropes") {
				$("#rope-filters").show();
			} else if(spotFilterType == "Bouldering") {
				$("#boulder-filters").show();
			}
		}
	} else {
		currentAjaxRequest.abort();
		$("#map-ajax-gif").hide();
		$(".specific-type-filters").fadeOut();
		clusterLayer.setVisible(true);
		$("#cluster-info-text").show();
		hideAllPopups();
	}
}
//load in spots in map viewport
function loadMultipleCoords() {
	//get left most longitude and right most longitude from current viewport
	var mapCoords = map.getView().calculateExtent(map.getSize());
	var rightLong = transformMapCoords([parseFloat(mapCoords[2])]);
	var leftLong = transformMapCoords([parseFloat(mapCoords[0])]);
	//get first 2 or 3 digits of both coords
	var shortLeftLong = String(leftLong[0]).split(".")[0].substring(1);
	var shortRightLong = String(rightLong[0]).split(".")[0].substring(1);
	//get number of degrees between two longs
	var numDegrees = shortLeftLong - shortRightLong;
	var coordArr = Array();
	//determine longs between left and right long
	if(numDegrees > 1) {
		var amount = 0;
		while(amount <= (numDegrees - 2)) {
			amount += 1;
			coordArr.push(String(shortLeftLong - amount));
		}
	}
	coordArr.push(shortLeftLong, shortRightLong);
	var ftsQuery = "";
	//go through new array of longs, determine if they haven't been loaded and build new query
	for(var i=0;i<coordArr.length;i++) {
		if(loadedPoints.indexOf(coordArr[i]) === -1) {
			loadedPoints.push(coordArr[i]);
			if(ftsQuery == "") {
				ftsQuery = coordArr[i] + "* ";
			} else {
				ftsQuery += coordArr[i] + "* ";
			}
		}
	}
	//send query to db, create new features from coords in query
	if(ftsQuery != "") {
		//create object to pass to server, add filter type
		var longObj = {"long": ftsQuery, "type": spotFilterType};
		//show map spinner
		mapLoadingGifs(true);
		//determine which table to load from
		if(sessionStorage.getItem('main_map_filter') == "Indoor" || spotFilterType == "Indoor") {
			gymAjaxRequest(longObj);
		} else {
			spotAjaxRequest(longObj);
		}
	} else {
		filterFeatures();
	}
}
function spotAjaxRequest(longObj) {
	currentAjaxRequest = $.post("ajax/explore/get_spots.php", longObj, function(data) {
		for(var i=0;i<data.length;i++) {
			//depending on filter use a different avg_diff
			if(sessionStorage.getItem('main_map_filter') == "Ropes") { var avg_diff = data[i].ropes_avg_diff } else { var avg_diff = data[i].bouldering_avg_diff }
			//this call returns spots and areas, if size is not an attribute its a spot
			if(data[i].size) {
				newAreaFeature(data[i].latitude, data[i].longitude, data[i].size, data[i].id);
			} else {
				newFeature(data[i].latitude, data[i].longitude, data[i].name, data[i].id, data[i].type_climbing, data[i].confirmed, avg_diff, data[i].num_routes, data[i].upvotes, data[i].top_pic, data[i].description, data[i].user_name, data[i].region, data[i].area_id);
			}
		}
		//run filters
		filterFeatures();
		//hide map spinner
		mapLoadingGifs();
	}, 'json');
}
function gymAjaxRequest(longObj) {
	currentAjaxRequest = $.post("ajax/explore/get_gyms.php", longObj, function(data) {
		for(var i=0;i<data.length;i++) {
			newGymFeature(data[i].latitude, data[i].longitude, data[i].name, data[i].description, data[i].city, data[i].state, data[i].phonenum, data[i].upvotes, data[i].top_pic, data[i].id, data[i].url, data[i].address);
		}
		//run filters
		filterFeatures();
		//hide map spinner
		mapLoadingGifs(false);
	}, 'json');
}
function mapLoadingGifs(show) {
	if(show) {
		$("#map-ajax-gif").show();
		$("#spot-list").html("<img src='assets/loaders/ripple.gif' class='loader-ripple spot-list-loader'/>");
	} else {
		$("#map-ajax-gif").hide();
		$(".spot-list-loader").remove();
	}
}
var transformMapCoords = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');
var transform = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
//new spot feature
function newFeature(lat, long, name, id, type, confirmed, avg_diff, routes, likes, top_pic, description, user_name, region, area_id) {
	var feature = new ol.Feature();
	feature.set('name', name);
	feature.setId(id);
	feature.set('type', type);
	feature.set('confirmed', confirmed);
	feature.set('avg_diff', avg_diff);
	feature.set('routes', routes);
	feature.set('likes', likes);
	feature.set('top_pic', top_pic);
	feature.set('description', description);
	feature.set('user_name', user_name);
	feature.set('region', region);
	feature.set('area_id', area_id);
	var coords = transform([parseFloat(long), parseFloat(lat)]);
	var geom = new ol.geom.Point(coords);
	feature.setGeometry(geom);
	//if spots area_id is not null, hide feature on map (will still appear in spot list as 'unconfirmed')
	if(area_id) {
		feature.setStyle(setFeatureStyle("assets/images/blue-pin.png", 0));
	}
	spotSource.addFeature(feature);
}
//new area feature
function newAreaFeature(lat, long, size, id) {
	var feature = new ol.Feature();
	feature.setId(id);
	feature.set('size', size);
	var coords = transform([parseFloat(long), parseFloat(lat)]);
	var geom = new ol.geom.Point(coords);
	feature.setGeometry(geom);
	areaSource.addFeature(feature);
}
function newGymFeature(lat, long, name, desc, city, state, phonenum, upvotes, top_pic, id, url, address) {
	var feature = new ol.Feature();
	feature.set('name', name);
	feature.setId(id);
	feature.set('likes', upvotes);
	feature.set('top_pic', top_pic);
	feature.set('description', desc);
	feature.set('state', state);
	feature.set('city', city);
	feature.set('phonenum', phonenum);
	feature.set('url', url);
	feature.set('address', address);
	var coords = transform([parseFloat(long), parseFloat(lat)]);
	var geom = new ol.geom.Point(coords);
	feature.setGeometry(geom);
	indoorSource.addFeature(feature);
}
/* ********************************
   --------------------------------
   |      Feature Interaction     |
   ________________________________
   ********************************
*/
//add new select interaction to spot layer
var select = new ol.interaction.Select({
	layers: [spotLayer, filterLayer],
	condition: ol.events.condition.singleClick,
	style: setFeatureStyle("assets/images/black-pin.png", 1)
});
map.addInteraction(select);
//events for selection
var selected = select.getFeatures();
selected.on('add', function(e) {
	selectedAreas.clear();
	$("#area-popup").hide();
	var feature = e.target.item(0);
	//change popup info
	popupSpotHTML(feature);
	$("#spot-map-popup").show();
});
selected.on('remove', function() {
	$("#spot-map-popup").hide();
});
//----------------------------------------|
//                 Popup			      |
//----------------------------------------|
//change spot popup html on hover
function popupSpotHTML(feature) {
	//break down feature keys into vars
	var name = feature.get('name');
	var id = feature.getId();
	var type = feature.get('type');
	var confirmed = feature.get('confirmed');
	var diff = feature.get('avg_diff');
	var routes = feature.get('routes');
	var topPic = feature.get('top_pic');
	var likes = feature.get("likes");
	var userName = feature.get('user_name')
	//add id
	$("#spot-popup-id").text(id);
	$("#popup-name").text(name);
	//check if user uploaded
	if(userName) {
		$("#popup-user").html('Discovered by <a href="profile/' + userName + '" class="default-link">' + element.get('user_name') + '</a> in ' + feature.get('region'));
	} else {
		$("#popup-user").html('Discovered by climbandseek in ' + feature.get('region'));
	}
	//set average difficulty
	if(!diff) {
		diff = "n/a";
	}
	$("#popup-diff").text(diff);
	//set number of routes
	$("#popup-routes").text(routes);
	//set new link
	$("#popup-name").attr("href", "spot/" + id + "/" + feature.get('name'));
	if(!likes) {
		likes = 0;
	}
	$("#popup-likes").text(likes);
	//set type color and type
	var newTypes = determineType(type);
	$("#popup-type").css("background", newTypes[1]).text(newTypes[0]);
	if(feature.get('confirmed') == 0 || feature.get('confirmed') == null) {
		$("#popup-confirm").html('<div class="popup-div-unconfirmed transition-all confirm-spot-location">' +
			'<div class="basic-text">UNCONFIRMED</div>' +
		'</div>');
	} else {
		$("#popup-confirm").html('<div class="popup-div-unconfirmed transition-all">' +
			'<div class="basic-text">CONFIRMED <span class="glyphicon glyphicon-ok-circle"></span></div>');
	}
	//add spot main pic as background image
	if(topPic) {
		var imgSrc = S3URL + "/" + topPic + "s";
	} else {
		var imgSrc = "assets/images/default-spot-small.jpg";
	}
	$("#spot-popup-pic").attr('src', imgSrc)
}
function typeColor(type) {
	switch(type) {
		case "Bouldering":
			return "#4ad153";
			break;
		case "Top Rope":
			return "#ff5555";
			break;
		case "Sport":
			return "#398bc6";
			break;
		case "Aid":
			return "#f7c308";
			break;
		case "Trad":
			return "#398bc6";
			break;
		case "Alpine":
			return "#cf7dfd";
			break;
		case "Sport/Trad":
			return "#398bc6";
			break;
		case "Ice":
			return "#cf7dfd";
			break;
		default:
			return "#404040";
			break;
	}
}
//parse types string, shorten if too long add full to title
function determineType(types) {
	if(!types) { return ["No Routes", "#404040"] }
	var typeArr = types.split(", ");
	var color = typeColor(typeArr[0]);
	if(typeArr[1] == "") {
		$("#popup-type").attr("title", "");
		return [typeArr[0], color];
	} else {
		var typesReturn = [types.substring(0, types.length - 2), color];
		$("#popup-type").attr("title", types);
		return typesReturn;
	}
}
//----------------------------------------|
//             Indoor Select		      |
//----------------------------------------|
var indoorSelect = new ol.interaction.Select({
	layers: [indoorLayer],
	style: setFeatureStyle("assets/images/black-pin.png", 1),
	condition: ol.events.condition.singleClick
});
map.addInteraction(indoorSelect);
//events for selection
var indoorSelected = indoorSelect.getFeatures();
indoorSelected.on('add', function(e) {
	var feature = e.target.item(0);
	//change popup info
	indoorPopupHTML(feature);
	$("#indoor-popup").show();
});
indoorSelected.on('remove', function() {
	$("#indoor-popup").hide();
});
//deactivate indoor select if not searching indoor, screws up spot selection
if(spotFilterType != "Indoor") {
	indoorSelect.setActive(false);
}
function indoorPopupHTML(feature) {
	//break down feature keys into vars
	var name = feature.get('name');
	var id = feature.getId();
	var topPic = feature.get('top_pic');
	var city = feature.get('city');
	var likes = feature.get('likes');
	var address = feature.get('address');
	//add id
	$("#indoor-popup-id").text(id);
	//shorten name if too long add a title
	$("#indoor-popup-name").text(name);
	$("#indoor-popup-name").attr("title", "");
	//set new link
	$("#indoor-popup-name").attr("href", "indoor/" + id + "/" + feature.get('name'));
	//add spot main pic as background image
	if(topPic) {
		var imgSrc = S3URL + "/" + topPic + "s";
	} else {
		var imgSrc = "assets/images/gym-default-small.jpg";
	}
	$("#indoor-popup-pic").attr('src', imgSrc);
	if(address) {
		var splitAdd = address.split(',');
		try {
			address = splitAdd[0];
		} catch(e) {
			address = address;
		}
	}
	$("#indoor-popup-location").text(address);
	$("#indoor-popup-likes").text(likes);
	$("#indoor-popup-type").text(city);
}
//----------------------------------------|
//             Area Select		          |
//----------------------------------------|
//add new select interaction to spot layer
var areaSelect = new ol.interaction.Select({
	layers: [areaLayer, filterAreaLayer],
	style: setAreaHoverStyle,
	condition: ol.events.condition.click
});
map.addInteraction(areaSelect);
//events for selection
var selectedAreas = areaSelect.getFeatures();
selectedAreas.on('add', function(e) {
	var feature = e.target.item(0);
	activateAreaOverlay(feature);
	$("#area-popup").show();
});
selectedAreas.on('remove', function() {
	$("#area-popup").hide();
});
function activateAreaOverlay(feature) {
	//clear and scroll list to top
	$("#area-spot-list").animate({
		scrollTop:0
	}, 200);
	$("#area-spot-list").html("");
	//get spots in area, add to overlay list
	areaSpots = getAreaSpots(feature);
	if(areaSpots.length < 20) {
		var areaLength = areaSpots.length;
	} else {
		var areaLength = 20;
	}
	for(var i=0;i<areaLength;i++) {
		areaSpotListDiv(areaSpots[i]);
	}
}
//----------------------------------------|
//            Area Spot List		      |
//----------------------------------------|
//finds unconfirmed spots in extent that belongs to areas in a cluster
function getAreaSpots(cluster) {
	//get current map extent
	var mapExtent = map.getView().calculateExtent(map.getSize());
	//get array of area id's in cluster
	var areaArr = getAreaIds(cluster.get('features'));
	//determine which layer to search through
	var source = spotSource;
	spotLayer.getVisible() ? source = spotSource : source = filterSource;
	var areaSpots = Array();
	source.forEachFeatureInExtent(mapExtent, function(feature) {
		for(var i=0;i<areaArr.length;i++) {
			if(feature.get('area_id') == areaArr[i]) {
				areaSpots.push(feature);
				break;
			}
		}
	});
	return areaSpots;
}
//finds individual area ids in a cluster
function getAreaIds(features) {
	var areaArr = Array();
	for(var i=0;i<features.length;i++) {
		areaArr.push(features[i].getId());
	}
	return areaArr;
}
function areaSpotListDiv(feature) {
	//get & set element vars depending on length/type etc
	var typeArr = feature.get('type').split(", ");
	if(typeArr[1] == "") {
		var type = typeArr[0];
	} else {
		var type = feature.get('type').substring(0, feature.get('type').length - 2);
	}
	//set background color for types
	var color = typeColor(typeArr[0]);
	var diff = "n/a";
	feature.get('avg_diff') ? diff = feature.get('avg_diff') : diff = "n/a";
	var likes = "0";
	feature.get('likes') ? likes = feature.get('likes') : likes = "0";
	if(feature.get('name').length > 25) {
		var name = feature.get('name').substring(0, 23) + "...";
	} else {
		var name = feature.get('name');
	}
	if(feature.get('top_pic')) {
		var topPic = "url('" + S3URL + "/" + topPic + "s" + ")";
	} else {
		var topPic = "assets/images/default-spot-small.jpg";
	}
	var areaSpotDiv = '<div class="area-spot-div transition-all">' +
						'<div class="area-div-background area-div-white"></div>' +
						'<div class="area-div-background area-div-more-white"></div>' +
						'<div class="area-div-background area-div-pic"><img src="' + topPic + '" class="area-background-pic"/></div>' +
						'<div class="main-area-div">' +
							'<div class="area-spot-id" style="display:none">' + feature.getId() + '</div>' +
							'<div style="text-align:center">' +
						  	'<a href="spot/' + feature.getId() + "/" + feature.get('name') + '" class="area-div-name bolder-text default-link" title="' + feature.get('name') + '">' + name + '</a>' +
						  	'</div>' +
					  		'<div class="row no-gutters spot-div-columns-row">' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column area-div-column transition-all">' +
										'<span class="glyphicon glyphicon-road" style="font-size:.9em"></span>' +
										'<span class="basic-text"> ' + feature.get('routes') + '</span>' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column area-div-column transition-all">' +
										'<span class="glyphicon glyphicon-stats"></span>' +
										'<span class="basic-text"> ' + diff + '</span>' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column area-div-column transition-all" style="border-right:none">' +
										'<span class="glyphicon glyphicon-thumbs-up"></span>' +
										'<span class="basic-text"> ' + likes + '</span>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="spot-div-types area-div-types basic-text" style="background:' + color + '">' + type + '</div>' +
							'<div class="spot-div-unconfirmed area-div-unconfirmed transition-all confirm-spot-location" data-toggle="tooltip" data-placement="bottom" title="Have you been here? Click to confirm!" style="width:100%;border-radius:0">' +
								'<div class="basic-text confirm-spot-location">CONFIRM?</div>' +
							'</div>' +
						'</div>' +
					  '</div>';
	$("#area-spot-list").append(areaSpotDiv);
}
$(document).on('mouseenter', ".area-spot-div", function() {
	$(this).find(".area-div-more-white").stop().fadeIn();
});
$(document).on('mouseleave', ".area-spot-div", function() {
	$(this).find(".area-div-more-white").stop().fadeOut();
});
//load more spot divs
$("#area-spot-list").scroll(function() {
	var scrollBar = $(this).scrollTop();
	var scrollHeight = ($(this).prop('scrollHeight') * .75) - 500;
	if(scrollBar >= $(this).prop("scrollHeight") - 1200) {
		if(areaSpots.length > $(".area-spot-div").length) {
			var i = 0;
			var length = $(".area-spot-div").length;
			//loading gif
			var areaLoader = "<div id='area-loader' style='text-align:center'>" +
								"<img src='assets/loaders/ripple.gif' class='loader-ripple'/>" +
							"</div>"
			$(this).append(areaLoader);
			while(i < 20) {
				if(areaSpots[length + i] !== undefined) {
					areaSpotListDiv(spotListFeatures[length + i]);
					i++;
				} else {
					break;
				}
			}
			$("#area-loader").remove();
		}
	}
});
function hideAllPopups() {
	selectedAreas.clear();
	selected.clear();
	indoorSelected.clear();
	$("#area-popup").hide();
	$("#spot-map-popup").hide();
	$("#indoor-popup").hide();
}
/* ********************************
   --------------------------------
   |     Spot List/Filters        |
   ________________________________
   ********************************
*/
//----------------------------------------|
//            Filter functions		      |
//----------------------------------------|
//puts features passing filter regex into filterLayer, hides spotLayer if filters are active
function filterFeatures() {
	var filterTypeLength = selectedFilters.type.length;
	var filterDiffLength = selectedFilters.difficulty.length;
	var mapExtent = map.getView().calculateExtent(map.getSize());
	//check if active features
	if(filterTypeLength !== 0 || filterDiffLength !== 0) {
		//clear previous selection
		filterSource.clear();
		filterAreaSource.clear();
		spotLayer.setVisible(false);
		areaLayer.setVisible(false);
		var filterRegex = new RegExp(getFilterRegex(), "i");
		//for other choice
		var otherFilter = false;
		for(var i=0;i<selectedFilters.type.length;i++) {
			if(selectedFilters.type[i] == "Other") {
				otherFilter = true;
				break;
			}
		}
		//test features in extent against current filters
		//if neither filter type is empty, feature must pass both filters
		if(filterTypeLength !== 0 && filterDiffLength !== 0) {
			spotSource.forEachFeatureInExtent(mapExtent, function(feature) {
				if(!feature.get('type')) {
					var spotType = "none";
				} else {
					var spotType = feature.get('type').toLowerCase();
				}
				if(filterRegex.test(spotType) && filterRegex.test(feature.get('avg_diff'))) {
					filterSource.addFeature(feature);
				} else if(otherFilter) {
					//if other is chosen as filter, go through each filter type, if it doesn't match any, add
					if(spotType !== "sport, " && spotType !== "trad, " && spotType !== "aid, " && spotType !== "ice, " && spotType !== "alpine, " && spotType !== "top rope, " && spotType !== "sport, trad" && filterRegex.test(feature.get('avg_diff'))) {
						filterSource.addFeature(feature);
					}
				}
			});
		} else {
			spotSource.forEachFeatureInExtent(mapExtent, function(feature) {
				if(!feature.get('type')) {
					var spotType = "none";
				} else {
					var spotType = feature.get('type').toLowerCase();
				}
				if(filterRegex.test(spotType) || filterRegex.test(feature.get('avg_diff'))) {
					filterSource.addFeature(feature);
				} else if(otherFilter) {
					if(spotType !== "sport, " && spotType !== "trad, " && spotType !== "aid, " && spotType !== "ice, " && spotType !== "alpine, " && spotType !== "top rope, " && spotType !== "sport/trad, " && spotType !== "none") {
						filterSource.addFeature(feature);
					}
				}
			});
		}
		//filter areas
		filterAreas();
		//save new spotListFeatures array
		spotListFeatures = filterSource.getFeatures();
	} else {
		if(!spotLayer.getVisible()) {
			spotLayer.setVisible(true);
			areaLayer.setVisible(true);
			filterAreaSource.clear();
		}
		if(spotFilterType == "Indoor") {
			spotListFeatures = indoorSource.getFeaturesInExtent(mapExtent);
		} else {
			spotListFeatures = spotSource.getFeaturesInExtent(mapExtent);
		}
	}
	//sort features in spotListFeatures (according to sortOptions) append first 20 into spot list
	sortListFeatures();
}
//dynamically creates a regex string to test against features in spotLayer
function getFilterRegex() {
	var regexQ = "(";
	//loop through all chosen filters
	for(var key in selectedFilters) {
		for(var i=0;i<selectedFilters[key].length;i++) {
			//for rope difficulties
			if(key == "difficulty" && spotFilterType == "Ropes") {
				//match the first 5.\d, take off num
				var diffEx = /\d\.\d+/i.exec(selectedFilters[key][i]);
				//split on the . so array is [5, \d]
				var diffArr = diffEx[0].split(".");
				//if second num is greater then ten it just has to match ten
				if(parseInt(diffArr[1]) < 10) {
					//if not query needs to match a range
					if(diffArr[1] == 0) {
						regexQ += ("5.[0-5]$|");
					} else {
						regexQ += ("5.[6-9]$|");
					}
				} else regexQ += (diffEx[0] + "|");
			//for bouldering difficulties
			} else if(key == "difficulty" && spotFilterType == "Bouldering") {
				var diffEx = /\d+/.exec(selectedFilters[key][i]);
				if(parseInt(diffEx[0]) < 10) {
					//add two to get range between difficulties, 3 for v6-v9
					var rangeNum;
					parseInt(diffEx[0]) == 6 ? rangeNum = 3 : rangeNum = 2
					var diffExRange = parseInt(diffEx[0]) + rangeNum;
					regexQ += ("V[" + diffEx[0] + "-" + diffExRange + "]$|");
				} else {
					var diffExRange = parseInt(diffEx[0]) + 3;
					regexQ += ("V1[" + diffEx[0].substring(1, 2) + "-" + String(diffExRange).substring(1, 2) + "]$|");
				}
			} else {
				regexQ += (selectedFilters[key][i] + "|");
			}
		}
	}
	//remove hanging | and close parenthesis
	return regexQ.substring(0, regexQ.length - 1) + ")";
}
//----------------------------------------|
//            Area Filtering		      |
//----------------------------------------|
function filterAreas() {
	var mapExtent = map.getView().calculateExtent(map.getSize());
	areaSource.forEachFeatureInExtent(mapExtent, function(feature) {
		var areaId = feature.getId();
		var areaCheck = spotsForFilterArea(areaId, mapExtent);
		if(areaCheck) {
			var cloneFeature = feature.clone();
			//give clone feature area id
			cloneFeature.setId(areaId);
			filterAreaSource.addFeature(cloneFeature);
		}
	});
}
function spotsForFilterArea(areaId, mapExtent) {
	var pass = false;
	filterSource.forEachFeatureInExtent(mapExtent, function(feature) {
		if(parseInt(feature.get('area_id')) === parseInt(areaId)) {
			pass = true;
			return;
		}
	});
	return pass;
}
//----------------------------------------|
//               Spot List			      |
//----------------------------------------|
//html for spot in spot-list
function spotDivHTML(element, top) {
	//get & set element vars depending on length/type etc
	if(!element.get('type')) {
		var type = "No Routes";
		var color = "#404040"
	} else {
		var typeArr = element.get('type').split(", ");
		if(typeArr[1] == "") {
			var type = typeArr[0];
		} else {
			var type = element.get('type').substring(0, element.get('type').length - 2);
		}
		//set background color for types
		var color = typeColor(typeArr[0]);
	}
	//check if name is too long
	var name = element.get('name');
	if(name.length > 28) {
		name = name.substring(0, 26) + "...";
	}
	//check against avg diff being null
	if(element.get('avg_diff') == null) {
		var diff = "n/a";
	} else {
		var diff = element.get('avg_diff');
	}
	//check top pic
	if(element.get('top_pic')) {
		var topPic = S3URL + "/" + element.get("top_pic");
	} else {
		var topPic = "assets/images/default-spot-small.jpg";
	}
	//shorten desc
	if(element.get('description').length >= 280) {
		var spotDesc = element.get('description').substring(0, 280);
	} else if(element.get('description').length > 0) {
		var spotDesc = element.get('description');
	} else {
		var spotDesc = "<em>It's a bit empty in here, go to this spots page and add a description!</em>";
	}
	//make likes 0 if null
	if(!element.get("likes")) {
		var likes = 0;
	} else var likes = element.get('likes');
	if(element.get('confirmed') == 0 || element.get('confirmed') == null) {
		var spotConfirmed = '<div class="spot-div-unconfirmed transition-all confirm-spot-location" data-toggle="tooltip" data-placement="bottom" title="Have you been here? Click to go on the map to confirm!" style="width:100%;border-radius:0">' +
			'<div class="basic-text">UNCONFIRMED</div>' +
		'</div>';
	} else var spotConfirmed = '<div class="spot-div-unconfirmed transition-all">' +
			'<div class="basic-text">CONFIRMED <span class="glyphicon glyphicon-ok-circle"></span></div>';
	//check if user uploaded
	if(element.get('user_name')) {
		var userName = 'Discovered by <a href="profile/' + element.get('user_name') + '" class="default-link">' + element.get('user_name') + '</a>';
	} else {
		var userName = 'Discovered by climbandseek';
	}
	var spotDiv = '<div class="mobile-spot-div">' +
					'<div class="area-div-background spot-div-white"></div>' +
					'<div class="area-div-background area-div-pic"><img src="' + topPic + '" class="mobile-background-pic"/></div>' +
					'<div class="spot-id" style="display:none">' + element.getId() + '</div>' +
					'<div class="spot-area-id" style="display:none">' + element.get('area_id') + '</div>' +
					'<div class="main-area-div">' +
						'<a href="spot/' + element.getId() + '/' + element.get('name') + '" class="spot-div-name bolder-text default-link" title="' + element.get('name') + '">' + name + '</a>' +
						'<div class="spot-div-user basic-text">' +
							userName +
							'<span> in ' + element.get('region') + '</span>' +
						'</div>' +
						'<div class="row no-gutters spot-div-columns-row">' +
							'<div class="col-xs-4">' +
								'<div class="spot-div-column transition-all">' +
									'<div class="spot-column-hover-info basic-text"># of Routes</div>' +
									'<span class="glyphicon glyphicon-road" style="font-size:.9em"></span>' +
									'<span class="basic-text"> ' + element.get('routes') + '</span>' +
								'</div>' +
							'</div>' +
							'<div class="col-xs-4">' +
								'<div class="spot-div-column transition-all">' +
									'<div class="spot-column-hover-info basic-text">Avg Difficulty</div>' +
									'<span class="glyphicon glyphicon-stats"></span>' +
									'<span class="basic-text"> ' + diff + '</span>' +
								'</div>' +
							'</div>' +
							'<div class="col-xs-4">' +
								'<div class="spot-div-column transition-all" style="border-right:none">' +
									'<div class="spot-column-hover-info basic-text">Likes</div>' +
									'<span class="glyphicon glyphicon-thumbs-up"></span>' +
									'<span class="basic-text"> ' + likes + '</span>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="spot-div-types basic-text" style="background:' + color + '">' + type + '</div>' +
					'</div>' +
					spotConfirmed +
				'</div>';	
	if(!top) {
		$("#spot-list").append(spotDiv);
	} else {
		$("#spot-list").prepend(spotDiv);
	}
}
//html for spot in spot-list
function indoorDivHTML(element, top) {
	//check if name is too long
	var name = element.get('name');

	var address = element.get('address');
	if(address) {
		var splitAdd = address.split(',');
		try {
			address = splitAdd[0];
		} catch(e) {
			address = address;
		}
	}
	//check top pic
	if(element.get('top_pic')) {
		var topPic = S3URL + "/" + element.get("top_pic");
	} else {
		var topPic = "assets/images/gym-mobile-small.jpg";
	}
	//shorten desc
	if(element.get('description').length >= 280) {
		var spotDesc = element.get('description').substring(0, 280);
	} else if(element.get('description').length > 0) {
		var spotDesc = element.get('description');
	} else {
		var spotDesc = "<em>It's a bit empty in here, go to this spots page and add a description!</em>";
	}
	//make likes 0 if null
	if(!element.get("likes")) {
		var likes = 0;
	} else var likes = element.get('likes');
	var spotDiv = '<div class="mobile-spot-div">' +
					'<div class="area-div-background spot-div-white"></div>' +
					'<div class="area-div-background area-div-pic"><img src="' + topPic + '" class="mobile-background-pic"/></div>' +
					'<div class="spot-id" style="display:none">' + element.getId() + '</div>' +
					'<div class="main-area-div">' +
						'<a href="spot/' + element.getId() + '/' + element.get('name') + '" class="spot-div-name bolder-text default-link" title="' + element.get('name') + '">' + name + '</a>' +
						'<div class="spot-div-user basic-text">' +
							'<span>at ' + address + '</span>' +
						'</div>' +
						'<div class="spot-div-types basic-text" style="background:#398bc6">' + element.get('city') + '</div>' +
						'<div class="spot-description basic-text">' + spotDesc + '</div>' +
						'<div class="indoor-div-likes">' +
							'<span class="glyphicon glyphicon-thumbs-up" style="margin-right:5px"></span>' +
							'<span class="basic-text">' + likes + '</span>' +
						'</div>' +
					'</div>' +
				'</div>';
	if(!top) {
		$("#spot-list").append(spotDiv);
	} else {
		$("#spot-list").prepend(spotDiv);
	}
}
function resetSpotList() {
	var emptySpot = "<div id='blank-spot-list' class='basic-text' style='color:rgba(0, 0, 0, 0.4);font-size:2.5em'>" +
						"Zoom in to discover spots!" +
					"</div>";
	$("#spot-list").html(emptySpot);
	$("#spot-num").html("");
}
//spot explore div animations
$("[data-toggle='tooltip']").tooltip();
$(document).on("mouseenter", ".spot-div-column", function() {
	$(this).find(".spot-column-hover-info").stop().fadeIn();
});
$(document).on("mouseleave", ".spot-div-column", function() {
	$(this).find(".spot-column-hover-info").stop().fadeOut();
});
//center to spot on div click
$(document).on("click", ".mobile-spot-div", function(e) {
	var id = $(this).find(".spot-id").text();
	var thisFeat;
	if(spotFilterType == "Indoor") {
		thisFeat = indoorSource.getFeatureById(id);
	} else {
		if(spotLayer.getVisible()) {
			thisFeat = spotSource.getFeatureById(id);
		} else {
			thisFeat = filterSource.getFeatureById(id);
		}
	}
	//if thisFeat is null, user clicked on 'unconfirmed' get feature from modifysource
	if(!thisFeat) {
		thisFeat = modifySource.getFeatures()[0];
	}
	panToCenter(thisFeat.getGeometry().getCoordinates());
});
//sort spotListFeatures
function sortListFeatures() {
	//sort spot list depending on vars in sortOptions
	spotListFeatures = mergeSort(spotListFeatures);
	//clear current spot list, append first 20 in spotListFeatures
	$("#spot-list").html("");
	for(var i=0;i<20;i++) {
		if(spotListFeatures[i] !== undefined) {
			if(spotFilterType == "Indoor") {
				indoorDivHTML(spotListFeatures[i], false);
			} else {
				spotDivHTML(spotListFeatures[i], false);
			}
		}
	}
	//get amount of spots in spot-list
	$("#spot-num").html("Found " + spotListFeatures.length +  " " + spotFilterType.toLowerCase() + " locations");
	//scroll spot list back to top
	$("#spot-list").animate({
		scrollTop:0
	}, 200);
	if(spotListFeatures.length < 1) {
 		$("#spot-list").html("<div id='blank-spot-list' class='basic-text' style='color:rgba(0, 0, 0, 0.4);font-size:2em'>No spots found, go on the map page to find more results</div>")
 	}
}
//load more spot divs
$("#spot-list").scroll(function() {
	var scrollBar = $(this).scrollTop();
	var scrollHeight = ($(this).prop('scrollHeight') * .75) - 500;
	if(scrollBar >= $(this).prop("scrollHeight") - 1200) {
		if(spotListFeatures.length > $(".mobile-spot-div").length) {
			var i = 0;
			var length = $(".mobile-spot-div").length;
			//loading gif
			var spotLoader = "<div id='spot-loader' style='text-align:center'>" +
								"<img src='assets/loaders/ripple.gif' class='loader-ripple'/>" +
							"</div>"
			$(this).append(spotLoader);
			while(i < 20) {
				if(spotListFeatures[length + i] !== undefined) {
					if(spotFilterType == "Indoor") {
						indoorDivHTML(spotListFeatures[length + i], false);
					} else {
						spotDivHTML(spotListFeatures[length + i], false);
					}
					i++;
				} else {
					break;
				}
			}
			$("#spot-loader").remove();
		}
	}
});
//sorts array depending on vars stored in sortOptions
function mergeSort(arr) {
	if(arr.length < 2) return arr;
	var half = Math.floor(arr.length/2);
	var subLeft = mergeSort(arr.slice(0, half));
	var subRight = mergeSort(arr.slice(half));
	if(sortOptions[1] == "avg_diff") {
		return mergeJoinDiff(subLeft, subRight);
	} else {
		return mergeJoin(subLeft, subRight);
	}
}
function mergeJoin(a, b) {
	var result = [];
	if(sortOptions[0] == 'Ascending') {
		while(a.length > 0 && b.length > 0) {
			result.push(sortNullCheck(a[0].get(sortOptions[1])) < sortNullCheck(b[0].get(sortOptions[1])) ? a.shift() : b.shift());
		}
	} else {
		while(a.length > 0 && b.length > 0) {
			result.push(sortNullCheck(a[0].get(sortOptions[1])) > sortNullCheck(b[0].get(sortOptions[1])) ? a.shift() : b.shift());
		}
	}
	return result.concat(a.length ? a : b);
}
function sortNullCheck(num) {
	var sortNum = parseInt(num)
	if(!sortNum) {
		return 0
	} else {
		return sortNum;
	}
}
function mergeJoinDiff(a, b) {
	var result = [];
	if(sortOptions[0] == "Ascending") {
		while(a.length > 0 && b.length > 0) {
			var aFeat = diffToNum(a[0].get(sortOptions[1]));
			var bFeat = diffToNum(b[0].get(sortOptions[1]));
			result.push(parseInt(aFeat) < parseInt(bFeat) ? a.shift() : b.shift());
		}
	} else {
		while(a.length > 0 && b.length > 0) {
			var aFeat = diffToNum(a[0].get(sortOptions[1]));
			var bFeat = diffToNum(b[0].get(sortOptions[1]))
			result.push(parseInt(aFeat) > parseInt(bFeat) ? a.shift() : b.shift());
		}
	}
	return result.concat(a.length ? a : b);
}
//turn spot difficulty into a number for sorting purposes
function diffToNum(diff) {
	if(!diff) { return -1 }
	if(spotFilterType == "Ropes") {
		var diffArr = diff.split(".");
		try {
			var newNum = diffArr[0] + parseInt(diffArr[1]);
		}
		catch(e) {
			var newNum = 0;
		}
		return parseInt(newNum);
	} else {
		var diffArr = diff.split("V");
		try {
			return parseInt(diffArr[1]);
		}
		catch(e) {
			return 0;
		}
	}
}
/* ********************************
   --------------------------------
   |     Add/Confirm Feature      |
   ________________________________
   ********************************
*/
//----------------------------------------|
//            Modify/Confirm			  |
//----------------------------------------|
//globals to hold modify object, feature being modifed, and new coords
var modifyInteraction;
var modfyFeature;
var modifyLocation = null;
//sets modify object
function setModifyInteraction(feature) {
	modifyInteraction = new ol.interaction.Modify({
		features: feature,
		pixelTolerance: 30,
		style: setFeatureStyle("assets/images/black-pin.png", 1)
	});
	map.addInteraction(modifyInteraction);
}
//sets modify interaction and handles user changing feature location from popup
$(document).on("click", ".confirm-spot-location", function() {
	//check if user isn't already modifying feature
	if(modifySource.getFeatures().length == 0) {
		//get current feature in selection
		var feature = select.getFeatures();
		//clone it and pass to modify source, will pass back new coords when done
		modifyFeature = feature.getArray()[0];
		if(!modifyFeature) {
			var spotId = $(this).parents(".mobile-spot-div").find(".spot-id").text();
			if(spotLayer.getVisible()) {
				modifyFeature = spotSource.getFeatureById(spotId);
			} else {
				modifyFeature = filterSource.getFeatureById(spotId);
			}
		}
		beginModify(modifyFeature);
	}
});
function beginModify(feature) {
	if(sessionStorage.getItem('username')) {
		if(!feature.get('confirmed')) {
			var clonedFeature = feature.clone();
			clonedFeature.setStyle(setFeatureStyle("assets/images/black-pin.png", 1));
			modifySource.addFeature(clonedFeature);
			//get collection from modify source and set new modify interaction, 
			//collection because thats what ol.interaction.Modify will take
			var cloneFeature = modifySource.getFeaturesCollection();
			setModifyInteraction(cloneFeature);
			//remove select interaction/popup, hide spotLayer, slide out event info div
			spotLayer.setVisible(false);
			filterLayer.setVisible(false);
			areaLayer.setVisible(false);
			select.setActive(false);
			var infoString = "Drag the marker to its proper location.";
			slideMapInfo(false, infoString);
			popupSpotHTML(modifyFeature);
			$("#spot-map-popup").show();
			swapToMap();
		} else showFlash("That spot has already been confirmed!", "danger", "alert");
	} else showFlash("Log in to confirm feature", "danger", "icon");
}
//confirm modify spot after drag
$("#confirm-new-location").click(function() {
	//hide confirm, add loading gif
	$(this).hide();
	$("#confirm-loader").show();
	var spotId = modifyFeature.getId();
	var spotName = modifyFeature.get('name');
	//if modifyLocation is null, marker wasn't moved use its current location
	if(!modifyLocation) {
		var currentLocation = modifyFeature.getGeometry().getCoordinates();
		modifyLocation = transformMapCoords([parseFloat(currentLocation[0]), parseFloat(currentLocation[1])]);
	}
	modifyLocation = ol.proj.transform(modifySource.getFeatures()[0].getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
	modifyFeature.setGeometry(new ol.geom.Point(transform([parseFloat(modifyLocation[0]), parseFloat(modifyLocation[1])])));
	var spotObj = {"id": spotId, "lat": modifyLocation[1], "long": modifyLocation[0]};
	//send to server, display message, return map to normal
	$.post("ajax/explore/update_location.php", spotObj, function(resp) {
		$("#confirm-loader").hide();
		//leave message
		if(resp) {
			$("#map-event-text").html("Nice! Add any more information on this spots page, " + "<a href='spot/" + spotId + "/" + spotName + "'>here</a>");
			//grab changed feature, set confirmed to true, activate popup
			modifyFeature.set('confirmed', true);
			modifyFeature.set('area_id', null);
			modifyFeature.setStyle(setFeatureStyle("assets/images/blue-pin.png", 1));
			selected.push(modifyFeature);
			popupSpotHTML(modifyFeature);
			//center confirmed spot
			panToCenter(modifyFeature.getGeometry().getCoordinates());
		} else {
			$("#map-event-text").html("Database error. Try again later.");
		}
		//display new message, then reset
		$("#confirm-new-location").hide();
		$("#info-remove-icon").hide();
		setTimeout(function() { slideMapInfo(); }, 7000);
		resetModify();
	}, 'json');
});
//cancels modify obj, resets back to normal
$("#info-remove-icon").click(function() {
	resetModify()
	slideMapInfo(true);
});
//----------------------------------------|
//           modify functions			  |
//----------------------------------------|
//slide out map info div
function slideMapInfo(visible, message) {
	if(visible == false) {
		$("#map-event-text").html(message)
		$("#confirm-new-location").show();
		$("#info-remove-icon").show();
		$("#map-event-info").fadeIn();
	} else {
		$("#map-event-info").fadeOut();
	}
}
//reset map back to normal after confirming a spot
function resetModify() {
	map.removeInteraction(modifyInteraction);
	modifySource.clear();
	select.setActive(true);
	//determine which layer to show
	if(selectedFilters.type.length == 0 && selectedFilters.difficulty.length == 0) {
		spotLayer.setVisible(true);
	} else {
		filterLayer.setVisible(true);
	}
	areaLayer.setVisible(true);
	modifyLocation = undefined;
	modifyFeature = undefined;
}
//smooth pans to center coord
function panToCenter(coord) {
	var pan = ol.animation.pan({
		duration: 1000,
		source: map.getView().getCenter()
	});
	map.beforeRender(pan);
	map.getView().setCenter(coord)
}
function zoomAndPanAnimation(lat, long, mapZoom, speed) {
	var pan = ol.animation.pan({
		duration: speed,
		source: map.getView().getCenter()
	});
	var zoom = ol.animation.zoom({
		duration: speed,
		resolution: map.getView().getResolution()
	});
	map.beforeRender(pan, zoom);
	map.getView().setCenter(ol.proj.transform([parseFloat(long), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857'));
	map.getView().setZoom(mapZoom);
}
//----------------------------------------|
//          Modify Popup Click			  |
//----------------------------------------|
//popups don't register clicks on child elements? It does on popup elements, popupSpotConfirm parses the event and will begin modify if click on confirm
$("#area-popup").click(function(e) {
	popupSpotConfirm(e.target, ".area-spot-div", ".area-spot-id");
});
$("#spot-map-popup").click(function(e) {
	popupSpotConfirm(e.target, "#spot-map-popup", "#spot-popup-id");
});
function popupSpotConfirm(target, parentDiv, spotId) {
	var areaDiv = $(target);
	var featId = areaDiv.parents(parentDiv).find(spotId).text();
	if(spotLayer.getVisible()) {
		var source = spotSource;
	} else var source = filterSource;
	var thisFeature = source.getFeatureById(featId);
	if(areaDiv.hasClass("confirm-spot-location")) {
		modifyFeature = thisFeature;
		selected.clear();
		selectedAreas.clear();
		beginModify(thisFeature);
	} 
}
//swaps to map container
function swapToMap() {
	$("#mobile-explore-list").fadeOut(400, function() {
 		$("#mobile-explore-map").css("z-index", "2");
 		$("#mobile-explore-list").css("z-index", "1").show();
 	});
}
/* ********************************
   --------------------------------
   |            Search  	      |
   ________________________________
   ********************************
*/
//---------------------------------------------------|
// Dynamically set widths for filter bar/results div |
//---------------------------------------------------|
//holds current search filter
var activeSearchFilter = "all";
//on focus, show overlay
$("#map-search-bar").click(function() {
	if($("#search-results").css("display") == "none") {
		$("#map-search-close").show();
		var winHeight = $(window).height() - ($("#navbar").height() + $("#map-search-form").height() + $("#search-top-bar").height());
		$("#search-results-scroll").css("height", winHeight + "px");
		$("#search-results").show();
		if($("#map-search-bar").val().length <= 0) {
			searchLoadingDiv("Start typing to search!", "true");
		}
	}
});
//----------------------------------------|
//         AJAX calls/sort data			  |
//----------------------------------------|
//var to hold current ajax request
var ajaxSearch = null;
var searchTimer;
//handle typing event from search bar, setTimeout is for fast typing, only makes calls when typing pauses
$("#map-search-bar").keyup(function() {
	clearTimeout(searchTimer);
	if($("#map-search-bar").val().length > 0) {
		if(activeSearchFilter == "all") {
			searchTimer = setTimeout(function(){ searchAllQueryAjax() }, 350);
		} else {
			searchTimer = setTimeout(function(){ searchSingleQueryAjax() }, 350);
		}
	} else {
		resetSearchResults();
		searchLoadingDiv("Start typing to search!", "true");
	}
}); 
//ajax call to fetch search query
function searchAllQueryAjax() {
	var searchDiv = $("#search-results");
	if(searchDiv.css("display") == "none") {
		searchDiv.show();
	}
	var q = $("#map-search-bar").val();
	if(q.length >= 2 && q != " ") {
		//if ajaxSearch isn't null that means an ajax request is active, abort old call if so
		if(ajaxSearch != null) {
			ajaxSearch.abort();
		}
		var queryObj = {"q": q};
		//display loader
		var loadingTimer = setTimeout(function(){ searchLoadingDiv("Loading...", "true") }, 100);
		//make call
		ajaxSearch = $.post("ajax/search/explore_search.php", queryObj, function(data) {
			//clear previously loaded results
			resetSearchResults();
			clearTimeout(loadingTimer);
			//record results found
			var resultLength;
			data.length === undefined ? resultLength = 0 : resultLength = data.length;
			$("#search-results-info").html(resultLength + " results found for \'" + $("#map-search-bar").val() + "\'");
			//don't run if data is 0
			if(resultLength == 0) {
				searchLoadingDiv("No Results Found", "true");
			} else {
				$("#search-loading-div").hide();
				sortAllSearchResults(data);
			}
			ajaxSearch = null;
		}, 'json');
	} 
}
//loops through search results and places up to 8 of each category into #search-results
function sortAllSearchResults(data) {
	//show number of results found
	var query = $("#map-search-bar").val();
	var cityArea = $("#city-results").find(".search-results");
	var spotArea = $("#spot-results").find(".search-results");
	var routeArea = $("#route-results").find(".search-results");
	var indoorArea = $("#indoor-results").find(".search-results");
	//keep track of how many result types have been appended to #search-results
	var cityDivs = 0;
	var spotDivs = 0;
	var routeDivs = 0;
	var indoorDivs = 0;
	//create new regex to highlight the parts of names that match the query
	if(query.length == 1) {
		var qStr = query.toUpperCase();
		var queryRegEx = new RegExp(qStr);
	} else var queryRegEx = new RegExp(query, "i");
	//loop through results, break if each typeDivs is 8
	//also object keys are off because in order to use UNION you must use the exact same # of cols for 
	//all tables, and every subsequent table after the first one uses the names of the first tables cols
	//use search.php for reference
	for(var i=0;i<data.length;i++) {
		if(data[i].name) {
			var name = data[i].name.replace(queryRegEx, "<strong>$&</strong>");
		} else {
			continue;
		}
		switch(data[i].search_type) {
			case "city":
				if(cityDivs < 8) {
					cityArea.append(searchLocationHTML(name, data[i].latitude, data[i].longitude));
					cityDivs += 1;
				}
				break;
			case "Indoor":
				if(indoorDivs < 8) {
					indoorArea.append(searchIndoorHTML(data[i].longitude, data[i].latitude, name));
					indoorDivs += 1;
				}
				break;
			case "Bouldering":
				if(spotDivs < 8) {
					spotArea.append(searchSpotHTML(data[i].longitude, data[i].search_type, name));
					spotDivs += 1;
				}
				break;
			case "Ropes":
				if(spotDivs < 8) {
					spotArea.append(searchSpotHTML(data[i].longitude, data[i].search_type, name));
					spotDivs += 1;
				}
				break;
			case "Both":
				if(spotDivs < 8) {
					spotArea.append(searchSpotHTML(data[i].longitude, "Ropes, Bouldering", name));
					spotDivs += 1;
				}
				break;
			case null:
				if(spotDivs < 8) {
					spotArea.append(searchSpotHTML(data[i].longitude, "Unknown", name));
					spotDivs += 1;
				}
				break;
			default:
				if(routeDivs < 8) {
					if(data[i].lat) {
						var diff = data[i].lat
					} else var diff = "Unknown";
					routeArea.append(searchRouteHTML(data[i].search_type, data[i].longitude, name, data[i].latitude));
					routeDivs += 1;
				}
				break;
		}
		var allDivs = cityDivs + spotDivs + routeDivs + indoorDivs;
		if(allDivs >= 32) {
			break;
		}
	}
	if(cityDivs >= 1) {
		$("#city-results").show();
		if(cityDivs >= 8) {
			$("#more-cities").css("display", "block")
		} else $("#more-cities").hide();
	}
	if(spotDivs >= 1) {
		$("#spot-results").show();
		if(spotDivs >= 8) {
			$("#more-spots").css("display", "block")
		} else $("#more-spots").hide();
	}
	if(routeDivs >= 1) {
		$("#route-results").show();
		if(routeDivs >= 8) {
			$("#more-routes").css("display", "block")
		} else $("#more-routes").hide();
	}
	if(indoorDivs >= 1) {
		$("#indoor-results").show();
		if(indoorDivs >= 8) {
			$("#more-gyms").css("display", "block")
		} else $("#more-gyms").hide();
	}
}
//holds all current data for one table
var singleSearchResults = Array();
//holds the current index for singleSearchResults
var currentlyLoadedResults = 0;
//query single table
function searchSingleQueryAjax() {
	var q = $("#map-search-bar").val();
	//if just a single letter, add spacing so query will search for standalone letter like 'A boulder'
	if(q.length == 1) {
		q = " " + q + " ";
	}
	//if ajaxSearch isn't null that means an ajax request is active, abort old call if so
	if(ajaxSearch != null) {
		ajaxSearch.abort();
	}
	//determine which table and location results will go
	if(activeSearchFilter == "places") {
		var tableName = "cities";
		var resultLocation = $("#city-results");
		var resultName = "places";
	} else if(activeSearchFilter == "spots") {
		var tableName = "spots";
		var resultLocation = $("#spot-results");
		var resultName = "spots";
	} else if(activeSearchFilter == "routes") {
		var tableName = "routes";
		var resultLocation = $("#route-results");
		var resultName = "routes";
	} else {
		var tableName = "indoor";
		var resultLocation = $("#indoor-results");
		var resultName = "indoor";
	}
	//set timer for loading...
	var loadingTimer = setTimeout(function(){ searchLoadingDiv("Loading...", "true") }, 100);
	var queryObj = {"table": tableName, "q": q};
	//make call
	ajaxSearch = $.post("ajax/search/search_single_table.php", queryObj, function(data) {
		//hide view all 
		$(".view-all-results").hide();
		clearTimeout(loadingTimer);
		//enter results found 
		var resultLength;
		data.length === undefined ? resultLength = 0 : resultLength = data.length
		$("#search-results-info").html(resultLength + " " + resultName + " found for \'" + $("#map-search-bar").val() + "\'");
		//hide all search results divs, clear them
		$(".search-results-div").each(function() {
			$(this).hide().find(".search-results").html("");
		});
		if(resultLength > 0) {
			//set up regex to highlight matches
			var query = $("#map-search-bar").val();
			if(query.length == 1) {
				var qStr = query.toUpperCase();
				var queryRegEx = new RegExp(qStr);
			} else var queryRegEx = new RegExp(query, "i");
			$("#search-loading-div").hide();
			//show selected result div
			resultLocation.show();
			//scroll spot list back to top
			$("#search-results").animate({
				scrollTop:0
			}, 200);
			var resultsDiv = resultLocation.find(".search-results");
			//reset singleSearchResults to hold new data
			singleSearchResults = data;
			currentlyLoadedResults = 100;
			var loopLength;
			if(data.length < 100) {
				var loopLength = data.length;
				currentlyLoadedResults = data.length;
			} else var loopLength = 100; currentlyLoadedResults = 100;
			//loop through first 100 results and append them to their proper div
			for(var i=0;i<loopLength;i++) {
				var name = data[i].name.replace(queryRegEx, "<strong>$&</strong>");
				switch(tableName) {
					case "cities":
						resultsDiv.append(searchLocationHTML(name, data[i].latitude, data[i].longitude));
						break;
					case "spots":
						resultsDiv.append(searchSpotHTML(data[i].id, data[i].filter_type, name));
						break;
					case "routes":
						resultsDiv.append(searchRouteHTML(data[i].spot_id, data[i].type_climbing, name, data[i].difficulty));
						break;
					case "indoor":
						resultsDiv.append(searchIndoorHTML(data[i].id, data[i].city, name));
						break;
				}
			}
		} else {
			searchLoadingDiv("No Results Found", "true");
			resetSearchResults();
		}
	}, 'json');
}
//handles infinite loading for single table searches
$("#search-results-scroll").scroll(function() {
	if(activeSearchFilter != "all") {
		var scrollBarHeight = $(this).scrollTop();
		//get about three quarters of height - 500 (height of search-results)
		var resultsHeight = ($(this).prop('scrollHeight') * .75) - 500;
		var resultsLength = singleSearchResults.length;
		$(".infinite-load-gif").css("display", "block");
		//set up regex to highlight matches
		var query = $("#map-search-bar").val();
		if(query.length == 1) {
			var qStr = query.toUpperCase();
			var queryRegEx = new RegExp(qStr);
		} else var queryRegEx = new RegExp(query, "i");
		if(scrollBarHeight >= resultsHeight && currentlyLoadedResults < resultsLength && resultsLength > 100) {
			for(var i=0;i<100;i++) {
				//if index is greater than singleSearchResults length then break
				if(currentlyLoadedResults >= resultsLength) {
					break;
				}
				var name = singleSearchResults[currentlyLoadedResults].name.replace(queryRegEx, "<strong>$&</strong>");
				//append the proper results page with result
				switch(activeSearchFilter) {
					case "places":
						$("#city-results").find(".search-results").append(searchLocationHTML(name, singleSearchResults[currentlyLoadedResults].latitude, singleSearchResults[currentlyLoadedResults].longitude));
						break;
					case "spots":
						$("#spot-results").find(".search-results").append(searchSpotHTML(singleSearchResults[currentlyLoadedResults].id, singleSearchResults[currentlyLoadedResults].filter_type, name));
						break;
					case "routes":
						$("#route-results").find(".search-results").append(searchRouteHTML(singleSearchResults[currentlyLoadedResults].spot_id, singleSearchResults[currentlyLoadedResults].type_climbing, name, singleSearchResults[currentlyLoadedResults].difficulty));
						break;
					case "indoor/gyms":
						$("#indoor-results").find(".search-results").append(searchIndoorHTML(singleSearchResults[currentlyLoadedResults].id, singleSearchResults[currentlyLoadedResults].city, name));
						break;
				}
				currentlyLoadedResults += 1;
			}
		}
		$(".infinite-load-gif").hide();
	}
});
//----------------------------------------|
//      Functions used/minor events       |
//----------------------------------------|
//exits search
$("#map-search-close").click(function() {
	$("#search-results").hide();
	$(this).hide();
});
//handle click on location search result, pans to location
$(document).on("click", ".search-result-location", function() {
	var rawCoords = $(this).find(".location-coords").html().split(", ");
	$("#search-results").hide();
	$("#search-overlay").fadeOut();
	centerMap(rawCoords[0], rawCoords[1], 8);
	//hide search close
	$("#map-search-close").hide();
});
//changes current search filter
$(".inside-search-filter").click(function() {
	$(".inside-search-filter").css({
		fontSize: "1.2em",
		color: "rgba(0, 0, 0, 0.7)"
	});
	$(this).css({
		fontSize: "1.5em",
		color: "black"
	});
	activeSearchFilter = $(this).text().toLowerCase();
	if($("#map-search-bar").val().length >= 1 && activeSearchFilter !== "all") {
		searchSingleQueryAjax();
	} else {
		searchAllQueryAjax();
	}
});
//shows all requests of selected type
$(".view-all-results").click(function(e) {
	e.preventDefault();
	//change css of filters/update active filter
	var thisId = $(this).attr('id');
	if(thisId == "more-cities") {
		activeSearchFilter = "places";
	} else if(thisId == "more-spots") {
		activeSearchFilter = "spots"
	} else if(thisId == "more-routes"){
		activeSearchFilter = "routes";
	} else {
		activeSearchFilter = "indoor/gyms";
	}
	$(".inside-search-filter").each(function() {
		if($(this).html().toLowerCase() == activeSearchFilter) {
			$(this).css({
				fontSize: "1.6em",
				color: "black"
			});
		} else {
			$(this).css({
				fontSize: "1.3em",
				color: "rgba(0, 0, 0, 0.7)"
			});
		}
	});
	//run new query
	searchSingleQueryAjax();
})
//shows/hides loading/message div
function searchLoadingDiv(message, show) {
	$("#search-loading-text").html(message);
	if(show == "true") {
		$("#search-loading-div").show();
	} else $("#search-loading-div").hide();
	resetSearchResults();
}
//clears search results page
function resetSearchResults() {
	//clear loaded results
	$("#search-results-info").html("");
	$("#city-results").hide().find(".search-results").html("");
	$("#spot-results").hide().find(".search-results").html("");
	$("#route-results").hide().find(".search-results").html("");
	$("#indoor-results").hide().find(".search-results").html("");
}
//functions for search results based on table
function searchLocationHTML(name, lat, long) {
	return "<div class='search-result-row search-result-location'>" +
				"<span class='glyphicon glyphicon-home search-result-icon'></span><span class='location-coords' style='display:none'>" + 
					lat + ", " + long + "</span> " + name + 
			"</div>";
}
function searchSpotHTML(id, type, name) {
	return "<a href='spot/" + id + "/" + name + "' class='search-result-row search-result-spot'>" +
				"<span class='glyphicon glyphicon-map-marker search-result-icon'></span> " + 
				name + "<div class='search-result-type'>" + type + "</div>" +
			"</a>"
}
function searchRouteHTML(id, type, name, diff) {
	if(!diff) {
		diff = "Unknown";
	} 
	return "<a href='spot/" + id + "' class='search-result-row search-result-spot'>" +
				"<span class='glyphicon glyphicon-road search-result-icon'></span> " + 
					name + "<div class='search-result-diff'>" + type + " - " + diff + "</div>" +
			"</a>";
}
function searchIndoorHTML(id, city, name) {
	return "<a href='indoor/" + id + "/" + name + "' class='search-result-row search-result-spot'>" +
				"<span class='glyphicon glyphicon-map-marker search-result-icon'></span> " + 
				name + "<div class='search-result-type'>" + city + "</div>" +
			"</a>"
}
$("#map-search-form").submit(function(e) {
	e.preventDefault();
});
</script>
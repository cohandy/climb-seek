<!-- mountain vector background -->
<div class="vector-background"></div>
<div class="container-fluid" style="margin-top:50px">
	<div class="basic-text terms-title">Contact</div>
	<div class="container">
		<div class="basic-text disclaimer-text">*before sending make sure your request and/or concern couldn't be handled by using our report system. Any problems will be handled much faster by reporting.</div>
		<form method="post" action="#" id="contact-form">
			<div class="row">
				<div class="col-xs-12">
					<div class="new-spot-form-div">
						<span class="new-spot-text">Message</span>
						<textarea rows="4" name="message" class="new-spot-input" placeholder="Questions/Concerns"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="new-spot-form-div">
						<span class="new-spot-text">Your Email</span>
						<input name="email" class="new-spot-input" placeholder="Your email">
					</div>
				</div>
			</div>
			<div class="row some-gutters">
				<div class="col-md-4 col-md-offset-8">
					<input type="submit" class="sliding-button large-button" style="float:right;margin:15px 0;background:white;padding:10px 20px;width:170px;font-size:1.5em" value="SEND"/>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});

$("#contact-form").submit(function(e) {
	e.preventDefault();
	var formData = $(this).serialize();
	$.post("ajax/static_pages/send_contact.php", formData, function() {
		showFlash("Your email has been sent and we will get to it as soon as possible! Thank you.", "success", "info-sign");
		$("#contact-form").trigger("reset");
	});
})
</script>
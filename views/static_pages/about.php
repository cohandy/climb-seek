<div class="container-fluid" id="body-background">
	<div class="container" style="padding:85px 0">
		<div class="row">
			<div class="col-md-12">
				<div class="body-header" style="margin-top:25px">
					<div class="about-header-text basic-text">About</div>
				</div>
				<div class="basic-text about-text">climb&seek was founded in Santa Cruz County by a very (very) small team of people who sought to create a simple, straight-forward website for rock climbers. At climb&seek rock climbers from around the United States can share their favorite spots, add to others, or help shape the spots we have found ourselves from around the web. Our main goal is to become the go to spot for rock climbers new and old to collaborate and create a trusted, complete climbing database.</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid" style="background:#f5f5f5;padding-top:30px">
	<div class="about-header-text basic-text">Team</div>
	<div class="container" style="padding:85px 0">
		<div class="row">
			<div class="col-md-6">
				<div class="about-profile">
					<img src="assets/images/connor_profile.jpg" class="about-image"/>
					<div class="bolder-text about-name-text">Connor Handy</div>
					<div class="basic-text about-titles">CO-FOUNDER & LEAD DEVELOPER/DESIGNER</div>
					<div class="basic-text about-bio">
						Connor built this site from the ground up, designing both the front and back end. He has a passion for web development (obviously) and is looking for employment in the field! You can email him (me) at cohandy@gmail.com
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="about-profile">
					<img src="assets/images/kyle_profile.jpg" class="about-image"/>
					<div class="bolder-text about-name-text">Kyle Danna</div>
					<div class="basic-text about-titles">CO-FOUNDER & LEAD DATA ENGINEER</div>
					<div class="basic-text about-bio">
						Kyle is responsible for seeking out a wealth of spots and routes using advanced web scraping techniques. Without him this site would be completely empty! He also loves golden retreivers.
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="text-align:center;margin-top:25px">
			<div class="col-xs-12">
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypal-donate-btn">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHNwYJKoZIhvcNAQcEoIIHKDCCByQCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAjpeDV5xTKdgrr/+T2pJO3Txrw8lQE9Lze34BxTjfhV1yde/dHwJa1hTWjnWj21+epX96qM6HvVA/faQ0l2kIyh2DgQgCc9AS+XsCljjHQv5XRrsOFvXyVdCyKiqqAr5MMbekGpxNBgBee9H8tIZ/iMUO/7cLPdbQtrKKh5LEw8DELMAkGBSsOAwIaBQAwgbQGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIILVXuIh5cVWAgZDZPaJDs1e3ZDKa2dCgCKMX+Qju7OvfHVJQznzYKj8ewv3+1UIZ6+niH06bwfzFBM6Nw9NOsGMX0ti36y2s7t6fTw45+Jsr9U7/NY+J0ilNwm2jPnlrDAXXuOgKULCaQzNt7Q6rQkc1Ye6ln/3qF8Chq/dkQxoHhGsm/pSu+QWxXVTWgh659GuebyPA7xyjb7ygggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNzAxMTIwMTUwNThaMCMGCSqGSIb3DQEJBDEWBBQxN4kfmyAztZfTroIOM0FlQSEesTANBgkqhkiG9w0BAQEFAASBgKZap8E1kLfeOCz5LQyQfAt5lsY9OvE2fcEDJp1Mg93yxq1vclLteNXQH4Vx8AhX5DaZ0cw3LYaZiJ8nV43QZP/qJHdyDZoYMxGfxyzOQ1nN0W/IZcj5/fr9oaP2FV327xkv4it5kG569dXV4lzk4cn+nDEMYYq8RLD4pt6L29CC-----END PKCS7-----
					">
					<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
			</div>
		</div>
	</div>
</div>
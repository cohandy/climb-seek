<?php
$error_code = "404 - Not Found";
$error_message = "Looks like we couldn't find the page you're looking for! Why don't you try one of the links below?";
switch(getenv("REDIRECT_STATUS")) {
	case 400:
		$error_code = '400 - Bad Request';
		$error_message = "Looks like something went wrong! Sorry about that, why don't you try one of the links below?";
		break;
	case 401:
		$error_code = "401 - Unauthorized";
		$error_message = "Looks like something went wrong! Sorry about that, why don't you try one of the links below?";
		break;
	case 403:
		$error_code = "403 - Forbidden";
		$error_message = "You are forbidden from this page! What have you done to deserve this? Try one of the links below.";
		break;
	case 404:
		$error_code = "404 - Not Found";
		$error_message = "Looks like we couldn't find the page you're looking for! Why don't you try one of the links below?";
		break;
	case 500:
		$error_code = "500 - Internal Server Error";
		$error_message = "Looks like something went wrong! Sorry about that, why don't you try one of the links below?";
		break;
}
?>
<!-- mountain vector background -->
<div class="vector-background"></div>
<div class="container-fluid" style="margin-top:50px;text-align:center">
	<div id="error-content">
		<span class="glyphicon glyphicon-exclamation-sign" id="error-page-icon"></span>
		<div class="bolder-text error-header"><?php echo $error_code?></div>
		<div class="basic-text term-sub-header"><?php echo $error_message ?></div>
		<div id="footer-links">
			<div class="footer-icon-col" style="font-size:1.3em">
				<a href="/" class="default-link basic-text">HOME</a>
				<span class="error-link-spacer">|</span>
				<a href="explore" class="default-link basic-text">EXPLORE</a>
				<span class="error-link-spacer">|</span>
				<a href="about" class="default-link basic-text">ABOUT</a>
				<span class="error-link-spacer">|</span>
				<a href="contact" class="default-link basic-text">CONTACT</a>
				<span class="error-link-spacer">|</span>
				<a href="terms" class="default-link basic-text">TERMS</a>
				<span class="error-link-spacer">|</span>
				<a href="privacy" class="default-link basic-text">PRIVACY</a>
			</div>
		</div>
	</div>
</div>
<script>
document.title = "Error | climb&seek";
</script>
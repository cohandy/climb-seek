<div id="report-modal">
	<div id="report-name" class="basic-text"></div>
	<div class="container-fluid" style="margin:10px 10px">
		<div id="report-form-errors" class="form-error-area" style="text-align:center"></div>
		<div class="row">
			<div class="col-md-4 report-box transition-all" style="border-radius:2px 0 0 2px">
				<div class="report-box-div report-box-inside">
					<span class="report-box-icon report-box-flair glyphicon glyphicon-map-marker"></span>
					<div class="report-box-text report-box-flair bolder-text">Inaccurate/False Information</div>
				</div>
				<div class="report-choice-div report-box-inside">
					<div class="basic-text">Choose One:</div>
					<ul class="report-radio-list">
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="inaccurate">Inaccurate location</span>
						</li>
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="false-info">False information</span>
						</li>
					</ul>
					<div class="submit-button report-submit" style="height:29px;line-height:21px">SUBMIT</div>
				</div>
			</div>
			<div class="col-md-4 report-box transition-all">
				<div class="report-box-div report-box-inside">
					<span class="report-box-icon report-box-flair glyphicon glyphicon-alert"></span>
					<div class="report-box-text report-box-flair bolder-text">Inappropriate/<br>Off-Topic Content</div>
				</div>
				<div class="report-choice-div report-box-inside">
					<div class="basic-text">Choose One:</div>
					<ul class="report-radio-list">
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="vulgar">Inappropriate language</span>
						</li>
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="vulgar">Vulgar</span>
						</li>
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="off-topic">Off topic/unhelpful</span>
						</li>
					</ul>
					<div class="submit-button report-submit" style="height:29px;line-height:21px">SUBMIT</div>
				</div>
			</div>
			<div class="col-md-4 report-box transition-all" style="border:none;border-radius:0 2px 2px 0">
				<div class="report-box-div report-box-inside">
					<span class="report-box-icon report-box-flair glyphicon glyphicon-piggy-bank"></span>
					<div class="report-box-text report-box-flair bolder-text">Self Promotion/Spam</div>
				</div>
				<div class="report-choice-div report-box-inside">
					<div class="basic-text">Choose One:</div>
					<ul class="report-radio-list">
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="spam">Spam</span>
						</li>
						<li class="radio-choice report-radio-div">
							<div class="empty-circle"></div>
							<span class="basic-text radio-text" id="spam">Self promotion</span>
						</li>
					</ul>
					<div class="submit-button report-submit" style="height:29px;line-height:21px">SUBMIT</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="new-spot-form-div">
					<div class="report-box-div">
						<div class="new-spot-text" style="text-align:left">Other</div>
						<textarea name="other" rows="2" class="new-spot-input" id="report-other"></textarea>
					</div>
				</div>
			</div>
			<div class="sliding-button small-button" id="other-submit" style="float:right;margin-right:10px;margin-top:10px">SUBMIT</div>
		</div>
	</div>
</div>
<script>
var reportType;
var reportId;
var reportName;
/*
$(".report-box").hover(
	function() {
		$(this).find(".report-box-text").css("color", "rgba(255, 255, 255, 0.9)");
	},
	function() {
		$(this).find(".report-box-text").css("color", "rgba(0, 0, 0, 0.7)");
	}
)
*/
$(".report-box-div").click(function() {
	var boxWidth = $(".report-box").first().width() + 20;
	var reportChoice = $(this).parent().find(".report-choice-div");
	reportChoice.css("right", "-" + boxWidth + "px").show();
	$(this).animate({
		left: "-" + boxWidth + "px"
	}, 300);
	reportChoice.animate({
		right: "0px"
	}, 300);
});
//holds radio choice offense
var offense = null;
//----------------------------------------|
//          Custom radio buttons  	      |
//----------------------------------------|
$(".radio-choice").hover(
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.9)");
		$(this).find(".empty-circle").css("border", "2px solid rgba(0, 0, 0, 0.9)");
	},
	function() {
		$(this).css("color", "rgba(0, 0, 0, 0.6)");
		$(this).find(".empty-circle").css("border", "2px solid rgba(0, 0, 0, 0.3)");
	}
);
//add inside-dot, change active filterTypeChoice
$(".radio-choice").click(function() {
	var circle = $(this).find(".empty-circle");
	$(".inside-dot").remove();
	circle.append('<div class="inside-dot"></div>');
	$(".empty-circle").css("background", "white");
	circle.css("background", "#398bc6");
	offense = $(this).find(".radio-text").attr('id');
});
//----------------------------------------|
//          Activate Modal Click  	      |
//----------------------------------------|
$(document).on("click", ".activate-report", function() {
	//close pic carousel if open
	$("#pic-carousel").fadeOut();
	//return report boxes to normal
	$("#report-form-errors").html("").hide();
	$(".report-box-div").css("left", "0px");
	$(".report-choice-div").hide();
	if(sessionStorage.getItem('username')) {
		var parent = $(this).parent();
		//get feature info
		reportType = parent.find(".report-type").text();
		reportName = parent.find(".report-name").text();
		reportId = parent.find(".report-id").text();
		//drop down modal
		$("#modal-overlay").fadeIn();
		$("#report-modal").show().animate({
			top: "75px"
		}, 300);
		//update report name
		if(reportType == "route" || reportType == "spot") {
			$("#report-name").text("Report " + reportName);
		} else {
			$("#report-name").text("Report " + reportType);
		}
	} else {
		slideUserModal(false, "login");
	}
});
//----------------------------------------|
//             Report Submit  	          |
//----------------------------------------|
$(".report-submit").click(function() {
	var reportObj = {"content_id": reportId, "content_type": reportType, "content_name": reportName, "offense": offense}
	$.post("ajax/report/new_report.php", reportObj, function(resp) {
		if(resp.success) {
			$("#report-modal").animate({
				top: "-1000px"
			}, 600);
			$("#modal-overlay").fadeOut();
			$("#report-form-errors").hide()
			showFlash("Success! Thanks for your input, actions will be taken accordingly", "success", "info-sign");
		} else {
			$("#report-form-errors").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
		}
	}, 'json');
});
//for submit to other
$("#other-submit").click(function() {
	var reportMessage = $("#report-other").val();
	if(reportMessage.length == 0) {
		$("#report-form-errors").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>Nothing typed in 'other' choose a category or enter your concern in the 'other' text box</div>");
	} else {
		var reportObj = {"content_id": reportId, "content_type": reportType, "content_name": reportName, "message": reportMessage}
		$.post("ajax/report/new_report.php", reportObj, function(resp) {
			if(resp.success) {
				$("#report-modal").animate({
					top: "-1000px"
				}, 600);
				$("#modal-overlay").fadeOut();
				$("#report-form-errors").hide()
				showFlash("Success! Thanks for your input, actions will be taken accordingly", "success", "info-sign");
			} else {
				$("#report-form-errors").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
			}
		}, 'json');
	}
});
</script>
<div id="mobile-upload-modal">
	<div id="dropzone-half">
		<span class="glyphicon glyphicon-remove close-fixed-modal" id="close-upload-modal"></span>
		<form action="" method="POST" enctype="multipart/form-data" id="pic-upload-form">
			<input type="hidden" name="key" value="">
			<input type="file" name="file" id="file-browse" multiple style="display:none">
		</form>
		<form action="" method="POST" enctype="multipart/form-data" id="preview-upload-form">
			<input type="hidden" name="key" value="">
			<input type="file" name="file" id="file-browse-preview" multiple style="display:none">
		</form>
		<div id="file-dropzone">
			<div id="upload-errors" class="form-error-area" style="position:absolute;width:100%;top:25px"></div>
			<div class="upload-info-area">
				<div id="upload-logo"></div>
				<div class="dropzone-text">Tap to Upload</div>
			</div>
		</div>
	</div>
	<div id="file-half">
		<div id="upload-info-header"></div>
		<div id="picture-list">
			<div id="search-loading-text">No Uploads</div>
		</div>
	</div>
	<div id="upload-info-footer">
		<div class="basic-text" id="upload-to-name"></div>
		<div class="sliding-button small-button" id="save-picture-list">Save</div>
	</div>
</div>
<script src="assets/js/load-image-all.min.js" type="text/javascript"></script>
<script src="assets/js/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.ui.widget.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-process.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-validate.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-image.js" type="text/javascript"></script>
<script src="assets/js/jquery.iframe-transport.js" type="text/javascript"></script>
<script type="text/javascript">
/******************************************
-------------------------------------------
 	     Jquery File Upload to S3
-------------------------------------------
*******************************************/
var uploadForm = $("#pic-upload-form");
var previewForm = $("#preview-upload-form");
var uploadUrl;
var uploadOwnerId;
var uploadType;
var uploadName;
//array of objects that will hold pic names along with public id's, for storing/deleting
var uploadedPictures = Array();
//activate modal, get policy for upload, append to form
$(document).on("click", ".activate-upload-modal", function() {
	if(sessionStorage.getItem('user_id')) {
		$.post("ajax/picture/upload_policy.php", function(resp) {
			if(resp) {
				uploadForm.attr("method", resp.url);
				uploadUrl = resp.url;
				$.each(resp.inputs, function(key, value) {
					var newInput = "<input type='hidden' name='" + key + "' value='" + value + "'>";
					uploadForm.append(newInput);
					previewForm.append(newInput);
				});
			}
		}, 'json');
		//assign vars to hidden id and type in whichever 'add photo' button is clicked
		uploadOwnerId = $(this).find(".upload-id-info").text();
		uploadType = $(this).find(".upload-type-info").text();
		uploadName = $(this).find(".upload-name-info").text();
		var displayName = uploadName;
		if(uploadName.length > 17) {
			displayName = uploadName.substring(0, 16) + "...";
		}
		$("#upload-to-name").html("<em><strong>" + displayName + "</strong></em>");
		$("#mobile-upload-modal").fadeIn();
	} else {
		showFlash("You must be logged in to add pictures!", "danger", "icon");
	}
});
//file upload
uploadForm.fileupload({
	url: S3URL,
	type: "POST",
	dataType: 'xml',
	dropZone: $("#file-dropzone"),
	fileInput: uploadForm.find('input[name="file"]'),
	disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
   	maxFileSize: 10000000,
   	minFileSize: 100000,
   	imageMaxWidth: 1200, 
   	imageMaxHeight: 1200,
   	imageMinWidth: 1000, 
   	imageMinHeight: 1000,
   	acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
    process: [
    	{
    		action: 'load',
    		fileTypes: /^image\/(gif|jpeg|png)$/,
    		maxFileSize: 20000000
    	},
    	{
    		action: 'resize',
    		imageOrientation: true,
    		always: true
    	},
    	{
    		action: 'save'
    	}
    ],
	add: function(event, data) {
		//warning message
		window.onbeforeunload = function() {
			console.log("no");
		}
		var file = data.files[0];
		//assign content type of file
		uploadForm.find('input[name="Content-Type"]').val(file.type);
		//create preview image if file is smaller than 5mb, use default if bigger (for better performance)
		var previewURL;
		if(file.size <= 5000000) {
			var reader = new FileReader();
			reader.onload = function(e) {
				previewURL = e.target.result;
			}
			reader.readAsDataURL(file);
		} else {
			previewURL = "assets/images/default-preview.png";
		}
		//process (resize) image before sending to s3
		var $this = $(this)
		data.process(function() {
			return $this.fileupload('process', data);
		}).done(function() {
			//create new file row with pic info
			fileListHTML(data.files[0].name, previewURL);
			//create new pub id in php return and submit to s3
			var pubObj = {"column_id": uploadOwnerId, "column": uploadType, "feature_name": uploadName, "js_date": new Date()};
			$.post("ajax/picture/create_public_id.php", pubObj, function(resp) {
				if(resp.success) {
					uploadForm.find('input[name="key"]').val(resp.public_id);
					//push name and publicId to array for possible deletion
					uploadedPictures.push({"name": file.name, "public_id": resp.public_id, 'user_name': resp.user_name, 'user_id': resp.user_id, 'user_avatar': resp.user_avatar, 'upvotes': 1, 'js_date': new Date(), "upvoted": true});
					//submit to s3
					data.submit();
					data.public_id = resp.public_id + "s"
					previewForm.fileupload('add', {files: data});
				} else {
					uploadError("You must be logged in to add pictures")
				}
			}, 'json') 
		});
	},
	progress: function(e, data) {
		var percent = Math.round((data.loaded / data.total) * 100);
		//find which file the percentage belongs too, update progress bar and percentage
		$(".file-row").each(function() {
			if($(this).find(".upload-file-name").attr('title') == data.files[0].name) {
				$(this).find(".upload-percent").html(percent + "% Complete");
				$(this).find(".progress-bar").css("width", percent + "%");
			}
		});
	},
	fail: function(e, data) {
		$(".file-row").each(function() {
			if($(this).find(".upload-file-name").attr('title') == data.files[0].name) {
				$(this).find(".upload-percent").html("Failed!");
				$(this).find(".progress-bar").css({
					width: "100%",
					background: "#ffe6e6"
				});
			}
		});
		//delete pic from local db 
		deletePicture(data.files[0].name, "db");
	},
	done: function(event, data) {
		//if file is larger than 5mb, replace default img with image now on s3
		if(data.files[0].size > 5000000) {
			$(".file-row").each(function() {
				var $this = $(this);
				var picName = $this.find(".upload-file-name").attr('title');
				if(picName == data.files[0].name) {
					for(var i=0;i<uploadedPictures.length;i++) {
						if(uploadedPictures[i].name == picName) {
							$this.find(".upload-image").attr('src', S3URL + "/" + uploadedPictures[i].public_id);
						}
					}
				}
			});
		}
	}
});
//uploadForm will pass data to this form in order to upload a second smaller file
previewForm.fileupload({
	url: S3URL,
	type: "POST",
	dataType: 'xml',
	dropZone: null,
	disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
   	maxFileSize: 10000000,
   	imageMaxWidth: 400, 
   	imageMaxHeight: 400,
   	imageMinWidth: 200, 
   	imageMinHeight: 200,
   	acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
   	process: [
    	{
    		action: 'load',
    		fileTypes: /^image\/(gif|jpeg|png)$/,
    		maxFileSize: 20000000
    	},
    	{
    		action: 'resize',
    		imageOrientation: true,
    		always: true
    	},
    	{
    		action: 'save'
    	}
    ],
   	add: function(e, data) {
   		//data var is different depending on click or drop, if form is not undefined it is from a click
   		if(typeof data.files[0].form != "undefined") {
   			var newData = data.files[0];
   			var thisPubId = newData.public_id;
	   		newData.form = $(this);
	   		newData.form[0][0].value = thisPubId;
	   		var $this = $(this);
	   		newData.process(function() {
				return $this.fileupload('process', newData);
			}).done(function() {
				previewForm.find('input[name="Content-Type"]').val(newData.files[0].type);
				previewForm.find('input[name="key"]').val(thisPubId);
				previewForm.fileupload('send', {files: newData.files[0]})
			});
   		} else {
   			var $this = $(this);
   			data.files[0].process(function() {
				return $this.fileupload('process', data.files[0]);
			}).done(function() {
				previewForm.find('input[name="Content-Type"]').val(data.files[0].files.type);
				previewForm.find('input[name="key"]').val(data.files[0].public_id);
				previewForm.fileupload('send', {files: data.files[0].files});
			});
   		}
   	}
});
//----------------------------------------|
//               Events                   |
//----------------------------------------|
//handles any validation failures
uploadForm.on('fileuploadprocessfail', function(e, data) {
	uploadError(data.files[data.index].error);
});
//opens file browser on drop zone click
$("#file-dropzone").click(function() {
	uploadForm.find('input[name="file"]').click();
});
//delete pic from s3 and db on glyphicon remove click
$(document).on("click", ".upload-remove-icon", function() {
	//get first parent
	var $this = $(this).parents(".file-row");
	//check if upload failed
	if($this.find(".upload-percent").html() != "Failed!") {
		//find picture name
		var picName = $this.find(".upload-file-name").attr('title');
		//delete pic
		deletePicture(picName, "both");
		$this.remove();
	} else if($this.find(".upload-percent").html() == "100% Complete") {
		$this.remove();
	}
	checkUploadList();
});
//clears picture list, removes modal
$("#save-picture-list").click(function() {
	if(uploadedPictures.length >= 1) {
		//push to spot pics
		var spotPicsLength = spotPics.length;
		if(spotPicsLength !== 0) {
			for(var i=0;i<uploadedPictures.length;i++) {
				spotPics.push(uploadedPictures[i]);
			}
		} else {
			spotPics = uploadedPictures;
		}
		//if uploading to route apply pic to routes div
		if(uploadType == "route") {
			$(".route-spot-div").each(function() {
				$this = $(this);
				var routeId = $this.find(".upload-id-info").text();
				if(routeId == uploadOwnerId) {
					$this.find(".route-pic").attr('src', generateS3URL(spotPics[spotPicsLength].public_id) + "s");
					//break
					return false;
				}
			});
		}
	}
	$(".file-row").remove();
	$("#search-loading-text").show();
	uploadedPictures = Array();
	//scroll to top
	$("html, body").animate({
		scrollTop: "0"
	}, 300);
	$("#mobile-upload-modal").fadeOut();
});
$("#close-upload-modal").click(function() {
	$("#mobile-upload-modal").fadeOut();
})
//delete pic from local db or s3 and local db depending on options (options is 'both' or anything else)
function deletePicture(picName, options) {
	var picId = false;
	//will hold index of matched obj in uploadedPictures, for splicing
	var objIndex;
	//find matching picName in uploadedPictures, get public id in same obj
	for(var i=0;i<uploadedPictures.length;i++) {
		if(uploadedPictures[i].name == picName) {
			picId = uploadedPictures[i].public_id;
			objIndex = i;
		}
	}
	//create obj to send to backend, 'options' is for choosing between deletion from s3 and the db or just local db
	var picObj = {"public_id": picId, "options": options};
	//send through ajax for deletion
	if(picId) {
		$.post("ajax/picture/delete_picture.php", picObj, function(resp) {
			//remove from array
			uploadedPictures.splice(objIndex, 1);
		}, 'json')
	}
}
//----------------------------------------|
//          Upload functions              |
//----------------------------------------|
//html for files added to file list
function fileListHTML(name, pic) {
	if(name.length > 25) {
		var newName = name.substring(0, 24) + "...";
	} else var newName = name;
	var fileDiv = '<div class="file-row">' +
					'<div class="progress" style="height:75px">' +
						'<div class="progress-bar" role="progressbar" style="width:0%;"></div>' +
						'<div class="upload-info">' +
							'<div class="row some-gutters">' +
								'<div class="col-xs-3">' +
									'<div class="upload-img-preview">' +
										'<img src="' + pic + '" class="upload-image" />' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-8">' +
									'<span class="upload-file-name" title="' + name + '">' + newName + '</span><br>' +
									'<span class="upload-percent">0% Complete</span>' +
								'</div>' +
								'<div class="col-xs-1">' +
									'<span class="upload-remove-icon glyphicon glyphicon-remove" title="Remove Picture"></span>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';
	$("#picture-list").append(fileDiv);
	//check if uploads div is empty, display text if so
	checkUploadList();
}
function uploadError(message) {
	$("#upload-errors").html("").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + message + "</div>");
	setTimeout(function() { $("#upload-errors").fadeOut() }, 5000);
}
//check if uploads div is empty, display text if so
function checkUploadList() {
	$(".file-row").length <= 0 ? $("#search-loading-text").show() : $("#search-loading-text").hide();
}
</script>
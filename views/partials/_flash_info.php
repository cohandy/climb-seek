<div id="flash-div" class="alert alert-warning" role="alert">
	<span class="glyphicon glyphicon-alert alert-icon" id="flash-icon" style="margin-right:10px;"></span>
	<span id="flash-text"></span>
	<span class="glyphicon glyphicon-remove flash-remove-icon" id="flash-remove-icon"></span>
</div>
<script>
//glyphicon-info-sign
//glyphicon-alert
function showFlash(message, type, icon) {
	$("#flash-div").show().removeClass().addClass("alert alert-" + type);
	$("#flash-icon").removeClass().addClass("glyphicon glyphicon-" + icon + " alert-icon");
	$("#flash-text").text(message);
	$("#flash-div").animate({
		top: "58px"
	}, 500);
}
$("#flash-remove-icon").click(function() {
	$("#flash-div").animate({
		top: "-100px"
	}, 500, function() {
		$("#flash-div").hide();
	});
});
//showFlash("This is a test", "success", "info-sign");
</script>
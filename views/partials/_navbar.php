<div id="navbar">
	<a href="/" class="bolder-text navbar-content transition-all" id="nav-logo">climb&seek</a>
	<a href="/" id="nav-mobile-logo"><img src="assets/images/small-nav-logo.png" class="nav-logo" /></a>
	<a href="explore" id="nav-explore" class="navbar-content basic-text transition-all default-link" style="float:right">EXPLORE</a>
	<!-- not logged in state -->
	<?php if(!isset($_SESSION['user'])) { ?>
		<div id="navbar-options" style="float:right;display:inline-block">
			<?php if($on_mobile) { ?>
				<a href="login" class="navbar-content basic-text transition-all">LOG IN</a>
				<a href="signup" class="navbar-content basic-text nav-button transition-all">SIGN UP</a>
			<?php } else { ?>
				<span class="navbar-content basic-text transition-all" id="login">LOG IN</span>
				<span class="navbar-content basic-text nav-button transition-all" id="signup">SIGN UP</span>
			<?php } //end of logn/signup link if ?>
		</div>
	<?php } else { ?>
	<!-- logged in state -->
		<div id="navbar-options" class="custom-dropdown-div transition-all" style="float:right;display:inline-block">
			<?php if($_SESSION['user']['avatar']) { 
				$avatar = AWS_URL . "/" . $_SESSION['user']['avatar'] . "s";
			} else {
				$avatar = "assets/images/default-avatar.png";
			}
			?>
			<span style="position:relative;bottom:1px"><img src=<?php echo $avatar; ?> class="img-circle" style="height:35px;width:35px"/></span>
			<span class="navbar-content basic-text" style="margin-left:5px"><?php echo strtoupper($_SESSION['user']['username']); ?></span>
			<span class="glyphicon glyphicon-menu-down navbar-content" style="margin-left:2px;font-size:.9em"></span>
			<div id="custom-dropdown-menu">
				<div class="user-dropdown-option transition-all" id="explore-dropdown-option">
					<a href="explore"><span class="div-link"></span></a>
					<span class="glyphicon glyphicon-picture"></span>
					<span class="basic-text"> EXPLORE</span>
				</div>
				<div class="user-dropdown-option transition-all">
					<a href="new"><span class="div-link"></span></a>
					<span class="glyphicon glyphicon-map-marker"></span>
					<span class="basic-text"> ADD SPOT</span>
				</div>
				<div class="user-dropdown-option transition-all">
					<a href=<?php echo "profile/" . $_SESSION['user']['username']; ?>><span class="div-link"></span></a>
					<span class="glyphicon glyphicon-user"></span>
					<span class="basic-text"> PROFILE PAGE</span>
				</div>
				<div class="user-dropdown-option transition-all default-link">
					<a href="edit"><span class="div-link"></span></a>
					<span class="glyphicon glyphicon-edit"></span>
					<span class="basic-text"> EDIT PROFILE</span>
				</div>
				<div class="user-dropdown-option transition-all" id="logout">
					<span class="glyphicon glyphicon-log-out"></span>
					<span class="basic-text"> LOG OUT</span>
				</div>
			</div>
		</div>
	<?php } //end of session if ?>
</div>
<div id="moving-nav"></div>
<script>
//global var for pic link url
var S3URL = <?php echo json_encode(AWS_URL); ?>;
var loggedIn = <?php if(isset($_SESSION['user'])) { echo json_encode($_SESSION['user']); } else { echo "false"; } ?>;
if(loggedIn) {
	sessionStorage.setItem("user_id", loggedIn.id);
	sessionStorage.setItem("username", loggedIn.username);
	if(loggedIn.avatar) {
		sessionStorage.setItem('avatar', loggedIn.avatar);
	}
} else {
	sessionStorage.removeItem('user_id');
	sessionStorage.removeItem('username');
	sessionStorage.removeItem('avatar');
}
//logout
$("#logout").click(function() {
	$.post("ajax/user/logout.php", function() {
		sessionStorage.removeItem('user_id');
		sessionStorage.removeItem('username');
		sessionStorage.removeItem('lat');
		sessionStorage.removeItem('long');
		sessionStorage.removeItem('avatar');
		sessionStorage.removeItem('checked');
		window.location.assign("");
	});
});
//initialize tooltips
$("[data-toggle='tooltip']").tooltip();
/* ********************************
   --------------------------------
   |        User Dropdown         |
   ________________________________
   ********************************
*/
$(".custom-dropdown-div").click(function(e) {
	e.stopPropagation();
	var menu = $("#custom-dropdown-menu");
	if(menu.css("display") == "none") {
		menu.slideDown("fast");
		$(this).css("background", "rgba(0, 0, 0, 0.4)");
	} else {
		menu.slideUp("fast");
		$(this).css("background", "none");
	}
});
$(".user-dropdown-option").click(function(e) {
	e.stopPropagation()
});
//close dropdown on any click
$(document).click(function() {
	$("#custom-dropdown-menu").slideUp("fast");
	$(".custom-dropdown-div").css("background", "none");
});
/* ********************************
   --------------------------------
   |        Navbar Events         |
   ________________________________
   ********************************
*/
$(document).scroll(function() {
	var moveNav = $("#moving-nav");
	if($(this).scrollTop() != 0) {
		moveNav.fadeIn();
	} else if(moveNav.css("background-color") !== "transparent") {
		moveNav.fadeOut();
	}
});
</script>
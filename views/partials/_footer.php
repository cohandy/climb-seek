<div class="container-fluid footer-container" id="footer">
	<div class="container">
		<div id="footer-icons">
			<div class="footer-icon-col">
				<div class="footer-icon-div">
					<a href="https://www.facebook.com/climbandseek" class="div-link" target="_blank"></a>
					<img src="assets/images/f-footer-icon.png" class="footer-icon"/ alt="facebook-icon">
				</div>
				<div class="footer-icon-div">
					<a href="https://twitter.com/climbandseek" class="div-link" target="_blank"></a>
					<img src="assets/images/t-footer-icon.png" class="footer-icon" alt="twitter-icon"/>
				</div>
				<div class="footer-icon-div">
					<a href="https://www.instagram.com/climbandseek" class="div-link" target="_blank"></a>
					<img src="assets/images/i-footer-icon.png" class="footer-icon" alt="instagram-icon"/>
				</div>
			</div>
		</div>
		<div id="footer-links">
			<div class="footer-icon-col">
				<a href="/" class="footer-link basic-text">HOME</a>
				<span class="footer-link-spacer">|</span>
				<a href="explore" class="footer-link basic-text">EXPLORE</a>
				<span class="footer-link-spacer">|</span>
				<a href="about" class="footer-link basic-text">ABOUT</a>
				<span class="footer-link-spacer">|</span>
				<a href="contact" class="footer-link basic-text">CONTACT</a>
				<span class="footer-link-spacer">|</span>
				<a href="terms" class="footer-link basic-text">TERMS</a>
				<span class="footer-link-spacer">|</span>
				<a href="privacy" class="footer-link basic-text">PRIVACY</a>
			</div>
		</div>
		<div class="row" style="margin-top:10px;text-align:center">
			<div class="col-xs-12">
				<div class="basic-text sub-footer-info-name">Made in Santa Cruz, California <span class="glyphicon glyphicon-map-marker"></span> </div>
			</div>
		</div>
		<div class="row" style="margin-top:8px;text-align:center">
			<div class="col-xs-12">
				<span class="basic-text sub-footer-copyright">&copy;climbandseek | 2017</span>
				<!-- paypal donate -->
			</div>
		</div>
	</div>
</div>
<script>
if(document.title == "Error | climb&seek") {
	$("#footer").hide();
}
</script>
<div id="flash-div">
	<span id="flash-text" class="basic-text"></span>
	<span class="glyphicon glyphicon-remove flash-remove-icon" id="flash-remove-icon"></span>
</div>
<script>
//glyphicon-info-sign
//glyphicon-alert
//green #d2ebc7
//red #ebcdcd
//blue #cae6f4
function showFlash(message, type, icon) {
	var background = "#d2ebc7";
	if(type == "success") {
		background = "#d2ebc7"
	} else if(type == "danger") {
		background = "#ebcdcd";
	} else {
		background = "#cae6f4";
	}
	$("#flash-div").css("background", background).show();
	$("#flash-text").text(message);
	$("#flash-div").animate({
		top: "58px"
	}, 500);
}
$("#flash-remove-icon").click(function() {
	$("#flash-div").animate({
		top: "-100px"
	}, 500, function() {
		$("#flash-div").hide();
	});
});
//showFlash("This is a test", "success", "info-sign");
</script>
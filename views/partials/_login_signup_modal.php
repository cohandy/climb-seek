<div id="modal-overlay"></div>
<div class="login-signup-modal" id="login-signup-modal"></div>
<script type="text/javascript">
//open modals
$("#login, #signup").click(function() {
	if($(this).attr('id') == "login") {
		slideUserModal(false, "login")
	} else {
		slideUserModal(false, "signup");
	}
})
//close modal with overlay click
$("#modal-overlay").click(function() {
	slideUserModal(true, "no");
	if($("#upload-modal").css("display") !== "none") {
		$("#upload-modal").animate({
			top: "-500px"
		}, 300);
	}
	if($("#report-modal").css("display") !== "none") {
		$("#report-modal").animate({
			top: "-1000px"
		}, 600);
	}
});
//if remember me is checked, true
var rememberMe = null;
$(document).on("click", ".remember-div", function() {
	if(!rememberMe) {
		$("#remember-box").html("<span class='glyphicon glyphicon-ok' id='checkmark'></span>");
		rememberMe = true;
	} else {
		$("#remember-box").html("");
		rememberMe = null;
	}
})
//in login form, switch to sign up if link is clicked
$(document).on("click", "#signup-link", function() {
	$("#login-signup-modal").html(signupForm);
	$("#login-signup-modal .login-input").first().focus();
})
$(document).on("click", "#login-link", function() {
	$("#login-signup-modal").html(loginForm);
	$("#login-signup-modal .login-input").first().focus();
})
//slide in/out lgoin/signup modal
function slideUserModal(visible, form) {
	if(form == "login") {
		$("#login-signup-modal").html(loginForm);
	} else if(form == "signup") {
		$("#login-signup-modal").html(signupForm);
	} 
	if(visible == false) {
		$("#modal-overlay").fadeIn();
		$("#login-signup-modal").show();
		$("#login-signup-modal").stop().animate({
			top: "75px"
		}, 300);
		$("#login-signup-modal .login-input").first().focus();	
	} else {
		$("#modal-overlay").fadeOut();
		$("#login-signup-modal").stop().animate({
			top: "-500px"
		}, 300, function() {
			$("#login-signup-modal").hide();
		});
	}
}
//check individual inputs via ajax, show correct/wrong icon depending on validations
$(document).on("blur", "#signup-form .login-input", function() {
	var input = $(this);
	var field = input.serialize();
	//if checking confirm, add first password for comparison
	if(input.attr('name') == "confirm") {
		field += "&password=" + $("#password-input").val();
	}
	$.ajax({
		url: "ajax/user/login_signup.php",
		type: "post",
		data: field,
		dataType: "json",
		success: function(resp) {
			if(!resp.success) {
				input.next().html('<span class="glyphicon glyphicon-remove-circle form-input-icon wrong-icon"></span>')
			} else {
				input.next().html('<span class="glyphicon glyphicon-ok-circle form-input-icon correct-icon"></span>')
			}
		}
	})
});
//submit login/signup
$(document).on("submit", "#signup-form, #login-form", function(e) {
	e.preventDefault();
	var fields = $(this).serialize();
	fields += "&remember=" + rememberChoice;
	$.ajax({
		url: "ajax/user/login_signup.php",
		type: "post",
		data: fields,
		dataType: "json",
		success: function(resp) {
			//if resp.length is undefined that means an array of errors was returned
			if(resp.length == undefined) {
				if(!resp.success) {
					$("#form-error-div").html("").show();
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
				} else {
					//close modal, change nav to logged in state
					sessionStorage.setItem('user_id', resp.user_id);
					sessionStorage.setItem('username', resp.username);
					sessionStorage.setItem('email', resp.email);
					sessionStorage.setItem('main_map_filter', "Ropes");
					if(resp.avatar) {
						sessionStorage.setItem('avatar', resp.avatar);
					}
					//go to page depending on current location
					if(document.title == "Home | climb&seek") {
						window.location.assign("index.php?p=profile&username=" + resp.username);
					} else {
						window.location.assign(window.location.href);
					}
				}
			} else {
				$("#form-error-div").html("").show();
				for(var i=0;i<resp.length;i++) {
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
				}
			}
		}
	});
});
//highlights inputs on focus
$(document).on("focus", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(document).on("blur", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
//----------------------------------------|
//           Custom checkbox      	      |
//----------------------------------------|
var rememberChoice = false;
$(document).on("mouseenter", ".login-checkbox", function() {
	$(this).css("color", "white");
	$(this).find(".check-box").css("border", "2px solid white");
});
$(document).on("mouseleave", ".login-checkbox", function() {
	$(this).css("color", "rgba(255, 255, 255, 0.8)");
	$(this).find(".check-box").css("border", "2px solid rgba(255, 255, 255, 0.8)");
});
$(document).on("click", ".login-checkbox", function() {
	if(rememberChoice) {
		$(this).find(".check-box").html("");
		rememberChoice = false;
	} else {
		$(this).find(".check-box").html("<span class='glyphicon glyphicon-ok' id='login-checkmark'></span>");
		rememberChoice = true;
	}
});
var loginForm = '<div class="login-background-image"></div>' +
				'<div class="modal-header-user">' +
					'<div class="modal-header-text">Log In</div>' +
					'<div class="modal-header-subtext">Continue your climbing adventure</div>' +
				'</div>' +
				'<div id="form-error-div" class="form-error-area">' +
				'</div>' +
				'<form method="post" action="#" id="login-form" class="login-signup-form">' +
					'<div class="input-div" style="margin-top:10px">' +
						'<div class="login-form-text basic-text">Email</div>' +
						'<input type="text" name="email" placeholder="Enter email..." class="login-input bolder-text transition-all">' +
					'</div>' +
					'<div class="input-div">' +
						'<div class="login-form-text basic-text">Password</div>' +
						'<input type="password" name="password" placeholder="Enter password..." class="login-input bolder-text transition-all">' +
					'</div>' +
					'<input type="hidden" name="form" value="login">' +
					'<div class="input-div" style="margin-top:20px;margin-bottom:50px">' +
						'<input type="submit" class="submit-button" id="login-submit" value="Log In">' +
					'</div>' +
				'</form>' +
				'<div class="login-footer">' +
					'<div>' +
						'<div class="check-box-div radio-checkbox-div login-checkbox">' +
							'<div class="check-box transition-all login-check-box"></div><span class="basic-text">Remember me</span>' +
						'</div>' +
						'<a href="forgot" class="login-modal-link basic-text" id="forgot-password" style="position:relative;font-size:1.2em;top:1px">Forgot Password?</a>' +
					'</div>' +
					'<div class="input-div" style="margin-top:40px">' +
						'<div class="input-form-text">Not a member yet? <span class="login-modal-link" id="signup-link">Sign up here</span></div>' +
					'</div>' +
				'</div>';
var signupForm = '<div class="login-background-image"></div>' + 
				'<div class="modal-header-user">' +
					'<div class="modal-header-text">Sign Up</div>' +
					'<div class="modal-header-subtext">Become a part of our climbing community</div>' +
				'</div>' +
				'<div id="form-error-div" class="form-error-area">' +
				'</div>' +
				'<form method="post" action="#" id="signup-form" class="login-signup-form">' +
					'<div class="input-div">' +
						'<div class="login-form-text basic-text">Email</div>' +
						'<div style="position:relative">' +
							'<input type="text" name="email" placeholder="Enter email..." class="login-input signup-input bolder-text transition-all">' +
							'<div class="icon-area"></div>' + 
						'</div>' + 
					'</div>' +
					'<div class="input-div">' +
						'<div class="login-form-text basic-text">Username</div>' +
						'<div style="position:relative">' +
							'<input type="text" name="username" placeholder="Enter username..." class="signup-input login-input bolder-text transition-all">' +
							'<div class="icon-area"></div>' +
						'</div>' + 
					'</div>' +
					'<div class="input-div">' +
						'<div class="login-form-text basic-text">Password</div>' +
						'<div style="position:relative">' +
							'<input type="password" name="password" placeholder="Enter password..." class="signup-input login-input bolder-text transition-all" id="password-input">' +
							'<div class="icon-area"></div>' +  
						'</div>' +
					'</div>' +
					'<div class="input-div">' +
						'<div class="login-form-text basic-text">Confirm Password</div>' +
						'<div style="position:relative">' +
							'<input type="password" name="confirm" placeholder="Confirm password..." class="signup-input login-input bolder-text transition-all">' +
							'<div class="icon-area"></div>' +
						'</div>' +
					'</div>' +
					'<input type="hidden" name="form" value="signup">' +
					'<div class="input-div" style="margin-top:20px;margin-bottom:75px">' +
						'<input type="submit" class="submit-button" id="login-submit" value="Sign Up">' +
					'</div>' +
					'<div class="input-div">' +
						'<div class="input-form-subtext">By signing up, you agree to our Terms & Privacy Policy.' +
					'</div>' +
				'</form>' +
				'<div class="input-div" style="margin-top:20px">' +
					'<div class="input-form-text">Already a member? <a href="#" class="login-modal-link" id="login-link">Log in here</a></div>' +
				'</div>';
</script>
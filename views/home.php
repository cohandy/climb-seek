<?php
if($on_mobile) {
	$signup_link = "";
} else {
	$signup_link = "id='signup-modal'";
}
?>
<div class="container-fluid" style="overflow:hidden">
	<div class="row">
		<div class="banner-image1" id="banner-image">
			<div class="home-banner-overlay"></div>
			<div id="banner-content">
				<div><img src="assets/images/homepage-logo-white.png" id="homepage-logo"/></div>
				<div class="basic-text homepage-subtext">Discover climbing spots around the US</div>
				<div class="row no-gutters" style="margin-top:30px">
					<div class="basic-text homepage-choose-text">Choose one:</div>
					<div class="col-xs-4 homepage-icon-div" id="rock-climbing-div">
						<img src="assets/images/home-climbing-icon.png" class="homepage-icon"/>
						<div class="basic-text under-icon-text">Rock Climbing</div>
					</div>
					<div class="col-xs-4 homepage-icon-div" id="bouldering-div">
						<img src="assets/images/home-bouldering-icon.png" class="homepage-icon"/>
						<div class="basic-text under-icon-text">Bouldering</div>
					</div>
					<div class="col-xs-4 homepage-icon-div" id="indoor-div">
						<img src="assets/images/home-indoor-icon.png" class="homepage-icon"/>
						<div class="basic-text under-icon-text">Indoor/Gyms</div>
					</div>
				</div>
				<div class="row" style="padding:25px 50px">
					<a href="explore" class="home-explore-button basic-text transition-all">EXPLORE</a>
				</div>
			</div>
		</div>
		<div id="floating-arrow-div"><span class="glyphicon glyphicon-menu-down" id="floating-arrow"></span></div>
	</div>
	<div class="row">
		<div class="homepage-tile" id="explore-tile">
			<div class="home-banner-overlay"></div>
			<div class="homepage-tile-content" id="explore-tile-content">
				<div class="home-tile-header basic-text" id="explore-tile-header">EXPLORE</div>
				<div class="home-tile-text basic-text">
				Find unknown <span class="home-highlight-text">spots</span> in your area. Use <span class="home-highlight-text">filters</span> to find only the spots that fit your experience level or equipment. Or just use the <span class="home-highlight-text">search</span> bar!
				</div>
				<a href="explore" class="sliding-button large-button" style="width:175px;margin-top:20px;color:rgba(255, 255, 255, 0.9)">GET STARTED</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="homepage-tile" id="create-tile">
			<div class="home-banner-overlay"></div>
			<div class="homepage-tile-content" id="create-tile-content">
				<div class="home-tile-header basic-text">CREATE</div>
				<div class="home-tile-text basic-text"><span class="home-highlight-text">Like,</span> <span class="home-highlight-text">comment,</span> and <span class="home-highlight-text">upload</span> on your favorite spot or gyms page. Add routes, attach photos and upvote others contributions.</div>
				<div class="home-tile-flair basic-text">You could always, oh I don't know...<span class="theme-link signup-link" <?php echo $signup_link ?> >sign up</span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="homepage-tile" id="share-tile">
			<div class="home-banner-overlay"></div>
			<div class="homepage-tile-content" id="share-tile-content">
				<div class="home-tile-header basic-text">SHARE</div>
				<div class="home-tile-text basic-text">Build your <span class="home-highlight-text">profile</span> and watch it grow as you make more <span class="home-highlight-text">contributions</span>. Eventually it'll become a compendium of the <span class="home-highlight-text">best</span> local climbing.</div>
				<div class="home-tile-flair basic-text">I think we're taking <span class="theme-link signup-link" <?php echo $signup_link ?>>sign ups</span> right now</div>
			</div>
		</div>
	</div>
</div>
<script>
$(".homepage-icon-div").hover(
	function() {
		$(this).find(".homepage-icon").stop().animate({
			opacity:1
		}, 300)
	},
	function() {
		var icon = $(this).find(".homepage-icon")
		if(icon.attr('id') != "chosen") {
			$(this).find(".homepage-icon").stop().animate({
				opacity: 0.6
			}, 300)
		}
	}
);
$(".homepage-icon-div").click(function() {
	var thisId = $(this).attr('id');
	if(thisId == "bouldering-div") {
		sessionStorage.setItem('main_map_filter', "Bouldering")
	} else if(thisId == "indoor-div") {
		sessionStorage.setItem('main_map_filter', "Indoor")
	} else {
		sessionStorage.setItem('main_map_filter', "Ropes")
	}
	resetIconOpacities();
	$(this).find(".homepage-icon").css("opacity", "1").attr('id', "chosen");
	$(this).find('.under-icon-text').css("text-decoration", "underline");
});
function resetIconOpacities() {
	$(".homepage-icon").css("opacity", "0.6");
	$(".under-icon-text").css("text-decoration", "none");
	$(".homepage-icon").each(function() {
		$(this).attr('id', "");
	});
}
$(".signup-link").click(function() {
	console.log($(this).attr('id'))
	if($(this).attr('id') == "signup-modal") {
		slideUserModal(false, "signup");
	} else {
		window.location.assign('signup');
	}
});
$("#floating-arrow-div").click(function() {
	$("html, body").animate({
		scrollTop: $("#explore-tile").offset().top - 50 + "px"
	}, 500);
});
</script>
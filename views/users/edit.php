<?php if(isset($_SESSION['user'])) { 
	if(!$_SESSION['user']['avatar']) {
		$avatar = "assets/images/default-avatar-big.png";
		//set whether avatar or form will be hidden, both need to be in dom so they can be used later
		$hide_avatar = 'style="display:none;margin-top:10px"';
		$hide_form = "";
	} else {
		$avatar = AWS_URL . "/" . $_SESSION['user']['avatar'];
		$hide_avatar = "";
		$hide_form = 'style="display:none;margin-top:10px"';
	}
?>
<!-- edit form -->
<div class="container-fluid">
	<div class="add-spot-header" style="margin-top:55px;text-align:center">Edit Profile</div>
	<div id="edit-form-errors" class="form-error-area" style="text-align:center"></div>
	<div class="container" style="margin-top:10px;">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<form method="post" action="#" id="edit-user-form">
					<div class="row">
						<div class="col-md-6">
							<div <?php echo $hide_avatar; ?> class="edit-form-div" id="user-avatar" style="text-align:center;position:relative">
								<div class="change-avatar img-circle" style="display:none">
									<div class="bolder-text" style="font-size:2em;margin-top:100px;color:rgba(0, 0, 0, 0.8)">Change avatar?</div>
								</div> 
								<div class="new-spot-text" id="avatar-field-text" style="text-align:left">Avatar</div>
								<img src=<?php echo $avatar; ?> class="img-circle avatar-edit-page" id="official-avatar" />
							</div>
							<div <?php echo $hide_form; ?> class="edit-form-div" id="avatar-upload-form">
								<span class="new-spot-text" id="avatar-field-text">Avatar</span>
								<div id="avatar-upload-area">
									<div id="upload-errors" class="form-error-area" style="position:absolute;width:100%;top:25px;z-index:9000"></div>
									<div class="upload-info-area" style="top:50%">
										<div class="upload-logo-avatar"></div>
										<div class="dropzone-text" style="font-size:1.4em;color:rgba(0, 0, 0, 0.5)">Drop image or click<br> to upload avatar</div>
										<div class="dropzone-text" style="font-size:1em;color:rgba(0, 0, 0, 0.4)">*square images work best</div>
									</div>
									<div id="avatar-uploading" style="height:100%;display:none">
										<div class="progress avatar-progress" style="height:100%;">
											<div class="progress-bar" role="progressbar" style="width:0%;height:100%"></div>
											<img src="assets/images/default-avatar-big.png" class="img-circle avatar-edit-page" id="uploading-preview-avatar" style="height:150px;width:150px;margin-top:10px"/>
											<div class="basic-text" id="avatar-upload-percent" style="margin-top:10px">0% Complete</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="edit-form-div">
								<span class="new-spot-text">Email</span>
								<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
									<span class="glyphicon glyphicon-exclamation-sign"></span>
									<span class="error-text" style="margin-left:2px"></span>
								</span>
								<input type="text" name="email" class="new-spot-input test-input-ajax" placeholder="Enter email..." value="<?php echo sanitize($_SESSION['user']['email']); ?>">
							</div>
							<div class="edit-form-div">
								<span class="new-spot-text">Location</span>
								<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
									<span class="glyphicon glyphicon-exclamation-sign"></span>
									<span class="error-text" style="margin-left:2px"></span>
								</span>
								<input type="text" name="location" class="new-spot-input"  id="location-input" placeholder="Enter location..." value="<?php echo sanitize($_SESSION['user']['location']); ?>">
							</div>
							<div class="edit-form-div">
								<div class="default-link basic-text" style="font-size:1.2em" id="change-password">Change password...</div>
							</div>
						</div>
					</div>
					<div id="change-password-inputs" style="display:none">
						<div class="row edit-form-div">
							<div class="col-md-12">
								<span class="new-spot-text">Old Password</span>
								<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
									<span class="glyphicon glyphicon-exclamation-sign"></span>
									<span class="error-text" style="margin-left:2px"></span>
								</span>
								<input type="password" name="old_password" class="new-spot-input" placeholder="Enter old password..." id="edit-password-input"/>
							</div>
						</div>
						<div class="row edit-form-div">
							<div class="col-md-12">
								<span class="new-spot-text">New Password</span>
								<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
									<span class="glyphicon glyphicon-exclamation-sign"></span>
									<span class="error-text" style="margin-left:2px"></span>
								</span>
								<input type="password" name="password" class="new-spot-input" placeholder="Enter new password..." id="edit-password-input"/>
							</div>
						</div>
						<div class="row edit-form-div" id="confirm-row">
							<div class="col-md-12">
								<span class="new-spot-text">Confirm New Password</span>
								<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
									<span class="glyphicon glyphicon-exclamation-sign"></span>
									<span class="error-text" style="margin-left:2px"></span>
								</span>
								<input type="password" name="confirm" class="new-spot-input" placeholder="Confirm password..."/>
							</div>
						</div>
					</div>
					<div class="row some-gutters">
						<div class="col-md-12">
							<input type="submit" class="sliding-button medium-button" style="float:right;background:white;margin-top:10px;padding:7px 15px;width:150px;font-size:1.3em" value="UPDATE"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- mountain vector background -->
<div class="vector-background mobile-vector-background"></div>
<!-- upload forms -->
<form action="" method="POST" enctype="multipart/form-data" id="pic-upload-form">
	<input type="hidden" name="key" value="">
	<input type="file" name="file" id="file-browse" multiple style="display:none">
</form>
<form action="" method="POST" enctype="multipart/form-data" id="preview-upload-form">
	<input type="hidden" name="key" value="">
	<input type="file" name="file" id="file-browse-preview" multiple style="display:none">
</form>
<?php } else { ?>
<div class="container-fluid" style="margin-top:50px">
	<div>You must be logged in to edit your profile!</div>
</div>
<?php } //end main if?>
<script src="assets/js/load-image-all.min.js" type="text/javascript"></script>
<script src="assets/js/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.ui.widget.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-process.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-validate.js" type="text/javascript"></script>
<script src="assets/js/jquery.fileupload-image.js" type="text/javascript"></script>
<script src="assets/js/jquery.iframe-transport.js" type="text/javascript"></script>
<script type="text/javascript">
/* ********************************
   --------------------------------
   |         form events         |
   ________________________________
   ********************************
*/
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
//logo animation
$("#avatar-upload-area").hover(
	function() {
		$(".upload-logo-avatar").stop().animate({
			height: "120px",
			width: "120px"
		}, 200)
	},
	function() {
		$(".upload-logo-avatar").stop().animate({
			height: "110px",
			width: "110px"
		}, 200)
	}
);
//show change avatar overlay on official avatar pic
$("#user-avatar").hover(
	function() {
		$(".change-avatar").stop().fadeIn();
	},
	function() {
		$(".change-avatar").stop().fadeOut();
	}
);
//change avatar
$("#user-avatar").click(function() {
	$(this).hide();
	$("#avatar-upload-form").show();
});
//show change password input
$("#change-password").click(function() {
	var passwordDiv = $("#change-password-inputs");
	if(passwordDiv.css("display") == "none") {
		passwordDiv.slideDown();
	} else {
		passwordDiv.slideUp();
	}
});
/* ********************************
   --------------------------------
   |          Edit Form           |
   ________________________________
   ********************************
*/
//check username/email inputs via ajax, show errors if so
$(document).on("blur", ".test-input-ajax", function() {
	var nameInput = $(this).attr('name');
	//don't run validation if session vars equal whats in form input
	if(nameInput == "username" && sessionStorage.getItem("username") != $(this).val() || nameInput == "email" && sessionStorage.getItem('email') != $(this).val()) {
		var input = $(this);
		var field = input.serialize();
		//if checking confirm, add first password for comparison
		if(input.attr('name') == "confirm") {
			field += "&password=" + $("#password-input").val();
		}
		$.ajax({
			url: "ajax/user/login_signup.php",
			type: "post",
			data: field,
			dataType: "json",
			success: function(resp) {
				if(!resp.success) {
					editError(resp.error, input, "");
				} else {
					input.parent().find(".form-error").fadeOut();
				}
			}
		});
	}
});
//validate location
$("#location-input").keyup(function() {
	if($(this).val().length >= 35) {
		editError("Location is too long!", $(this), "");
	} else {
		$(this).parent().find(".form-error").fadeOut();
	}
});
//validate passwords
$(".password-inputs").blur(function() {
	if($(this).val().length < 7) {
		editError("Password is too short!", $(this), "");
	} else if($(this).val().length >= 35) {
		editError("Password is too long!", $(this), "");
	} else {
		$(this).parent().find(".form-error").fadeOut();
	}
});
//inline validation error
function editError(message, element, tip) {
	var formError = element.parent().find(".form-error")
	element.parent().find(".error-text").html(message);
	formError.show();
	formError.attr("data-original-title", tip);
}
//submit edit form
$("#edit-user-form").submit(function(e) {
	e.preventDefault();
	//check if user is changing passwords
	if($("#change-password-inputs").css('display') == "none") {
		var changePassword = false
	} else var changePassword = true;
	var formData = $(this).serialize();
	formData += "&change_password=" + changePassword;
	$.post("ajax/user/edit_profile.php", formData, function(resp) {
		//if resp.length is undefined that means an array of errors was returned
		if(resp.length == undefined) {
			if(!resp.success) {
				$("#edit-form-errors").html("").show();
				$("#edit-form-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
			} else {
				showFlash("Account successfully updated!", "success", "thumbs-up");
				//go to newly created spot page
				//window.location.replace("index.php?p=profile&username=" + resp.username);
			}
		} else {
			$("#edit-form-errors").html("").show();
			for(var i=0;i<resp.length;i++) {
				$("#edit-form-errors").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
			}
		}
	}, 'json');
});
/* ********************************
   --------------------------------
   |        Avatar Upload         |
   ________________________________
   ********************************
*/
var uploadForm = $("#pic-upload-form");
var previewForm = $("#preview-upload-form");
$.post("ajax/picture/upload_policy.php", function(resp) {
	if(resp) {
		uploadForm.attr("method", resp.url);
		uploadUrl = resp.url;
		$.each(resp.inputs, function(key, value) {
			var newInput = "<input type='hidden' name='" + key + "' value='" + value + "'>";
			uploadForm.append(newInput);
			previewForm.append(newInput);
		});
	}
}, 'json');
var uploadUrl;
var mainUploadDone = false;
//file upload
uploadForm.fileupload({
	url: S3URL,
	type: "POST",
	dataType: 'xml',
	dropZone: $("#avatar-upload-area"),
	fileInput: uploadForm.find('input[name="file"]'),
	disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
   	maxFileSize: 10000000,
   	minFileSize: 30000,
   	imageMaxWidth: 600, 
   	imageMaxHeight: 600,
   	imageMinWidth: 300, 
   	imageMinHeight: 300,
   	acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
    process: [
    	{
    		action: 'load',
    		fileTypes: /^image\/(gif|jpeg|png)$/,
    		maxFileSize: 20000000
    	},
    	{
    		action: 'resize',
    		imageOrientation: true,
    		always: true
    	},
    	{
    		action: 'save'
    	}
    ],
	add: function(event, data) {
		//warning message
		window.onbeforeunload = function() {
			console.log("no");
		}
		var file = data.files[0];
		//assign content type of file
		uploadForm.find('input[name="Content-Type"]').val(file.type);
		//create preview image if file is smaller than 5mb, use default if bigger (for better performance)
		var previewURL;
		if(file.size <= 5000000) {
			var reader = new FileReader();
			reader.onload = function(e) {
				previewURL = e.target.result;
			}
			reader.readAsDataURL(file);
		} else {
			previewURL = "assets/images/default-avatar-big.png";
		}
		//process (resize) image before sending to s3
		var $this = $(this)
		data.process(function() {
			return $this.fileupload('process', data);
		}).done(function() {
			//create new pub id in php return and submit to s3
			var pubObj = {"column": "avatar", "js_date": new Date()};
			$.post("ajax/picture/create_public_id.php", pubObj, function(resp) {
				if(resp.success) {
					uploadForm.find('input[name="key"]').val(resp.public_id);
					//submit to s3
					data.submit();
					data.public_id = resp.public_id + "s";
					previewForm.fileupload('add', {files: data});
					//update client side session avatar
					sessionStorage.setItem('avatar', resp.public_id);
					//hide logo and info, replace with preview and progress bar
					$(".upload-info-area").hide();
					$("#avatar-uploading").show();
					$("#uploading-preview-avatar").attr('src', previewURL);
				} else {
					uploadError("You must be logged in to add pictures");
				}
			}, 'json');
		});
	},
	progress: function(e, data) {
		var percent = Math.round((data.loaded / data.total) * 100);
		$("#avatar-upload-percent").text(percent + "% Complete");
		$(".progress-bar").css("width", percent + "%");
	},
	fail: function(e, data) {
		$(".progress-bar").css("width", "0%");
		$("#avatar-upload-percent").text("Failed! Reload page to try again");
	},
	done: function(event, data) {
		$("#avatar-upload-percent").text("Complete!");
		//refresh page, new avatar won't show up if not
		mainUploadDone = true;
		if(previewUploadDone) {
			location.reload(false);
		}
	}
});
var previewUploadDone = false;
//uploadForm will pass data to this form in order to upload a second smaller file
previewForm.fileupload({
	url: S3URL,
	type: "POST",
	dataType: 'xml',
	dropZone: null,
	disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
   	maxFileSize: 10000000,
   	imageMaxWidth: 100, 
   	imageMaxHeight: 100,
   	imageMinWidth: 50, 
   	imageMinHeight: 50,
   	acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
   	process: [
    	{
    		action: 'load',
    		fileTypes: /^image\/(gif|jpeg|png)$/,
    		maxFileSize: 20000000
    	},
    	{
    		action: 'resize',
    		imageOrientation: true,
    		always: true
    	},
    	{
    		action: 'save'
    	}
    ],
   	add: function(e, data) {
   		//data var is different depending on click or drop, if form is not undefined it is from a click
   		if(typeof data.files[0].form != "undefined") {
   			var newData = data.files[0];
   			var thisPubId = newData.public_id;
	   		newData.form = $(this);
	   		newData.form[0][0].value = thisPubId;
	   		var $this = $(this);
	   		newData.process(function() {
				return $this.fileupload('process', newData);
			}).done(function() {
				previewForm.find('input[name="Content-Type"]').val(newData.files[0].type);
				previewForm.find('input[name="key"]').val(thisPubId);
				previewForm.fileupload('send', {files: newData.files[0]})
			});
   		} else {
   			var $this = $(this);
   			data.files[0].process(function() {
				return $this.fileupload('process', data.files[0]);
			}).done(function() {
				previewForm.find('input[name="Content-Type"]').val(data.files[0].files.type);
				previewForm.find('input[name="key"]').val(data.files[0].public_id);
				previewForm.fileupload('send', {files: data.files[0].files});
			});
   		}
   	},
   	done: function(e, data) {
   		//make sure both uploads are finished before reloading page
   		previewUploadDone = true;
   		if(mainUploadDone) {
   			location.reload(false);
   		}
   	}
});
//----------------------------------------|
//               Events                   |
//----------------------------------------|
//handles any validation failures
uploadForm.on('fileuploadprocessfail', function(e, data) {
	uploadError(data.files[data.index].error);
});
//opens file browser on drop zone click
$("#avatar-upload-area").click(function() {
	uploadForm.find('input[name="file"]').click();
});
function uploadError(message) {
	$("#upload-errors").html("").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + message + "</div>");
	setTimeout(function() { $("#upload-errors").fadeOut() }, 5000);
}
</script>
<?php
//check recover param against password_recoveries in db
$pdo = SQLiteDB::getInstance();
if(isset($_GET['email']) && isset($_GET['recover'])) {
	$recover_check = UserRecover::checkRecover($_GET['email'], $_GET['recover'], $pdo);
} else {
	$recover_check = false;
}
if($recover_check) {
?>
<!-- mountain vector background -->
<div class="vector-background"></div>
<!-- edit form -->
<div class="container-fluid">
	<div class="add-spot-header" style="margin-top:55px;text-align:center">Enter New Password</div>
	<div id="reset-form-errors" class="form-error-area" style="text-align:center"></div>
	<div class="container" style="margin-top:10px;">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form method="post" action="#" id="reset-password-form">
					<div class="edit-form-div">
						<span class="new-spot-text">Password</span>
						<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
							<span class="glyphicon glyphicon-exclamation-sign"></span>
							<span class="error-text" style="margin-left:2px"></span>
						</span>
						<input type="password" name="password" class="new-spot-input test-input-ajax" placeholder="Enter password...">
					</div>
					<div class="edit-form-div">
						<span class="new-spot-text">Confirm Password</span>
						<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
							<span class="glyphicon glyphicon-exclamation-sign"></span>
							<span class="error-text" style="margin-left:2px"></span>
						</span>
						<input type="password" name="confirm" class="new-spot-input test-input-ajax" placeholder="Enter password...">
					</div>
					<input type="submit" class="sliding-button small-button" id="send-email" style="float:right;margin-top:10px;background:white" value="submit"/>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
$("#reset-password-form").submit(function(e) {
	e.preventDefault();
	var formData = $(this).serialize();
	$.post("ajax/user/reset_password.php", formData, function(resp) {
		if(resp.success) {
			showFlash("Success! Redirecting to account page...", "success", "info-sign");
			setTimeout(function() { window.location.assign("index.php?p=profile&username=" + resp.username)}, 1000);
		} else {
			$("#reset-form-errors").html('');
			$("#reset-form-errors").show().append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
		}
	}, 'json');
});
</script>
<?php } else { ?>
<!-- mountain vector background -->
<div class="vector-background"></div>
<!-- edit form -->
<div class="container-fluid">
	<div class="add-spot-header" style="margin-top:100px;text-align:center">Authorization Failed, resend recovery email to reset</div>
</div>
<?php } //end of main if ?>
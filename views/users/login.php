<div class="login-container">
	<div class="modal-header-user"> 
		<div class="modal-header-text">Log In</div> 
		<div class="modal-header-subtext">Continue your climbing adventure</div> 
	</div> 
	<div id="form-error-div" class="form-error-area"> 
	</div> 
	<form method="post" action="#" id="login-form" class="login-signup-form"> 
		<div class="input-div" style="margin-top:10px"> 
			<div class="login-form-text basic-text">Email</div> 
			<input type="text" name="email" placeholder="Enter email..." class="login-input bolder-text transition-all"> 
		</div> 
		<div class="input-div"> 
			<div class="login-form-text basic-text">Password</div> 
			<input type="password" name="password" placeholder="Enter password..." class="login-input bolder-text transition-all"> 
		</div> 
		<input type="hidden" name="form" value="login"> 
		<div class="input-div" style="margin-top:20px;margin-bottom:50px"> 
			<input type="submit" class="submit-button" id="login-submit" value="Log In"> 
		</div> 
	</form> 
	<div class="login-background-image">
		<div class="login-footer"> 
			<div class="row no-gutters"> 
				<div class="col-xs-6">
					<div class="check-box-div radio-checkbox-div login-checkbox"> 
						<div class="check-box transition-all login-check-box"></div><span class="basic-text">Remember me</span> 
					</div>
				</div>
				<div class="col-xs-6">
					<a href="forgot" class="login-modal-link basic-text" id="forgot-password" style="position:relative;font-size:1.2em;top:1px">Forgot Password?</a> 
				</div>
			</div> 
			<div class="input-div" style="margin-top:30px"> 
				<div class="input-form-text">Not a member yet? <a href="signup" class="login-modal-link" id="signup-link">Sign up here</a></div> 
			</div> 
		</div>
	</div> 
</div>
<script type="text/javascript">
//----------------------------------------|
//           Custom checkbox      	      |
//----------------------------------------|
var loginRemember = false;
$(document).on("mouseenter", ".login-checkbox", function() {
	$(this).css("color", "white");
	$(this).find(".check-box").css("border", "2px solid white");
});
$(document).on("mouseleave", ".login-checkbox", function() {
	$(this).css("color", "rgba(255, 255, 255, 0.9)");
	$(this).find(".check-box").css("border", "2px solid rgba(255, 255, 255, 0.9)");
});
$(document).on("click", ".login-checkbox", function() {
	console.log(loginRemember);
	if(loginRemember) {
		$(this).find(".check-box").html("");
		loginRemember = false;
	} else {
		$(this).find(".check-box").html("<span class='glyphicon glyphicon-ok' id='login-checkmark'></span>");
		loginRemember = true;
	}
});
//submit login/signup
$(document).on("submit", "#login-form", function(e) {
	e.preventDefault();
	var fields = $(this).serialize();
	$.ajax({
		url: "ajax/user/login_signup.php",
		type: "post",
		data: fields,
		dataType: "json",
		success: function(resp) {
			//if resp.length is undefined that means an array of errors was returned
			if(resp.length == undefined) {
				if(!resp.success) {
					$("#form-error-div").html("").show();
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
				} else {
					//close modal, change nav to logged in state
					sessionStorage.setItem('user_id', resp.user_id);
					sessionStorage.setItem('username', resp.username);
					sessionStorage.setItem('email', resp.email);
					sessionStorage.setItem('main_map_filter', "Ropes");
					if(resp.avatar) {
						sessionStorage.setItem('avatar', resp.avatar);
					}
					//go to page depending on current location
					if(document.title == "Home | climb&seek" || document.title == "Log In | climb&seek" || document.title == "Sign Up | climb&seek") {
						window.location.assign("index.php?p=profile&username=" + resp.username);
					} else {
						window.location.assign(window.location.href);
					}
				}
			} else {
				$("#form-error-div").html("").show();
				for(var i=0;i<resp.length;i++) {
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
				}
			}
		}
	});
});
//highlights inputs on focus
$(document).on("focus", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(document).on("blur", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
</script>
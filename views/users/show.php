<?php
$pdo = SQLiteDB::getInstance();
if($pdo && isset($_GET['username'])) {
	$stmt = $pdo->prepare("SELECT id, username, avatar, created_at, email, cover_pic, location, cover_pic FROM users WHERE username=?");
	$stmt->execute([$_GET['username']]);
	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	if($user) {
		//get recent activities
		$activities = Activity::getActivities($user['id'], 0, $pdo);
		//get comments upvoted (upvotes_count), number of routes (routes_count), and number of spots(spots_count)
		$user_upvotes = new Upvote($_GET, $pdo);
		$stats = $user_upvotes->getUserStats($user['id']);
		//get visiting users upvotes (to mark them)
		if(isset($_SESSION['user'])) {
			//get user upvotes affiliated with this user
			$new_upvotes = $user_upvotes->getUserUpvotesProfile($user['id']);
		} else {
			$new_upvotes = false;
		}
		unset($user_upvotes);
		//get comments with user_id
		$new_comments = new Comment($user, $pdo);
		$comments = $new_comments->fetchUserComments($user['id'], 0);
		unset($new_comments);
		//get routes with user_id
		$new_spots = new Spot($user, $pdo);
		$spots = $new_spots->getUserSpots($user['id'], 0);
		unset($new_spots);
		//use default avatar if user has none
		if($user['avatar']) {
			$avatar = AWS_URL . "/" . $user['avatar'];
		} else {
			$avatar = "assets/images/default-avatar-big.png";
		}
	} else $user = null;
} else $user = null;
if($user) {
?>
<!-- start of page -->
<div id="pic-carousel">
	<span class="glyphicon glyphicon-remove close-fixed-modal" id="close-pic-carousel"></span>
	<div id="pic-left" class="carousel-pic-selector pic-selector-div" style="left:0">
		<img src='assets/images/pic-left.png' class="spot-image-selector"/>
	</div>
	<div id="pic-right" class="carousel-pic-selector pic-selector-div" style="right:0">
		<img src='assets/images/pic-right.png' class="spot-image-selector"/>
	</div>
	<!-- pic info div (user card) -->
	<div id="picture-info-div">
		<span class="report-id" style="display:none"></span>
		<span class="report-type" style="display:none">picture</span>
		<div id="picture-info-card">
			<div class="picture-vote-div" title="Upvote picture!">
				<span class="glyphicon glyphicon-menu-up upvote-icon"></span>
				<div id="pic-upvotes" class="upvote-amount basic-text">0</div>
			</div>
			<img src="assets/images/default-avatar.png" id='user-pic-avatar' class="img-circle"/>
			<div id="pic-user-info">
				<a href="#" id="pic-user-name"></a>
				<div id="pic-upload-date"></div>
			</div>
			<div id="pic-feature-name" style="display:inline-block">
				<div class="pic-feature-col" id="pic-feature-spot">
					<div class="pic-feature-type bolder-text">Spot:</div>
					<div class="basic-text pic-feature-text"></div>
				</div>
				<div class="pic-feature-col" id="pic-feature-route">
					<div class="pic-feature-type bolder-text">Route:</div>
					<div class="basic-text pic-feature-text"></div>
				</div>
			</div>
			<div id="pic-info-choices">
				<div class="glyphicon glyphicon-trash pic-choice-icon" id="delete-spot-picture" style="color:#ff4d4d" data-toggle="tooltip" data-placement="right" title="Delete Picture"></div>
				<div class="glyphicon glyphicon-edit pic-choice-icon add-caption" id="edit-caption" style="color:#6ba8e1" data-toggle="tooltip" data-placement="right" title="Edit Caption"></div>
			</div>
		</div>
	</div>
	<div id="pic-holder">
		<img src="#" id="current-carousel-pic" class="carousel-pic"/>
		<img src="#" id="next-carousel-pic" class="carousel-pic"/>
		<img src="#" id="last-carousel-pic" class="carousel-pic"/>
	</div>
	<!-- pic caption -->
	<div id="show-banner-caption">
		<div id="caption-text" class="basic-text"></div>
	</div>
	<span id="report-picture" class="activate-report">
		<span class="basic-text">report</span>
	</span>
	<div class="basic-text" id="carousel-pic-count"></div>
	<div class="basic-text" id="set-cover-pic">SET COVER PIC</div>
</div>
<div class="container-fluid">
	<div class="row" id="show-page-header"><!--picture row -->
		<div id="pic-left" class="pic-selector pic-selector-div" style="left:0">
			<img src='assets/images/pic-left.png' class="spot-image-selector"/>
		</div>
		<div id="pic-right" class="pic-selector pic-selector-div" style="right:0">
			<img src='assets/images/pic-right.png' class="spot-image-selector"/>
		</div>
		<div id="show-user-main-image">
			<?php 
			if($user['cover_pic']) {
				echo '<img src="' . AWS_URL . "/" . $user['cover_pic'] . '" id="show-spot-image"/>';
			} else {
				echo '<img src="assets/images/user-default-cover.jpg" id="show-spot-image"/>';
			}
			?>
		</div>
		<div class="show-spot-banner-sub" id="show-spot-banner-name" style="font-size:3.5em">
			<img src=<?php echo $avatar; ?> style="height:90px;width:90px" class="img-circle" id="main-user-avatar"/>
			<span style="position:relative;top:5px"><?php echo sanitize($user['username']) ?></span>
		</div>
		<div class="show-banner-username show-banner-element basic-text" id="set-cover-pic">SET AS COVER PIC</div>
		<div class="show-banner-username" id="spot-picture-username" style="display:none"></div>
	</div>
	<div class="row" id="placeholder-bar" style="display:none;height:60px"></div>
	<div class="row" id="show-spot-info-bar"> <!-- info bar row -->
		<div class="info-bar-column-left" id="info-bar-slider">
			<div class="info-bar-div" id="info-bar-name" style="display:none">
				<?php echo sanitize($user['username']) ?>
			</div><div class="info-bar-div" id="info-bar-claims">
				<?php echo $stats['upvotes_count']; ?> LIKES
			</div><div class="info-bar-div" id="info-bar-spots" style="background:rgba(0, 0, 0, 0.5)">
				<?php echo $stats['spots_count']; ?> SPOTS
			</div><div class="info-bar-div" id="info-bar-routes" style="background:rgba(0, 0, 0, 0.4)">
				<?php echo $stats['routes_count']; ?> ROUTES
			</div>
		</div>
		<div class="info-bar-column-right">
		<?php if($user['location']) { ?>
			<div class="basic-text infobar-user-location"><span class="glyphicon glyphicon-map-marker" style="margin-right:5px"></span><? echo $user['location']; ?></div>
		<?php } //end of location if ?>
		</div>
	</div>
</div>
<!-- Recent activity -->
<div class="container-fluid" style="background:#f5f5f5;padding:15px 0;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="body-header">
					<div class="body-header-text">RECENT ACTIVITY:</div>
				</div>
				<!-- acitvites go here -->
				<div id="user-feed">
					<?php
					if($activities) { 
						foreach($activities as $row) {
							Activity::activityHTML($row, $user);
						}
					} else {
					?>
						<div id="no-activities" class="spot-page-placeholder-div">
							<div class="basic-text no-comments-text"><?php echo $user['username'] ?> hasn't done anything yet!</div>
							<div class="glyphicon glyphicon-tasks" style="font-size:5em"></div>
							<div class="basic-text no-comments-text">Come on <?php echo $user['username'] ?>, get out there and help the community!</div>
						</div>
					<?php } // end of activity if ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($activities) { ?>
<!-- load more activities div -->
<div class="load-more-div" id="load-more-activities" style="background:#f5f5f5;margin:0;">
	<div class="load-more-background"></div>
	<div class="basic-text">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center;background:#f5f5f5;">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-activities-gif" alt="Loading..."/>
</div>
<?php } //end of load more activities ?>
<!-- Spots -->
<?php if($spots) { ?>
<div class="container-fluid" style="margin-top:15px" id="spots-container">
	<div class="row">
		<div class="col-lg-12">
			<div class="body-header">
				<div class="body-header-text"><?php echo strtoupper($user['username']) . "'S "; ?>SPOTS:</div>
			</div>
		</div>
	</div>
	<!-- spots go here -->
	<div id="user-spots" style="text-align:center">
		<?php
		foreach($spots as $row) {
			if(!$row['top_pic']) {
				$top_pic = "assets/images/extra-image5.jpg";
			} else {
				$top_pic = AWS_URL . "/" . $row['top_pic'] . "s";
			}
			if(strlen($row['name']) > 25) {
				$name = substr($row['name'], 0, 25) . "...";
			} else $name = $row['name'];
			if(!$row['upvotes']) {
				$upvotes = 0;
			} else $upvotes = $row['upvotes'];
			$type_diff = "";
			if($row['type_climbing']) {
				$type_arr = explode(", ", $row['type_climbing']);
				$type_color = typeColor($type_arr[0]);
				$types = substr($row['type_climbing'], 0, -2);
				if($type_arr[0] == "Bouldering") {
					$type_diff = "bouldering_avg_diff";
				} else {
					$type_diff = "ropes_avg_diff";
				}
			} else {
				$types = "<em>No routes</em>";
				$type_color = "";
			}
			if($type_diff != "") {
				$diff = $row[$type_diff];
			} else $diff = "n/a";
			if($row['num_routes']) {
				$routes = $row['num_routes'];
			} else $routes = 0;
			echo '<div class="spot-card" style="text-align:left">' .
					'<div class="spot-div">' .
						'<div class="spot-div-pic-holder">' .
							'<img src="' . $top_pic . '" class="route-pic" />' .
						'</div>' .
						'<div class="spot-info-half">' .
							'<a href="spot/' . $row['id'] . '/' . $row['name'] . '" class="spot-div-name bolder-text default-link">' . $name . '</a>' .
							'<div class="spot-div-user basic-text">' .
							'Discovered by <a href="profile/' . $row['user_name'] . '" class="default-link">' . $row['user_name'] . '</a>' .
								'<span> in ' . $row['region'] . '</span>' .
							'</div>' .
							'<div class="row no-gutters spot-div-columns-row">' .
								'<div class="col-xs-4">' .
									'<div class="spot-div-column transition-all">' .
										'<div class="spot-column-hover-info basic-text"># of Routes</div>' .
										'<span class="glyphicon glyphicon-road" style="font-size:.9em"></span>' .
										'<span class="basic-text"> ' . $routes . '</span>' .
									'</div>' .
								'</div>' .
								'<div class="col-xs-4">' .
									'<div class="spot-div-column transition-all">' .
										'<div class="spot-column-hover-info basic-text">Avg Difficulty</div>' .
										'<span class="glyphicon glyphicon-stats"></span>' .
										'<span class="basic-text"> ' . $diff . '</span>' .
									'</div>' .
								'</div>' .
								'<div class="col-xs-4">' .
									'<div class="spot-div-column transition-all" style="border-right:none">' .
										'<div class="spot-column-hover-info basic-text">Likes</div>' .
										'<span class="glyphicon glyphicon-thumbs-up"></span>' .
										'<span class="basic-text"> ' . $upvotes . '</span>' .
									'</div>' .
								'</div>' .
							'</div>' .
							'<div class="spot-div-types basic-text" style="background:' . $type_color . '">' . $types . '</div>' .
							'<div class="spot-description basic-text">' . $row['description'] . '</div>' .
						'</div>' .
					'</div>' .
				'</div>';
		}
		?>
	</div>
</div>
<!-- load more spots div -->
<div class="load-more-div" id="load-more-spots">
	<div class="load-more-background"></div>
	<div class="basic-text">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center;">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-spots-gif" alt="Loading..."/>
</div>
<?php } //end of spots if ?>
<?php if($stats['routes_count'] > 0) { ?>
<!-- routes -->
<div class="container-fluid" style="margin-top:15px" id="routes-container">
	<div class="row">
		<div class="col-lg-12" id="show-spot-routes">
			<div class="body-header">
				<div class="body-header-text"><?php echo strtoupper($user['username']) . "'S "; ?>ROUTES:</div>
			</div>
		</div>
	</div>
	<!-- routes go here -->
	<div id="route-area"></div>
</div>
<!-- load more routes div -->
<div class="load-more-div" id="load-more-routes">
	<div class="load-more-background"></div>
	<div class="basic-text" style="margin-top:10px">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-routes-gif" alt="Loading..."/>
</div>
<?php } //end of routes if ?>
<!-- start of comments -->
<div class="container-fluid" style="background:#f5f5f5;padding:15px 0;" id="comments-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="body-header">
					<div class="body-header-text"><?php echo strtoupper($user['username']) . "'S "; ?>COMMENTS:</div>
				</div>
				<!-- new comment form -->
				<div id="new-comment-div" style="display:none;margin-top:10px;padding-bottom:10px">
					<div class="form-error-area" id="comment-form-errors" style="text-align:center"></div>
					<form method="post" action="#" id="new-comment-form">
						<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
						<span class="comment-name"></span>
						<span class="alert alert-danger form-error" style="float:right;position:relative;top:24px" data-toggle="tooltip">
							<span class="glyphicon glyphicon-exclamation-sign"></span>
							<span class="error-text"></span>
						</span>
						<div style="padding-left:55px">
							<textarea rows="3" type="text" name="body" id="new-comment-body" class="new-spot-input" placeholder="Enter comment..." style="padding:10px 10px 0 10px"></textarea>
						</div>
						<div style="height:50px">
							<input type="submit" class="sliding-button smallish-button" style="float:right;margin-top:5px;padding:5px 5px;width:100px;font-size:1.2em" value="POST" id="comment-submit"/>
						</div>
					</form>
				</div>
				<!-- comments -->
				<div id="spot-comments">
					<?php
					if($comments) {
						foreach($comments as $row) { ?>
						<?php //convert/check data
						!$row['upvotes'] ? $upvotes = "0" : $upvotes = $row['upvotes'];
						//change date to readable format
						$date = date_create($row['created_at']);
						$use_date = date_format($date, "F d Y");
						//check if user has upvoted any comments
						if($upvotes) {
							$check_upvote = Upvote::checkIfUpvoted($new_upvotes, $row['id'], "comment");
							if($check_upvote) {
								$upvoted = true;
							} else $upvoted = false;
						}
						if($row['gym_id']) {
							$url = "indoor/" . $row['gym_id'] . '/' . $row['spot_name'];
						} else {
							$url = "spot/" . $row['spot_id'] . '/' . $row['spot_name'];
						}
						?>
						<div class="spot-comment-div">
							<div class="comment-vote-div">
								<?php if($upvoted) {
									echo '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
								} else echo '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
								?>
								<div class="upvote-amount basic-text" style="font-size:1.3em;bottom:10px"><?php echo $upvotes; ?></div>
								<div class="comment-id" style="display:none"><?php echo $row['id']; ?></div>
							</div>
							<div class="comment-header">
								<?php if($row['user_avatar']) { ?>
									<span class="comment-pic"><img src=<?php echo AWS_URL . "/" . $row['user_avatar'] . "s" ?> class="comment-image"/></span>
								<?php } else { ?>
									<span class="comment-pic"><img src="assets/images/default-avatar.png" class="comment-image"/></span>
								<?php } //end avatar if ?>
								<span class="comment-name"><?php echo $row['user_name']; ?></span>
								<a href="<?php echo $url?>" class="comment-name" style="color:#347db2;font-size:1.2em">at <?php echo $row['spot_name']; ?></a>
								<span class="comment-time"><?php echo $use_date; ?></span>
								<div class="comment-body"><?php echo $row['body']; ?></div>
								<div class="comment-options">
									<?php 
									if(isset($_SESSION['user']) && $_SESSION['user']['id'] == $row['user_id']) {
										echo '<span class="delete-comment comment-option">delete</span>';
									} else {
										echo '<span class="report-comment comment-option activate-report">report</span>';
									}
									?>
									<div class="report-id" style="display:none"><?php echo $row['id']; ?></div>
									<div class="report-type" style="display:none">comment</div>
								</div>
							</div>
						</div>
					<?php } //end of comment loop 
					} else { ?>
						<div id="no-comments" class="spot-page-placeholder-div">
							<div class="basic-text no-comments-text">No Comments....yet</div>
							<div class="glyphicon glyphicon-comment" style="font-size:5em"></div>
							<div class="basic-text no-comments-text">Go to a spot and start commenting!</div>
						</div>
					<?php } // end of comments if ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- load more routes div -->
<div class="load-more-div" id="load-more-comments" style="background:#f5f5f5;margin-top:0;border:none">
	<div class="load-more-background"></div>
	<div class="basic-text">Load More</div>
	<div>
		<span class="glyphicon glyphicon-menu-down little-arrow load-more-arrow" style="top:3px"></span>
		<span class="glyphicon glyphicon-menu-down big-arrow load-more-arrow" style="font-size:1.5em"></span>
	</div>
</div>
<div style="text-align:center;background:#f5f5f5;margin-top:0">
	<img src="assets/loaders/ripple.gif" class="infinite-load-gif" id="load-comments-gif" alt="Loading..."/>
</div>
<?php include "views/partials/_upload_modal.php"; ?>
<?php include "views/partials/_report_modal.php"; ?>
<script type="text/javascript" id="main-script">
var thisUser = <?php echo json_encode($user); ?>;
var userUpvotes = <?php echo json_encode($new_upvotes); ?>;
//initialize tooltips
$("[data-toggle='tooltip']").tooltip();
$(document).on("mouseenter", ".spot-div-column", function() {
	$(this).find(".spot-column-hover-info").stop().fadeIn();
});
$(document).on("mouseleave", ".spot-div-column", function() {
	$(this).find(".spot-column-hover-info").stop().fadeOut();
});
/* ********************************
   --------------------------------
   |     Page load ajax calls     |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Get pic urls from db                   |
//----------------------------------------|
var spotPics = Array();
$.post("ajax/picture/get_spot_pics.php", {"content_type": "picture", "user_id": thisUser.id}, function(data) {
	if(data.length > 0) {
		$(".pic-selector").fadeIn();
		spotPics = data;
		//add 'upvoted' col to pics if currently logged in user has upvoted it
		if(userUpvotes.length > 0) {
			for(var i=0;i<spotPics.length;i++) {
				var upvoted = checkIfUpvoted(spotPics[i].public_id, "picture");
				if(upvoted) {
					spotPics[i].upvoted = true;
				}
			}
		}
	} else {
		$(".pic-selector").hide();
	}
}, 'json');
//----------------------------------------|
// Get routes from db                     |
//----------------------------------------|
$.post("ajax/route/get_routes_from_spot.php", {"offset": 0, "user_id": thisUser.id}, function(data) {
	if(data) {
		//hide/show load more routes button
		if(data.length < 8 || parseInt($("#info-bar-routes").text()) == 8) {
			$("#load-more-routes").hide();
		} else {
			$("#load-more-routes").show()
		}
		for(var i=0;i<data.length;i++) {
			newRouteHTML(data[i].id, data[i].name, data[i].top_pic, data[i].spot_name, data[i].description, data[i].difficulty, data[i].type_climbing, data[i].upvotes, false, data[i].spot_id, data[i].user_name);
		}
	} else {
		$("#routes-container").hide();
		$("#load-more-routes").hide();
	}
}, 'json');
function newRouteHTML(id, name, topPic, spotName, desc, diff, type, upvotes, newRoute, spotId, userName) {
	//check vars and replace if null/too long
	if(!desc) {
		desc = "";
	}
	if(!upvotes) {
		upvotes = 0;
	}
	//shorten name if too long
	if(name.length > 24) {
		var displayName = name.substring(0, 22) + "...";
	} else var displayName = name;
	//shorten desc if too long, put full version hidden below
	if(desc.length > 120) {
		var displayDesc = desc.substring(0, 120) + "...<span class='default-link more-desc'>more</span>";
		desc = desc + " ...<span class='default-link less-desc'>less</span>";
	} else var displayDesc = desc;
	//check if route has been upvoted alter css if so
	var checkUpvote = checkIfUpvoted(id, "route");
	if(checkUpvote || newRoute) {
		var recommend = '<div class="route-button basic-text upvote-route" style="background:rgba(0, 0, 0, 0.8)">Recommended</div>'
	} else var recommend = '<div class="route-button basic-text upvote-route">Recommend</div>';
	//set picture
	if(topPic) {
		var picture = generateS3URL(topPic) + "s";
		var carousel = '<div class="route-carousel-div transition-all">' +
							'<span class="see-full-image basic-text" style="color:rgba(255, 255, 255, 0.6);cursor:pointer">SEE FULL IMAGE</span>' +
							'<span class="route-carousel-selector glyphicon glyphicon-chevron-left transition-all" id= "route-carousel-left" style="left:3px"></span>' +
							'<span class="route-carousel-selector glyphicon glyphicon-chevron-right transition-all" id= "route-carousel-right" style="right:3px"></span>' +
						'</div>';
	} else {
		var picture = "assets/images/route-default.jpg";
		var carousel = "";
	}
	//get unique color depending on type
	var iconColor = typeColor(type);
	if(sessionStorage.getItem('username') == userName) {
		var editReport = '<div class="route-button basic-text edit-route-desc edit-report-button">Edit</div>';
	} else {
		var editReport = '<div class="route-button basic-text activate-report edit-report-button">Report</div>' +
						'<div class="report-id" style="display:none">' + id + '</div>' +
						'<div class="report-type" style="display:none">route</div>' +
						'<div class="report-name" style="display:none">' + name + '</div>';
	}
	var route = '<div class="route-spot-div">' +
					'<div class="route-id" style="display:none">' + id + '</div>' +
					'<div class="route-name bolder-text">' + displayName + '</div>' +
					'<a href="spot/' + spotId + '/' + spotName + '" class="route-user basic-text default-link" style="display:block">at ' + spotName + '</a>' +
					'<div class="route-pic-holder">' +
						carousel +
						'<img src="' + picture + '" class="route-pic" id="' + topPic + '"/>' +
						'<div class="row no-gutters route-interactions">' +
							'<div class="col-xs-4">' +
								recommend +
							'</div>' +
							'<div class="col-xs-4">' +
								'<div class="route-button basic-text activate-upload-modal">' + 
									'<span class="upload-id-info" style="display:none">' + id + '</span>' +
									'<span class="upload-type-info" style="display:none">route</span>' +
									'<span class="upload-name-info" style="display:none">' + name + '</span>' +
									'Add Photo' +
								'</div>' +
							'</div>' +
							'<div class="col-xs-4">' +
								editReport +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="row no-gutters route-info-squares">' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-stats info-square-icon"></span>' +
							'<div class="route-square-text basic-text">' + diff + '</div>' +
						'</div>' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-link info-square-icon" style="opacity:0.3;color:' + iconColor + '"></span>' +
							'<div class="route-square-text basic-text">' + type + '</div>' +
						'</div>' +
						'<div class="col-xs-4 route-info-square">' +
							'<span class="glyphicon glyphicon-thumbs-up info-square-icon"></span>' +
							'<div class="route-square-text basic-text route-upvotes">' + upvotes + '</div>' +
						'</div>' +
					'</div>' +
					'<div class="route-description basic-text">' + displayDesc + '</div>' +
					'<div class="route-description basic-text long-desc" style="display:none">' + desc + '</div>' +
				 '</div>';
	if(newRoute) {
		$("#route-area").prepend(route);
	} else {
		$("#route-area").append(route);
	}
}
//check if user has upvoted content
function checkIfUpvoted(contentId, contentType) {
	if(userUpvotes.length > 0) {
		for(var i=0;i<userUpvotes.length;i++) {
			if(userUpvotes[i].content_type == contentType) {
				if(userUpvotes[i].content_id == contentId) {
					userUpvotes.splice(i, 1);
					return true;
				}
			}
		}
		return false;
	} else return false;
}
/* ********************************
   --------------------------------
   |       Button Animations  	  |
   ________________________________
   ********************************
*/
$("#make-claim").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-23px");
	}
);
$("#leave-comment").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-18px");
	}
);
$("#add-route").hover(
	function() {
		buttonAnimationOn($(this));
	},
	function() {
		buttonAnimationOff($(this), "-23px");
	}
);
//slides text in button to left, fades in hidden icon
function buttonAnimationOn(element) {
	element.find(".claim-text").stop().animate({
		marginRight: "0px"
	}, 500);
	element.find(".glyphicon").stop().animate({
		opacity: "1"
	}, 600);
}
function buttonAnimationOff(element, margin) {
	element.find(".claim-text").stop().animate({
		marginRight: margin
	}, 500);
	element.find(".glyphicon").stop().animate({
		opacity: "0"
	}, 300);
}
/* ********************************
   --------------------------------
   |   Route/Comment Animations   |
   ________________________________
   ********************************
*/
//pic selector icon animations
$(".pic-selector-div").hover(
	function() {
		$(this).find(".spot-image-selector").stop().animate({
			opacity: "0.8"
		}, 300);
		$(this).stop().animate({
			width: "90px"
		}, 300);
	},
	function() {
		$(this).find(".spot-image-selector").stop().animate({
			opacity: "0.5"
		}, 300);
		$(this).stop().animate({
			width: "100px"
		}, 300);
	}
);
//shows rest of description
$(document).on("click", ".more-desc", function() {
	$(this).parent().hide();
	$(this).parent().next().show();
});
$(document).on("click", ".less-desc", function() {
	$(this).parent().hide();
	$(this).parent().prev().show();
});
function typeColor(type) {
	switch(type) {
		case "Bouldering":
			return "#4ad153";
			break;
		case "Top Rope":
			return "#ff5555";
			break;
		case "Sport":
			return "#398bc6";
			break;
		case "Trad":
			return "#398bc6";
			break;
		case "Alpine":
			return "#cf7dfd";
			break;
		case "Ice":
			return "#cf7dfd";
			break;
		case "Sport/Trad":
			return "#398bc6";
			break;
		default:
			return "rgba(0, 0, 0, 0.2)";
			break;
	}
}
/* ********************************
   --------------------------------
   |       Main Pic Carousel  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
//          Carousel Functions            |
//----------------------------------------|
//get next point in array of spotPics
function nextPic(picArr, currentImage, direction) {
	//loop through spotPics and find position of currentImage
	var place = findPublicIdIndex(picArr, currentImage);
	if(direction == "pic-right" || direction == "route-carousel-right") {
		//if place + 1 will be non-existant point, return to beginning
		if((place + 1) > (picArr.length - 1)) {
			return 0;
		} else return place + 1;
	} else {
		//if place - 1 is negative, go to end of array
		if((place - 1) < 0) {
			return picArr.length - 1;
		} else return place - 1;
	}
}
//change pic info on new pic
function updatePicInfo(point) {
	//show pic options if its the users pic
	if(spotPics[point].user_name == sessionStorage.getItem("username")) {
		$("#pic-info-choices").show();
		$("#set-cover-pic").show();
	} else {
		$("#pic-info-choices").hide();
		$("#set-cover-pic").hide();
	}
	//replace caption if any
	if(spotPics[point].caption) {
		$("#show-banner-caption").show();
		$("#caption-text").text(spotPics[point].caption);
	} else if(spotPics[point].user_name == sessionStorage.getItem('username')) {
		$("#show-banner-caption").show();
		$("#caption-text").html("<div class='regular-button add-caption'>ADD CAPTION</div>");
	} else $("#show-banner-caption").hide();
	//replace name
	$("#pic-user-name").text(spotPics[point].user_name);
	//set height of show banner caption so text is right under pic
	var offset = $("#current-carousel-pic").offset();
	if(offset.top == 0 || offset.top >= 500) {
		offset = {"top": 50}
	}
	$("#show-banner-caption").css("height", offset.top + "px");
	//replace avatar
	if(spotPics[point].user_avatar != null) {
		$("#user-pic-avatar").attr('src', generateS3URL(spotPics[point].user_avatar));
	} else {
		$("#user-pic-avatar").attr('src', "assets/images/default-avatar.png");
	}
	//upload date
	$("#pic-upload-date").text("Uploaded " + timeSince(new Date(spotPics[point].js_date)) + " ago");
	if(spotPics[point].upvotes) {
		$("#pic-upvotes").text(spotPics[point].upvotes);
	} else {
		$("#pic-upvotes").text("0");
	}
	//if upvoted
	if(spotPics[point].upvoted) {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#347db2");
	} else {
		$(".picture-vote-div").find(".upvote-icon").css("color", "#e9e9e9");
	}
	//if pic belongs to a route
	if(spotPics[point].route_id) {
		$("#pic-feature-spot").hide();
		$("#pic-feature-route").css("display", "inline-block");
		$("#pic-feature-route").find(".pic-feature-text").html(spotPics[point].feature_name);
	} else {
		$("#pic-feature-route").hide();
		$("#pic-feature-spot").find('.pic-feature-text').text(spotPics[point].feature_name);
		$("#pic-feature-spot").css("display", "inline-block");
	}
	$("#pic-profile").attr("href", "profile/" + spotPics[point].user_name);
	//add public id to report
	$("#picture-info-div").find(".report-id").text(spotPics[point].public_id);
	$("#pic-user-name").attr("href", "profile/" + spotPics[point].user_name);
	//update pic count
	$("#carousel-pic-count").text((point + 1) + " / " + spotPics.length);
};
//loop through spotPics and find position of public id
function findPublicIdIndex(picArr, publicId) {
	for(var i=0;i<picArr.length;i++) {
		if(picArr[i].public_id == publicId) {
			var place = i;
			break;
		}
	}
	return place;
}
function generateS3URL(publicId) {
	return S3URL + "/" + publicId;
}
//----------------------------------------|
// Pic Carousel                           |
//----------------------------------------|
$(".pic-selector").click(function() {
	if(spotPics.length > 0) {
		beginCarousel();
	}
});
$(".carousel-pic-selector").click(function() {
	if($(this).attr('id') == "pic-right") {
		slideNextPic("right");
	} else {
		slideNextPic("left");
	}
});
function beginCarousel() {
	//set first pic in array as current
	$("#current-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id)).show();
	//set next pic as second in array
	if(spotPics[1] !== undefined) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id))
	}
	//set last pic as last entry in array
	var lastPoint = spotPics.length - 1;
	if(spotPics[lastPoint] !== undefined) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[lastPoint].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	$("#pic-carousel").fadeIn();
	updatePicInfo(0);
}
//will be true when next pics are still being decided during animation
var loadingPics = false;
function slideNextPic(direction) {
	if(!loadingPics) {
		loadingPics = true;
		var winWidth = $(window).width() + 10;
		if(direction == "right") {
			var newCurrentPic = "next";
			$("#next-carousel-pic").css("left", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				right: winWidth + "px"
			}, 250);
			$("#next-carousel-pic").animate({
				left: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
		} else {
			var newCurrentPic = "last";
			$("#last-carousel-pic").css("right", winWidth + "px").show();
			$("#current-carousel-pic").animate({
				left: winWidth + "px"
			}, 250);
			$("#last-carousel-pic").animate({
				right: 0
			}, 250, function() {
				setUpNextPic(newCurrentPic);
			});
		}
	}
}
function setUpNextPic(newCurrentPic) {
	//these vars hold the current status of the ids before swap
	var currentPic = $("#current-carousel-pic");
	var nextPic = $("#next-carousel-pic");
	var lastPic = $("#last-carousel-pic");
	//swap pic ids according to new position
	if(newCurrentPic == "next") {
		currentPic.attr('id', "next-carousel-pic");
		nextPic.attr('id', "current-carousel-pic");
	} else {
		currentPic.attr('id', 'last-carousel-pic');
		lastPic.attr('id', 'current-carousel-pic');
	}
	//remove attr gained from previous slide, using newly swapped ids
	$("#current-carousel-pic").css({
		right: "",
		left: ""
	});
	$("#next-carousel-pic").removeAttr('style');
	$("#last-carousel-pic").removeAttr('style');
	//get public id from new current pic and find its index in spotPics
	var currentPublicId = $("#current-carousel-pic").attr('src').split("/").pop();
	var currentIndex = findPublicIdIndex(spotPics, currentPublicId);
	//update pic card info
	updatePicInfo(currentIndex);
	//going from currentIndex, find out the pic in front and behind it
	if((currentIndex + 1) <= spotPics.length - 1) {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex + 1].public_id));
	} else {
		$("#next-carousel-pic").attr('src', generateS3URL(spotPics[0].public_id));
	}
	if((currentIndex - 1) >= 0) {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[currentIndex - 1].public_id));
	} else {
		$("#last-carousel-pic").attr('src', generateS3URL(spotPics[spotPics.length - 1].public_id));
	}
	loadingPics = false;
}
//close pic carousel
$("#close-pic-carousel").click(function() {
	$("#pic-carousel").fadeOut();
});
//set width for pic holder for margin auto to work
$("#pic-holder").css("width", $(window).width() + "px");
$(window).resize(function() {
	$("#pic-holder").css("width", $(window).width() + "px");
});
//----------------------------------------|
// Edit/Delete Pic                        |
//----------------------------------------|
//add caption
$(document).on("click", ".add-caption", function() {
	//check if editing or adding caption
	if($("#caption-text").text() != "ADD CAPTION") {
		var oldCaption = $("#caption-text").text();
	} else var oldCaption = "";
	//new textarea + save button
	var captionInput = "<textarea type='text' rows='1' class='add-caption-textarea basic-text' name='caption' placeholder='Enter caption...'>" + oldCaption + "</textarea>" + "<span class='regular-button' id='save-caption' style='border-radius:0 5px 5px 0;vertical-align:top;padding:12.5px 5px;'>Save</span>";
	//replace old button or caption with textarea, focus
	$("#caption-text").html(captionInput);
	$(".add-caption-textarea").focus();
});
//on caption save
$(document).on("click", "#save-caption", function() {
	var capObj = {"caption": $(".add-caption-textarea").val(), "public_id": $("#current-carousel-pic").attr('src').split("/").pop()};
	$.post("ajax/picture/add_caption.php", capObj, function(resp) {
		if(resp.success) {
			$("#caption-text").html(capObj.caption);
		} else {
			showFlash(resp.error, "danger", "alert");
		}
	}, 'json');
});
//add caption textarea css
$(document).on("focus", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0.5)");
});
$(document).on("blur", ".add-caption-textarea", function() {
	$(this).css("border", "2px solid rgba(255, 255, 255, 0)");
});
//delete existing picture
$("#delete-spot-picture").click(function() {
	if(sessionStorage.getItem("username") == $("#pic-user-name").text()) {
		var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
		$.post("ajax/picture/delete_picture.php", {"public_id": publicId, "options": "both"}, function(resp) {
			if(resp) {
				var picIndex = findPublicIdIndex(publicId);
				spotPics.splice(picIndex, 1);
				$("#spot-banner-close").click();
			} else {
				showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
				window.location.replace("index.php");
			}
		});
	} else {
		showFlash("You can only delete pictures that are yours!", "danger", "glyphicon-alert");
		window.location.replace("index.php");
	}
});
//set cover picture
$("#set-cover-pic").click(function() {
	var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
	$.post('ajax/user/set_cover_pic.php', {"public_id": publicId}, function(resp) {
		var topPic = generateS3URL(publicId);
		$("#show-spot-image").attr('src', topPic);
		$("#pic-carousel").fadeOut();
	});
});
/* ********************************
   --------------------------------
   |      Route Pic Carousel  	  |
   ________________________________
   ********************************
*/
var routePics = Array();
$(document).on("click", ".route-carousel-selector", function() {
	if(spotPics.length > 0) {
		var routeDiv = $(this).parents(".route-spot-div");
		var currentRoutePic = routeDiv.find(".route-pic").attr('id');
		findRoutePics(routeDiv, $(this));
		var picIndex = nextPic(routePics, currentRoutePic, $(this).attr('id'));
		routeDiv.find('.route-pic').attr('src', generateS3URL(routePics[picIndex].public_id) + 's').attr('id', routePics[picIndex].public_id);
	}
});
function findRoutePics(routeDiv, $this) {
	var routeDiv = $this.parents(".route-spot-div");
	var routeId = routeDiv.find(".upload-id-info").text();
	if(routePics.length == 0 || routePics[0].route_id != routeId) {
		routePics = Array();
		for(var i=0;i<spotPics.length;i++) {
			if(spotPics[i].route_id == routeId) {
				routePics.push(spotPics[i]);
			}
		}
	}
}
//in route carousel, show current image in main carousel
$(document).on("click", ".see-full-image", function() {
	var routeDiv = $(this).parents(".route-spot-div");
	var smallImage = routeDiv.find('.route-pic').attr('id');
	var place = findPublicIdIndex(spotPics, smallImage);
	$("#next-carousel-pic").attr("src", generateS3URL(spotPics[place].public_id));
	setUpNextPic("next");
	$("#current-carousel-pic").show();
	$("#pic-carousel").fadeIn();
});
/* ********************************
   --------------------------------
   |       Sticky Info Bar  	  |
   ________________________________
   ********************************
*/
//get position of info bar
var infoBar = $("#show-spot-info-bar");
var barTop = infoBar.offset().top;
//make info bar fixed when user scrolls down
$(window).scroll(function() {
	var winScroll = $(window).scrollTop();
	var infoStick = barTop - $("#navbar").height();
	if(winScroll >= infoStick && infoBar.hasClass("fixed-info-bar") != true) {
		infoBar.addClass("fixed-info-bar");
		//display hidden row the rest of the page doens't jump 50px on transition
		$("#placeholder-bar").show();
		//slide out spot name
		slideName("show");
	} else if(winScroll <= infoStick && infoBar.hasClass("fixed-info-bar")) {
		infoBar.removeClass("fixed-info-bar");
		$("#placeholder-bar").hide();
		slideName("hide");
	}
});
//----------------------------------------|
// Slide out spot name                    |
//----------------------------------------|
var nameWidth = Math.floor($("#info-bar-name").width());
function slideName(visible) {
	var nameDiv = $("#info-bar-slider");
	var spotName = $("#info-bar-name");
	if(visible == "show") {
		nameDiv.css("margin-left", "-" + nameWidth + "px");
		nameDiv.animate({
			"margin-left": "0px"
		}, 300);
		spotName.show();
	} else {
		spotName.hide();
	}
}
/* ********************************
   --------------------------------
   |          Upvotes    	      |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Picture/Comment upvotes                |
//----------------------------------------|
//upvote picture
$(".picture-vote-div").click(function() {
	//grab currently displaying image, split to get public id
	var publicId = $("#current-carousel-pic").attr('src').split("/").pop();
	upvoteCommentPicture($(this), publicId, "picture");
});
//upvote comment
$(document).on("click", ".comment-vote-div", function() {
	var commentId = $(this).find(".comment-id").text();
	upvoteCommentPicture($(this), commentId, "comment");
});
//handles comments/picture upvotes
function upvoteCommentPicture($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		var upObj =  {"content_id": contentId, "content_type": contentType, "user_page": "true"}
		$.post("ajax/upvote/upvote_content.php", upObj, function(resp) {
			if(resp.success) {
				//if resp.action == upvote add one to existing num, if not minus 1
				var upvoteDisplay = $this.find(".upvote-amount");
				var currentUpvotes = parseInt(upvoteDisplay.text());
				var upIcon = $this.find(".glyphicon-menu-up");
				if(resp.action == "upvote") {
					upvoteDisplay.text(currentUpvotes + 1);
					upIcon.css("color", "#347db2");
				} else {
					upvoteDisplay.text(currentUpvotes - 1);
					upIcon.css("color", "#e9e9e9");
				}
				//update spot pics
				if(contentType == "picture") {
					var picIndex = findPublicIdIndex(contentId);
					spotPics[picIndex].upvoted = true;
					spotPics[picIndex].upvotes = $("#pic-upvotes").text();
				}
			} else {
				slideUserModal(false, "login");
			}
		}, 'json');
	} else slideUserModal(false, "login");
}
//----------------------------------------|
// Spot/Route upvotes                     |
//----------------------------------------|
//like spot
$("#like-spot").click(function() {
	var spotId = thisSpot.id;
	upvoteRouteSpot($(this), spotId, "spot");
});
$(document).on("click", ".upvote-route", function() {
	var routeId = $(this).parents(".route-interactions").find(".upload-id-info").text();
	upvoteRouteSpot($(this), routeId, "route");
});
//handles route/spot upvote
function upvoteRouteSpot($this, contentId, contentType) {
	if(sessionStorage.getItem('user_id')) {
		$.post("ajax/upvote/upvote_content.php", {"content_id": contentId, "content_type": contentType, "user_page": "true"}, function(resp) {
			if(resp.success) {
				if(contentType == "spot") {
					likeSpot(resp.action);
				} else {
					upvoteRoute($this, resp.action);
				}
			} else {
				slideUserModal(false, "login");
			}
		}, 'json');
	} else slideUserModal(false, "login");
}
function likeSpot(action) {
	if(action == "upvote") {
		var currentLikes = parseInt($("#info-bar-claims").text()) + 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "#398bc6",
			color: "white"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKED");
	} else {
		var currentLikes = parseInt($("#info-bar-claims").text()) - 1;
		$("#info-bar-claims").text(currentLikes + " LIKES");
		$("#like-spot").css({
			background: "white",
			color: "#398bc6"
		});
		$("#like-spot").html("<span class='glyphicon glyphicon-thumbs-up'></span> LIKE!");
	}
}
function upvoteRoute($this, action) {
	if(action == "upvote") {
		$this.text("Recommended")
		$this.css({
			background: "rgba(0, 0, 0, 0.8)"
		});
		upvoteDiv = $this.parents(".route-spot-div").find(".route-upvotes")
		var currentUpvotes = parseInt(upvoteDiv.text()) + 1;
		upvoteDiv.text(currentUpvotes);
	} else {
		$this.text("Recommend")
		$this.css({
			background: "rgba(0, 0, 0, 0.5)"
		});
		upvoteDiv = $this.parents(".route-spot-div").find(".route-upvotes")
		var currentUpvotes = parseInt(upvoteDiv.text()) - 1;
		upvoteDiv.text(currentUpvotes);
	}
}
/* ********************************
   --------------------------------
   |     Comment HTML/events  	  |
   ________________________________
   ********************************
*/
//new comment html
function newCommentHTML(id, name, upvotes, avatar, date, body, upvoted, newComment, spotId, spotName, gymId) {
	//check avatar
	if(avatar) {
		avatar = generateS3URL(avatar) + "s";
	} else avatar = "assets/images/default-avatar.png";
	//check if comment is upvoted by user
	if(upvoted) {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote" style="color:#347db2"></span>';
	} else  {
		var upvoteIcon = '<span class="glyphicon glyphicon-menu-up comment-upvote"></span>';
	}
	//change comment options depending on whether comment belongs to logged in user
	if(name == sessionStorage.getItem('username')) {
		var commentOptions = '<span class="delete-comment comment-option">delete</span>';
	} else {
		var commentOptions = '<span class="report-comment comment-option activate-report">report</span>'
	}
	if(gymId) {
		var url = "indoor/" + gymId + "/" + spotName;
	} else {
		var url = "spot/" + spotId + "/" + spotName;
	}
	var comment = '<div class="spot-comment-div">' +
						'<div class="comment-vote-div">' +
							upvoteIcon + 
							'<div class="upvote-amount basic-text" style="font-size:1.3em;bottom:10px">' + upvotes + '</div>' +
							'<div class="comment-id" style="display:none">' + id + '</div>' +
						'</div>' +
						'<div class="comment-header">' +
							'<span class="comment-pic"><img src="' + avatar + '" class="comment-image"/></span>' +
							'<span class="comment-name"> ' + name + '</span>' +
							'<a href=' + url + ' class="comment-name basic-text" style="color:#347db2;font-size:1.2em"> at ' + spotName + '</a>' +
							'<span class="comment-time">' + date + '</span>' +
							'<div class="comment-body">' + body + '</div>' +
							'<div class="comment-options">' +
								commentOptions +
								'<div class="report-id" style="display:none">' + id + "</div>" +
								'<div class="report-type" style="display:none">comment</div>' +
								'<div class="report-name" style="display:none">null</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	if(newComment) {
		$("#spot-comments").prepend(comment);
	} else {
		$("#spot-comments").append(comment);
	}
}
//----------------------------------------|
// Comment Events                         |
//----------------------------------------|
//fade in/out comment options
$(document).on("mouseenter", ".spot-comment-div", function() {
	$(this).find(".comment-options").stop().fadeIn("fast");
});
$(document).on("mouseleave", ".spot-comment-div", function() {
	$(this).find(".comment-options").stop().fadeOut("fast");
});
//delete comment
$(document).on("click", ".delete-comment", function() {
	var commentDiv = $(this).parents(".spot-comment-div");
	var commentId = commentDiv.find(".comment-id").text();
	$.post("ajax/comment/delete_comment.php", {"comment_id": commentId}, function(resp) {
		if(resp) {
			commentDiv.fadeOut();
		} else {
			showFlash("You cannot delete a comment that isn't yours!", "danger", "glyphicon-alert");
		}
	})
});
//----------------------------------------|
// Activity Events                         |
//----------------------------------------|
//fade in/out activity options
$(document).on("mouseenter", ".activity-feed-div", function() {
	$(this).find(".comment-options").stop().fadeIn("fast");
});
$(document).on("mouseleave", ".activity-feed-div", function() {
	$(this).find(".comment-options").stop().fadeOut("fast");
});
$(document).on("click", ".delete-activity", function(resp) {
	var actDiv = $(this).parents(".activity-feed-div");
	var actContentId = actDiv.find(".activity-content-id").text();
	var actContentType = actDiv.find(".activity-content-type").text();
	$.post("ajax/user/delete_activity.php", {"content_id": actContentId, "content_type": actContentType}, function(resp) {
		if(resp) {
			actDiv.fadeOut();
		} else {
			showFlash("You cannot delete an activity that isn't yours!", "danger", "glyphicon-alert");
		}
	})
});
/* ********************************
   --------------------------------
   |       Infinite Loading  	  |
   ________________________________
   ********************************
*/
//----------------------------------------|
// Spots                                  |
//----------------------------------------|
var loadSpotLength = 5;
//hide load more button if less than initial db fetch amount
if($(".spot-card").length < loadSpotLength) {
	$("#load-more-spots").hide();
}
$("#load-more-spots").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".spot-card").length;
	//hide load-more button, show loading gif
	$("#load-more-spots").hide();
	$("#load-spots-gif").show();
	//need to actually make spot div
	$.post("ajax/spot/get_user_spots.php", {"offset":offset, "user_id": thisUser.id}, function(data) {
		$("#load-spots-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				newSpotHTML(data[i].id, data[i].name, data[i].top_pic, data[i].user_name, data[i].description, data[i].difficulty, data[i].type_climbing, data[i].upvotes, data[i].num_routes);
			}
			//check if all routes have been loaded
			if(data.length == loadSpotLength) {
				$("#load-more-spots").show();
			}
		}
	}, 'json')
});
function newSpotHTML(id, spotName, topPic, userName, description, avgDiff, types, upvotes, numRoutes) {
	//get & set element vars depending on length/type etc
	if(types) {
		var typeArr = types.split(", ");
		var color = typeColor(typeArr[0]);
		if(typeArr[1] == "") {
			var type = typeArr[0];
		} else {
			var type = types.substring(0, -1);
		}
	} else var type = "<em>No routes</em>";
	//set background color for types
	//check if name is too long
	var name = spotName;
	if(name.length > 28) {
		name = name.substring(0, 26) + "...";
	}
	//check against avg diff being null
	if(avgDiff == null) {
		var diff = "n/a";
	} else {
		var diff = element.get('avg_diff');
	}
	if(!numRoutes) {
		var routes = 0;
	} else var routes = numRoutes;
	//check top pic
	if(topPic) {
		var topPic = S3URL + "/" + topPic + "s";
	} else {
		var topPic = "assets/images/extra-image5.jpg";
	}
	if(!upvotes) {
		upvotes = 0;
	}
	var spotDiv = '<div class="spot-card transition-all">' +
					'<div class="spot-div">' +
						'<div class="spot-div-pic-holder">' +
							'<img src="' + topPic +  '" class="route-pic" />' +
						'</div>' +
						'<div class="spot-info-half" style="text-align:left">' +
							'<a href="spot/' + id + '/' + element.get('name') + '" class="spot-div-name bolder-text default-link" title="' + name + '">' + name + '</a>' +
							'<div class="spot-div-user basic-text">' +
							'Discovered by <a href="profile/' + userName + '" class="default-link">' + userName + '</a>' +
								'<span> in still need to add location</span>' +
							'</div>' +
							'<div class="row no-gutters spot-div-columns-row">' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column transition-all">' +
										'<div class="spot-column-hover-info basic-text"># of Routes</div>' +
										'<span class="glyphicon glyphicon-road" style="font-size:.9em"></span>' +
										'<span class="basic-text"> ' + routes + '</span>' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column transition-all">' +
										'<div class="spot-column-hover-info basic-text">Avg Difficulty</div>' +
										'<span class="glyphicon glyphicon-stats"></span>' +
										'<span class="basic-text"> ' + diff + '</span>' +
									'</div>' +
								'</div>' +
								'<div class="col-xs-4">' +
									'<div class="spot-div-column transition-all" style="border-right:none">' +
										'<div class="spot-column-hover-info basic-text">Likes</div>' +
										'<span class="glyphicon glyphicon-thumbs-up"></span>' +
										'<span class="basic-text"> ' + upvotes + '</span>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="spot-div-types basic-text" style="background:' + color + '">' + type + '</div>' +
							'<div class="spot-description basic-text">' + description + '</div>' +
						'</div>' +
					'</div>' +
				'</div>';
	$("#user-spots").append(spotDiv);
}
function typeColor(type) {
	switch(type) {
		case "Bouldering":
			return "#4ad153";
			break;
		case "Top Rope":
			return "#ff5555";
			break;
		case "Sport":
			return "#398bc6";
			break;
		case "Trad":
			return "#398bc6";
			break;
		case "Alpine":
			return "#cf7dfd";
			break;
		case "Ice":
			return "#cf7dfd";
			break;
		default:
			return "#404040";
			break;
	}
}
//----------------------------------------|
// Routes                                 |
//----------------------------------------|
var loadRouteLength = 8;
$("#load-more-routes").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".route-spot-div").length;
	//hide load-more button, show loading gif
	$("#load-more-routes").hide();
	$("#load-routes-gif").show()
	$.post("ajax/route/get_routes_from_spot.php", {"offset":offset, "user_id": thisUser.id}, function(data) {
		$("#load-routes-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				newRouteHTML(data[i].id, data[i].name, data[i].top_pic, data[i].user_name, data[i].description, data[i].difficulty, data[i].type_climbing, data[i].upvotes, false, data[i].spot_id, data[i].user_name);
			}
			//check if all routes have been loaded
			if(data.length == loadRouteLength) {
				$("#load-more-routes").show();
			}
		}
	}, 'json')
});
//----------------------------------------|
// Comments                               |
//----------------------------------------|
//amount of comments db will fetch for each call
var loadCommentLength = 6;
//hide load more button if less than initial db fetch amount
if($(".spot-comment-div").length < loadCommentLength) {
	$("#load-more-comments").hide();
}
$("#load-more-comments").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".spot-comment-div").length;
	//hide load-more button, show loading gif
	$("#load-more-comments").hide();
	$("#load-comments-gif").show()
	$.post("ajax/comment/load_more_comments.php", {"offset":offset, "user_id": thisUser.id}, function(data) {
		$("#load-comments-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				var upvoted = checkIfUpvoted(data[i].id, "comment");
				var displayDate = normalizeDate(new Date(data[i].created_at));
				newCommentHTML(data[i].id, data[i].user_name, data[i].upvotes, data[i].user_avatar, displayDate, data[i].body, upvoted, false, data[i].spot_id, data[i].spot_name, data[i].gym_id);
			}
			//check if all comments have been loaded
			if(data.length == loadCommentLength) {
				$("#load-more-comments").show();
			}
		}
	}, 'json')
});
//turn js date obj into 'Month day year'
function normalizeDate(date) {
	var monthIndex = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October","November", "December"];
	var day = date.getDate();
	var month = monthIndex[date.getMonth()];
	var year = date.getFullYear();
	return month + " " + day + " " + year;
}
//----------------------------------------|
// Activities                             |
//----------------------------------------|
var loadActivityLength = 5;
//hide load more button if less than initial db fetch amount
if($(".activity-feed-div").length < loadActivityLength) {
	$("#load-more-activities").hide();
}
$("#load-more-activities").click(function(e) {
	e.preventDefault();
	//get offset from amount of comments loaded
	var offset = $(".activity-feed-div").length;
	//hide load-more button, show loading gif
	$("#load-more-activities").hide();
	$("#load-activities-gif").show()
	$.post("ajax/user/get_activities.php", {"offset":offset, "user_id": thisUser.id, "user": thisUser}, function(data) {
		$("#load-activities-gif").hide()
		if(data) {
			for(var i=0;i<data.length;i++) {
				activityHTML(data[i], thisUser);
			}
			//check if all activities have been loaded
			if(data.length == loadActivityLength) {
				$("#load-more-activities").show();
			}
		}
	}, 'json')
});
//load more div animation 
$(".load-more-div").hover(
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "13px"
		}, 350);
		$(this).find(".big-arrow").stop().animate({
			top:"6px"
		}, 300)
	},
	function() {
		$(this).find(".little-arrow").stop().animate({
			top: "3px"
		}, 300);
		$(this).find(".big-arrow").stop().animate({
			top:"0px"
		}, 250)
	}
);
//----------------------------------------|
// Activity Divs                          |
//----------------------------------------|
function activityHTML(activity, user) {
	if(activity.content_type == "comment") {
		var new_post = commentActivityHTML(activity, user);
	} else if(activity.content_type == "picture" || activity.content_type == "avatar") {
		if(activity.content_type == "picture") {
			var new_post = pictureActivityHTML(activity, user, false);
		} else {
			var new_post = pictureActivityHTML(activity, user, true);
		}
	} else if(activity.content_type == "spot" || activity.content_type == "confirm") {
		if(activity.content_type == "confirm") {
			var new_post = spotActivityHTML(activity, user, true);
		} else {
			var new_post = spotActivityHTML(activity, user, false);
		}
	} else {
		var new_post = routeActivityHTML(activity, user);
	}
	$("#user-feed").append(new_post);
}

function commentActivityHTML(comment, user) {
	var deleteOption = activityUserCheck(user);
	if(comment.gym_id) {
		var url = 'indoor/' + comment.gym_id + "/" + comment.spot_name;
	} else {
		var url = 'spot/' + comment.spot_id + "/" + comment.spot_name;
	}
	var new_post =  "<div class='activity-feed-div'>" +
					"<div style='display:none'>" +
						"<div class='activity-content-id'>" + comment.content_id + "</div>" +
						"<div class='activity-content-type'>comment</div>" + 
					"</div>" +
					"<div class='comment-time' style='float:none'>About " + timeSince(new Date(comment.created_at)) + " ago</div>" +
					"<div class='activity-header'>" +
						"<span class='bolder-text theme-link link-activity-text'>" + user['username'] + "</span>" +
						"<span class='basic-text regular-activity-text'> posted a comment on </span>" +
						"<a href=" + url + " class='theme-link bolder-text link-activity-text'>" + comment.spot_name + "'s</a>" + "<span class='basic-text regular-activity-text'> page:</span>" +
					"</div>" +
					"<div class='comment-activity basic-text'>" + comment.message + "</div>" +
					deleteOption +
				 "</div>";
	return new_post;
}

function spotActivityHTML(spot, user, confirm) {
	if(confirm) {
		var message = " confirmed a spot ";
		var content_type = "<div class='activity-content-type'>confirm</div>";
	} else {
		var message = " discovered a new spot ";
		var content_type = "<div class='activity-content-type'>spot</div>";
	}
	var deleteOption = activityUserCheck(user);
	var new_post =  "<div class='activity-feed-div' style='min-height:120px;position:relative'>" +
					"<div style='display:none'>" +
						"<div class='activity-content-id'>" + spot.content_id + "</div>" +
						content_type +
					"</div>" +
					"<div class='comment-time' style='float:none'>About " + timeSince(new Date(spot.created_at)) + " ago</div>" +
					"<span class='new-spot-activity-icon glyphicon glyphicon-map-marker'></span>" +
					"<div class='activity-header' style='text-align:center;font-size:1.3em;margin-top:5px;position:relative;z-index:1'>" +
						"<span class='bolder-text theme-link link-activity-text'>" + user.username + "</span>" +
						"<span class='basic-text regular-activity-text'>" + message + "</span>" +
						"<a href='spot/" + spot.spot_id + "/" + spot.spot_name + "' class='theme-link bolder-text link-activity-text'>" + spot.spot_name + "!</a>" +
					"</div>" +
					deleteOption + 
				 "</div>";
	return new_post;
}

function routeActivityHTML(route, user) {
	var deleteOption = activityUserCheck(user);
	var new_post =  "<div class='activity-feed-div' style='min-height:120px;position:relative'>" +
					"<div style='display:none'>" +
						"<div class='activity-content-id'>" + route.content_id + "</div>" +
						"<div class='activity-content-type'>route</div>" + 
					"</div>" +
					"<div class='comment-time' style='float:none'>About " + timeSince(new Date(route.created_at)) + " ago</div>" +
					"<span class='new-spot-activity-icon glyphicon glyphicon-road'></span>" +
					"<div class='activity-header' style='text-align:center;font-size:1.3em;margin-top:5px;position:relative;z-index:1'>" +
						"<span class='bolder-text theme-link link-activity-text'>" + user.username + "</span>" +
						"<span class='basic-text regular-activity-text'> discovered a new route </span>" +
						"<a href='spot/" + route.spot_id + "/" + route.spot_name + "' class='theme-link bolder-text link-activity-text'>" + route.message + "</a>" +
						"<span class='basic-text regular-activity-text'> at </span>" +
						"<a href='spot/" + route.spot_id + "/" + route.spot_name + "' class='theme-link bolder-text link-activity-text'>" + route.spot_name + "!</a>" +
					"</div>" +
					deleteOption +
				 "</div>";
	return new_post;
}

function pictureActivityHTML(picture, user, avatar) {
	var deleteOption = activityUserCheck(user);
	if(picture.gym_id) {
		var url = 'indoor/' + picture.gym_id + "/" + picture.spot_name;
	} else {
		var url = 'spot/' + picture.spot_id + "/" + picture.spot_name;
	}
	if(avatar) {
		var pic_message = "<span class='basic-text regular-activity-text'> uploaded a new avatar:</span>";
		var pic_url = "<img src='" + generateS3URL(picture.content_id) + "' class='pic-activity'/>";
		var content_type = "<div class='activity-content-type'>avatar</div>";
	} else {
		var pic_message = "<span class='basic-text regular-activity-text'> posted a picture on </span>" +
						"<a href=" + url + " class='theme-link bolder-text link-activity-text'>" + picture.spot_name + "'s</a>" + "<span class='basic-text regular-activity-text'> page:</span>";
		var pic_url = "<img src='" + generateS3URL(picture.content_id) + "s" + "' class='pic-activity'/>";
		var content_type = "<div class='activity-content-type'>picture</div>";
	}
	var new_post =  "<div class='activity-feed-div'>" +
					"<div style='display:none'>" +
						"<div class='activity-content-id'>" + picture.content_id + "</div>" +
						content_type + 
					"</div>" +
					"<div class='comment-time' style='float:none'>About " + timeSince(new Date(picture.created_at)) + " ago</div>" +
					"<div class='activity-header'>" +
						"<span class='bolder-text theme-link link-activity-text'>" + user.username + "</span>" +
						pic_message +
					"</div>" +
					"<div style='text-align:center;margin-top:15px;'>" +
						pic_url + 
					"</div>" +
					deleteOption + 
				 "</div>";
	return new_post;
}

function activityUserCheck(user) {
	if(sessionStorage.getItem('username') == user.username) {
		return '<div class="comment-options">' +
					'<span class="delete-activity comment-option">delete</span>' +
				'</div>';
	} else return "";
}
/* ********************************
   --------------------------------
   |     Info Bar Div Clicks 	  |
   ________________________________
   ********************************
*/
$(".info-bar-div").click(function() {
	var thisId = $(this).attr('id');
	var divOffset = {"top": 0};
	switch(thisId) {
		case "info-bar-routes":
			divOffset = $("#routes-container").offset();
			break;
		case "info-bar-spots":
			divOffset = $("#spots-container").offset();
			break;
		case "info-bar-comments":
			divOffset = $("#comments-container").offset();
			break;
		default:
			divOffset = {"top": 0};
			break;
	}
	if(!divOffset) {
		divOffset = {"top": 0};
	}
	$("html, body").animate({
		scrollTop: (divOffset.top - 110)
	}, 300)
});
//----------------------------------------|
// Page wide functions                    |
//----------------------------------------|
//get x seconds ago... from date string
function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}
$(document).on("click", ".activate-report", function() {
	console.log("sdfjksd")
});
/* ********************************
   --------------------------------
   |       Route Desc Edit  	  |
   ________________________________
   ********************************
*/
$(document).on("click", ".edit-route-desc", function() {
	if(sessionStorage.getItem('username')) {
		var parentDiv = $(this).parents(".route-spot-div");
		var reportToSave = parentDiv.find(".edit-report-button");
		parentDiv.find('.route-description').html("<textarea name='route-desc' class='spot-edit-form route-desc-edit' wrap='hard' style='width:100%'></textarea>");
		$(".route-desc-edit").focus();
		reportToSave.removeClass().addClass("save-route-desc basic-text route-button").text("Save").css("color", "rgba(0, 0, 0, 1)");
	} else {
		slideUserModal(false, "login");
	}
});
$(document).on("click", ".save-route-desc", function() {
	var parentDiv = $(this).parents(".route-spot-div");
	var routeDesc = parentDiv.find(".route-desc-edit").val();
	var routeId = parentDiv.find(".route-id").text();
	var editObj = {"route_id": routeId, "desc": routeDesc};
	$.post("ajax/route/edit_desc.php", editObj, function(resp) {
		if(resp.success) {
			parentDiv.find('.route-description').text(routeDesc);
			parentDiv.find(".save-route-desc").removeClass().addClass("route-button basic-text edit-user-route-desc").text("Edit").css("color", "white");
		} else {
			showFlash(resp.error, "danger", "alert-sign");
		}
	}, 'json');
});
</script>
<?php
} else {
	include("views/static_pages/error.php");
}
?>
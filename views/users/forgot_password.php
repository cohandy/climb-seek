<!-- mountain vector background -->
<div class="vector-background"></div>
<!-- edit form -->
<div class="container-fluid">
	<div class="add-spot-header" style="margin-top:55px;text-align:center">Recover Password</div>
	<div class="container" style="margin-top:10px;">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="edit-form-div">
					<span class="new-spot-text">Email</span>
					<span class="alert alert-danger form-error" style="float:right" data-toggle="tooltip">
						<span class="glyphicon glyphicon-exclamation-sign"></span>
						<span class="error-text" style="margin-left:2px"></span>
					</span>
					<input type="text" name="email" class="new-spot-input test-input-ajax" placeholder="Enter email..." id="email-input">
				</div>
				<div class="sliding-button small-button" id="send-email" style="float:right;margin-top:10px">Send Email</div>
			</div>
		</div>
	</div>
</div>
<script>
//highlights inputs/textareas on focus
$(".new-spot-input").focus(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(".new-spot-input").blur(function() {
	var inputName = $(this).parent().find(".new-spot-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
$("#send-email").click(function() {
	var email = $("#email-input").val();
	$.post("ajax/user/send_recover_email.php", {"email": email}, function(resp) {
		if(resp) {
			showFlash("Email sent! Check your inbox soon to reset your password", "success", "info-sign");
		} else {
			showFlash("Email was not found in our database", "danger", "alert");
		}
	});
});
</script>
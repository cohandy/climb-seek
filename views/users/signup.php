<div class="login-container">
	<div class="modal-header-user"> 
		<div class="modal-header-text">Sign Up</div> 
		<div class="modal-header-subtext">Become a part of our climbing community</div> 
	</div> 
	<div id="form-error-div" class="form-error-area"> 
	</div> 
	<form method="post" action="#" id="signup-form" class="login-signup-form"> 
		<div class="input-div"> 
			<div class="login-form-text basic-text">Email</div> 
			<div style="position:relative"> 
				<input type="text" name="email" placeholder="Enter email..." class="login-input signup-input bolder-text transition-all"> 
				<div class="icon-area"></div>  
			</div>  
		</div> 
		<div class="input-div"> 
			<div class="login-form-text basic-text">Username</div> 
			<div style="position:relative"> 
				<input type="text" name="username" placeholder="Enter username..." class="signup-input login-input bolder-text transition-all"> 
				<div class="icon-area"></div> 
			</div>  
		</div> 
		<div class="input-div"> 
			<div class="login-form-text basic-text">Password</div> 
			<div style="position:relative"> 
				<input type="password" name="password" placeholder="Enter password..." class="signup-input login-input bolder-text transition-all" id="password-input"> 
				<div class="icon-area"></div>   
			</div> 
		</div> 
		<div class="input-div"> 
			<div class="login-form-text basic-text">Confirm Password</div> 
			<div style="position:relative"> 
				<input type="password" name="confirm" placeholder="Confirm password..." class="signup-input login-input bolder-text transition-all"> 
				<div class="icon-area"></div> 
			</div> 
		</div> 
		<input type="hidden" name="form" value="signup"> 
		<div class="input-div" style="margin-top:20px;"> 
			<input type="submit" class="submit-button" id="login-submit" value="Sign Up"> 
		</div>
	</form> 
	<div class="login-background-image-signup">
		<div class="login-footer">
			<div class="input-div"> 
				<div class="input-form-subtext">By signing up, you agree to our Terms & Privacy Policy. 
			</div>
			<div class="input-div" style="margin-top:20px"> 
				<div class="input-form-text">Already a member? <a href="login" class="login-modal-link" id="login-link">Log in here</a></div>
			</div>
		</div>
	</div> 
</div>
<script type="text/javascript">
//check individual inputs via ajax, show correct/wrong icon depending on validations
$(document).on("blur", "#signup-form .login-input", function() {
	var input = $(this);
	var field = input.serialize();
	//if checking confirm, add first password for comparison
	if(input.attr('name') == "confirm") {
		field += "&password=" + $("#password-input").val();
	}
	$.ajax({
		url: "ajax/user/login_signup.php",
		type: "post",
		data: field,
		dataType: "json",
		success: function(resp) {
			if(!resp.success) {
				input.next().html('<span class="glyphicon glyphicon-remove-circle form-input-icon wrong-icon"></span>')
			} else {
				input.next().html('<span class="glyphicon glyphicon-ok-circle form-input-icon correct-icon"></span>')
			}
		}
	})
});
//submit login/signup
$(document).on("submit", "#signup-form, #login-form", function(e) {
	e.preventDefault();
	var fields = $(this).serialize();
	$.ajax({
		url: "ajax/user/login_signup.php",
		type: "post",
		data: fields,
		dataType: "json",
		success: function(resp) {
			//if resp.length is undefined that means an array of errors was returned
			if(resp.length == undefined) {
				if(!resp.success) {
					$("#form-error-div").html("").show();
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp.error + "</div>");
				} else {
					//close modal, change nav to logged in state
					sessionStorage.setItem('user_id', resp.user_id);
					sessionStorage.setItem('username', resp.username);
					sessionStorage.setItem('email', resp.email);
					sessionStorage.setItem('main_map_filter', "Ropes");
					if(resp.avatar) {
						sessionStorage.setItem('avatar', resp.avatar);
					}
					//go to page depending on current location
					if(document.title == "Home | climb&seek") {
						window.location.assign("index.php?p=profile&username=" + resp.username);
					} else {
						window.location.assign(window.location.href);
					}
				}
			} else {
				$("#form-error-div").html("").show();
				for(var i=0;i<resp.length;i++) {
					$("#form-error-div").append("<div class='list-error'><span class='glyphicon glyphicon-exclamation-sign' style='font-size:1.1em;margin-right:3px;position:relative;top:2px'></span>" + resp[i].error + "</div>");
				}
			}
		}
	});
});
//highlights inputs on focus
$(document).on("focus", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "#347db2");
	$(this).css("border", "2px solid #347db2");
});
$(document).on("blur", ".login-input", function() {
	var inputName = $(this).parent().find(".login-form-text");
	inputName.css("color", "rgba(0, 0, 0, 0.6)");
	$(this).css("border", "2px solid rgba(0, 0, 0, 0.2)");
});
checkBackgroundImage();
function checkBackgroundImage() {
	if($(window).height() > 700) {
		$(".login-background-image-signup").css("position", "absolute");
	} else {
		$(".login-background-image-signup").css("position", "relative");
	}
}
$(window).resize(function() {
	checkBackgroundImage();
})
</script>
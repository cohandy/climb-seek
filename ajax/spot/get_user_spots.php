<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/spot.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$spot = new Spot($_POST, $pdo);
	$spots = $spot->getUserSpots($_POST['user_id'], $_POST['offset']);
	echo json_encode($spots);
} echo false;
?>
<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/spot.php";
require_once "../../classes/spot_edit.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$update = new SpotEdit($_POST, $pdo);
	$update->updateTopPic();
}
?>
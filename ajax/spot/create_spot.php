<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/activity.php";
require_once "../../classes/spot.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$spot = new Spot($_POST, $pdo);
	$validations = $spot->createSpot();
	echo json_encode($validations);
} else echo json_encode(Spot::returnError("Database could not be accessed, please try again in a few minutes"))
?>
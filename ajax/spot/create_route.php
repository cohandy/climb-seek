<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/activity.php";
require_once "../../classes/route.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$route = new Route($_POST, $pdo);
	$create = $route->createRoute();
	echo json_encode($create);
} else echo json_encode(Route::returnError("Database could not be accessed, please try again later"));
?>
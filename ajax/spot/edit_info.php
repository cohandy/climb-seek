<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/spot.php";
require_once "../../classes/spot_edit.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$new_edit = new SpotEdit($_POST, $pdo);
	//update spot
	if($_SESSION['spot']['user_id'] == $_SESSION['user']['id']) {
		$edit = $new_edit->updateSpot();
	} else {
		$edit = array('success' => true);
	}
	//update desc and tips separately for simplicity purposes, old desc and tips get archived
	//current spot fetches current desc/tips for archiving purposes
	$current_spot = $new_edit->getCurrentSpot();
	$desc = $new_edit->editDesc($current_spot);
	$tips = $new_edit->editTips($current_spot);
	if($desc['success'] && $tips['success'] && $edit['success']) {
		echo json_encode(array('success' => true));
	} else {
		if(!$desc['success']) {
			echo json_encode($desc);
		} else if(!$tips['success']) {
			echo json_encode($tips);
		} else {
			echo json_encode($edit);
		}
	}
} else echo json_encode(array('success' => "No connection to database, try again later"));
?>
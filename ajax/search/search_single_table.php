<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/search.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$search = new Search($_POST, $pdo);
	if($_POST['table'] == "cities") {
		$results = $search->searchLocations();
	} elseif($_POST['table'] == "spots") {
		$results = $search->searchSpots();
	} elseif($_POST['table'] == "routes") {
		$results = $search->searchRoutes();
	} else {
		$results = $search->searchIndoor();
	}
	echo json_encode($results);
	unset($search);
}
?>
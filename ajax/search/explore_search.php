<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/search.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$search = new Search($_POST, $pdo);
	$spots = $search->searchAll();
	echo json_encode($spots);
	unset($search);
}
?>
<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/route.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$route = new Route($_POST, $pdo);
	if(isset($_POST['user_id'])) {
		$routes = $route->getRoutesFromUser();
	} else {
		$routes = $route->getRoutesFromSpot();
	}
	echo json_encode($routes);
} else echo false;
?>
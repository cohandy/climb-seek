<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/route.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$edit = new Route($_POST, $pdo);
	$success = $edit->editDesc();
	echo json_encode($success);
} else echo json_encode(array('success' => false, 'error' => "No connection to database"));
?>
<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/picture.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$pic = new Picture($_POST, $pdo);
	//if user_id is set its coming from the user show page
	$spot_pics = $pic->getGymPictures();
	echo json_encode($spot_pics);
} else echo false;
?>
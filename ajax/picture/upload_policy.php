<?php
session_start();
require_once "../../config.php";
require_once "../../classes/picture.php";
$s3FormDetails = Picture::getS3Details(AWS_BUCKET, AWS_REGION);

echo json_encode($s3FormDetails);

?>
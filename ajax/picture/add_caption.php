<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/picture.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$add = new Picture($_POST, $pdo);
	$check = $add->addCaption();
	echo json_encode($check);
} else echo json_encode(array("success" => false, "error" => "No connection to database"));
?>
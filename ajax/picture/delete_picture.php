<?php
session_start();
require_once "../../config.php";
require_once '../../vendor/autoload.php';
require_once "../../classes/database.php";
require_once "../../classes/picture.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$pic = new Picture($_POST, $pdo);
	$delete = $pic->deletePicture();
	echo $delete;
} else echo false;
?>
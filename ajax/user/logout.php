<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/user.php";
$pdo = SQLiteDB::getInstance();
$user = new User(array(), $pdo);
$user->destroyCookie();
session_destroy();
?>
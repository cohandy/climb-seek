<?php
session_start();
require_once "../../config.php";
require_once '../../vendor/autoload.php';
require_once "../../classes/database.php";
require_once "../../classes/user.php";
require_once "../../classes/user_recover.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$recover = new UserRecover($_POST, $pdo);
	$edit = $recover->updatePassword();
	echo json_encode($edit);
} else echo array('success' => false);
?>
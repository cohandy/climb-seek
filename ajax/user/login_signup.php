<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/user.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	//create new user object
	$user = new User($_POST, $pdo);
	if(isset($_POST['form'])) {
		if($_POST['form'] == "login") {
			$login = $user->loginUser();
			echo json_encode($login);
		} else {
			$signup = $user->createUser();
			echo json_encode($signup);
		}
	} else {
		//individually check fields
		if(isset($_POST['email'])) {
			$validate = $user->validateEmail();
		} elseif(isset($_POST['username'])) {
			$validate = $user->validateUsername();
		} elseif(isset($_POST['confirm'])) {
			$validate = $user->confirmPassword();
		} elseif(isset($_POST['password'])) {
			$validate = $user->validatePassword();
		}
		echo json_encode($validate);
	}
} else echo json_encode(array("success" => false, "error" => "Database maintenance, try again in 10 minutes."));
?>
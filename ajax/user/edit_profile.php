<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/user.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$user = new User($_POST, $pdo);
	if($_POST['change_password'] == "false") {
		$edit = $user->updateUserNoPassword();
	} else $edit = $user->updateUser();
	echo json_encode($edit);
} else echo json_encode(array("success" => false, "error" => "No connection to database, try again later"));
?>
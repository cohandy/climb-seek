<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/activity.php";
require_once "../../classes/user.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$activities = Activity::getActivities($_POST['user_id'], $_POST['offset'], $pdo);
	echo json_encode($activities);
} else echo false;
?>
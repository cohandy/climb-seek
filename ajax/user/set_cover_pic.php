<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/user.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$edit = new User($_POST, $pdo);
	$edit->setCoverPic();
	echo true;
}
?>
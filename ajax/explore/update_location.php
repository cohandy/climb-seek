<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/activity.php";
require_once "../../classes/spot.php";
require_once "../../classes/spot_edit.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$spot = new SpotEdit($_POST, $pdo);
	$update = $spot->updateLocation();
	echo $update;
} else return false;
?>
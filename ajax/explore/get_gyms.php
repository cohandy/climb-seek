<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/map.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$locations = new Map($_POST, $pdo);
	$gyms = $locations->getGymsInLong();
	if(count($gyms) > 0) {
		echo json_encode($gyms);
	} else echo false;
} else echo false;
?>
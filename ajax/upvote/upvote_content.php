<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/upvote.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$upvote = new Upvote($_POST, $pdo);
	$return = $upvote->upvoteContent();
	echo json_encode($return);
} else echo json_encode(array('success' => false))
?>
<?php
session_start();
require_once "../../config.php";
require_once '../../vendor/autoload.php';
require_once "../../classes/database.php";
require_once "../../classes/report.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$report = new Report($_POST, $pdo);
	if(isset($_POST['message'])) {
		$send = $report->otherReport();
		echo json_encode($send);
	} else {
		$create = $report->newReport();
		echo json_encode($create);
	}
} else echo json_encode(array('success' => false, 'error' => 'No connection to database'));
?>
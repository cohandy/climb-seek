<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/activity.php";
require_once "../../classes/comment.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$comment = new Comment($_POST, $pdo);
	if($_POST['page'] == "spot") {
		$new_comment = $comment->createComment();
	} else {
		$new_comment = $comment->createGymComment();
	}
	echo json_encode($new_comment);
} else echo json_encode(array('success' => false, "error" => "No database connection"));
?>
<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/comment.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$fetch = new Comment($_POST, $pdo);
	if(isset($_POST['user_id'])) {
		$comments = $fetch->fetchUserComments($_POST['user_id'], $_POST['offset']);
	} else {
		$comments = $fetch->fetchSpotComments($_POST['offset']);
	}
	echo json_encode($comments);
} else echo false;
?>
<?php
session_start();
require_once "../../config.php";
require_once "../../classes/database.php";
require_once "../../classes/comment.php";
$pdo = SQLiteDB::getInstance();
if($pdo) {
	$comment = new Comment($_POST, $pdo);
	$delete = $comment->deleteComment();
	echo $delete;
} else echo false;
?>